// ==============================================================
// RTL generated by Vivado(TM) HLS - High-Level Synthesis from C, C++ and OpenCL
// Version: 2019.2
// Copyright (C) 1986-2019 Xilinx, Inc. All Rights Reserved.
// 
// ===========================================================

`timescale 1 ns / 1 ps 

module product_dense_ap_fixed_ap_fixed_ap_fixed_s (
        ap_ready,
        a_V,
        w_V,
        ap_return
);


output   ap_ready;
input  [31:0] a_V;
input  [31:0] w_V;
output  [31:0] ap_return;

wire  signed [31:0] r_V_fu_32_p0;
wire  signed [31:0] r_V_fu_32_p1;
wire   [47:0] r_V_fu_32_p2;

assign ap_ready = 1'b1;

assign ap_return = {{r_V_fu_32_p2[47:16]}};

assign r_V_fu_32_p0 = w_V;

assign r_V_fu_32_p1 = a_V;

assign r_V_fu_32_p2 = ($signed(r_V_fu_32_p0) * $signed(r_V_fu_32_p1));

endmodule //product_dense_ap_fixed_ap_fixed_ap_fixed_s
