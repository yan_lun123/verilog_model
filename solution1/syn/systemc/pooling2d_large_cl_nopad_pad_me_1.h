// ==============================================================
// RTL generated by Vivado(TM) HLS - High-Level Synthesis from C, C++ and OpenCL
// Version: 2019.2
// Copyright (C) 1986-2019 Xilinx, Inc. All Rights Reserved.
// 
// ===========================================================

#ifndef _pooling2d_large_cl_nopad_pad_me_1_HH_
#define _pooling2d_large_cl_nopad_pad_me_1_HH_

#include "systemc.h"
#include "AESL_pkg.h"

#include "cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config26_s.h"

namespace ap_rtl {

struct pooling2d_large_cl_nopad_pad_me_1 : public sc_module {
    // Port declarations 16
    sc_in_clk ap_clk;
    sc_in< sc_logic > ap_rst;
    sc_in< sc_logic > ap_start;
    sc_in< sc_logic > start_full_n;
    sc_out< sc_logic > ap_done;
    sc_in< sc_logic > ap_continue;
    sc_out< sc_logic > ap_idle;
    sc_out< sc_logic > ap_ready;
    sc_out< sc_logic > start_out;
    sc_out< sc_logic > start_write;
    sc_in< sc_lv<32> > data_V_V_dout;
    sc_in< sc_logic > data_V_V_empty_n;
    sc_out< sc_logic > data_V_V_read;
    sc_out< sc_lv<32> > res_V_V_din;
    sc_in< sc_logic > res_V_V_full_n;
    sc_out< sc_logic > res_V_V_write;


    // Module declarations
    pooling2d_large_cl_nopad_pad_me_1(sc_module_name name);
    SC_HAS_PROCESS(pooling2d_large_cl_nopad_pad_me_1);

    ~pooling2d_large_cl_nopad_pad_me_1();

    sc_trace_file* mVcdFile;

    cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config26_s* call_ret_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config26_s_fu_325;
    sc_signal< sc_logic > real_start;
    sc_signal< sc_logic > start_once_reg;
    sc_signal< sc_logic > ap_done_reg;
    sc_signal< sc_lv<70> > ap_CS_fsm;
    sc_signal< sc_logic > ap_CS_fsm_state1;
    sc_signal< sc_logic > internal_ap_ready;
    sc_signal< sc_lv<8192> > layer_in_V_17;
    sc_signal< sc_lv<32> > sX_3;
    sc_signal< sc_lv<32> > sY_3;
    sc_signal< sc_lv<32> > pY_3;
    sc_signal< sc_lv<32> > pX_3;
    sc_signal< sc_logic > data_V_V_blk_n;
    sc_signal< sc_logic > ap_CS_fsm_state2;
    sc_signal< sc_lv<1> > icmp_ln274_fu_528_p2;
    sc_signal< sc_logic > ap_CS_fsm_state3;
    sc_signal< sc_logic > ap_CS_fsm_state4;
    sc_signal< sc_logic > ap_CS_fsm_state5;
    sc_signal< sc_logic > ap_CS_fsm_state6;
    sc_signal< sc_logic > ap_CS_fsm_state7;
    sc_signal< sc_logic > ap_CS_fsm_state8;
    sc_signal< sc_logic > ap_CS_fsm_state9;
    sc_signal< sc_logic > ap_CS_fsm_state10;
    sc_signal< sc_logic > ap_CS_fsm_state11;
    sc_signal< sc_logic > ap_CS_fsm_state12;
    sc_signal< sc_logic > ap_CS_fsm_state13;
    sc_signal< sc_logic > ap_CS_fsm_state14;
    sc_signal< sc_logic > ap_CS_fsm_state15;
    sc_signal< sc_logic > ap_CS_fsm_state16;
    sc_signal< sc_logic > ap_CS_fsm_state17;
    sc_signal< sc_logic > ap_CS_fsm_state18;
    sc_signal< sc_logic > ap_CS_fsm_state19;
    sc_signal< sc_logic > ap_CS_fsm_state20;
    sc_signal< sc_logic > ap_CS_fsm_state21;
    sc_signal< sc_logic > ap_CS_fsm_state22;
    sc_signal< sc_logic > ap_CS_fsm_state23;
    sc_signal< sc_logic > ap_CS_fsm_state24;
    sc_signal< sc_logic > ap_CS_fsm_state25;
    sc_signal< sc_logic > ap_CS_fsm_state26;
    sc_signal< sc_logic > ap_CS_fsm_state27;
    sc_signal< sc_logic > ap_CS_fsm_state28;
    sc_signal< sc_logic > ap_CS_fsm_state29;
    sc_signal< sc_logic > ap_CS_fsm_state30;
    sc_signal< sc_logic > ap_CS_fsm_state31;
    sc_signal< sc_logic > ap_CS_fsm_state32;
    sc_signal< sc_logic > ap_CS_fsm_state33;
    sc_signal< sc_logic > ap_CS_fsm_state34;
    sc_signal< sc_logic > ap_CS_fsm_state35;
    sc_signal< sc_logic > ap_CS_fsm_state36;
    sc_signal< sc_logic > ap_CS_fsm_state37;
    sc_signal< sc_logic > ap_CS_fsm_state38;
    sc_signal< sc_logic > ap_CS_fsm_state39;
    sc_signal< sc_logic > ap_CS_fsm_state40;
    sc_signal< sc_logic > ap_CS_fsm_state41;
    sc_signal< sc_logic > ap_CS_fsm_state42;
    sc_signal< sc_logic > ap_CS_fsm_state43;
    sc_signal< sc_logic > ap_CS_fsm_state44;
    sc_signal< sc_logic > ap_CS_fsm_state45;
    sc_signal< sc_logic > ap_CS_fsm_state46;
    sc_signal< sc_logic > ap_CS_fsm_state47;
    sc_signal< sc_logic > ap_CS_fsm_state48;
    sc_signal< sc_logic > ap_CS_fsm_state49;
    sc_signal< sc_logic > ap_CS_fsm_state50;
    sc_signal< sc_logic > ap_CS_fsm_state51;
    sc_signal< sc_logic > ap_CS_fsm_state52;
    sc_signal< sc_logic > ap_CS_fsm_state53;
    sc_signal< sc_logic > ap_CS_fsm_state54;
    sc_signal< sc_logic > ap_CS_fsm_state55;
    sc_signal< sc_logic > ap_CS_fsm_state56;
    sc_signal< sc_logic > ap_CS_fsm_state57;
    sc_signal< sc_logic > ap_CS_fsm_state58;
    sc_signal< sc_logic > ap_CS_fsm_state59;
    sc_signal< sc_logic > ap_CS_fsm_state60;
    sc_signal< sc_logic > ap_CS_fsm_state61;
    sc_signal< sc_logic > ap_CS_fsm_state62;
    sc_signal< sc_logic > ap_CS_fsm_state63;
    sc_signal< sc_logic > ap_CS_fsm_state64;
    sc_signal< sc_logic > ap_CS_fsm_state65;
    sc_signal< sc_logic > res_V_V_blk_n;
    sc_signal< sc_logic > ap_CS_fsm_state68;
    sc_signal< sc_lv<1> > icmp_ln303_fu_1173_p2;
    sc_signal< sc_lv<8> > i_ih_fu_534_p2;
    sc_signal< sc_lv<8> > i_ih_reg_1324;
    sc_signal< bool > ap_block_state2;
    sc_signal< sc_lv<32> > tmp_V_reg_1329;
    sc_signal< sc_lv<32> > tmp_V_769_reg_1334;
    sc_signal< sc_lv<32> > tmp_V_770_reg_1339;
    sc_signal< sc_lv<32> > tmp_V_771_reg_1344;
    sc_signal< sc_lv<32> > tmp_V_772_reg_1349;
    sc_signal< sc_lv<32> > tmp_V_773_reg_1354;
    sc_signal< sc_lv<32> > tmp_V_774_reg_1359;
    sc_signal< sc_lv<32> > tmp_V_775_reg_1364;
    sc_signal< sc_lv<32> > tmp_V_776_reg_1369;
    sc_signal< sc_lv<32> > tmp_V_777_reg_1374;
    sc_signal< sc_lv<32> > tmp_V_778_reg_1379;
    sc_signal< sc_lv<32> > tmp_V_779_reg_1384;
    sc_signal< sc_lv<32> > tmp_V_780_reg_1389;
    sc_signal< sc_lv<32> > tmp_V_781_reg_1394;
    sc_signal< sc_lv<32> > tmp_V_782_reg_1399;
    sc_signal< sc_lv<32> > tmp_V_783_reg_1404;
    sc_signal< sc_lv<32> > tmp_V_784_reg_1409;
    sc_signal< sc_lv<32> > tmp_V_785_reg_1414;
    sc_signal< sc_lv<32> > tmp_V_786_reg_1419;
    sc_signal< sc_lv<32> > tmp_V_787_reg_1424;
    sc_signal< sc_lv<32> > tmp_V_788_reg_1429;
    sc_signal< sc_lv<32> > tmp_V_789_reg_1434;
    sc_signal< sc_lv<32> > tmp_V_790_reg_1439;
    sc_signal< sc_lv<32> > tmp_V_791_reg_1444;
    sc_signal< sc_lv<32> > tmp_V_792_reg_1449;
    sc_signal< sc_lv<32> > tmp_V_793_reg_1454;
    sc_signal< sc_lv<32> > tmp_V_794_reg_1459;
    sc_signal< sc_lv<32> > tmp_V_795_reg_1464;
    sc_signal< sc_lv<32> > tmp_V_796_reg_1469;
    sc_signal< sc_lv<32> > tmp_V_797_reg_1474;
    sc_signal< sc_lv<32> > tmp_V_798_reg_1479;
    sc_signal< sc_lv<32> > tmp_V_799_reg_1484;
    sc_signal< sc_lv<32> > tmp_V_800_reg_1489;
    sc_signal< sc_lv<32> > tmp_V_801_reg_1494;
    sc_signal< sc_lv<32> > tmp_V_802_reg_1499;
    sc_signal< sc_lv<32> > tmp_V_803_reg_1504;
    sc_signal< sc_lv<32> > tmp_V_804_reg_1509;
    sc_signal< sc_lv<32> > tmp_V_805_reg_1514;
    sc_signal< sc_lv<32> > tmp_V_806_reg_1519;
    sc_signal< sc_lv<32> > tmp_V_807_reg_1524;
    sc_signal< sc_lv<32> > tmp_V_808_reg_1529;
    sc_signal< sc_lv<32> > tmp_V_809_reg_1534;
    sc_signal< sc_lv<32> > tmp_V_810_reg_1539;
    sc_signal< sc_lv<32> > tmp_V_811_reg_1544;
    sc_signal< sc_lv<32> > tmp_V_812_reg_1549;
    sc_signal< sc_lv<32> > tmp_V_813_reg_1554;
    sc_signal< sc_lv<32> > tmp_V_814_reg_1559;
    sc_signal< sc_lv<32> > tmp_V_815_reg_1564;
    sc_signal< sc_lv<32> > tmp_V_816_reg_1569;
    sc_signal< sc_lv<32> > tmp_V_817_reg_1574;
    sc_signal< sc_lv<32> > tmp_V_818_reg_1579;
    sc_signal< sc_lv<32> > tmp_V_819_reg_1584;
    sc_signal< sc_lv<32> > tmp_V_820_reg_1589;
    sc_signal< sc_lv<32> > tmp_V_821_reg_1594;
    sc_signal< sc_lv<32> > tmp_V_822_reg_1599;
    sc_signal< sc_lv<32> > tmp_V_823_reg_1604;
    sc_signal< sc_lv<32> > tmp_V_824_reg_1609;
    sc_signal< sc_lv<32> > tmp_V_825_reg_1614;
    sc_signal< sc_lv<32> > tmp_V_826_reg_1619;
    sc_signal< sc_lv<32> > tmp_V_827_reg_1624;
    sc_signal< sc_lv<32> > tmp_V_828_reg_1629;
    sc_signal< sc_lv<32> > tmp_V_829_reg_1634;
    sc_signal< sc_lv<32> > tmp_V_830_reg_1639;
    sc_signal< sc_lv<32> > sX_3_load_reg_1644;
    sc_signal< sc_lv<1> > icmp_ln288_fu_550_p2;
    sc_signal< sc_lv<1> > icmp_ln288_reg_1649;
    sc_signal< sc_lv<32> > sY_3_load_reg_1654;
    sc_signal< sc_lv<1> > icmp_ln288_7_fu_560_p2;
    sc_signal< sc_lv<1> > icmp_ln288_7_reg_1659;
    sc_signal< sc_lv<32> > pY_3_load_reg_1664;
    sc_signal< sc_lv<32> > pX_3_load_reg_1670;
    sc_signal< sc_lv<1> > and_ln288_6_fu_598_p2;
    sc_signal< sc_lv<1> > and_ln288_6_reg_1676;
    sc_signal< sc_lv<7> > i1_fu_610_p2;
    sc_signal< sc_lv<7> > i1_reg_1683;
    sc_signal< sc_logic > ap_CS_fsm_state66;
    sc_signal< sc_lv<11> > tmp_s_fu_620_p3;
    sc_signal< sc_lv<11> > tmp_s_reg_1688;
    sc_signal< sc_lv<1> > icmp_ln290_fu_604_p2;
    sc_signal< sc_lv<11> > empty_121_fu_628_p2;
    sc_signal< sc_lv<11> > empty_121_reg_1693;
    sc_signal< sc_lv<1> > icmp_ln203_fu_634_p2;
    sc_signal< sc_lv<1> > icmp_ln203_reg_1698;
    sc_signal< sc_lv<1> > icmp_ln313_fu_640_p2;
    sc_signal< sc_lv<1> > icmp_ln313_reg_1705;
    sc_signal< sc_lv<32> > select_ln323_fu_707_p3;
    sc_signal< sc_lv<1> > icmp_ln317_fu_686_p2;
    sc_signal< sc_lv<32> > pool_res_V_fu_807_p1;
    sc_signal< sc_logic > ap_CS_fsm_state67;
    sc_signal< sc_lv<128> > pool_V_fu_1161_p5;
    sc_signal< sc_lv<128> > pool_V_reg_1722;
    sc_signal< sc_lv<32> > trunc_ln1494_fu_1293_p1;
    sc_signal< sc_lv<32> > trunc_ln1494_reg_1731;
    sc_signal< bool > ap_block_state68;
    sc_signal< sc_lv<3> > i_fu_1297_p2;
    sc_signal< sc_lv<3> > i_reg_1737;
    sc_signal< sc_lv<32> > pool_res_V_3_fu_1308_p3;
    sc_signal< sc_logic > ap_CS_fsm_state69;
    sc_signal< sc_logic > call_ret_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config26_s_fu_325_ap_start;
    sc_signal< sc_logic > call_ret_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config26_s_fu_325_ap_done;
    sc_signal< sc_logic > call_ret_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config26_s_fu_325_ap_idle;
    sc_signal< sc_logic > call_ret_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config26_s_fu_325_ap_ready;
    sc_signal< sc_lv<8192> > call_ret_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config26_s_fu_325_ap_return;
    sc_signal< sc_lv<8> > i_ih_0_reg_269;
    sc_signal< bool > ap_block_state1;
    sc_signal< sc_logic > ap_CS_fsm_state70;
    sc_signal< sc_lv<7> > i1_0_reg_280;
    sc_signal< sc_lv<32> > tmp_V_751_reg_292;
    sc_signal< sc_lv<3> > i_0_reg_303;
    sc_signal< sc_lv<32> > storemerge_reg_314;
    sc_signal< sc_lv<32> > select_ln328_fu_661_p3;
    sc_signal< sc_lv<32> > add_ln321_fu_691_p2;
    sc_signal< sc_lv<32> > add_ln326_fu_645_p2;
    sc_signal< sc_lv<1> > icmp_ln288_8_fu_570_p2;
    sc_signal< sc_lv<1> > icmp_ln288_9_fu_580_p2;
    sc_signal< sc_lv<1> > and_ln288_5_fu_592_p2;
    sc_signal< sc_lv<1> > and_ln288_fu_586_p2;
    sc_signal< sc_lv<6> > empty_120_fu_616_p1;
    sc_signal< sc_lv<32> > add_ln328_fu_656_p2;
    sc_signal< sc_lv<32> > add_ln323_fu_702_p2;
    sc_signal< sc_lv<14> > zext_ln203_fu_720_p1;
    sc_signal< sc_lv<14> > zext_ln203_31_fu_723_p1;
    sc_signal< sc_lv<14> > sub_ln203_fu_736_p2;
    sc_signal< sc_lv<14> > sub_ln203_23_fu_748_p2;
    sc_signal< sc_lv<8192> > tmp_14_fu_726_p4;
    sc_signal< sc_lv<14> > xor_ln203_9_fu_742_p2;
    sc_signal< sc_lv<14> > select_ln203_fu_754_p3;
    sc_signal< sc_lv<14> > select_ln203_24_fu_768_p3;
    sc_signal< sc_lv<14> > sub_ln203_24_fu_775_p2;
    sc_signal< sc_lv<8192> > select_ln203_23_fu_761_p3;
    sc_signal< sc_lv<8192> > zext_ln203_32_fu_781_p1;
    sc_signal< sc_lv<8192> > zext_ln203_33_fu_785_p1;
    sc_signal< sc_lv<8192> > lshr_ln203_fu_789_p2;
    sc_signal< sc_lv<8192> > lshr_ln203_15_fu_795_p2;
    sc_signal< sc_lv<8192> > and_ln203_fu_801_p2;
    sc_signal< sc_lv<7> > xor_ln203_fu_811_p2;
    sc_signal< sc_lv<12> > tmp_4_fu_817_p3;
    sc_signal< sc_lv<12> > empty_122_fu_825_p2;
    sc_signal< sc_lv<14> > zext_ln203_34_fu_837_p1;
    sc_signal< sc_lv<14> > zext_ln203_35_fu_841_p1;
    sc_signal< sc_lv<1> > icmp_ln203_7_fu_831_p2;
    sc_signal< sc_lv<14> > sub_ln203_25_fu_855_p2;
    sc_signal< sc_lv<14> > sub_ln203_26_fu_867_p2;
    sc_signal< sc_lv<8192> > tmp_15_fu_845_p4;
    sc_signal< sc_lv<14> > xor_ln203_10_fu_861_p2;
    sc_signal< sc_lv<14> > select_ln203_25_fu_873_p3;
    sc_signal< sc_lv<14> > select_ln203_27_fu_889_p3;
    sc_signal< sc_lv<14> > sub_ln203_27_fu_897_p2;
    sc_signal< sc_lv<8192> > select_ln203_26_fu_881_p3;
    sc_signal< sc_lv<8192> > zext_ln203_36_fu_903_p1;
    sc_signal< sc_lv<8192> > zext_ln203_37_fu_907_p1;
    sc_signal< sc_lv<8192> > lshr_ln203_16_fu_911_p2;
    sc_signal< sc_lv<8192> > lshr_ln203_17_fu_917_p2;
    sc_signal< sc_lv<8192> > and_ln203_7_fu_923_p2;
    sc_signal< sc_lv<13> > tmp_5_fu_933_p4;
    sc_signal< sc_lv<13> > empty_123_fu_943_p2;
    sc_signal< sc_lv<14> > zext_ln203_38_fu_955_p1;
    sc_signal< sc_lv<14> > zext_ln203_39_fu_959_p1;
    sc_signal< sc_lv<1> > icmp_ln203_8_fu_949_p2;
    sc_signal< sc_lv<14> > sub_ln203_28_fu_973_p2;
    sc_signal< sc_lv<14> > sub_ln203_29_fu_985_p2;
    sc_signal< sc_lv<8192> > tmp_16_fu_963_p4;
    sc_signal< sc_lv<14> > xor_ln203_11_fu_979_p2;
    sc_signal< sc_lv<14> > select_ln203_28_fu_991_p3;
    sc_signal< sc_lv<14> > select_ln203_30_fu_1007_p3;
    sc_signal< sc_lv<14> > sub_ln203_30_fu_1015_p2;
    sc_signal< sc_lv<8192> > select_ln203_29_fu_999_p3;
    sc_signal< sc_lv<8192> > zext_ln203_40_fu_1021_p1;
    sc_signal< sc_lv<8192> > zext_ln203_41_fu_1025_p1;
    sc_signal< sc_lv<8192> > lshr_ln203_18_fu_1029_p2;
    sc_signal< sc_lv<8192> > lshr_ln203_19_fu_1035_p2;
    sc_signal< sc_lv<8192> > and_ln203_8_fu_1041_p2;
    sc_signal< sc_lv<13> > empty_124_fu_1051_p1;
    sc_signal< sc_lv<13> > p_cast1_fu_1055_p1;
    sc_signal< sc_lv<14> > zext_ln203_42_fu_1065_p1;
    sc_signal< sc_lv<14> > zext_ln203_43_fu_1069_p1;
    sc_signal< sc_lv<1> > icmp_ln203_9_fu_1059_p2;
    sc_signal< sc_lv<14> > sub_ln203_31_fu_1083_p2;
    sc_signal< sc_lv<14> > sub_ln203_32_fu_1095_p2;
    sc_signal< sc_lv<8192> > tmp_17_fu_1073_p4;
    sc_signal< sc_lv<14> > xor_ln203_12_fu_1089_p2;
    sc_signal< sc_lv<14> > select_ln203_31_fu_1101_p3;
    sc_signal< sc_lv<14> > select_ln203_33_fu_1117_p3;
    sc_signal< sc_lv<14> > sub_ln203_33_fu_1125_p2;
    sc_signal< sc_lv<8192> > select_ln203_32_fu_1109_p3;
    sc_signal< sc_lv<8192> > zext_ln203_44_fu_1131_p1;
    sc_signal< sc_lv<8192> > zext_ln203_45_fu_1135_p1;
    sc_signal< sc_lv<8192> > lshr_ln203_20_fu_1139_p2;
    sc_signal< sc_lv<8192> > lshr_ln203_21_fu_1145_p2;
    sc_signal< sc_lv<8192> > and_ln203_9_fu_1151_p2;
    sc_signal< sc_lv<32> > trunc_ln203_9_fu_1157_p1;
    sc_signal< sc_lv<32> > trunc_ln203_8_fu_1047_p1;
    sc_signal< sc_lv<32> > trunc_ln203_7_fu_929_p1;
    sc_signal< sc_lv<2> > empty_126_fu_1179_p1;
    sc_signal< sc_lv<7> > tmp_6_fu_1183_p3;
    sc_signal< sc_lv<7> > empty_127_fu_1191_p2;
    sc_signal< sc_lv<8> > zext_ln1494_fu_1203_p1;
    sc_signal< sc_lv<8> > zext_ln1494_7_fu_1207_p1;
    sc_signal< sc_lv<1> > icmp_ln1494_3_fu_1197_p2;
    sc_signal< sc_lv<8> > sub_ln1494_fu_1220_p2;
    sc_signal< sc_lv<8> > sub_ln1494_5_fu_1232_p2;
    sc_signal< sc_lv<128> > tmp_18_fu_1211_p4;
    sc_signal< sc_lv<8> > xor_ln1494_fu_1226_p2;
    sc_signal< sc_lv<8> > select_ln1494_fu_1238_p3;
    sc_signal< sc_lv<8> > select_ln1494_6_fu_1253_p3;
    sc_signal< sc_lv<8> > sub_ln1494_6_fu_1261_p2;
    sc_signal< sc_lv<128> > select_ln1494_5_fu_1246_p3;
    sc_signal< sc_lv<128> > zext_ln1494_8_fu_1267_p1;
    sc_signal< sc_lv<128> > zext_ln1494_9_fu_1271_p1;
    sc_signal< sc_lv<128> > lshr_ln1494_fu_1275_p2;
    sc_signal< sc_lv<128> > lshr_ln1494_3_fu_1281_p2;
    sc_signal< sc_lv<128> > and_ln1494_fu_1287_p2;
    sc_signal< sc_lv<1> > icmp_ln1494_fu_1303_p2;
    sc_signal< sc_lv<70> > ap_NS_fsm;
    sc_signal< bool > ap_condition_844;
    sc_signal< bool > ap_condition_846;
    sc_signal< bool > ap_condition_702;
    sc_signal< bool > ap_condition_832;
    static const sc_logic ap_const_logic_1;
    static const sc_logic ap_const_logic_0;
    static const sc_lv<70> ap_ST_fsm_state1;
    static const sc_lv<70> ap_ST_fsm_state2;
    static const sc_lv<70> ap_ST_fsm_state3;
    static const sc_lv<70> ap_ST_fsm_state4;
    static const sc_lv<70> ap_ST_fsm_state5;
    static const sc_lv<70> ap_ST_fsm_state6;
    static const sc_lv<70> ap_ST_fsm_state7;
    static const sc_lv<70> ap_ST_fsm_state8;
    static const sc_lv<70> ap_ST_fsm_state9;
    static const sc_lv<70> ap_ST_fsm_state10;
    static const sc_lv<70> ap_ST_fsm_state11;
    static const sc_lv<70> ap_ST_fsm_state12;
    static const sc_lv<70> ap_ST_fsm_state13;
    static const sc_lv<70> ap_ST_fsm_state14;
    static const sc_lv<70> ap_ST_fsm_state15;
    static const sc_lv<70> ap_ST_fsm_state16;
    static const sc_lv<70> ap_ST_fsm_state17;
    static const sc_lv<70> ap_ST_fsm_state18;
    static const sc_lv<70> ap_ST_fsm_state19;
    static const sc_lv<70> ap_ST_fsm_state20;
    static const sc_lv<70> ap_ST_fsm_state21;
    static const sc_lv<70> ap_ST_fsm_state22;
    static const sc_lv<70> ap_ST_fsm_state23;
    static const sc_lv<70> ap_ST_fsm_state24;
    static const sc_lv<70> ap_ST_fsm_state25;
    static const sc_lv<70> ap_ST_fsm_state26;
    static const sc_lv<70> ap_ST_fsm_state27;
    static const sc_lv<70> ap_ST_fsm_state28;
    static const sc_lv<70> ap_ST_fsm_state29;
    static const sc_lv<70> ap_ST_fsm_state30;
    static const sc_lv<70> ap_ST_fsm_state31;
    static const sc_lv<70> ap_ST_fsm_state32;
    static const sc_lv<70> ap_ST_fsm_state33;
    static const sc_lv<70> ap_ST_fsm_state34;
    static const sc_lv<70> ap_ST_fsm_state35;
    static const sc_lv<70> ap_ST_fsm_state36;
    static const sc_lv<70> ap_ST_fsm_state37;
    static const sc_lv<70> ap_ST_fsm_state38;
    static const sc_lv<70> ap_ST_fsm_state39;
    static const sc_lv<70> ap_ST_fsm_state40;
    static const sc_lv<70> ap_ST_fsm_state41;
    static const sc_lv<70> ap_ST_fsm_state42;
    static const sc_lv<70> ap_ST_fsm_state43;
    static const sc_lv<70> ap_ST_fsm_state44;
    static const sc_lv<70> ap_ST_fsm_state45;
    static const sc_lv<70> ap_ST_fsm_state46;
    static const sc_lv<70> ap_ST_fsm_state47;
    static const sc_lv<70> ap_ST_fsm_state48;
    static const sc_lv<70> ap_ST_fsm_state49;
    static const sc_lv<70> ap_ST_fsm_state50;
    static const sc_lv<70> ap_ST_fsm_state51;
    static const sc_lv<70> ap_ST_fsm_state52;
    static const sc_lv<70> ap_ST_fsm_state53;
    static const sc_lv<70> ap_ST_fsm_state54;
    static const sc_lv<70> ap_ST_fsm_state55;
    static const sc_lv<70> ap_ST_fsm_state56;
    static const sc_lv<70> ap_ST_fsm_state57;
    static const sc_lv<70> ap_ST_fsm_state58;
    static const sc_lv<70> ap_ST_fsm_state59;
    static const sc_lv<70> ap_ST_fsm_state60;
    static const sc_lv<70> ap_ST_fsm_state61;
    static const sc_lv<70> ap_ST_fsm_state62;
    static const sc_lv<70> ap_ST_fsm_state63;
    static const sc_lv<70> ap_ST_fsm_state64;
    static const sc_lv<70> ap_ST_fsm_state65;
    static const sc_lv<70> ap_ST_fsm_state66;
    static const sc_lv<70> ap_ST_fsm_state67;
    static const sc_lv<70> ap_ST_fsm_state68;
    static const sc_lv<70> ap_ST_fsm_state69;
    static const sc_lv<70> ap_ST_fsm_state70;
    static const sc_lv<32> ap_const_lv32_0;
    static const sc_lv<32> ap_const_lv32_1;
    static const sc_lv<1> ap_const_lv1_0;
    static const sc_lv<32> ap_const_lv32_2;
    static const sc_lv<32> ap_const_lv32_3;
    static const sc_lv<32> ap_const_lv32_4;
    static const sc_lv<32> ap_const_lv32_5;
    static const sc_lv<32> ap_const_lv32_6;
    static const sc_lv<32> ap_const_lv32_7;
    static const sc_lv<32> ap_const_lv32_8;
    static const sc_lv<32> ap_const_lv32_9;
    static const sc_lv<32> ap_const_lv32_A;
    static const sc_lv<32> ap_const_lv32_B;
    static const sc_lv<32> ap_const_lv32_C;
    static const sc_lv<32> ap_const_lv32_D;
    static const sc_lv<32> ap_const_lv32_E;
    static const sc_lv<32> ap_const_lv32_F;
    static const sc_lv<32> ap_const_lv32_10;
    static const sc_lv<32> ap_const_lv32_11;
    static const sc_lv<32> ap_const_lv32_12;
    static const sc_lv<32> ap_const_lv32_13;
    static const sc_lv<32> ap_const_lv32_14;
    static const sc_lv<32> ap_const_lv32_15;
    static const sc_lv<32> ap_const_lv32_16;
    static const sc_lv<32> ap_const_lv32_17;
    static const sc_lv<32> ap_const_lv32_18;
    static const sc_lv<32> ap_const_lv32_19;
    static const sc_lv<32> ap_const_lv32_1A;
    static const sc_lv<32> ap_const_lv32_1B;
    static const sc_lv<32> ap_const_lv32_1C;
    static const sc_lv<32> ap_const_lv32_1D;
    static const sc_lv<32> ap_const_lv32_1E;
    static const sc_lv<32> ap_const_lv32_1F;
    static const sc_lv<32> ap_const_lv32_20;
    static const sc_lv<32> ap_const_lv32_21;
    static const sc_lv<32> ap_const_lv32_22;
    static const sc_lv<32> ap_const_lv32_23;
    static const sc_lv<32> ap_const_lv32_24;
    static const sc_lv<32> ap_const_lv32_25;
    static const sc_lv<32> ap_const_lv32_26;
    static const sc_lv<32> ap_const_lv32_27;
    static const sc_lv<32> ap_const_lv32_28;
    static const sc_lv<32> ap_const_lv32_29;
    static const sc_lv<32> ap_const_lv32_2A;
    static const sc_lv<32> ap_const_lv32_2B;
    static const sc_lv<32> ap_const_lv32_2C;
    static const sc_lv<32> ap_const_lv32_2D;
    static const sc_lv<32> ap_const_lv32_2E;
    static const sc_lv<32> ap_const_lv32_2F;
    static const sc_lv<32> ap_const_lv32_30;
    static const sc_lv<32> ap_const_lv32_31;
    static const sc_lv<32> ap_const_lv32_32;
    static const sc_lv<32> ap_const_lv32_33;
    static const sc_lv<32> ap_const_lv32_34;
    static const sc_lv<32> ap_const_lv32_35;
    static const sc_lv<32> ap_const_lv32_36;
    static const sc_lv<32> ap_const_lv32_37;
    static const sc_lv<32> ap_const_lv32_38;
    static const sc_lv<32> ap_const_lv32_39;
    static const sc_lv<32> ap_const_lv32_3A;
    static const sc_lv<32> ap_const_lv32_3B;
    static const sc_lv<32> ap_const_lv32_3C;
    static const sc_lv<32> ap_const_lv32_3D;
    static const sc_lv<32> ap_const_lv32_3E;
    static const sc_lv<32> ap_const_lv32_3F;
    static const sc_lv<32> ap_const_lv32_40;
    static const sc_lv<32> ap_const_lv32_43;
    static const sc_lv<1> ap_const_lv1_1;
    static const sc_lv<32> ap_const_lv32_41;
    static const sc_lv<32> ap_const_lv32_42;
    static const sc_lv<32> ap_const_lv32_44;
    static const sc_lv<8> ap_const_lv8_0;
    static const sc_lv<32> ap_const_lv32_45;
    static const sc_lv<7> ap_const_lv7_0;
    static const sc_lv<3> ap_const_lv3_1;
    static const sc_lv<8> ap_const_lv8_B6;
    static const sc_lv<8> ap_const_lv8_1;
    static const sc_lv<7> ap_const_lv7_40;
    static const sc_lv<7> ap_const_lv7_1;
    static const sc_lv<5> ap_const_lv5_0;
    static const sc_lv<11> ap_const_lv11_1F;
    static const sc_lv<32> ap_const_lv32_1FFF;
    static const sc_lv<14> ap_const_lv14_1FFF;
    static const sc_lv<8192> ap_const_lv8192_lc_7;
    static const sc_lv<12> ap_const_lv12_1F;
    static const sc_lv<13> ap_const_lv13_1F;
    static const sc_lv<3> ap_const_lv3_4;
    static const sc_lv<7> ap_const_lv7_1F;
    static const sc_lv<32> ap_const_lv32_7F;
    static const sc_lv<8> ap_const_lv8_7F;
    static const sc_lv<128> ap_const_lv128_lc_4;
    static const bool ap_const_boolean_1;
    // Thread declarations
    void thread_ap_clk_no_reset_();
    void thread_add_ln321_fu_691_p2();
    void thread_add_ln323_fu_702_p2();
    void thread_add_ln326_fu_645_p2();
    void thread_add_ln328_fu_656_p2();
    void thread_and_ln1494_fu_1287_p2();
    void thread_and_ln203_7_fu_923_p2();
    void thread_and_ln203_8_fu_1041_p2();
    void thread_and_ln203_9_fu_1151_p2();
    void thread_and_ln203_fu_801_p2();
    void thread_and_ln288_5_fu_592_p2();
    void thread_and_ln288_6_fu_598_p2();
    void thread_and_ln288_fu_586_p2();
    void thread_ap_CS_fsm_state1();
    void thread_ap_CS_fsm_state10();
    void thread_ap_CS_fsm_state11();
    void thread_ap_CS_fsm_state12();
    void thread_ap_CS_fsm_state13();
    void thread_ap_CS_fsm_state14();
    void thread_ap_CS_fsm_state15();
    void thread_ap_CS_fsm_state16();
    void thread_ap_CS_fsm_state17();
    void thread_ap_CS_fsm_state18();
    void thread_ap_CS_fsm_state19();
    void thread_ap_CS_fsm_state2();
    void thread_ap_CS_fsm_state20();
    void thread_ap_CS_fsm_state21();
    void thread_ap_CS_fsm_state22();
    void thread_ap_CS_fsm_state23();
    void thread_ap_CS_fsm_state24();
    void thread_ap_CS_fsm_state25();
    void thread_ap_CS_fsm_state26();
    void thread_ap_CS_fsm_state27();
    void thread_ap_CS_fsm_state28();
    void thread_ap_CS_fsm_state29();
    void thread_ap_CS_fsm_state3();
    void thread_ap_CS_fsm_state30();
    void thread_ap_CS_fsm_state31();
    void thread_ap_CS_fsm_state32();
    void thread_ap_CS_fsm_state33();
    void thread_ap_CS_fsm_state34();
    void thread_ap_CS_fsm_state35();
    void thread_ap_CS_fsm_state36();
    void thread_ap_CS_fsm_state37();
    void thread_ap_CS_fsm_state38();
    void thread_ap_CS_fsm_state39();
    void thread_ap_CS_fsm_state4();
    void thread_ap_CS_fsm_state40();
    void thread_ap_CS_fsm_state41();
    void thread_ap_CS_fsm_state42();
    void thread_ap_CS_fsm_state43();
    void thread_ap_CS_fsm_state44();
    void thread_ap_CS_fsm_state45();
    void thread_ap_CS_fsm_state46();
    void thread_ap_CS_fsm_state47();
    void thread_ap_CS_fsm_state48();
    void thread_ap_CS_fsm_state49();
    void thread_ap_CS_fsm_state5();
    void thread_ap_CS_fsm_state50();
    void thread_ap_CS_fsm_state51();
    void thread_ap_CS_fsm_state52();
    void thread_ap_CS_fsm_state53();
    void thread_ap_CS_fsm_state54();
    void thread_ap_CS_fsm_state55();
    void thread_ap_CS_fsm_state56();
    void thread_ap_CS_fsm_state57();
    void thread_ap_CS_fsm_state58();
    void thread_ap_CS_fsm_state59();
    void thread_ap_CS_fsm_state6();
    void thread_ap_CS_fsm_state60();
    void thread_ap_CS_fsm_state61();
    void thread_ap_CS_fsm_state62();
    void thread_ap_CS_fsm_state63();
    void thread_ap_CS_fsm_state64();
    void thread_ap_CS_fsm_state65();
    void thread_ap_CS_fsm_state66();
    void thread_ap_CS_fsm_state67();
    void thread_ap_CS_fsm_state68();
    void thread_ap_CS_fsm_state69();
    void thread_ap_CS_fsm_state7();
    void thread_ap_CS_fsm_state70();
    void thread_ap_CS_fsm_state8();
    void thread_ap_CS_fsm_state9();
    void thread_ap_block_state1();
    void thread_ap_block_state2();
    void thread_ap_block_state68();
    void thread_ap_condition_702();
    void thread_ap_condition_832();
    void thread_ap_condition_844();
    void thread_ap_condition_846();
    void thread_ap_done();
    void thread_ap_idle();
    void thread_ap_ready();
    void thread_call_ret_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config26_s_fu_325_ap_start();
    void thread_data_V_V_blk_n();
    void thread_data_V_V_read();
    void thread_empty_120_fu_616_p1();
    void thread_empty_121_fu_628_p2();
    void thread_empty_122_fu_825_p2();
    void thread_empty_123_fu_943_p2();
    void thread_empty_124_fu_1051_p1();
    void thread_empty_126_fu_1179_p1();
    void thread_empty_127_fu_1191_p2();
    void thread_i1_fu_610_p2();
    void thread_i_fu_1297_p2();
    void thread_i_ih_fu_534_p2();
    void thread_icmp_ln1494_3_fu_1197_p2();
    void thread_icmp_ln1494_fu_1303_p2();
    void thread_icmp_ln203_7_fu_831_p2();
    void thread_icmp_ln203_8_fu_949_p2();
    void thread_icmp_ln203_9_fu_1059_p2();
    void thread_icmp_ln203_fu_634_p2();
    void thread_icmp_ln274_fu_528_p2();
    void thread_icmp_ln288_7_fu_560_p2();
    void thread_icmp_ln288_8_fu_570_p2();
    void thread_icmp_ln288_9_fu_580_p2();
    void thread_icmp_ln288_fu_550_p2();
    void thread_icmp_ln290_fu_604_p2();
    void thread_icmp_ln303_fu_1173_p2();
    void thread_icmp_ln313_fu_640_p2();
    void thread_icmp_ln317_fu_686_p2();
    void thread_internal_ap_ready();
    void thread_lshr_ln1494_3_fu_1281_p2();
    void thread_lshr_ln1494_fu_1275_p2();
    void thread_lshr_ln203_15_fu_795_p2();
    void thread_lshr_ln203_16_fu_911_p2();
    void thread_lshr_ln203_17_fu_917_p2();
    void thread_lshr_ln203_18_fu_1029_p2();
    void thread_lshr_ln203_19_fu_1035_p2();
    void thread_lshr_ln203_20_fu_1139_p2();
    void thread_lshr_ln203_21_fu_1145_p2();
    void thread_lshr_ln203_fu_789_p2();
    void thread_p_cast1_fu_1055_p1();
    void thread_pool_V_fu_1161_p5();
    void thread_pool_res_V_3_fu_1308_p3();
    void thread_pool_res_V_fu_807_p1();
    void thread_real_start();
    void thread_res_V_V_blk_n();
    void thread_res_V_V_din();
    void thread_res_V_V_write();
    void thread_select_ln1494_5_fu_1246_p3();
    void thread_select_ln1494_6_fu_1253_p3();
    void thread_select_ln1494_fu_1238_p3();
    void thread_select_ln203_23_fu_761_p3();
    void thread_select_ln203_24_fu_768_p3();
    void thread_select_ln203_25_fu_873_p3();
    void thread_select_ln203_26_fu_881_p3();
    void thread_select_ln203_27_fu_889_p3();
    void thread_select_ln203_28_fu_991_p3();
    void thread_select_ln203_29_fu_999_p3();
    void thread_select_ln203_30_fu_1007_p3();
    void thread_select_ln203_31_fu_1101_p3();
    void thread_select_ln203_32_fu_1109_p3();
    void thread_select_ln203_33_fu_1117_p3();
    void thread_select_ln203_fu_754_p3();
    void thread_select_ln323_fu_707_p3();
    void thread_select_ln328_fu_661_p3();
    void thread_start_out();
    void thread_start_write();
    void thread_sub_ln1494_5_fu_1232_p2();
    void thread_sub_ln1494_6_fu_1261_p2();
    void thread_sub_ln1494_fu_1220_p2();
    void thread_sub_ln203_23_fu_748_p2();
    void thread_sub_ln203_24_fu_775_p2();
    void thread_sub_ln203_25_fu_855_p2();
    void thread_sub_ln203_26_fu_867_p2();
    void thread_sub_ln203_27_fu_897_p2();
    void thread_sub_ln203_28_fu_973_p2();
    void thread_sub_ln203_29_fu_985_p2();
    void thread_sub_ln203_30_fu_1015_p2();
    void thread_sub_ln203_31_fu_1083_p2();
    void thread_sub_ln203_32_fu_1095_p2();
    void thread_sub_ln203_33_fu_1125_p2();
    void thread_sub_ln203_fu_736_p2();
    void thread_tmp_14_fu_726_p4();
    void thread_tmp_15_fu_845_p4();
    void thread_tmp_16_fu_963_p4();
    void thread_tmp_17_fu_1073_p4();
    void thread_tmp_18_fu_1211_p4();
    void thread_tmp_4_fu_817_p3();
    void thread_tmp_5_fu_933_p4();
    void thread_tmp_6_fu_1183_p3();
    void thread_tmp_s_fu_620_p3();
    void thread_trunc_ln1494_fu_1293_p1();
    void thread_trunc_ln203_7_fu_929_p1();
    void thread_trunc_ln203_8_fu_1047_p1();
    void thread_trunc_ln203_9_fu_1157_p1();
    void thread_xor_ln1494_fu_1226_p2();
    void thread_xor_ln203_10_fu_861_p2();
    void thread_xor_ln203_11_fu_979_p2();
    void thread_xor_ln203_12_fu_1089_p2();
    void thread_xor_ln203_9_fu_742_p2();
    void thread_xor_ln203_fu_811_p2();
    void thread_zext_ln1494_7_fu_1207_p1();
    void thread_zext_ln1494_8_fu_1267_p1();
    void thread_zext_ln1494_9_fu_1271_p1();
    void thread_zext_ln1494_fu_1203_p1();
    void thread_zext_ln203_31_fu_723_p1();
    void thread_zext_ln203_32_fu_781_p1();
    void thread_zext_ln203_33_fu_785_p1();
    void thread_zext_ln203_34_fu_837_p1();
    void thread_zext_ln203_35_fu_841_p1();
    void thread_zext_ln203_36_fu_903_p1();
    void thread_zext_ln203_37_fu_907_p1();
    void thread_zext_ln203_38_fu_955_p1();
    void thread_zext_ln203_39_fu_959_p1();
    void thread_zext_ln203_40_fu_1021_p1();
    void thread_zext_ln203_41_fu_1025_p1();
    void thread_zext_ln203_42_fu_1065_p1();
    void thread_zext_ln203_43_fu_1069_p1();
    void thread_zext_ln203_44_fu_1131_p1();
    void thread_zext_ln203_45_fu_1135_p1();
    void thread_zext_ln203_fu_720_p1();
    void thread_ap_NS_fsm();
};

}

using namespace ap_rtl;

#endif
