// ==============================================================
// RTL generated by Vivado(TM) HLS - High-Level Synthesis from C, C++ and OpenCL
// Version: 2019.2
// Copyright (C) 1986-2019 Xilinx, Inc. All Rights Reserved.
// 
// ===========================================================

#ifndef _cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config26_s_HH_
#define _cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config26_s_HH_

#include "systemc.h"
#include "AESL_pkg.h"

#include "cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config26_s_layf16.h"

namespace ap_rtl {

struct cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config26_s : public sc_module {
    // Port declarations 72
    sc_in_clk ap_clk;
    sc_in< sc_logic > ap_rst;
    sc_in< sc_logic > ap_start;
    sc_out< sc_logic > ap_done;
    sc_out< sc_logic > ap_idle;
    sc_out< sc_logic > ap_ready;
    sc_in< sc_lv<32> > data_0_V_read;
    sc_in< sc_lv<32> > data_1_V_read;
    sc_in< sc_lv<32> > data_2_V_read;
    sc_in< sc_lv<32> > data_3_V_read;
    sc_in< sc_lv<32> > data_4_V_read;
    sc_in< sc_lv<32> > data_5_V_read;
    sc_in< sc_lv<32> > data_6_V_read;
    sc_in< sc_lv<32> > data_7_V_read;
    sc_in< sc_lv<32> > data_8_V_read;
    sc_in< sc_lv<32> > data_9_V_read;
    sc_in< sc_lv<32> > data_10_V_read;
    sc_in< sc_lv<32> > data_11_V_read;
    sc_in< sc_lv<32> > data_12_V_read;
    sc_in< sc_lv<32> > data_13_V_read;
    sc_in< sc_lv<32> > data_14_V_read;
    sc_in< sc_lv<32> > data_15_V_read;
    sc_in< sc_lv<32> > data_16_V_read;
    sc_in< sc_lv<32> > data_17_V_read;
    sc_in< sc_lv<32> > data_18_V_read;
    sc_in< sc_lv<32> > data_19_V_read;
    sc_in< sc_lv<32> > data_20_V_read;
    sc_in< sc_lv<32> > data_21_V_read;
    sc_in< sc_lv<32> > data_22_V_read;
    sc_in< sc_lv<32> > data_23_V_read;
    sc_in< sc_lv<32> > data_24_V_read;
    sc_in< sc_lv<32> > data_25_V_read;
    sc_in< sc_lv<32> > data_26_V_read;
    sc_in< sc_lv<32> > data_27_V_read;
    sc_in< sc_lv<32> > data_28_V_read;
    sc_in< sc_lv<32> > data_29_V_read;
    sc_in< sc_lv<32> > data_30_V_read;
    sc_in< sc_lv<32> > data_31_V_read;
    sc_in< sc_lv<32> > data_32_V_read;
    sc_in< sc_lv<32> > data_33_V_read;
    sc_in< sc_lv<32> > data_34_V_read;
    sc_in< sc_lv<32> > data_35_V_read;
    sc_in< sc_lv<32> > data_36_V_read;
    sc_in< sc_lv<32> > data_37_V_read;
    sc_in< sc_lv<32> > data_38_V_read;
    sc_in< sc_lv<32> > data_39_V_read;
    sc_in< sc_lv<32> > data_40_V_read;
    sc_in< sc_lv<32> > data_41_V_read;
    sc_in< sc_lv<32> > data_42_V_read;
    sc_in< sc_lv<32> > data_43_V_read;
    sc_in< sc_lv<32> > data_44_V_read;
    sc_in< sc_lv<32> > data_45_V_read;
    sc_in< sc_lv<32> > data_46_V_read;
    sc_in< sc_lv<32> > data_47_V_read;
    sc_in< sc_lv<32> > data_48_V_read;
    sc_in< sc_lv<32> > data_49_V_read;
    sc_in< sc_lv<32> > data_50_V_read;
    sc_in< sc_lv<32> > data_51_V_read;
    sc_in< sc_lv<32> > data_52_V_read;
    sc_in< sc_lv<32> > data_53_V_read;
    sc_in< sc_lv<32> > data_54_V_read;
    sc_in< sc_lv<32> > data_55_V_read;
    sc_in< sc_lv<32> > data_56_V_read;
    sc_in< sc_lv<32> > data_57_V_read;
    sc_in< sc_lv<32> > data_58_V_read;
    sc_in< sc_lv<32> > data_59_V_read;
    sc_in< sc_lv<32> > data_60_V_read;
    sc_in< sc_lv<32> > data_61_V_read;
    sc_in< sc_lv<32> > data_62_V_read;
    sc_in< sc_lv<32> > data_63_V_read;
    sc_in< sc_lv<8192> > output_V_read;
    sc_out< sc_lv<8192> > ap_return;
    sc_signal< sc_lv<4> > ap_var_for_const0;


    // Module declarations
    cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config26_s(sc_module_name name);
    SC_HAS_PROCESS(cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config26_s);

    ~cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config26_s();

    sc_trace_file* mVcdFile;

    cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config26_s_layf16* layer_in_row_Array_V_7_0_0_U;
    cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config26_s_layf16* layer_in_row_Array_V_7_0_1_U;
    cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config26_s_layf16* layer_in_row_Array_V_7_0_2_U;
    cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config26_s_layf16* layer_in_row_Array_V_7_0_3_U;
    cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config26_s_layf16* layer_in_row_Array_V_7_0_4_U;
    cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config26_s_layf16* layer_in_row_Array_V_7_0_5_U;
    cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config26_s_layf16* layer_in_row_Array_V_7_0_6_U;
    cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config26_s_layf16* layer_in_row_Array_V_7_0_7_U;
    cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config26_s_layf16* layer_in_row_Array_V_7_0_8_U;
    cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config26_s_layf16* layer_in_row_Array_V_7_0_9_U;
    cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config26_s_layf16* layer_in_row_Array_V_7_0_10_U;
    cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config26_s_layf16* layer_in_row_Array_V_7_0_11_U;
    cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config26_s_layf16* layer_in_row_Array_V_7_0_12_U;
    cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config26_s_layf16* layer_in_row_Array_V_7_0_13_U;
    cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config26_s_layf16* layer_in_row_Array_V_7_0_14_U;
    cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config26_s_layf16* layer_in_row_Array_V_7_0_15_U;
    cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config26_s_layf16* layer_in_row_Array_V_7_0_16_U;
    cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config26_s_layf16* layer_in_row_Array_V_7_0_17_U;
    cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config26_s_layf16* layer_in_row_Array_V_7_0_18_U;
    cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config26_s_layf16* layer_in_row_Array_V_7_0_19_U;
    cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config26_s_layf16* layer_in_row_Array_V_7_0_20_U;
    cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config26_s_layf16* layer_in_row_Array_V_7_0_21_U;
    cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config26_s_layf16* layer_in_row_Array_V_7_0_22_U;
    cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config26_s_layf16* layer_in_row_Array_V_7_0_23_U;
    cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config26_s_layf16* layer_in_row_Array_V_7_0_24_U;
    cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config26_s_layf16* layer_in_row_Array_V_7_0_25_U;
    cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config26_s_layf16* layer_in_row_Array_V_7_0_26_U;
    cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config26_s_layf16* layer_in_row_Array_V_7_0_27_U;
    cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config26_s_layf16* layer_in_row_Array_V_7_0_28_U;
    cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config26_s_layf16* layer_in_row_Array_V_7_0_29_U;
    cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config26_s_layf16* layer_in_row_Array_V_7_0_30_U;
    cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config26_s_layf16* layer_in_row_Array_V_7_0_31_U;
    cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config26_s_layf16* layer_in_row_Array_V_7_0_32_U;
    cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config26_s_layf16* layer_in_row_Array_V_7_0_33_U;
    cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config26_s_layf16* layer_in_row_Array_V_7_0_34_U;
    cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config26_s_layf16* layer_in_row_Array_V_7_0_35_U;
    cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config26_s_layf16* layer_in_row_Array_V_7_0_36_U;
    cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config26_s_layf16* layer_in_row_Array_V_7_0_37_U;
    cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config26_s_layf16* layer_in_row_Array_V_7_0_38_U;
    cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config26_s_layf16* layer_in_row_Array_V_7_0_39_U;
    cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config26_s_layf16* layer_in_row_Array_V_7_0_40_U;
    cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config26_s_layf16* layer_in_row_Array_V_7_0_41_U;
    cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config26_s_layf16* layer_in_row_Array_V_7_0_42_U;
    cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config26_s_layf16* layer_in_row_Array_V_7_0_43_U;
    cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config26_s_layf16* layer_in_row_Array_V_7_0_44_U;
    cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config26_s_layf16* layer_in_row_Array_V_7_0_45_U;
    cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config26_s_layf16* layer_in_row_Array_V_7_0_46_U;
    cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config26_s_layf16* layer_in_row_Array_V_7_0_47_U;
    cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config26_s_layf16* layer_in_row_Array_V_7_0_48_U;
    cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config26_s_layf16* layer_in_row_Array_V_7_0_49_U;
    cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config26_s_layf16* layer_in_row_Array_V_7_0_50_U;
    cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config26_s_layf16* layer_in_row_Array_V_7_0_51_U;
    cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config26_s_layf16* layer_in_row_Array_V_7_0_52_U;
    cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config26_s_layf16* layer_in_row_Array_V_7_0_53_U;
    cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config26_s_layf16* layer_in_row_Array_V_7_0_54_U;
    cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config26_s_layf16* layer_in_row_Array_V_7_0_55_U;
    cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config26_s_layf16* layer_in_row_Array_V_7_0_56_U;
    cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config26_s_layf16* layer_in_row_Array_V_7_0_57_U;
    cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config26_s_layf16* layer_in_row_Array_V_7_0_58_U;
    cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config26_s_layf16* layer_in_row_Array_V_7_0_59_U;
    cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config26_s_layf16* layer_in_row_Array_V_7_0_60_U;
    cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config26_s_layf16* layer_in_row_Array_V_7_0_61_U;
    cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config26_s_layf16* layer_in_row_Array_V_7_0_62_U;
    cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config26_s_layf16* layer_in_row_Array_V_7_0_63_U;
    sc_signal< sc_lv<1> > ap_CS_fsm;
    sc_signal< sc_logic > ap_CS_fsm_state1;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_0_ce0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_0_we0;
    sc_signal< sc_lv<32> > layer_in_row_Array_V_7_0_0_q0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_1_ce0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_1_we0;
    sc_signal< sc_lv<32> > layer_in_row_Array_V_7_0_1_q0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_2_ce0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_2_we0;
    sc_signal< sc_lv<32> > layer_in_row_Array_V_7_0_2_q0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_3_ce0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_3_we0;
    sc_signal< sc_lv<32> > layer_in_row_Array_V_7_0_3_q0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_4_ce0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_4_we0;
    sc_signal< sc_lv<32> > layer_in_row_Array_V_7_0_4_q0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_5_ce0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_5_we0;
    sc_signal< sc_lv<32> > layer_in_row_Array_V_7_0_5_q0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_6_ce0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_6_we0;
    sc_signal< sc_lv<32> > layer_in_row_Array_V_7_0_6_q0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_7_ce0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_7_we0;
    sc_signal< sc_lv<32> > layer_in_row_Array_V_7_0_7_q0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_8_ce0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_8_we0;
    sc_signal< sc_lv<32> > layer_in_row_Array_V_7_0_8_q0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_9_ce0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_9_we0;
    sc_signal< sc_lv<32> > layer_in_row_Array_V_7_0_9_q0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_10_ce0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_10_we0;
    sc_signal< sc_lv<32> > layer_in_row_Array_V_7_0_10_q0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_11_ce0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_11_we0;
    sc_signal< sc_lv<32> > layer_in_row_Array_V_7_0_11_q0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_12_ce0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_12_we0;
    sc_signal< sc_lv<32> > layer_in_row_Array_V_7_0_12_q0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_13_ce0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_13_we0;
    sc_signal< sc_lv<32> > layer_in_row_Array_V_7_0_13_q0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_14_ce0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_14_we0;
    sc_signal< sc_lv<32> > layer_in_row_Array_V_7_0_14_q0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_15_ce0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_15_we0;
    sc_signal< sc_lv<32> > layer_in_row_Array_V_7_0_15_q0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_16_ce0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_16_we0;
    sc_signal< sc_lv<32> > layer_in_row_Array_V_7_0_16_q0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_17_ce0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_17_we0;
    sc_signal< sc_lv<32> > layer_in_row_Array_V_7_0_17_q0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_18_ce0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_18_we0;
    sc_signal< sc_lv<32> > layer_in_row_Array_V_7_0_18_q0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_19_ce0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_19_we0;
    sc_signal< sc_lv<32> > layer_in_row_Array_V_7_0_19_q0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_20_ce0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_20_we0;
    sc_signal< sc_lv<32> > layer_in_row_Array_V_7_0_20_q0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_21_ce0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_21_we0;
    sc_signal< sc_lv<32> > layer_in_row_Array_V_7_0_21_q0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_22_ce0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_22_we0;
    sc_signal< sc_lv<32> > layer_in_row_Array_V_7_0_22_q0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_23_ce0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_23_we0;
    sc_signal< sc_lv<32> > layer_in_row_Array_V_7_0_23_q0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_24_ce0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_24_we0;
    sc_signal< sc_lv<32> > layer_in_row_Array_V_7_0_24_q0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_25_ce0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_25_we0;
    sc_signal< sc_lv<32> > layer_in_row_Array_V_7_0_25_q0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_26_ce0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_26_we0;
    sc_signal< sc_lv<32> > layer_in_row_Array_V_7_0_26_q0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_27_ce0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_27_we0;
    sc_signal< sc_lv<32> > layer_in_row_Array_V_7_0_27_q0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_28_ce0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_28_we0;
    sc_signal< sc_lv<32> > layer_in_row_Array_V_7_0_28_q0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_29_ce0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_29_we0;
    sc_signal< sc_lv<32> > layer_in_row_Array_V_7_0_29_q0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_30_ce0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_30_we0;
    sc_signal< sc_lv<32> > layer_in_row_Array_V_7_0_30_q0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_31_ce0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_31_we0;
    sc_signal< sc_lv<32> > layer_in_row_Array_V_7_0_31_q0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_32_ce0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_32_we0;
    sc_signal< sc_lv<32> > layer_in_row_Array_V_7_0_32_q0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_33_ce0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_33_we0;
    sc_signal< sc_lv<32> > layer_in_row_Array_V_7_0_33_q0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_34_ce0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_34_we0;
    sc_signal< sc_lv<32> > layer_in_row_Array_V_7_0_34_q0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_35_ce0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_35_we0;
    sc_signal< sc_lv<32> > layer_in_row_Array_V_7_0_35_q0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_36_ce0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_36_we0;
    sc_signal< sc_lv<32> > layer_in_row_Array_V_7_0_36_q0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_37_ce0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_37_we0;
    sc_signal< sc_lv<32> > layer_in_row_Array_V_7_0_37_q0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_38_ce0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_38_we0;
    sc_signal< sc_lv<32> > layer_in_row_Array_V_7_0_38_q0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_39_ce0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_39_we0;
    sc_signal< sc_lv<32> > layer_in_row_Array_V_7_0_39_q0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_40_ce0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_40_we0;
    sc_signal< sc_lv<32> > layer_in_row_Array_V_7_0_40_q0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_41_ce0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_41_we0;
    sc_signal< sc_lv<32> > layer_in_row_Array_V_7_0_41_q0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_42_ce0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_42_we0;
    sc_signal< sc_lv<32> > layer_in_row_Array_V_7_0_42_q0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_43_ce0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_43_we0;
    sc_signal< sc_lv<32> > layer_in_row_Array_V_7_0_43_q0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_44_ce0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_44_we0;
    sc_signal< sc_lv<32> > layer_in_row_Array_V_7_0_44_q0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_45_ce0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_45_we0;
    sc_signal< sc_lv<32> > layer_in_row_Array_V_7_0_45_q0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_46_ce0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_46_we0;
    sc_signal< sc_lv<32> > layer_in_row_Array_V_7_0_46_q0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_47_ce0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_47_we0;
    sc_signal< sc_lv<32> > layer_in_row_Array_V_7_0_47_q0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_48_ce0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_48_we0;
    sc_signal< sc_lv<32> > layer_in_row_Array_V_7_0_48_q0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_49_ce0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_49_we0;
    sc_signal< sc_lv<32> > layer_in_row_Array_V_7_0_49_q0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_50_ce0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_50_we0;
    sc_signal< sc_lv<32> > layer_in_row_Array_V_7_0_50_q0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_51_ce0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_51_we0;
    sc_signal< sc_lv<32> > layer_in_row_Array_V_7_0_51_q0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_52_ce0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_52_we0;
    sc_signal< sc_lv<32> > layer_in_row_Array_V_7_0_52_q0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_53_ce0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_53_we0;
    sc_signal< sc_lv<32> > layer_in_row_Array_V_7_0_53_q0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_54_ce0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_54_we0;
    sc_signal< sc_lv<32> > layer_in_row_Array_V_7_0_54_q0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_55_ce0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_55_we0;
    sc_signal< sc_lv<32> > layer_in_row_Array_V_7_0_55_q0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_56_ce0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_56_we0;
    sc_signal< sc_lv<32> > layer_in_row_Array_V_7_0_56_q0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_57_ce0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_57_we0;
    sc_signal< sc_lv<32> > layer_in_row_Array_V_7_0_57_q0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_58_ce0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_58_we0;
    sc_signal< sc_lv<32> > layer_in_row_Array_V_7_0_58_q0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_59_ce0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_59_we0;
    sc_signal< sc_lv<32> > layer_in_row_Array_V_7_0_59_q0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_60_ce0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_60_we0;
    sc_signal< sc_lv<32> > layer_in_row_Array_V_7_0_60_q0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_61_ce0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_61_we0;
    sc_signal< sc_lv<32> > layer_in_row_Array_V_7_0_61_q0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_62_ce0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_62_we0;
    sc_signal< sc_lv<32> > layer_in_row_Array_V_7_0_62_q0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_63_ce0;
    sc_signal< sc_logic > layer_in_row_Array_V_7_0_63_we0;
    sc_signal< sc_lv<32> > layer_in_row_Array_V_7_0_63_q0;
    sc_signal< sc_lv<2048> > tmp_fu_1446_p4;
    sc_signal< sc_lv<2048> > tmp_3_fu_1456_p4;
    sc_signal< sc_lv<1> > ap_NS_fsm;
    static const sc_logic ap_const_logic_1;
    static const sc_logic ap_const_logic_0;
    static const sc_lv<1> ap_ST_fsm_state1;
    static const sc_lv<32> ap_const_lv32_0;
    static const sc_lv<1> ap_const_lv1_1;
    static const sc_lv<4> ap_const_lv4_C;
    static const sc_lv<32> ap_const_lv32_1800;
    static const sc_lv<32> ap_const_lv32_1FFF;
    static const sc_lv<32> ap_const_lv32_800;
    static const sc_lv<32> ap_const_lv32_FFF;
    static const bool ap_const_boolean_1;
    // Thread declarations
    void thread_ap_var_for_const0();
    void thread_ap_clk_no_reset_();
    void thread_ap_CS_fsm_state1();
    void thread_ap_done();
    void thread_ap_idle();
    void thread_ap_ready();
    void thread_ap_return();
    void thread_layer_in_row_Array_V_7_0_0_ce0();
    void thread_layer_in_row_Array_V_7_0_0_we0();
    void thread_layer_in_row_Array_V_7_0_10_ce0();
    void thread_layer_in_row_Array_V_7_0_10_we0();
    void thread_layer_in_row_Array_V_7_0_11_ce0();
    void thread_layer_in_row_Array_V_7_0_11_we0();
    void thread_layer_in_row_Array_V_7_0_12_ce0();
    void thread_layer_in_row_Array_V_7_0_12_we0();
    void thread_layer_in_row_Array_V_7_0_13_ce0();
    void thread_layer_in_row_Array_V_7_0_13_we0();
    void thread_layer_in_row_Array_V_7_0_14_ce0();
    void thread_layer_in_row_Array_V_7_0_14_we0();
    void thread_layer_in_row_Array_V_7_0_15_ce0();
    void thread_layer_in_row_Array_V_7_0_15_we0();
    void thread_layer_in_row_Array_V_7_0_16_ce0();
    void thread_layer_in_row_Array_V_7_0_16_we0();
    void thread_layer_in_row_Array_V_7_0_17_ce0();
    void thread_layer_in_row_Array_V_7_0_17_we0();
    void thread_layer_in_row_Array_V_7_0_18_ce0();
    void thread_layer_in_row_Array_V_7_0_18_we0();
    void thread_layer_in_row_Array_V_7_0_19_ce0();
    void thread_layer_in_row_Array_V_7_0_19_we0();
    void thread_layer_in_row_Array_V_7_0_1_ce0();
    void thread_layer_in_row_Array_V_7_0_1_we0();
    void thread_layer_in_row_Array_V_7_0_20_ce0();
    void thread_layer_in_row_Array_V_7_0_20_we0();
    void thread_layer_in_row_Array_V_7_0_21_ce0();
    void thread_layer_in_row_Array_V_7_0_21_we0();
    void thread_layer_in_row_Array_V_7_0_22_ce0();
    void thread_layer_in_row_Array_V_7_0_22_we0();
    void thread_layer_in_row_Array_V_7_0_23_ce0();
    void thread_layer_in_row_Array_V_7_0_23_we0();
    void thread_layer_in_row_Array_V_7_0_24_ce0();
    void thread_layer_in_row_Array_V_7_0_24_we0();
    void thread_layer_in_row_Array_V_7_0_25_ce0();
    void thread_layer_in_row_Array_V_7_0_25_we0();
    void thread_layer_in_row_Array_V_7_0_26_ce0();
    void thread_layer_in_row_Array_V_7_0_26_we0();
    void thread_layer_in_row_Array_V_7_0_27_ce0();
    void thread_layer_in_row_Array_V_7_0_27_we0();
    void thread_layer_in_row_Array_V_7_0_28_ce0();
    void thread_layer_in_row_Array_V_7_0_28_we0();
    void thread_layer_in_row_Array_V_7_0_29_ce0();
    void thread_layer_in_row_Array_V_7_0_29_we0();
    void thread_layer_in_row_Array_V_7_0_2_ce0();
    void thread_layer_in_row_Array_V_7_0_2_we0();
    void thread_layer_in_row_Array_V_7_0_30_ce0();
    void thread_layer_in_row_Array_V_7_0_30_we0();
    void thread_layer_in_row_Array_V_7_0_31_ce0();
    void thread_layer_in_row_Array_V_7_0_31_we0();
    void thread_layer_in_row_Array_V_7_0_32_ce0();
    void thread_layer_in_row_Array_V_7_0_32_we0();
    void thread_layer_in_row_Array_V_7_0_33_ce0();
    void thread_layer_in_row_Array_V_7_0_33_we0();
    void thread_layer_in_row_Array_V_7_0_34_ce0();
    void thread_layer_in_row_Array_V_7_0_34_we0();
    void thread_layer_in_row_Array_V_7_0_35_ce0();
    void thread_layer_in_row_Array_V_7_0_35_we0();
    void thread_layer_in_row_Array_V_7_0_36_ce0();
    void thread_layer_in_row_Array_V_7_0_36_we0();
    void thread_layer_in_row_Array_V_7_0_37_ce0();
    void thread_layer_in_row_Array_V_7_0_37_we0();
    void thread_layer_in_row_Array_V_7_0_38_ce0();
    void thread_layer_in_row_Array_V_7_0_38_we0();
    void thread_layer_in_row_Array_V_7_0_39_ce0();
    void thread_layer_in_row_Array_V_7_0_39_we0();
    void thread_layer_in_row_Array_V_7_0_3_ce0();
    void thread_layer_in_row_Array_V_7_0_3_we0();
    void thread_layer_in_row_Array_V_7_0_40_ce0();
    void thread_layer_in_row_Array_V_7_0_40_we0();
    void thread_layer_in_row_Array_V_7_0_41_ce0();
    void thread_layer_in_row_Array_V_7_0_41_we0();
    void thread_layer_in_row_Array_V_7_0_42_ce0();
    void thread_layer_in_row_Array_V_7_0_42_we0();
    void thread_layer_in_row_Array_V_7_0_43_ce0();
    void thread_layer_in_row_Array_V_7_0_43_we0();
    void thread_layer_in_row_Array_V_7_0_44_ce0();
    void thread_layer_in_row_Array_V_7_0_44_we0();
    void thread_layer_in_row_Array_V_7_0_45_ce0();
    void thread_layer_in_row_Array_V_7_0_45_we0();
    void thread_layer_in_row_Array_V_7_0_46_ce0();
    void thread_layer_in_row_Array_V_7_0_46_we0();
    void thread_layer_in_row_Array_V_7_0_47_ce0();
    void thread_layer_in_row_Array_V_7_0_47_we0();
    void thread_layer_in_row_Array_V_7_0_48_ce0();
    void thread_layer_in_row_Array_V_7_0_48_we0();
    void thread_layer_in_row_Array_V_7_0_49_ce0();
    void thread_layer_in_row_Array_V_7_0_49_we0();
    void thread_layer_in_row_Array_V_7_0_4_ce0();
    void thread_layer_in_row_Array_V_7_0_4_we0();
    void thread_layer_in_row_Array_V_7_0_50_ce0();
    void thread_layer_in_row_Array_V_7_0_50_we0();
    void thread_layer_in_row_Array_V_7_0_51_ce0();
    void thread_layer_in_row_Array_V_7_0_51_we0();
    void thread_layer_in_row_Array_V_7_0_52_ce0();
    void thread_layer_in_row_Array_V_7_0_52_we0();
    void thread_layer_in_row_Array_V_7_0_53_ce0();
    void thread_layer_in_row_Array_V_7_0_53_we0();
    void thread_layer_in_row_Array_V_7_0_54_ce0();
    void thread_layer_in_row_Array_V_7_0_54_we0();
    void thread_layer_in_row_Array_V_7_0_55_ce0();
    void thread_layer_in_row_Array_V_7_0_55_we0();
    void thread_layer_in_row_Array_V_7_0_56_ce0();
    void thread_layer_in_row_Array_V_7_0_56_we0();
    void thread_layer_in_row_Array_V_7_0_57_ce0();
    void thread_layer_in_row_Array_V_7_0_57_we0();
    void thread_layer_in_row_Array_V_7_0_58_ce0();
    void thread_layer_in_row_Array_V_7_0_58_we0();
    void thread_layer_in_row_Array_V_7_0_59_ce0();
    void thread_layer_in_row_Array_V_7_0_59_we0();
    void thread_layer_in_row_Array_V_7_0_5_ce0();
    void thread_layer_in_row_Array_V_7_0_5_we0();
    void thread_layer_in_row_Array_V_7_0_60_ce0();
    void thread_layer_in_row_Array_V_7_0_60_we0();
    void thread_layer_in_row_Array_V_7_0_61_ce0();
    void thread_layer_in_row_Array_V_7_0_61_we0();
    void thread_layer_in_row_Array_V_7_0_62_ce0();
    void thread_layer_in_row_Array_V_7_0_62_we0();
    void thread_layer_in_row_Array_V_7_0_63_ce0();
    void thread_layer_in_row_Array_V_7_0_63_we0();
    void thread_layer_in_row_Array_V_7_0_6_ce0();
    void thread_layer_in_row_Array_V_7_0_6_we0();
    void thread_layer_in_row_Array_V_7_0_7_ce0();
    void thread_layer_in_row_Array_V_7_0_7_we0();
    void thread_layer_in_row_Array_V_7_0_8_ce0();
    void thread_layer_in_row_Array_V_7_0_8_we0();
    void thread_layer_in_row_Array_V_7_0_9_ce0();
    void thread_layer_in_row_Array_V_7_0_9_we0();
    void thread_tmp_3_fu_1456_p4();
    void thread_tmp_fu_1446_p4();
    void thread_ap_NS_fsm();
};

}

using namespace ap_rtl;

#endif
