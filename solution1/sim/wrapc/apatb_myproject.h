// ==============================================================
// Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC v2019.2 (64-bit)
// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// ==============================================================

extern void AESL_WRAP_myproject (
hls::stream<ap_fixed<32, 16, (ap_q_mode) 5, (ap_o_mode)3, 0> > (&em_barrel),
hls::stream<ap_fixed<32, 16, (ap_q_mode) 5, (ap_o_mode)3, 0> > (&layer55_out),
unsigned short (&const_size_in_1),
unsigned short (&const_size_out_1),
ap_fixed<32, 16, (ap_q_mode) 5, (ap_o_mode)3, 0> w13[9216],
ap_fixed<32, 16, (ap_q_mode) 5, (ap_o_mode)3, 0> w18[18432],
ap_fixed<32, 16, (ap_q_mode) 5, (ap_o_mode)3, 0> w22[36864],
ap_fixed<32, 16, (ap_q_mode) 5, (ap_o_mode)3, 0> w27[73728],
ap_fixed<32, 16, (ap_q_mode) 5, (ap_o_mode)3, 0> w31[147456],
ap_fixed<32, 16, (ap_q_mode) 5, (ap_o_mode)3, 0> w36[294912],
ap_fixed<32, 16, (ap_q_mode) 5, (ap_o_mode)3, 0> w40[589824],
ap_fixed<32, 16, (ap_q_mode) 5, (ap_o_mode)3, 0> w45[589824],
ap_fixed<32, 16, (ap_q_mode) 5, (ap_o_mode)3, 0> w49[65536]);
