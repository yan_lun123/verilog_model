
log_wave -r /
set designtopgroup [add_wave_group "Design Top Signals"]
set coutputgroup [add_wave_group "C Outputs" -into $designtopgroup]
set const_size_out_1_group [add_wave_group const_size_out_1(wire) -into $coutputgroup]
add_wave /apatb_myproject_top/AESL_inst_myproject/const_size_out_1_ap_vld -into $const_size_out_1_group -color #ffff00 -radix hex
add_wave /apatb_myproject_top/AESL_inst_myproject/const_size_out_1 -into $const_size_out_1_group -radix hex
set const_size_in_1_group [add_wave_group const_size_in_1(wire) -into $coutputgroup]
add_wave /apatb_myproject_top/AESL_inst_myproject/const_size_in_1_ap_vld -into $const_size_in_1_group -color #ffff00 -radix hex
add_wave /apatb_myproject_top/AESL_inst_myproject/const_size_in_1 -into $const_size_in_1_group -radix hex
set layer55_out_group [add_wave_group layer55_out(axis) -into $coutputgroup]
add_wave /apatb_myproject_top/AESL_inst_myproject/layer55_out_V_V_TREADY -into $layer55_out_group -color #ffff00 -radix hex
add_wave /apatb_myproject_top/AESL_inst_myproject/layer55_out_V_V_TVALID -into $layer55_out_group -color #ffff00 -radix hex
add_wave /apatb_myproject_top/AESL_inst_myproject/layer55_out_V_V_TDATA -into $layer55_out_group -radix hex
set cinputgroup [add_wave_group "C Inputs" -into $designtopgroup]
set w49_group [add_wave_group w49(memory) -into $cinputgroup]
add_wave /apatb_myproject_top/AESL_inst_myproject/w49_V_we1 -into $w49_group -color #ffff00 -radix hex
add_wave /apatb_myproject_top/AESL_inst_myproject/w49_V_q1 -into $w49_group -radix hex
add_wave /apatb_myproject_top/AESL_inst_myproject/w49_V_d1 -into $w49_group -radix hex
add_wave /apatb_myproject_top/AESL_inst_myproject/w49_V_ce1 -into $w49_group -color #ffff00 -radix hex
add_wave /apatb_myproject_top/AESL_inst_myproject/w49_V_address1 -into $w49_group -radix hex
add_wave /apatb_myproject_top/AESL_inst_myproject/w49_V_we0 -into $w49_group -color #ffff00 -radix hex
add_wave /apatb_myproject_top/AESL_inst_myproject/w49_V_q0 -into $w49_group -radix hex
add_wave /apatb_myproject_top/AESL_inst_myproject/w49_V_d0 -into $w49_group -radix hex
add_wave /apatb_myproject_top/AESL_inst_myproject/w49_V_ce0 -into $w49_group -color #ffff00 -radix hex
add_wave /apatb_myproject_top/AESL_inst_myproject/w49_V_address0 -into $w49_group -radix hex
set w45_group [add_wave_group w45(memory) -into $cinputgroup]
add_wave /apatb_myproject_top/AESL_inst_myproject/w45_V_we1 -into $w45_group -color #ffff00 -radix hex
add_wave /apatb_myproject_top/AESL_inst_myproject/w45_V_q1 -into $w45_group -radix hex
add_wave /apatb_myproject_top/AESL_inst_myproject/w45_V_d1 -into $w45_group -radix hex
add_wave /apatb_myproject_top/AESL_inst_myproject/w45_V_ce1 -into $w45_group -color #ffff00 -radix hex
add_wave /apatb_myproject_top/AESL_inst_myproject/w45_V_address1 -into $w45_group -radix hex
add_wave /apatb_myproject_top/AESL_inst_myproject/w45_V_we0 -into $w45_group -color #ffff00 -radix hex
add_wave /apatb_myproject_top/AESL_inst_myproject/w45_V_q0 -into $w45_group -radix hex
add_wave /apatb_myproject_top/AESL_inst_myproject/w45_V_d0 -into $w45_group -radix hex
add_wave /apatb_myproject_top/AESL_inst_myproject/w45_V_ce0 -into $w45_group -color #ffff00 -radix hex
add_wave /apatb_myproject_top/AESL_inst_myproject/w45_V_address0 -into $w45_group -radix hex
set w40_group [add_wave_group w40(memory) -into $cinputgroup]
add_wave /apatb_myproject_top/AESL_inst_myproject/w40_V_we1 -into $w40_group -color #ffff00 -radix hex
add_wave /apatb_myproject_top/AESL_inst_myproject/w40_V_q1 -into $w40_group -radix hex
add_wave /apatb_myproject_top/AESL_inst_myproject/w40_V_d1 -into $w40_group -radix hex
add_wave /apatb_myproject_top/AESL_inst_myproject/w40_V_ce1 -into $w40_group -color #ffff00 -radix hex
add_wave /apatb_myproject_top/AESL_inst_myproject/w40_V_address1 -into $w40_group -radix hex
add_wave /apatb_myproject_top/AESL_inst_myproject/w40_V_we0 -into $w40_group -color #ffff00 -radix hex
add_wave /apatb_myproject_top/AESL_inst_myproject/w40_V_q0 -into $w40_group -radix hex
add_wave /apatb_myproject_top/AESL_inst_myproject/w40_V_d0 -into $w40_group -radix hex
add_wave /apatb_myproject_top/AESL_inst_myproject/w40_V_ce0 -into $w40_group -color #ffff00 -radix hex
add_wave /apatb_myproject_top/AESL_inst_myproject/w40_V_address0 -into $w40_group -radix hex
set w36_group [add_wave_group w36(memory) -into $cinputgroup]
add_wave /apatb_myproject_top/AESL_inst_myproject/w36_V_we1 -into $w36_group -color #ffff00 -radix hex
add_wave /apatb_myproject_top/AESL_inst_myproject/w36_V_q1 -into $w36_group -radix hex
add_wave /apatb_myproject_top/AESL_inst_myproject/w36_V_d1 -into $w36_group -radix hex
add_wave /apatb_myproject_top/AESL_inst_myproject/w36_V_ce1 -into $w36_group -color #ffff00 -radix hex
add_wave /apatb_myproject_top/AESL_inst_myproject/w36_V_address1 -into $w36_group -radix hex
add_wave /apatb_myproject_top/AESL_inst_myproject/w36_V_we0 -into $w36_group -color #ffff00 -radix hex
add_wave /apatb_myproject_top/AESL_inst_myproject/w36_V_q0 -into $w36_group -radix hex
add_wave /apatb_myproject_top/AESL_inst_myproject/w36_V_d0 -into $w36_group -radix hex
add_wave /apatb_myproject_top/AESL_inst_myproject/w36_V_ce0 -into $w36_group -color #ffff00 -radix hex
add_wave /apatb_myproject_top/AESL_inst_myproject/w36_V_address0 -into $w36_group -radix hex
set w31_group [add_wave_group w31(memory) -into $cinputgroup]
add_wave /apatb_myproject_top/AESL_inst_myproject/w31_V_we1 -into $w31_group -color #ffff00 -radix hex
add_wave /apatb_myproject_top/AESL_inst_myproject/w31_V_q1 -into $w31_group -radix hex
add_wave /apatb_myproject_top/AESL_inst_myproject/w31_V_d1 -into $w31_group -radix hex
add_wave /apatb_myproject_top/AESL_inst_myproject/w31_V_ce1 -into $w31_group -color #ffff00 -radix hex
add_wave /apatb_myproject_top/AESL_inst_myproject/w31_V_address1 -into $w31_group -radix hex
add_wave /apatb_myproject_top/AESL_inst_myproject/w31_V_we0 -into $w31_group -color #ffff00 -radix hex
add_wave /apatb_myproject_top/AESL_inst_myproject/w31_V_q0 -into $w31_group -radix hex
add_wave /apatb_myproject_top/AESL_inst_myproject/w31_V_d0 -into $w31_group -radix hex
add_wave /apatb_myproject_top/AESL_inst_myproject/w31_V_ce0 -into $w31_group -color #ffff00 -radix hex
add_wave /apatb_myproject_top/AESL_inst_myproject/w31_V_address0 -into $w31_group -radix hex
set w27_group [add_wave_group w27(memory) -into $cinputgroup]
add_wave /apatb_myproject_top/AESL_inst_myproject/w27_V_we1 -into $w27_group -color #ffff00 -radix hex
add_wave /apatb_myproject_top/AESL_inst_myproject/w27_V_q1 -into $w27_group -radix hex
add_wave /apatb_myproject_top/AESL_inst_myproject/w27_V_d1 -into $w27_group -radix hex
add_wave /apatb_myproject_top/AESL_inst_myproject/w27_V_ce1 -into $w27_group -color #ffff00 -radix hex
add_wave /apatb_myproject_top/AESL_inst_myproject/w27_V_address1 -into $w27_group -radix hex
add_wave /apatb_myproject_top/AESL_inst_myproject/w27_V_we0 -into $w27_group -color #ffff00 -radix hex
add_wave /apatb_myproject_top/AESL_inst_myproject/w27_V_q0 -into $w27_group -radix hex
add_wave /apatb_myproject_top/AESL_inst_myproject/w27_V_d0 -into $w27_group -radix hex
add_wave /apatb_myproject_top/AESL_inst_myproject/w27_V_ce0 -into $w27_group -color #ffff00 -radix hex
add_wave /apatb_myproject_top/AESL_inst_myproject/w27_V_address0 -into $w27_group -radix hex
set w22_group [add_wave_group w22(memory) -into $cinputgroup]
add_wave /apatb_myproject_top/AESL_inst_myproject/w22_V_we1 -into $w22_group -color #ffff00 -radix hex
add_wave /apatb_myproject_top/AESL_inst_myproject/w22_V_q1 -into $w22_group -radix hex
add_wave /apatb_myproject_top/AESL_inst_myproject/w22_V_d1 -into $w22_group -radix hex
add_wave /apatb_myproject_top/AESL_inst_myproject/w22_V_ce1 -into $w22_group -color #ffff00 -radix hex
add_wave /apatb_myproject_top/AESL_inst_myproject/w22_V_address1 -into $w22_group -radix hex
add_wave /apatb_myproject_top/AESL_inst_myproject/w22_V_we0 -into $w22_group -color #ffff00 -radix hex
add_wave /apatb_myproject_top/AESL_inst_myproject/w22_V_q0 -into $w22_group -radix hex
add_wave /apatb_myproject_top/AESL_inst_myproject/w22_V_d0 -into $w22_group -radix hex
add_wave /apatb_myproject_top/AESL_inst_myproject/w22_V_ce0 -into $w22_group -color #ffff00 -radix hex
add_wave /apatb_myproject_top/AESL_inst_myproject/w22_V_address0 -into $w22_group -radix hex
set w18_group [add_wave_group w18(memory) -into $cinputgroup]
add_wave /apatb_myproject_top/AESL_inst_myproject/w18_V_we1 -into $w18_group -color #ffff00 -radix hex
add_wave /apatb_myproject_top/AESL_inst_myproject/w18_V_q1 -into $w18_group -radix hex
add_wave /apatb_myproject_top/AESL_inst_myproject/w18_V_d1 -into $w18_group -radix hex
add_wave /apatb_myproject_top/AESL_inst_myproject/w18_V_ce1 -into $w18_group -color #ffff00 -radix hex
add_wave /apatb_myproject_top/AESL_inst_myproject/w18_V_address1 -into $w18_group -radix hex
add_wave /apatb_myproject_top/AESL_inst_myproject/w18_V_we0 -into $w18_group -color #ffff00 -radix hex
add_wave /apatb_myproject_top/AESL_inst_myproject/w18_V_q0 -into $w18_group -radix hex
add_wave /apatb_myproject_top/AESL_inst_myproject/w18_V_d0 -into $w18_group -radix hex
add_wave /apatb_myproject_top/AESL_inst_myproject/w18_V_ce0 -into $w18_group -color #ffff00 -radix hex
add_wave /apatb_myproject_top/AESL_inst_myproject/w18_V_address0 -into $w18_group -radix hex
set w13_group [add_wave_group w13(memory) -into $cinputgroup]
add_wave /apatb_myproject_top/AESL_inst_myproject/w13_V_we1 -into $w13_group -color #ffff00 -radix hex
add_wave /apatb_myproject_top/AESL_inst_myproject/w13_V_q1 -into $w13_group -radix hex
add_wave /apatb_myproject_top/AESL_inst_myproject/w13_V_d1 -into $w13_group -radix hex
add_wave /apatb_myproject_top/AESL_inst_myproject/w13_V_ce1 -into $w13_group -color #ffff00 -radix hex
add_wave /apatb_myproject_top/AESL_inst_myproject/w13_V_address1 -into $w13_group -radix hex
add_wave /apatb_myproject_top/AESL_inst_myproject/w13_V_we0 -into $w13_group -color #ffff00 -radix hex
add_wave /apatb_myproject_top/AESL_inst_myproject/w13_V_q0 -into $w13_group -radix hex
add_wave /apatb_myproject_top/AESL_inst_myproject/w13_V_d0 -into $w13_group -radix hex
add_wave /apatb_myproject_top/AESL_inst_myproject/w13_V_ce0 -into $w13_group -color #ffff00 -radix hex
add_wave /apatb_myproject_top/AESL_inst_myproject/w13_V_address0 -into $w13_group -radix hex
set em_barrel_group [add_wave_group em_barrel(axis) -into $cinputgroup]
add_wave /apatb_myproject_top/AESL_inst_myproject/em_barrel_V_V_TREADY -into $em_barrel_group -color #ffff00 -radix hex
add_wave /apatb_myproject_top/AESL_inst_myproject/em_barrel_V_V_TVALID -into $em_barrel_group -color #ffff00 -radix hex
add_wave /apatb_myproject_top/AESL_inst_myproject/em_barrel_V_V_TDATA -into $em_barrel_group -radix hex
set blocksiggroup [add_wave_group "Block-level IO Handshake" -into $designtopgroup]
add_wave /apatb_myproject_top/AESL_inst_myproject/ap_start -into $blocksiggroup
add_wave /apatb_myproject_top/AESL_inst_myproject/ap_done -into $blocksiggroup
add_wave /apatb_myproject_top/AESL_inst_myproject/ap_ready -into $blocksiggroup
add_wave /apatb_myproject_top/AESL_inst_myproject/ap_idle -into $blocksiggroup
set resetgroup [add_wave_group "Reset" -into $designtopgroup]
add_wave /apatb_myproject_top/AESL_inst_myproject/ap_rst_n -into $resetgroup
set clockgroup [add_wave_group "Clock" -into $designtopgroup]
add_wave /apatb_myproject_top/AESL_inst_myproject/ap_clk -into $clockgroup
set testbenchgroup [add_wave_group "Test Bench Signals"]
set tbinternalsiggroup [add_wave_group "Internal Signals" -into $testbenchgroup]
set tb_simstatus_group [add_wave_group "Simulation Status" -into $tbinternalsiggroup]
set tb_portdepth_group [add_wave_group "Port Depth" -into $tbinternalsiggroup]
add_wave /apatb_myproject_top/AUTOTB_TRANSACTION_NUM -into $tb_simstatus_group -radix hex
add_wave /apatb_myproject_top/ready_cnt -into $tb_simstatus_group -radix hex
add_wave /apatb_myproject_top/done_cnt -into $tb_simstatus_group -radix hex
add_wave /apatb_myproject_top/LENGTH_em_barrel_V_V -into $tb_portdepth_group -radix hex
add_wave /apatb_myproject_top/LENGTH_layer55_out_V_V -into $tb_portdepth_group -radix hex
add_wave /apatb_myproject_top/LENGTH_const_size_in_1 -into $tb_portdepth_group -radix hex
add_wave /apatb_myproject_top/LENGTH_const_size_out_1 -into $tb_portdepth_group -radix hex
add_wave /apatb_myproject_top/LENGTH_w13_V -into $tb_portdepth_group -radix hex
add_wave /apatb_myproject_top/LENGTH_w18_V -into $tb_portdepth_group -radix hex
add_wave /apatb_myproject_top/LENGTH_w22_V -into $tb_portdepth_group -radix hex
add_wave /apatb_myproject_top/LENGTH_w27_V -into $tb_portdepth_group -radix hex
add_wave /apatb_myproject_top/LENGTH_w31_V -into $tb_portdepth_group -radix hex
add_wave /apatb_myproject_top/LENGTH_w36_V -into $tb_portdepth_group -radix hex
add_wave /apatb_myproject_top/LENGTH_w40_V -into $tb_portdepth_group -radix hex
add_wave /apatb_myproject_top/LENGTH_w45_V -into $tb_portdepth_group -radix hex
add_wave /apatb_myproject_top/LENGTH_w49_V -into $tb_portdepth_group -radix hex
set tbcoutputgroup [add_wave_group "C Outputs" -into $testbenchgroup]
set tb_const_size_out_1_group [add_wave_group const_size_out_1(wire) -into $tbcoutputgroup]
add_wave /apatb_myproject_top/const_size_out_1_ap_vld -into $tb_const_size_out_1_group -color #ffff00 -radix hex
add_wave /apatb_myproject_top/const_size_out_1 -into $tb_const_size_out_1_group -radix hex
set tb_const_size_in_1_group [add_wave_group const_size_in_1(wire) -into $tbcoutputgroup]
add_wave /apatb_myproject_top/const_size_in_1_ap_vld -into $tb_const_size_in_1_group -color #ffff00 -radix hex
add_wave /apatb_myproject_top/const_size_in_1 -into $tb_const_size_in_1_group -radix hex
set tb_layer55_out_group [add_wave_group layer55_out(axis) -into $tbcoutputgroup]
add_wave /apatb_myproject_top/layer55_out_V_V_TREADY -into $tb_layer55_out_group -color #ffff00 -radix hex
add_wave /apatb_myproject_top/layer55_out_V_V_TVALID -into $tb_layer55_out_group -color #ffff00 -radix hex
add_wave /apatb_myproject_top/layer55_out_V_V_TDATA -into $tb_layer55_out_group -radix hex
set tbcinputgroup [add_wave_group "C Inputs" -into $testbenchgroup]
set tb_w49_group [add_wave_group w49(memory) -into $tbcinputgroup]
add_wave /apatb_myproject_top/w49_V_we1 -into $tb_w49_group -color #ffff00 -radix hex
add_wave /apatb_myproject_top/w49_V_q1 -into $tb_w49_group -radix hex
add_wave /apatb_myproject_top/w49_V_d1 -into $tb_w49_group -radix hex
add_wave /apatb_myproject_top/w49_V_ce1 -into $tb_w49_group -color #ffff00 -radix hex
add_wave /apatb_myproject_top/w49_V_address1 -into $tb_w49_group -radix hex
add_wave /apatb_myproject_top/w49_V_we0 -into $tb_w49_group -color #ffff00 -radix hex
add_wave /apatb_myproject_top/w49_V_q0 -into $tb_w49_group -radix hex
add_wave /apatb_myproject_top/w49_V_d0 -into $tb_w49_group -radix hex
add_wave /apatb_myproject_top/w49_V_ce0 -into $tb_w49_group -color #ffff00 -radix hex
add_wave /apatb_myproject_top/w49_V_address0 -into $tb_w49_group -radix hex
set tb_w45_group [add_wave_group w45(memory) -into $tbcinputgroup]
add_wave /apatb_myproject_top/w45_V_we1 -into $tb_w45_group -color #ffff00 -radix hex
add_wave /apatb_myproject_top/w45_V_q1 -into $tb_w45_group -radix hex
add_wave /apatb_myproject_top/w45_V_d1 -into $tb_w45_group -radix hex
add_wave /apatb_myproject_top/w45_V_ce1 -into $tb_w45_group -color #ffff00 -radix hex
add_wave /apatb_myproject_top/w45_V_address1 -into $tb_w45_group -radix hex
add_wave /apatb_myproject_top/w45_V_we0 -into $tb_w45_group -color #ffff00 -radix hex
add_wave /apatb_myproject_top/w45_V_q0 -into $tb_w45_group -radix hex
add_wave /apatb_myproject_top/w45_V_d0 -into $tb_w45_group -radix hex
add_wave /apatb_myproject_top/w45_V_ce0 -into $tb_w45_group -color #ffff00 -radix hex
add_wave /apatb_myproject_top/w45_V_address0 -into $tb_w45_group -radix hex
set tb_w40_group [add_wave_group w40(memory) -into $tbcinputgroup]
add_wave /apatb_myproject_top/w40_V_we1 -into $tb_w40_group -color #ffff00 -radix hex
add_wave /apatb_myproject_top/w40_V_q1 -into $tb_w40_group -radix hex
add_wave /apatb_myproject_top/w40_V_d1 -into $tb_w40_group -radix hex
add_wave /apatb_myproject_top/w40_V_ce1 -into $tb_w40_group -color #ffff00 -radix hex
add_wave /apatb_myproject_top/w40_V_address1 -into $tb_w40_group -radix hex
add_wave /apatb_myproject_top/w40_V_we0 -into $tb_w40_group -color #ffff00 -radix hex
add_wave /apatb_myproject_top/w40_V_q0 -into $tb_w40_group -radix hex
add_wave /apatb_myproject_top/w40_V_d0 -into $tb_w40_group -radix hex
add_wave /apatb_myproject_top/w40_V_ce0 -into $tb_w40_group -color #ffff00 -radix hex
add_wave /apatb_myproject_top/w40_V_address0 -into $tb_w40_group -radix hex
set tb_w36_group [add_wave_group w36(memory) -into $tbcinputgroup]
add_wave /apatb_myproject_top/w36_V_we1 -into $tb_w36_group -color #ffff00 -radix hex
add_wave /apatb_myproject_top/w36_V_q1 -into $tb_w36_group -radix hex
add_wave /apatb_myproject_top/w36_V_d1 -into $tb_w36_group -radix hex
add_wave /apatb_myproject_top/w36_V_ce1 -into $tb_w36_group -color #ffff00 -radix hex
add_wave /apatb_myproject_top/w36_V_address1 -into $tb_w36_group -radix hex
add_wave /apatb_myproject_top/w36_V_we0 -into $tb_w36_group -color #ffff00 -radix hex
add_wave /apatb_myproject_top/w36_V_q0 -into $tb_w36_group -radix hex
add_wave /apatb_myproject_top/w36_V_d0 -into $tb_w36_group -radix hex
add_wave /apatb_myproject_top/w36_V_ce0 -into $tb_w36_group -color #ffff00 -radix hex
add_wave /apatb_myproject_top/w36_V_address0 -into $tb_w36_group -radix hex
set tb_w31_group [add_wave_group w31(memory) -into $tbcinputgroup]
add_wave /apatb_myproject_top/w31_V_we1 -into $tb_w31_group -color #ffff00 -radix hex
add_wave /apatb_myproject_top/w31_V_q1 -into $tb_w31_group -radix hex
add_wave /apatb_myproject_top/w31_V_d1 -into $tb_w31_group -radix hex
add_wave /apatb_myproject_top/w31_V_ce1 -into $tb_w31_group -color #ffff00 -radix hex
add_wave /apatb_myproject_top/w31_V_address1 -into $tb_w31_group -radix hex
add_wave /apatb_myproject_top/w31_V_we0 -into $tb_w31_group -color #ffff00 -radix hex
add_wave /apatb_myproject_top/w31_V_q0 -into $tb_w31_group -radix hex
add_wave /apatb_myproject_top/w31_V_d0 -into $tb_w31_group -radix hex
add_wave /apatb_myproject_top/w31_V_ce0 -into $tb_w31_group -color #ffff00 -radix hex
add_wave /apatb_myproject_top/w31_V_address0 -into $tb_w31_group -radix hex
set tb_w27_group [add_wave_group w27(memory) -into $tbcinputgroup]
add_wave /apatb_myproject_top/w27_V_we1 -into $tb_w27_group -color #ffff00 -radix hex
add_wave /apatb_myproject_top/w27_V_q1 -into $tb_w27_group -radix hex
add_wave /apatb_myproject_top/w27_V_d1 -into $tb_w27_group -radix hex
add_wave /apatb_myproject_top/w27_V_ce1 -into $tb_w27_group -color #ffff00 -radix hex
add_wave /apatb_myproject_top/w27_V_address1 -into $tb_w27_group -radix hex
add_wave /apatb_myproject_top/w27_V_we0 -into $tb_w27_group -color #ffff00 -radix hex
add_wave /apatb_myproject_top/w27_V_q0 -into $tb_w27_group -radix hex
add_wave /apatb_myproject_top/w27_V_d0 -into $tb_w27_group -radix hex
add_wave /apatb_myproject_top/w27_V_ce0 -into $tb_w27_group -color #ffff00 -radix hex
add_wave /apatb_myproject_top/w27_V_address0 -into $tb_w27_group -radix hex
set tb_w22_group [add_wave_group w22(memory) -into $tbcinputgroup]
add_wave /apatb_myproject_top/w22_V_we1 -into $tb_w22_group -color #ffff00 -radix hex
add_wave /apatb_myproject_top/w22_V_q1 -into $tb_w22_group -radix hex
add_wave /apatb_myproject_top/w22_V_d1 -into $tb_w22_group -radix hex
add_wave /apatb_myproject_top/w22_V_ce1 -into $tb_w22_group -color #ffff00 -radix hex
add_wave /apatb_myproject_top/w22_V_address1 -into $tb_w22_group -radix hex
add_wave /apatb_myproject_top/w22_V_we0 -into $tb_w22_group -color #ffff00 -radix hex
add_wave /apatb_myproject_top/w22_V_q0 -into $tb_w22_group -radix hex
add_wave /apatb_myproject_top/w22_V_d0 -into $tb_w22_group -radix hex
add_wave /apatb_myproject_top/w22_V_ce0 -into $tb_w22_group -color #ffff00 -radix hex
add_wave /apatb_myproject_top/w22_V_address0 -into $tb_w22_group -radix hex
set tb_w18_group [add_wave_group w18(memory) -into $tbcinputgroup]
add_wave /apatb_myproject_top/w18_V_we1 -into $tb_w18_group -color #ffff00 -radix hex
add_wave /apatb_myproject_top/w18_V_q1 -into $tb_w18_group -radix hex
add_wave /apatb_myproject_top/w18_V_d1 -into $tb_w18_group -radix hex
add_wave /apatb_myproject_top/w18_V_ce1 -into $tb_w18_group -color #ffff00 -radix hex
add_wave /apatb_myproject_top/w18_V_address1 -into $tb_w18_group -radix hex
add_wave /apatb_myproject_top/w18_V_we0 -into $tb_w18_group -color #ffff00 -radix hex
add_wave /apatb_myproject_top/w18_V_q0 -into $tb_w18_group -radix hex
add_wave /apatb_myproject_top/w18_V_d0 -into $tb_w18_group -radix hex
add_wave /apatb_myproject_top/w18_V_ce0 -into $tb_w18_group -color #ffff00 -radix hex
add_wave /apatb_myproject_top/w18_V_address0 -into $tb_w18_group -radix hex
set tb_w13_group [add_wave_group w13(memory) -into $tbcinputgroup]
add_wave /apatb_myproject_top/w13_V_we1 -into $tb_w13_group -color #ffff00 -radix hex
add_wave /apatb_myproject_top/w13_V_q1 -into $tb_w13_group -radix hex
add_wave /apatb_myproject_top/w13_V_d1 -into $tb_w13_group -radix hex
add_wave /apatb_myproject_top/w13_V_ce1 -into $tb_w13_group -color #ffff00 -radix hex
add_wave /apatb_myproject_top/w13_V_address1 -into $tb_w13_group -radix hex
add_wave /apatb_myproject_top/w13_V_we0 -into $tb_w13_group -color #ffff00 -radix hex
add_wave /apatb_myproject_top/w13_V_q0 -into $tb_w13_group -radix hex
add_wave /apatb_myproject_top/w13_V_d0 -into $tb_w13_group -radix hex
add_wave /apatb_myproject_top/w13_V_ce0 -into $tb_w13_group -color #ffff00 -radix hex
add_wave /apatb_myproject_top/w13_V_address0 -into $tb_w13_group -radix hex
set tb_em_barrel_group [add_wave_group em_barrel(axis) -into $tbcinputgroup]
add_wave /apatb_myproject_top/em_barrel_V_V_TREADY -into $tb_em_barrel_group -color #ffff00 -radix hex
add_wave /apatb_myproject_top/em_barrel_V_V_TVALID -into $tb_em_barrel_group -color #ffff00 -radix hex
add_wave /apatb_myproject_top/em_barrel_V_V_TDATA -into $tb_em_barrel_group -radix hex
save_wave_config myproject.wcfg
run all
quit

