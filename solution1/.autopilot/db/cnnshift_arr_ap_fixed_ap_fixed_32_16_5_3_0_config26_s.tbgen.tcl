set moduleName cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config26_s
set isTopModule 0
set isTaskLevelControl 1
set isCombinational 0
set isDatapathOnly 0
set isFreeRunPipelineModule 0
set isPipelined 0
set pipeline_type function
set FunctionProtocol ap_ctrl_hs
set isOneStateSeq 1
set ProfileFlag 0
set StallSigGenFlag 0
set isEnableWaveformDebug 1
set C_modelName {cnnshift_arr<ap_fixed,ap_fixed<32,16,5,3,0>,config26>}
set C_modelType { int 8192 }
set C_modelArgList {
	{ data_0_V_read int 32 regular  }
	{ data_1_V_read int 32 regular  }
	{ data_2_V_read int 32 regular  }
	{ data_3_V_read int 32 regular  }
	{ data_4_V_read int 32 regular  }
	{ data_5_V_read int 32 regular  }
	{ data_6_V_read int 32 regular  }
	{ data_7_V_read int 32 regular  }
	{ data_8_V_read int 32 regular  }
	{ data_9_V_read int 32 regular  }
	{ data_10_V_read int 32 regular  }
	{ data_11_V_read int 32 regular  }
	{ data_12_V_read int 32 regular  }
	{ data_13_V_read int 32 regular  }
	{ data_14_V_read int 32 regular  }
	{ data_15_V_read int 32 regular  }
	{ data_16_V_read int 32 regular  }
	{ data_17_V_read int 32 regular  }
	{ data_18_V_read int 32 regular  }
	{ data_19_V_read int 32 regular  }
	{ data_20_V_read int 32 regular  }
	{ data_21_V_read int 32 regular  }
	{ data_22_V_read int 32 regular  }
	{ data_23_V_read int 32 regular  }
	{ data_24_V_read int 32 regular  }
	{ data_25_V_read int 32 regular  }
	{ data_26_V_read int 32 regular  }
	{ data_27_V_read int 32 regular  }
	{ data_28_V_read int 32 regular  }
	{ data_29_V_read int 32 regular  }
	{ data_30_V_read int 32 regular  }
	{ data_31_V_read int 32 regular  }
	{ data_32_V_read int 32 regular  }
	{ data_33_V_read int 32 regular  }
	{ data_34_V_read int 32 regular  }
	{ data_35_V_read int 32 regular  }
	{ data_36_V_read int 32 regular  }
	{ data_37_V_read int 32 regular  }
	{ data_38_V_read int 32 regular  }
	{ data_39_V_read int 32 regular  }
	{ data_40_V_read int 32 regular  }
	{ data_41_V_read int 32 regular  }
	{ data_42_V_read int 32 regular  }
	{ data_43_V_read int 32 regular  }
	{ data_44_V_read int 32 regular  }
	{ data_45_V_read int 32 regular  }
	{ data_46_V_read int 32 regular  }
	{ data_47_V_read int 32 regular  }
	{ data_48_V_read int 32 regular  }
	{ data_49_V_read int 32 regular  }
	{ data_50_V_read int 32 regular  }
	{ data_51_V_read int 32 regular  }
	{ data_52_V_read int 32 regular  }
	{ data_53_V_read int 32 regular  }
	{ data_54_V_read int 32 regular  }
	{ data_55_V_read int 32 regular  }
	{ data_56_V_read int 32 regular  }
	{ data_57_V_read int 32 regular  }
	{ data_58_V_read int 32 regular  }
	{ data_59_V_read int 32 regular  }
	{ data_60_V_read int 32 regular  }
	{ data_61_V_read int 32 regular  }
	{ data_62_V_read int 32 regular  }
	{ data_63_V_read int 32 regular  }
	{ output_V_read int 8192 regular  }
}
set C_modelArgMapList {[ 
	{ "Name" : "data_0_V_read", "interface" : "wire", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "data_1_V_read", "interface" : "wire", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "data_2_V_read", "interface" : "wire", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "data_3_V_read", "interface" : "wire", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "data_4_V_read", "interface" : "wire", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "data_5_V_read", "interface" : "wire", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "data_6_V_read", "interface" : "wire", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "data_7_V_read", "interface" : "wire", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "data_8_V_read", "interface" : "wire", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "data_9_V_read", "interface" : "wire", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "data_10_V_read", "interface" : "wire", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "data_11_V_read", "interface" : "wire", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "data_12_V_read", "interface" : "wire", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "data_13_V_read", "interface" : "wire", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "data_14_V_read", "interface" : "wire", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "data_15_V_read", "interface" : "wire", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "data_16_V_read", "interface" : "wire", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "data_17_V_read", "interface" : "wire", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "data_18_V_read", "interface" : "wire", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "data_19_V_read", "interface" : "wire", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "data_20_V_read", "interface" : "wire", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "data_21_V_read", "interface" : "wire", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "data_22_V_read", "interface" : "wire", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "data_23_V_read", "interface" : "wire", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "data_24_V_read", "interface" : "wire", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "data_25_V_read", "interface" : "wire", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "data_26_V_read", "interface" : "wire", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "data_27_V_read", "interface" : "wire", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "data_28_V_read", "interface" : "wire", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "data_29_V_read", "interface" : "wire", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "data_30_V_read", "interface" : "wire", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "data_31_V_read", "interface" : "wire", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "data_32_V_read", "interface" : "wire", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "data_33_V_read", "interface" : "wire", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "data_34_V_read", "interface" : "wire", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "data_35_V_read", "interface" : "wire", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "data_36_V_read", "interface" : "wire", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "data_37_V_read", "interface" : "wire", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "data_38_V_read", "interface" : "wire", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "data_39_V_read", "interface" : "wire", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "data_40_V_read", "interface" : "wire", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "data_41_V_read", "interface" : "wire", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "data_42_V_read", "interface" : "wire", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "data_43_V_read", "interface" : "wire", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "data_44_V_read", "interface" : "wire", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "data_45_V_read", "interface" : "wire", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "data_46_V_read", "interface" : "wire", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "data_47_V_read", "interface" : "wire", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "data_48_V_read", "interface" : "wire", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "data_49_V_read", "interface" : "wire", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "data_50_V_read", "interface" : "wire", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "data_51_V_read", "interface" : "wire", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "data_52_V_read", "interface" : "wire", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "data_53_V_read", "interface" : "wire", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "data_54_V_read", "interface" : "wire", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "data_55_V_read", "interface" : "wire", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "data_56_V_read", "interface" : "wire", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "data_57_V_read", "interface" : "wire", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "data_58_V_read", "interface" : "wire", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "data_59_V_read", "interface" : "wire", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "data_60_V_read", "interface" : "wire", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "data_61_V_read", "interface" : "wire", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "data_62_V_read", "interface" : "wire", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "data_63_V_read", "interface" : "wire", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "output_V_read", "interface" : "wire", "bitwidth" : 8192, "direction" : "READONLY"} , 
 	{ "Name" : "ap_return", "interface" : "wire", "bitwidth" : 8192} ]}
# RTL Port declarations: 
set portNum 72
set portList { 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst sc_in sc_logic 1 reset -1 active_high_sync } 
	{ ap_start sc_in sc_logic 1 start -1 } 
	{ ap_done sc_out sc_logic 1 predone -1 } 
	{ ap_idle sc_out sc_logic 1 done -1 } 
	{ ap_ready sc_out sc_logic 1 ready -1 } 
	{ data_0_V_read sc_in sc_lv 32 signal 0 } 
	{ data_1_V_read sc_in sc_lv 32 signal 1 } 
	{ data_2_V_read sc_in sc_lv 32 signal 2 } 
	{ data_3_V_read sc_in sc_lv 32 signal 3 } 
	{ data_4_V_read sc_in sc_lv 32 signal 4 } 
	{ data_5_V_read sc_in sc_lv 32 signal 5 } 
	{ data_6_V_read sc_in sc_lv 32 signal 6 } 
	{ data_7_V_read sc_in sc_lv 32 signal 7 } 
	{ data_8_V_read sc_in sc_lv 32 signal 8 } 
	{ data_9_V_read sc_in sc_lv 32 signal 9 } 
	{ data_10_V_read sc_in sc_lv 32 signal 10 } 
	{ data_11_V_read sc_in sc_lv 32 signal 11 } 
	{ data_12_V_read sc_in sc_lv 32 signal 12 } 
	{ data_13_V_read sc_in sc_lv 32 signal 13 } 
	{ data_14_V_read sc_in sc_lv 32 signal 14 } 
	{ data_15_V_read sc_in sc_lv 32 signal 15 } 
	{ data_16_V_read sc_in sc_lv 32 signal 16 } 
	{ data_17_V_read sc_in sc_lv 32 signal 17 } 
	{ data_18_V_read sc_in sc_lv 32 signal 18 } 
	{ data_19_V_read sc_in sc_lv 32 signal 19 } 
	{ data_20_V_read sc_in sc_lv 32 signal 20 } 
	{ data_21_V_read sc_in sc_lv 32 signal 21 } 
	{ data_22_V_read sc_in sc_lv 32 signal 22 } 
	{ data_23_V_read sc_in sc_lv 32 signal 23 } 
	{ data_24_V_read sc_in sc_lv 32 signal 24 } 
	{ data_25_V_read sc_in sc_lv 32 signal 25 } 
	{ data_26_V_read sc_in sc_lv 32 signal 26 } 
	{ data_27_V_read sc_in sc_lv 32 signal 27 } 
	{ data_28_V_read sc_in sc_lv 32 signal 28 } 
	{ data_29_V_read sc_in sc_lv 32 signal 29 } 
	{ data_30_V_read sc_in sc_lv 32 signal 30 } 
	{ data_31_V_read sc_in sc_lv 32 signal 31 } 
	{ data_32_V_read sc_in sc_lv 32 signal 32 } 
	{ data_33_V_read sc_in sc_lv 32 signal 33 } 
	{ data_34_V_read sc_in sc_lv 32 signal 34 } 
	{ data_35_V_read sc_in sc_lv 32 signal 35 } 
	{ data_36_V_read sc_in sc_lv 32 signal 36 } 
	{ data_37_V_read sc_in sc_lv 32 signal 37 } 
	{ data_38_V_read sc_in sc_lv 32 signal 38 } 
	{ data_39_V_read sc_in sc_lv 32 signal 39 } 
	{ data_40_V_read sc_in sc_lv 32 signal 40 } 
	{ data_41_V_read sc_in sc_lv 32 signal 41 } 
	{ data_42_V_read sc_in sc_lv 32 signal 42 } 
	{ data_43_V_read sc_in sc_lv 32 signal 43 } 
	{ data_44_V_read sc_in sc_lv 32 signal 44 } 
	{ data_45_V_read sc_in sc_lv 32 signal 45 } 
	{ data_46_V_read sc_in sc_lv 32 signal 46 } 
	{ data_47_V_read sc_in sc_lv 32 signal 47 } 
	{ data_48_V_read sc_in sc_lv 32 signal 48 } 
	{ data_49_V_read sc_in sc_lv 32 signal 49 } 
	{ data_50_V_read sc_in sc_lv 32 signal 50 } 
	{ data_51_V_read sc_in sc_lv 32 signal 51 } 
	{ data_52_V_read sc_in sc_lv 32 signal 52 } 
	{ data_53_V_read sc_in sc_lv 32 signal 53 } 
	{ data_54_V_read sc_in sc_lv 32 signal 54 } 
	{ data_55_V_read sc_in sc_lv 32 signal 55 } 
	{ data_56_V_read sc_in sc_lv 32 signal 56 } 
	{ data_57_V_read sc_in sc_lv 32 signal 57 } 
	{ data_58_V_read sc_in sc_lv 32 signal 58 } 
	{ data_59_V_read sc_in sc_lv 32 signal 59 } 
	{ data_60_V_read sc_in sc_lv 32 signal 60 } 
	{ data_61_V_read sc_in sc_lv 32 signal 61 } 
	{ data_62_V_read sc_in sc_lv 32 signal 62 } 
	{ data_63_V_read sc_in sc_lv 32 signal 63 } 
	{ output_V_read sc_in sc_lv 8192 signal 64 } 
	{ ap_return sc_out sc_lv 8192 signal -1 } 
}
set NewPortList {[ 
	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst", "role": "default" }} , 
 	{ "name": "ap_start", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "start", "bundle":{"name": "ap_start", "role": "default" }} , 
 	{ "name": "ap_done", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "predone", "bundle":{"name": "ap_done", "role": "default" }} , 
 	{ "name": "ap_idle", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "done", "bundle":{"name": "ap_idle", "role": "default" }} , 
 	{ "name": "ap_ready", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "ready", "bundle":{"name": "ap_ready", "role": "default" }} , 
 	{ "name": "data_0_V_read", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "data_0_V_read", "role": "default" }} , 
 	{ "name": "data_1_V_read", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "data_1_V_read", "role": "default" }} , 
 	{ "name": "data_2_V_read", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "data_2_V_read", "role": "default" }} , 
 	{ "name": "data_3_V_read", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "data_3_V_read", "role": "default" }} , 
 	{ "name": "data_4_V_read", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "data_4_V_read", "role": "default" }} , 
 	{ "name": "data_5_V_read", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "data_5_V_read", "role": "default" }} , 
 	{ "name": "data_6_V_read", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "data_6_V_read", "role": "default" }} , 
 	{ "name": "data_7_V_read", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "data_7_V_read", "role": "default" }} , 
 	{ "name": "data_8_V_read", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "data_8_V_read", "role": "default" }} , 
 	{ "name": "data_9_V_read", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "data_9_V_read", "role": "default" }} , 
 	{ "name": "data_10_V_read", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "data_10_V_read", "role": "default" }} , 
 	{ "name": "data_11_V_read", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "data_11_V_read", "role": "default" }} , 
 	{ "name": "data_12_V_read", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "data_12_V_read", "role": "default" }} , 
 	{ "name": "data_13_V_read", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "data_13_V_read", "role": "default" }} , 
 	{ "name": "data_14_V_read", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "data_14_V_read", "role": "default" }} , 
 	{ "name": "data_15_V_read", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "data_15_V_read", "role": "default" }} , 
 	{ "name": "data_16_V_read", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "data_16_V_read", "role": "default" }} , 
 	{ "name": "data_17_V_read", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "data_17_V_read", "role": "default" }} , 
 	{ "name": "data_18_V_read", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "data_18_V_read", "role": "default" }} , 
 	{ "name": "data_19_V_read", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "data_19_V_read", "role": "default" }} , 
 	{ "name": "data_20_V_read", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "data_20_V_read", "role": "default" }} , 
 	{ "name": "data_21_V_read", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "data_21_V_read", "role": "default" }} , 
 	{ "name": "data_22_V_read", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "data_22_V_read", "role": "default" }} , 
 	{ "name": "data_23_V_read", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "data_23_V_read", "role": "default" }} , 
 	{ "name": "data_24_V_read", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "data_24_V_read", "role": "default" }} , 
 	{ "name": "data_25_V_read", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "data_25_V_read", "role": "default" }} , 
 	{ "name": "data_26_V_read", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "data_26_V_read", "role": "default" }} , 
 	{ "name": "data_27_V_read", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "data_27_V_read", "role": "default" }} , 
 	{ "name": "data_28_V_read", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "data_28_V_read", "role": "default" }} , 
 	{ "name": "data_29_V_read", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "data_29_V_read", "role": "default" }} , 
 	{ "name": "data_30_V_read", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "data_30_V_read", "role": "default" }} , 
 	{ "name": "data_31_V_read", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "data_31_V_read", "role": "default" }} , 
 	{ "name": "data_32_V_read", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "data_32_V_read", "role": "default" }} , 
 	{ "name": "data_33_V_read", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "data_33_V_read", "role": "default" }} , 
 	{ "name": "data_34_V_read", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "data_34_V_read", "role": "default" }} , 
 	{ "name": "data_35_V_read", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "data_35_V_read", "role": "default" }} , 
 	{ "name": "data_36_V_read", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "data_36_V_read", "role": "default" }} , 
 	{ "name": "data_37_V_read", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "data_37_V_read", "role": "default" }} , 
 	{ "name": "data_38_V_read", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "data_38_V_read", "role": "default" }} , 
 	{ "name": "data_39_V_read", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "data_39_V_read", "role": "default" }} , 
 	{ "name": "data_40_V_read", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "data_40_V_read", "role": "default" }} , 
 	{ "name": "data_41_V_read", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "data_41_V_read", "role": "default" }} , 
 	{ "name": "data_42_V_read", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "data_42_V_read", "role": "default" }} , 
 	{ "name": "data_43_V_read", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "data_43_V_read", "role": "default" }} , 
 	{ "name": "data_44_V_read", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "data_44_V_read", "role": "default" }} , 
 	{ "name": "data_45_V_read", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "data_45_V_read", "role": "default" }} , 
 	{ "name": "data_46_V_read", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "data_46_V_read", "role": "default" }} , 
 	{ "name": "data_47_V_read", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "data_47_V_read", "role": "default" }} , 
 	{ "name": "data_48_V_read", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "data_48_V_read", "role": "default" }} , 
 	{ "name": "data_49_V_read", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "data_49_V_read", "role": "default" }} , 
 	{ "name": "data_50_V_read", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "data_50_V_read", "role": "default" }} , 
 	{ "name": "data_51_V_read", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "data_51_V_read", "role": "default" }} , 
 	{ "name": "data_52_V_read", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "data_52_V_read", "role": "default" }} , 
 	{ "name": "data_53_V_read", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "data_53_V_read", "role": "default" }} , 
 	{ "name": "data_54_V_read", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "data_54_V_read", "role": "default" }} , 
 	{ "name": "data_55_V_read", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "data_55_V_read", "role": "default" }} , 
 	{ "name": "data_56_V_read", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "data_56_V_read", "role": "default" }} , 
 	{ "name": "data_57_V_read", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "data_57_V_read", "role": "default" }} , 
 	{ "name": "data_58_V_read", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "data_58_V_read", "role": "default" }} , 
 	{ "name": "data_59_V_read", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "data_59_V_read", "role": "default" }} , 
 	{ "name": "data_60_V_read", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "data_60_V_read", "role": "default" }} , 
 	{ "name": "data_61_V_read", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "data_61_V_read", "role": "default" }} , 
 	{ "name": "data_62_V_read", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "data_62_V_read", "role": "default" }} , 
 	{ "name": "data_63_V_read", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "data_63_V_read", "role": "default" }} , 
 	{ "name": "output_V_read", "direction": "in", "datatype": "sc_lv", "bitwidth":8192, "type": "signal", "bundle":{"name": "output_V_read", "role": "default" }} , 
 	{ "name": "ap_return", "direction": "out", "datatype": "sc_lv", "bitwidth":8192, "type": "signal", "bundle":{"name": "ap_return", "role": "default" }}  ]}

set RtlHierarchyInfo {[
	{"ID" : "0", "Level" : "0", "Path" : "`AUTOTB_DUT_INST", "Parent" : "", "Child" : ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59", "60", "61", "62", "63", "64"],
		"CDFG" : "cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config26_s",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "0", "ap_idle" : "1",
		"Pipeline" : "None", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "0", "EstimateLatencyMin" : "0", "EstimateLatencyMax" : "0",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "0",
		"HasNonBlockingOperation" : "0",
		"Port" : [
			{"Name" : "data_0_V_read", "Type" : "None", "Direction" : "I"},
			{"Name" : "data_1_V_read", "Type" : "None", "Direction" : "I"},
			{"Name" : "data_2_V_read", "Type" : "None", "Direction" : "I"},
			{"Name" : "data_3_V_read", "Type" : "None", "Direction" : "I"},
			{"Name" : "data_4_V_read", "Type" : "None", "Direction" : "I"},
			{"Name" : "data_5_V_read", "Type" : "None", "Direction" : "I"},
			{"Name" : "data_6_V_read", "Type" : "None", "Direction" : "I"},
			{"Name" : "data_7_V_read", "Type" : "None", "Direction" : "I"},
			{"Name" : "data_8_V_read", "Type" : "None", "Direction" : "I"},
			{"Name" : "data_9_V_read", "Type" : "None", "Direction" : "I"},
			{"Name" : "data_10_V_read", "Type" : "None", "Direction" : "I"},
			{"Name" : "data_11_V_read", "Type" : "None", "Direction" : "I"},
			{"Name" : "data_12_V_read", "Type" : "None", "Direction" : "I"},
			{"Name" : "data_13_V_read", "Type" : "None", "Direction" : "I"},
			{"Name" : "data_14_V_read", "Type" : "None", "Direction" : "I"},
			{"Name" : "data_15_V_read", "Type" : "None", "Direction" : "I"},
			{"Name" : "data_16_V_read", "Type" : "None", "Direction" : "I"},
			{"Name" : "data_17_V_read", "Type" : "None", "Direction" : "I"},
			{"Name" : "data_18_V_read", "Type" : "None", "Direction" : "I"},
			{"Name" : "data_19_V_read", "Type" : "None", "Direction" : "I"},
			{"Name" : "data_20_V_read", "Type" : "None", "Direction" : "I"},
			{"Name" : "data_21_V_read", "Type" : "None", "Direction" : "I"},
			{"Name" : "data_22_V_read", "Type" : "None", "Direction" : "I"},
			{"Name" : "data_23_V_read", "Type" : "None", "Direction" : "I"},
			{"Name" : "data_24_V_read", "Type" : "None", "Direction" : "I"},
			{"Name" : "data_25_V_read", "Type" : "None", "Direction" : "I"},
			{"Name" : "data_26_V_read", "Type" : "None", "Direction" : "I"},
			{"Name" : "data_27_V_read", "Type" : "None", "Direction" : "I"},
			{"Name" : "data_28_V_read", "Type" : "None", "Direction" : "I"},
			{"Name" : "data_29_V_read", "Type" : "None", "Direction" : "I"},
			{"Name" : "data_30_V_read", "Type" : "None", "Direction" : "I"},
			{"Name" : "data_31_V_read", "Type" : "None", "Direction" : "I"},
			{"Name" : "data_32_V_read", "Type" : "None", "Direction" : "I"},
			{"Name" : "data_33_V_read", "Type" : "None", "Direction" : "I"},
			{"Name" : "data_34_V_read", "Type" : "None", "Direction" : "I"},
			{"Name" : "data_35_V_read", "Type" : "None", "Direction" : "I"},
			{"Name" : "data_36_V_read", "Type" : "None", "Direction" : "I"},
			{"Name" : "data_37_V_read", "Type" : "None", "Direction" : "I"},
			{"Name" : "data_38_V_read", "Type" : "None", "Direction" : "I"},
			{"Name" : "data_39_V_read", "Type" : "None", "Direction" : "I"},
			{"Name" : "data_40_V_read", "Type" : "None", "Direction" : "I"},
			{"Name" : "data_41_V_read", "Type" : "None", "Direction" : "I"},
			{"Name" : "data_42_V_read", "Type" : "None", "Direction" : "I"},
			{"Name" : "data_43_V_read", "Type" : "None", "Direction" : "I"},
			{"Name" : "data_44_V_read", "Type" : "None", "Direction" : "I"},
			{"Name" : "data_45_V_read", "Type" : "None", "Direction" : "I"},
			{"Name" : "data_46_V_read", "Type" : "None", "Direction" : "I"},
			{"Name" : "data_47_V_read", "Type" : "None", "Direction" : "I"},
			{"Name" : "data_48_V_read", "Type" : "None", "Direction" : "I"},
			{"Name" : "data_49_V_read", "Type" : "None", "Direction" : "I"},
			{"Name" : "data_50_V_read", "Type" : "None", "Direction" : "I"},
			{"Name" : "data_51_V_read", "Type" : "None", "Direction" : "I"},
			{"Name" : "data_52_V_read", "Type" : "None", "Direction" : "I"},
			{"Name" : "data_53_V_read", "Type" : "None", "Direction" : "I"},
			{"Name" : "data_54_V_read", "Type" : "None", "Direction" : "I"},
			{"Name" : "data_55_V_read", "Type" : "None", "Direction" : "I"},
			{"Name" : "data_56_V_read", "Type" : "None", "Direction" : "I"},
			{"Name" : "data_57_V_read", "Type" : "None", "Direction" : "I"},
			{"Name" : "data_58_V_read", "Type" : "None", "Direction" : "I"},
			{"Name" : "data_59_V_read", "Type" : "None", "Direction" : "I"},
			{"Name" : "data_60_V_read", "Type" : "None", "Direction" : "I"},
			{"Name" : "data_61_V_read", "Type" : "None", "Direction" : "I"},
			{"Name" : "data_62_V_read", "Type" : "None", "Direction" : "I"},
			{"Name" : "data_63_V_read", "Type" : "None", "Direction" : "I"},
			{"Name" : "output_V_read", "Type" : "None", "Direction" : "I"},
			{"Name" : "layer_in_row_Array_V_7_0_0", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_7_0_1", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_7_0_2", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_7_0_3", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_7_0_4", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_7_0_5", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_7_0_6", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_7_0_7", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_7_0_8", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_7_0_9", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_7_0_10", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_7_0_11", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_7_0_12", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_7_0_13", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_7_0_14", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_7_0_15", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_7_0_16", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_7_0_17", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_7_0_18", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_7_0_19", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_7_0_20", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_7_0_21", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_7_0_22", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_7_0_23", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_7_0_24", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_7_0_25", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_7_0_26", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_7_0_27", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_7_0_28", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_7_0_29", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_7_0_30", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_7_0_31", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_7_0_32", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_7_0_33", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_7_0_34", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_7_0_35", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_7_0_36", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_7_0_37", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_7_0_38", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_7_0_39", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_7_0_40", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_7_0_41", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_7_0_42", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_7_0_43", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_7_0_44", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_7_0_45", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_7_0_46", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_7_0_47", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_7_0_48", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_7_0_49", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_7_0_50", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_7_0_51", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_7_0_52", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_7_0_53", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_7_0_54", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_7_0_55", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_7_0_56", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_7_0_57", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_7_0_58", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_7_0_59", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_7_0_60", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_7_0_61", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_7_0_62", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_7_0_63", "Type" : "Memory", "Direction" : "X"}]},
	{"ID" : "1", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.layer_in_row_Array_V_7_0_0_U", "Parent" : "0"},
	{"ID" : "2", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.layer_in_row_Array_V_7_0_1_U", "Parent" : "0"},
	{"ID" : "3", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.layer_in_row_Array_V_7_0_2_U", "Parent" : "0"},
	{"ID" : "4", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.layer_in_row_Array_V_7_0_3_U", "Parent" : "0"},
	{"ID" : "5", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.layer_in_row_Array_V_7_0_4_U", "Parent" : "0"},
	{"ID" : "6", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.layer_in_row_Array_V_7_0_5_U", "Parent" : "0"},
	{"ID" : "7", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.layer_in_row_Array_V_7_0_6_U", "Parent" : "0"},
	{"ID" : "8", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.layer_in_row_Array_V_7_0_7_U", "Parent" : "0"},
	{"ID" : "9", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.layer_in_row_Array_V_7_0_8_U", "Parent" : "0"},
	{"ID" : "10", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.layer_in_row_Array_V_7_0_9_U", "Parent" : "0"},
	{"ID" : "11", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.layer_in_row_Array_V_7_0_10_U", "Parent" : "0"},
	{"ID" : "12", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.layer_in_row_Array_V_7_0_11_U", "Parent" : "0"},
	{"ID" : "13", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.layer_in_row_Array_V_7_0_12_U", "Parent" : "0"},
	{"ID" : "14", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.layer_in_row_Array_V_7_0_13_U", "Parent" : "0"},
	{"ID" : "15", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.layer_in_row_Array_V_7_0_14_U", "Parent" : "0"},
	{"ID" : "16", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.layer_in_row_Array_V_7_0_15_U", "Parent" : "0"},
	{"ID" : "17", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.layer_in_row_Array_V_7_0_16_U", "Parent" : "0"},
	{"ID" : "18", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.layer_in_row_Array_V_7_0_17_U", "Parent" : "0"},
	{"ID" : "19", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.layer_in_row_Array_V_7_0_18_U", "Parent" : "0"},
	{"ID" : "20", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.layer_in_row_Array_V_7_0_19_U", "Parent" : "0"},
	{"ID" : "21", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.layer_in_row_Array_V_7_0_20_U", "Parent" : "0"},
	{"ID" : "22", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.layer_in_row_Array_V_7_0_21_U", "Parent" : "0"},
	{"ID" : "23", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.layer_in_row_Array_V_7_0_22_U", "Parent" : "0"},
	{"ID" : "24", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.layer_in_row_Array_V_7_0_23_U", "Parent" : "0"},
	{"ID" : "25", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.layer_in_row_Array_V_7_0_24_U", "Parent" : "0"},
	{"ID" : "26", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.layer_in_row_Array_V_7_0_25_U", "Parent" : "0"},
	{"ID" : "27", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.layer_in_row_Array_V_7_0_26_U", "Parent" : "0"},
	{"ID" : "28", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.layer_in_row_Array_V_7_0_27_U", "Parent" : "0"},
	{"ID" : "29", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.layer_in_row_Array_V_7_0_28_U", "Parent" : "0"},
	{"ID" : "30", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.layer_in_row_Array_V_7_0_29_U", "Parent" : "0"},
	{"ID" : "31", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.layer_in_row_Array_V_7_0_30_U", "Parent" : "0"},
	{"ID" : "32", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.layer_in_row_Array_V_7_0_31_U", "Parent" : "0"},
	{"ID" : "33", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.layer_in_row_Array_V_7_0_32_U", "Parent" : "0"},
	{"ID" : "34", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.layer_in_row_Array_V_7_0_33_U", "Parent" : "0"},
	{"ID" : "35", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.layer_in_row_Array_V_7_0_34_U", "Parent" : "0"},
	{"ID" : "36", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.layer_in_row_Array_V_7_0_35_U", "Parent" : "0"},
	{"ID" : "37", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.layer_in_row_Array_V_7_0_36_U", "Parent" : "0"},
	{"ID" : "38", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.layer_in_row_Array_V_7_0_37_U", "Parent" : "0"},
	{"ID" : "39", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.layer_in_row_Array_V_7_0_38_U", "Parent" : "0"},
	{"ID" : "40", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.layer_in_row_Array_V_7_0_39_U", "Parent" : "0"},
	{"ID" : "41", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.layer_in_row_Array_V_7_0_40_U", "Parent" : "0"},
	{"ID" : "42", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.layer_in_row_Array_V_7_0_41_U", "Parent" : "0"},
	{"ID" : "43", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.layer_in_row_Array_V_7_0_42_U", "Parent" : "0"},
	{"ID" : "44", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.layer_in_row_Array_V_7_0_43_U", "Parent" : "0"},
	{"ID" : "45", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.layer_in_row_Array_V_7_0_44_U", "Parent" : "0"},
	{"ID" : "46", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.layer_in_row_Array_V_7_0_45_U", "Parent" : "0"},
	{"ID" : "47", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.layer_in_row_Array_V_7_0_46_U", "Parent" : "0"},
	{"ID" : "48", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.layer_in_row_Array_V_7_0_47_U", "Parent" : "0"},
	{"ID" : "49", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.layer_in_row_Array_V_7_0_48_U", "Parent" : "0"},
	{"ID" : "50", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.layer_in_row_Array_V_7_0_49_U", "Parent" : "0"},
	{"ID" : "51", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.layer_in_row_Array_V_7_0_50_U", "Parent" : "0"},
	{"ID" : "52", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.layer_in_row_Array_V_7_0_51_U", "Parent" : "0"},
	{"ID" : "53", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.layer_in_row_Array_V_7_0_52_U", "Parent" : "0"},
	{"ID" : "54", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.layer_in_row_Array_V_7_0_53_U", "Parent" : "0"},
	{"ID" : "55", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.layer_in_row_Array_V_7_0_54_U", "Parent" : "0"},
	{"ID" : "56", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.layer_in_row_Array_V_7_0_55_U", "Parent" : "0"},
	{"ID" : "57", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.layer_in_row_Array_V_7_0_56_U", "Parent" : "0"},
	{"ID" : "58", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.layer_in_row_Array_V_7_0_57_U", "Parent" : "0"},
	{"ID" : "59", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.layer_in_row_Array_V_7_0_58_U", "Parent" : "0"},
	{"ID" : "60", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.layer_in_row_Array_V_7_0_59_U", "Parent" : "0"},
	{"ID" : "61", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.layer_in_row_Array_V_7_0_60_U", "Parent" : "0"},
	{"ID" : "62", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.layer_in_row_Array_V_7_0_61_U", "Parent" : "0"},
	{"ID" : "63", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.layer_in_row_Array_V_7_0_62_U", "Parent" : "0"},
	{"ID" : "64", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.layer_in_row_Array_V_7_0_63_U", "Parent" : "0"}]}


set ArgLastReadFirstWriteLatency {
	cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config26_s {
		data_0_V_read {Type I LastRead 0 FirstWrite -1}
		data_1_V_read {Type I LastRead 0 FirstWrite -1}
		data_2_V_read {Type I LastRead 0 FirstWrite -1}
		data_3_V_read {Type I LastRead 0 FirstWrite -1}
		data_4_V_read {Type I LastRead 0 FirstWrite -1}
		data_5_V_read {Type I LastRead 0 FirstWrite -1}
		data_6_V_read {Type I LastRead 0 FirstWrite -1}
		data_7_V_read {Type I LastRead 0 FirstWrite -1}
		data_8_V_read {Type I LastRead 0 FirstWrite -1}
		data_9_V_read {Type I LastRead 0 FirstWrite -1}
		data_10_V_read {Type I LastRead 0 FirstWrite -1}
		data_11_V_read {Type I LastRead 0 FirstWrite -1}
		data_12_V_read {Type I LastRead 0 FirstWrite -1}
		data_13_V_read {Type I LastRead 0 FirstWrite -1}
		data_14_V_read {Type I LastRead 0 FirstWrite -1}
		data_15_V_read {Type I LastRead 0 FirstWrite -1}
		data_16_V_read {Type I LastRead 0 FirstWrite -1}
		data_17_V_read {Type I LastRead 0 FirstWrite -1}
		data_18_V_read {Type I LastRead 0 FirstWrite -1}
		data_19_V_read {Type I LastRead 0 FirstWrite -1}
		data_20_V_read {Type I LastRead 0 FirstWrite -1}
		data_21_V_read {Type I LastRead 0 FirstWrite -1}
		data_22_V_read {Type I LastRead 0 FirstWrite -1}
		data_23_V_read {Type I LastRead 0 FirstWrite -1}
		data_24_V_read {Type I LastRead 0 FirstWrite -1}
		data_25_V_read {Type I LastRead 0 FirstWrite -1}
		data_26_V_read {Type I LastRead 0 FirstWrite -1}
		data_27_V_read {Type I LastRead 0 FirstWrite -1}
		data_28_V_read {Type I LastRead 0 FirstWrite -1}
		data_29_V_read {Type I LastRead 0 FirstWrite -1}
		data_30_V_read {Type I LastRead 0 FirstWrite -1}
		data_31_V_read {Type I LastRead 0 FirstWrite -1}
		data_32_V_read {Type I LastRead 0 FirstWrite -1}
		data_33_V_read {Type I LastRead 0 FirstWrite -1}
		data_34_V_read {Type I LastRead 0 FirstWrite -1}
		data_35_V_read {Type I LastRead 0 FirstWrite -1}
		data_36_V_read {Type I LastRead 0 FirstWrite -1}
		data_37_V_read {Type I LastRead 0 FirstWrite -1}
		data_38_V_read {Type I LastRead 0 FirstWrite -1}
		data_39_V_read {Type I LastRead 0 FirstWrite -1}
		data_40_V_read {Type I LastRead 0 FirstWrite -1}
		data_41_V_read {Type I LastRead 0 FirstWrite -1}
		data_42_V_read {Type I LastRead 0 FirstWrite -1}
		data_43_V_read {Type I LastRead 0 FirstWrite -1}
		data_44_V_read {Type I LastRead 0 FirstWrite -1}
		data_45_V_read {Type I LastRead 0 FirstWrite -1}
		data_46_V_read {Type I LastRead 0 FirstWrite -1}
		data_47_V_read {Type I LastRead 0 FirstWrite -1}
		data_48_V_read {Type I LastRead 0 FirstWrite -1}
		data_49_V_read {Type I LastRead 0 FirstWrite -1}
		data_50_V_read {Type I LastRead 0 FirstWrite -1}
		data_51_V_read {Type I LastRead 0 FirstWrite -1}
		data_52_V_read {Type I LastRead 0 FirstWrite -1}
		data_53_V_read {Type I LastRead 0 FirstWrite -1}
		data_54_V_read {Type I LastRead 0 FirstWrite -1}
		data_55_V_read {Type I LastRead 0 FirstWrite -1}
		data_56_V_read {Type I LastRead 0 FirstWrite -1}
		data_57_V_read {Type I LastRead 0 FirstWrite -1}
		data_58_V_read {Type I LastRead 0 FirstWrite -1}
		data_59_V_read {Type I LastRead 0 FirstWrite -1}
		data_60_V_read {Type I LastRead 0 FirstWrite -1}
		data_61_V_read {Type I LastRead 0 FirstWrite -1}
		data_62_V_read {Type I LastRead 0 FirstWrite -1}
		data_63_V_read {Type I LastRead 0 FirstWrite -1}
		output_V_read {Type I LastRead 0 FirstWrite -1}
		layer_in_row_Array_V_7_0_0 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_7_0_1 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_7_0_2 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_7_0_3 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_7_0_4 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_7_0_5 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_7_0_6 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_7_0_7 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_7_0_8 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_7_0_9 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_7_0_10 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_7_0_11 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_7_0_12 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_7_0_13 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_7_0_14 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_7_0_15 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_7_0_16 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_7_0_17 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_7_0_18 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_7_0_19 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_7_0_20 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_7_0_21 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_7_0_22 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_7_0_23 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_7_0_24 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_7_0_25 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_7_0_26 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_7_0_27 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_7_0_28 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_7_0_29 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_7_0_30 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_7_0_31 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_7_0_32 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_7_0_33 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_7_0_34 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_7_0_35 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_7_0_36 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_7_0_37 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_7_0_38 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_7_0_39 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_7_0_40 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_7_0_41 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_7_0_42 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_7_0_43 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_7_0_44 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_7_0_45 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_7_0_46 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_7_0_47 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_7_0_48 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_7_0_49 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_7_0_50 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_7_0_51 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_7_0_52 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_7_0_53 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_7_0_54 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_7_0_55 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_7_0_56 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_7_0_57 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_7_0_58 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_7_0_59 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_7_0_60 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_7_0_61 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_7_0_62 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_7_0_63 {Type X LastRead -1 FirstWrite -1}}}

set hasDtUnsupportedChannel 0

set PerformanceInfo {[
	{"Name" : "Latency", "Min" : "0", "Max" : "0"}
	, {"Name" : "Interval", "Min" : "1", "Max" : "1"}
]}

set PipelineEnableSignalInfo {[
]}

set Spec2ImplPortList { 
	data_0_V_read { ap_none {  { data_0_V_read in_data 0 32 } } }
	data_1_V_read { ap_none {  { data_1_V_read in_data 0 32 } } }
	data_2_V_read { ap_none {  { data_2_V_read in_data 0 32 } } }
	data_3_V_read { ap_none {  { data_3_V_read in_data 0 32 } } }
	data_4_V_read { ap_none {  { data_4_V_read in_data 0 32 } } }
	data_5_V_read { ap_none {  { data_5_V_read in_data 0 32 } } }
	data_6_V_read { ap_none {  { data_6_V_read in_data 0 32 } } }
	data_7_V_read { ap_none {  { data_7_V_read in_data 0 32 } } }
	data_8_V_read { ap_none {  { data_8_V_read in_data 0 32 } } }
	data_9_V_read { ap_none {  { data_9_V_read in_data 0 32 } } }
	data_10_V_read { ap_none {  { data_10_V_read in_data 0 32 } } }
	data_11_V_read { ap_none {  { data_11_V_read in_data 0 32 } } }
	data_12_V_read { ap_none {  { data_12_V_read in_data 0 32 } } }
	data_13_V_read { ap_none {  { data_13_V_read in_data 0 32 } } }
	data_14_V_read { ap_none {  { data_14_V_read in_data 0 32 } } }
	data_15_V_read { ap_none {  { data_15_V_read in_data 0 32 } } }
	data_16_V_read { ap_none {  { data_16_V_read in_data 0 32 } } }
	data_17_V_read { ap_none {  { data_17_V_read in_data 0 32 } } }
	data_18_V_read { ap_none {  { data_18_V_read in_data 0 32 } } }
	data_19_V_read { ap_none {  { data_19_V_read in_data 0 32 } } }
	data_20_V_read { ap_none {  { data_20_V_read in_data 0 32 } } }
	data_21_V_read { ap_none {  { data_21_V_read in_data 0 32 } } }
	data_22_V_read { ap_none {  { data_22_V_read in_data 0 32 } } }
	data_23_V_read { ap_none {  { data_23_V_read in_data 0 32 } } }
	data_24_V_read { ap_none {  { data_24_V_read in_data 0 32 } } }
	data_25_V_read { ap_none {  { data_25_V_read in_data 0 32 } } }
	data_26_V_read { ap_none {  { data_26_V_read in_data 0 32 } } }
	data_27_V_read { ap_none {  { data_27_V_read in_data 0 32 } } }
	data_28_V_read { ap_none {  { data_28_V_read in_data 0 32 } } }
	data_29_V_read { ap_none {  { data_29_V_read in_data 0 32 } } }
	data_30_V_read { ap_none {  { data_30_V_read in_data 0 32 } } }
	data_31_V_read { ap_none {  { data_31_V_read in_data 0 32 } } }
	data_32_V_read { ap_none {  { data_32_V_read in_data 0 32 } } }
	data_33_V_read { ap_none {  { data_33_V_read in_data 0 32 } } }
	data_34_V_read { ap_none {  { data_34_V_read in_data 0 32 } } }
	data_35_V_read { ap_none {  { data_35_V_read in_data 0 32 } } }
	data_36_V_read { ap_none {  { data_36_V_read in_data 0 32 } } }
	data_37_V_read { ap_none {  { data_37_V_read in_data 0 32 } } }
	data_38_V_read { ap_none {  { data_38_V_read in_data 0 32 } } }
	data_39_V_read { ap_none {  { data_39_V_read in_data 0 32 } } }
	data_40_V_read { ap_none {  { data_40_V_read in_data 0 32 } } }
	data_41_V_read { ap_none {  { data_41_V_read in_data 0 32 } } }
	data_42_V_read { ap_none {  { data_42_V_read in_data 0 32 } } }
	data_43_V_read { ap_none {  { data_43_V_read in_data 0 32 } } }
	data_44_V_read { ap_none {  { data_44_V_read in_data 0 32 } } }
	data_45_V_read { ap_none {  { data_45_V_read in_data 0 32 } } }
	data_46_V_read { ap_none {  { data_46_V_read in_data 0 32 } } }
	data_47_V_read { ap_none {  { data_47_V_read in_data 0 32 } } }
	data_48_V_read { ap_none {  { data_48_V_read in_data 0 32 } } }
	data_49_V_read { ap_none {  { data_49_V_read in_data 0 32 } } }
	data_50_V_read { ap_none {  { data_50_V_read in_data 0 32 } } }
	data_51_V_read { ap_none {  { data_51_V_read in_data 0 32 } } }
	data_52_V_read { ap_none {  { data_52_V_read in_data 0 32 } } }
	data_53_V_read { ap_none {  { data_53_V_read in_data 0 32 } } }
	data_54_V_read { ap_none {  { data_54_V_read in_data 0 32 } } }
	data_55_V_read { ap_none {  { data_55_V_read in_data 0 32 } } }
	data_56_V_read { ap_none {  { data_56_V_read in_data 0 32 } } }
	data_57_V_read { ap_none {  { data_57_V_read in_data 0 32 } } }
	data_58_V_read { ap_none {  { data_58_V_read in_data 0 32 } } }
	data_59_V_read { ap_none {  { data_59_V_read in_data 0 32 } } }
	data_60_V_read { ap_none {  { data_60_V_read in_data 0 32 } } }
	data_61_V_read { ap_none {  { data_61_V_read in_data 0 32 } } }
	data_62_V_read { ap_none {  { data_62_V_read in_data 0 32 } } }
	data_63_V_read { ap_none {  { data_63_V_read in_data 0 32 } } }
	output_V_read { ap_none {  { output_V_read in_data 0 8192 } } }
}
