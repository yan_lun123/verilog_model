`timescale 1 ns / 1 ps

module AESL_deadlock_detector (
    input reset,
    input clock);

    wire [9:0] proc_dep_vld_vec_0;
    reg [9:0] proc_dep_vld_vec_0_reg;
    wire [9:0] in_chan_dep_vld_vec_0;
    wire [399:0] in_chan_dep_data_vec_0;
    wire [9:0] token_in_vec_0;
    wire [9:0] out_chan_dep_vld_vec_0;
    wire [39:0] out_chan_dep_data_0;
    wire [9:0] token_out_vec_0;
    wire dl_detect_out_0;
    wire dep_chan_vld_1_0;
    wire [39:0] dep_chan_data_1_0;
    wire token_1_0;
    wire dep_chan_vld_11_0;
    wire [39:0] dep_chan_data_11_0;
    wire token_11_0;
    wire dep_chan_vld_15_0;
    wire [39:0] dep_chan_data_15_0;
    wire token_15_0;
    wire dep_chan_vld_18_0;
    wire [39:0] dep_chan_data_18_0;
    wire token_18_0;
    wire dep_chan_vld_22_0;
    wire [39:0] dep_chan_data_22_0;
    wire token_22_0;
    wire dep_chan_vld_25_0;
    wire [39:0] dep_chan_data_25_0;
    wire token_25_0;
    wire dep_chan_vld_29_0;
    wire [39:0] dep_chan_data_29_0;
    wire token_29_0;
    wire dep_chan_vld_32_0;
    wire [39:0] dep_chan_data_32_0;
    wire token_32_0;
    wire dep_chan_vld_34_0;
    wire [39:0] dep_chan_data_34_0;
    wire token_34_0;
    wire dep_chan_vld_36_0;
    wire [39:0] dep_chan_data_36_0;
    wire token_36_0;
    wire [10:0] proc_dep_vld_vec_1;
    reg [10:0] proc_dep_vld_vec_1_reg;
    wire [10:0] in_chan_dep_vld_vec_1;
    wire [439:0] in_chan_dep_data_vec_1;
    wire [10:0] token_in_vec_1;
    wire [10:0] out_chan_dep_vld_vec_1;
    wire [39:0] out_chan_dep_data_1;
    wire [10:0] token_out_vec_1;
    wire dl_detect_out_1;
    wire dep_chan_vld_0_1;
    wire [39:0] dep_chan_data_0_1;
    wire token_0_1;
    wire dep_chan_vld_2_1;
    wire [39:0] dep_chan_data_2_1;
    wire token_2_1;
    wire dep_chan_vld_11_1;
    wire [39:0] dep_chan_data_11_1;
    wire token_11_1;
    wire dep_chan_vld_15_1;
    wire [39:0] dep_chan_data_15_1;
    wire token_15_1;
    wire dep_chan_vld_18_1;
    wire [39:0] dep_chan_data_18_1;
    wire token_18_1;
    wire dep_chan_vld_22_1;
    wire [39:0] dep_chan_data_22_1;
    wire token_22_1;
    wire dep_chan_vld_25_1;
    wire [39:0] dep_chan_data_25_1;
    wire token_25_1;
    wire dep_chan_vld_29_1;
    wire [39:0] dep_chan_data_29_1;
    wire token_29_1;
    wire dep_chan_vld_32_1;
    wire [39:0] dep_chan_data_32_1;
    wire token_32_1;
    wire dep_chan_vld_34_1;
    wire [39:0] dep_chan_data_34_1;
    wire token_34_1;
    wire dep_chan_vld_36_1;
    wire [39:0] dep_chan_data_36_1;
    wire token_36_1;
    wire [1:0] proc_dep_vld_vec_2;
    reg [1:0] proc_dep_vld_vec_2_reg;
    wire [1:0] in_chan_dep_vld_vec_2;
    wire [79:0] in_chan_dep_data_vec_2;
    wire [1:0] token_in_vec_2;
    wire [1:0] out_chan_dep_vld_vec_2;
    wire [39:0] out_chan_dep_data_2;
    wire [1:0] token_out_vec_2;
    wire dl_detect_out_2;
    wire dep_chan_vld_1_2;
    wire [39:0] dep_chan_data_1_2;
    wire token_1_2;
    wire dep_chan_vld_3_2;
    wire [39:0] dep_chan_data_3_2;
    wire token_3_2;
    wire [1:0] proc_dep_vld_vec_3;
    reg [1:0] proc_dep_vld_vec_3_reg;
    wire [1:0] in_chan_dep_vld_vec_3;
    wire [79:0] in_chan_dep_data_vec_3;
    wire [1:0] token_in_vec_3;
    wire [1:0] out_chan_dep_vld_vec_3;
    wire [39:0] out_chan_dep_data_3;
    wire [1:0] token_out_vec_3;
    wire dl_detect_out_3;
    wire dep_chan_vld_2_3;
    wire [39:0] dep_chan_data_2_3;
    wire token_2_3;
    wire dep_chan_vld_4_3;
    wire [39:0] dep_chan_data_4_3;
    wire token_4_3;
    wire [1:0] proc_dep_vld_vec_4;
    reg [1:0] proc_dep_vld_vec_4_reg;
    wire [1:0] in_chan_dep_vld_vec_4;
    wire [79:0] in_chan_dep_data_vec_4;
    wire [1:0] token_in_vec_4;
    wire [1:0] out_chan_dep_vld_vec_4;
    wire [39:0] out_chan_dep_data_4;
    wire [1:0] token_out_vec_4;
    wire dl_detect_out_4;
    wire dep_chan_vld_3_4;
    wire [39:0] dep_chan_data_3_4;
    wire token_3_4;
    wire dep_chan_vld_5_4;
    wire [39:0] dep_chan_data_5_4;
    wire token_5_4;
    wire [1:0] proc_dep_vld_vec_5;
    reg [1:0] proc_dep_vld_vec_5_reg;
    wire [1:0] in_chan_dep_vld_vec_5;
    wire [79:0] in_chan_dep_data_vec_5;
    wire [1:0] token_in_vec_5;
    wire [1:0] out_chan_dep_vld_vec_5;
    wire [39:0] out_chan_dep_data_5;
    wire [1:0] token_out_vec_5;
    wire dl_detect_out_5;
    wire dep_chan_vld_4_5;
    wire [39:0] dep_chan_data_4_5;
    wire token_4_5;
    wire dep_chan_vld_6_5;
    wire [39:0] dep_chan_data_6_5;
    wire token_6_5;
    wire [1:0] proc_dep_vld_vec_6;
    reg [1:0] proc_dep_vld_vec_6_reg;
    wire [1:0] in_chan_dep_vld_vec_6;
    wire [79:0] in_chan_dep_data_vec_6;
    wire [1:0] token_in_vec_6;
    wire [1:0] out_chan_dep_vld_vec_6;
    wire [39:0] out_chan_dep_data_6;
    wire [1:0] token_out_vec_6;
    wire dl_detect_out_6;
    wire dep_chan_vld_5_6;
    wire [39:0] dep_chan_data_5_6;
    wire token_5_6;
    wire dep_chan_vld_7_6;
    wire [39:0] dep_chan_data_7_6;
    wire token_7_6;
    wire [1:0] proc_dep_vld_vec_7;
    reg [1:0] proc_dep_vld_vec_7_reg;
    wire [1:0] in_chan_dep_vld_vec_7;
    wire [79:0] in_chan_dep_data_vec_7;
    wire [1:0] token_in_vec_7;
    wire [1:0] out_chan_dep_vld_vec_7;
    wire [39:0] out_chan_dep_data_7;
    wire [1:0] token_out_vec_7;
    wire dl_detect_out_7;
    wire dep_chan_vld_6_7;
    wire [39:0] dep_chan_data_6_7;
    wire token_6_7;
    wire dep_chan_vld_8_7;
    wire [39:0] dep_chan_data_8_7;
    wire token_8_7;
    wire [1:0] proc_dep_vld_vec_8;
    reg [1:0] proc_dep_vld_vec_8_reg;
    wire [1:0] in_chan_dep_vld_vec_8;
    wire [79:0] in_chan_dep_data_vec_8;
    wire [1:0] token_in_vec_8;
    wire [1:0] out_chan_dep_vld_vec_8;
    wire [39:0] out_chan_dep_data_8;
    wire [1:0] token_out_vec_8;
    wire dl_detect_out_8;
    wire dep_chan_vld_7_8;
    wire [39:0] dep_chan_data_7_8;
    wire token_7_8;
    wire dep_chan_vld_9_8;
    wire [39:0] dep_chan_data_9_8;
    wire token_9_8;
    wire [1:0] proc_dep_vld_vec_9;
    reg [1:0] proc_dep_vld_vec_9_reg;
    wire [1:0] in_chan_dep_vld_vec_9;
    wire [79:0] in_chan_dep_data_vec_9;
    wire [1:0] token_in_vec_9;
    wire [1:0] out_chan_dep_vld_vec_9;
    wire [39:0] out_chan_dep_data_9;
    wire [1:0] token_out_vec_9;
    wire dl_detect_out_9;
    wire dep_chan_vld_8_9;
    wire [39:0] dep_chan_data_8_9;
    wire token_8_9;
    wire dep_chan_vld_10_9;
    wire [39:0] dep_chan_data_10_9;
    wire token_10_9;
    wire [1:0] proc_dep_vld_vec_10;
    reg [1:0] proc_dep_vld_vec_10_reg;
    wire [1:0] in_chan_dep_vld_vec_10;
    wire [79:0] in_chan_dep_data_vec_10;
    wire [1:0] token_in_vec_10;
    wire [1:0] out_chan_dep_vld_vec_10;
    wire [39:0] out_chan_dep_data_10;
    wire [1:0] token_out_vec_10;
    wire dl_detect_out_10;
    wire dep_chan_vld_9_10;
    wire [39:0] dep_chan_data_9_10;
    wire token_9_10;
    wire dep_chan_vld_11_10;
    wire [39:0] dep_chan_data_11_10;
    wire token_11_10;
    wire [11:0] proc_dep_vld_vec_11;
    reg [11:0] proc_dep_vld_vec_11_reg;
    wire [11:0] in_chan_dep_vld_vec_11;
    wire [479:0] in_chan_dep_data_vec_11;
    wire [11:0] token_in_vec_11;
    wire [11:0] out_chan_dep_vld_vec_11;
    wire [39:0] out_chan_dep_data_11;
    wire [11:0] token_out_vec_11;
    wire dl_detect_out_11;
    wire dep_chan_vld_0_11;
    wire [39:0] dep_chan_data_0_11;
    wire token_0_11;
    wire dep_chan_vld_1_11;
    wire [39:0] dep_chan_data_1_11;
    wire token_1_11;
    wire dep_chan_vld_10_11;
    wire [39:0] dep_chan_data_10_11;
    wire token_10_11;
    wire dep_chan_vld_12_11;
    wire [39:0] dep_chan_data_12_11;
    wire token_12_11;
    wire dep_chan_vld_15_11;
    wire [39:0] dep_chan_data_15_11;
    wire token_15_11;
    wire dep_chan_vld_18_11;
    wire [39:0] dep_chan_data_18_11;
    wire token_18_11;
    wire dep_chan_vld_22_11;
    wire [39:0] dep_chan_data_22_11;
    wire token_22_11;
    wire dep_chan_vld_25_11;
    wire [39:0] dep_chan_data_25_11;
    wire token_25_11;
    wire dep_chan_vld_29_11;
    wire [39:0] dep_chan_data_29_11;
    wire token_29_11;
    wire dep_chan_vld_32_11;
    wire [39:0] dep_chan_data_32_11;
    wire token_32_11;
    wire dep_chan_vld_34_11;
    wire [39:0] dep_chan_data_34_11;
    wire token_34_11;
    wire dep_chan_vld_36_11;
    wire [39:0] dep_chan_data_36_11;
    wire token_36_11;
    wire [1:0] proc_dep_vld_vec_12;
    reg [1:0] proc_dep_vld_vec_12_reg;
    wire [1:0] in_chan_dep_vld_vec_12;
    wire [79:0] in_chan_dep_data_vec_12;
    wire [1:0] token_in_vec_12;
    wire [1:0] out_chan_dep_vld_vec_12;
    wire [39:0] out_chan_dep_data_12;
    wire [1:0] token_out_vec_12;
    wire dl_detect_out_12;
    wire dep_chan_vld_11_12;
    wire [39:0] dep_chan_data_11_12;
    wire token_11_12;
    wire dep_chan_vld_13_12;
    wire [39:0] dep_chan_data_13_12;
    wire token_13_12;
    wire [1:0] proc_dep_vld_vec_13;
    reg [1:0] proc_dep_vld_vec_13_reg;
    wire [1:0] in_chan_dep_vld_vec_13;
    wire [79:0] in_chan_dep_data_vec_13;
    wire [1:0] token_in_vec_13;
    wire [1:0] out_chan_dep_vld_vec_13;
    wire [39:0] out_chan_dep_data_13;
    wire [1:0] token_out_vec_13;
    wire dl_detect_out_13;
    wire dep_chan_vld_12_13;
    wire [39:0] dep_chan_data_12_13;
    wire token_12_13;
    wire dep_chan_vld_14_13;
    wire [39:0] dep_chan_data_14_13;
    wire token_14_13;
    wire [1:0] proc_dep_vld_vec_14;
    reg [1:0] proc_dep_vld_vec_14_reg;
    wire [1:0] in_chan_dep_vld_vec_14;
    wire [79:0] in_chan_dep_data_vec_14;
    wire [1:0] token_in_vec_14;
    wire [1:0] out_chan_dep_vld_vec_14;
    wire [39:0] out_chan_dep_data_14;
    wire [1:0] token_out_vec_14;
    wire dl_detect_out_14;
    wire dep_chan_vld_13_14;
    wire [39:0] dep_chan_data_13_14;
    wire token_13_14;
    wire dep_chan_vld_15_14;
    wire [39:0] dep_chan_data_15_14;
    wire token_15_14;
    wire [11:0] proc_dep_vld_vec_15;
    reg [11:0] proc_dep_vld_vec_15_reg;
    wire [11:0] in_chan_dep_vld_vec_15;
    wire [479:0] in_chan_dep_data_vec_15;
    wire [11:0] token_in_vec_15;
    wire [11:0] out_chan_dep_vld_vec_15;
    wire [39:0] out_chan_dep_data_15;
    wire [11:0] token_out_vec_15;
    wire dl_detect_out_15;
    wire dep_chan_vld_0_15;
    wire [39:0] dep_chan_data_0_15;
    wire token_0_15;
    wire dep_chan_vld_1_15;
    wire [39:0] dep_chan_data_1_15;
    wire token_1_15;
    wire dep_chan_vld_11_15;
    wire [39:0] dep_chan_data_11_15;
    wire token_11_15;
    wire dep_chan_vld_14_15;
    wire [39:0] dep_chan_data_14_15;
    wire token_14_15;
    wire dep_chan_vld_16_15;
    wire [39:0] dep_chan_data_16_15;
    wire token_16_15;
    wire dep_chan_vld_18_15;
    wire [39:0] dep_chan_data_18_15;
    wire token_18_15;
    wire dep_chan_vld_22_15;
    wire [39:0] dep_chan_data_22_15;
    wire token_22_15;
    wire dep_chan_vld_25_15;
    wire [39:0] dep_chan_data_25_15;
    wire token_25_15;
    wire dep_chan_vld_29_15;
    wire [39:0] dep_chan_data_29_15;
    wire token_29_15;
    wire dep_chan_vld_32_15;
    wire [39:0] dep_chan_data_32_15;
    wire token_32_15;
    wire dep_chan_vld_34_15;
    wire [39:0] dep_chan_data_34_15;
    wire token_34_15;
    wire dep_chan_vld_36_15;
    wire [39:0] dep_chan_data_36_15;
    wire token_36_15;
    wire [1:0] proc_dep_vld_vec_16;
    reg [1:0] proc_dep_vld_vec_16_reg;
    wire [1:0] in_chan_dep_vld_vec_16;
    wire [79:0] in_chan_dep_data_vec_16;
    wire [1:0] token_in_vec_16;
    wire [1:0] out_chan_dep_vld_vec_16;
    wire [39:0] out_chan_dep_data_16;
    wire [1:0] token_out_vec_16;
    wire dl_detect_out_16;
    wire dep_chan_vld_15_16;
    wire [39:0] dep_chan_data_15_16;
    wire token_15_16;
    wire dep_chan_vld_17_16;
    wire [39:0] dep_chan_data_17_16;
    wire token_17_16;
    wire [1:0] proc_dep_vld_vec_17;
    reg [1:0] proc_dep_vld_vec_17_reg;
    wire [1:0] in_chan_dep_vld_vec_17;
    wire [79:0] in_chan_dep_data_vec_17;
    wire [1:0] token_in_vec_17;
    wire [1:0] out_chan_dep_vld_vec_17;
    wire [39:0] out_chan_dep_data_17;
    wire [1:0] token_out_vec_17;
    wire dl_detect_out_17;
    wire dep_chan_vld_16_17;
    wire [39:0] dep_chan_data_16_17;
    wire token_16_17;
    wire dep_chan_vld_18_17;
    wire [39:0] dep_chan_data_18_17;
    wire token_18_17;
    wire [11:0] proc_dep_vld_vec_18;
    reg [11:0] proc_dep_vld_vec_18_reg;
    wire [11:0] in_chan_dep_vld_vec_18;
    wire [479:0] in_chan_dep_data_vec_18;
    wire [11:0] token_in_vec_18;
    wire [11:0] out_chan_dep_vld_vec_18;
    wire [39:0] out_chan_dep_data_18;
    wire [11:0] token_out_vec_18;
    wire dl_detect_out_18;
    wire dep_chan_vld_0_18;
    wire [39:0] dep_chan_data_0_18;
    wire token_0_18;
    wire dep_chan_vld_1_18;
    wire [39:0] dep_chan_data_1_18;
    wire token_1_18;
    wire dep_chan_vld_11_18;
    wire [39:0] dep_chan_data_11_18;
    wire token_11_18;
    wire dep_chan_vld_15_18;
    wire [39:0] dep_chan_data_15_18;
    wire token_15_18;
    wire dep_chan_vld_17_18;
    wire [39:0] dep_chan_data_17_18;
    wire token_17_18;
    wire dep_chan_vld_19_18;
    wire [39:0] dep_chan_data_19_18;
    wire token_19_18;
    wire dep_chan_vld_22_18;
    wire [39:0] dep_chan_data_22_18;
    wire token_22_18;
    wire dep_chan_vld_25_18;
    wire [39:0] dep_chan_data_25_18;
    wire token_25_18;
    wire dep_chan_vld_29_18;
    wire [39:0] dep_chan_data_29_18;
    wire token_29_18;
    wire dep_chan_vld_32_18;
    wire [39:0] dep_chan_data_32_18;
    wire token_32_18;
    wire dep_chan_vld_34_18;
    wire [39:0] dep_chan_data_34_18;
    wire token_34_18;
    wire dep_chan_vld_36_18;
    wire [39:0] dep_chan_data_36_18;
    wire token_36_18;
    wire [1:0] proc_dep_vld_vec_19;
    reg [1:0] proc_dep_vld_vec_19_reg;
    wire [1:0] in_chan_dep_vld_vec_19;
    wire [79:0] in_chan_dep_data_vec_19;
    wire [1:0] token_in_vec_19;
    wire [1:0] out_chan_dep_vld_vec_19;
    wire [39:0] out_chan_dep_data_19;
    wire [1:0] token_out_vec_19;
    wire dl_detect_out_19;
    wire dep_chan_vld_18_19;
    wire [39:0] dep_chan_data_18_19;
    wire token_18_19;
    wire dep_chan_vld_20_19;
    wire [39:0] dep_chan_data_20_19;
    wire token_20_19;
    wire [1:0] proc_dep_vld_vec_20;
    reg [1:0] proc_dep_vld_vec_20_reg;
    wire [1:0] in_chan_dep_vld_vec_20;
    wire [79:0] in_chan_dep_data_vec_20;
    wire [1:0] token_in_vec_20;
    wire [1:0] out_chan_dep_vld_vec_20;
    wire [39:0] out_chan_dep_data_20;
    wire [1:0] token_out_vec_20;
    wire dl_detect_out_20;
    wire dep_chan_vld_19_20;
    wire [39:0] dep_chan_data_19_20;
    wire token_19_20;
    wire dep_chan_vld_21_20;
    wire [39:0] dep_chan_data_21_20;
    wire token_21_20;
    wire [1:0] proc_dep_vld_vec_21;
    reg [1:0] proc_dep_vld_vec_21_reg;
    wire [1:0] in_chan_dep_vld_vec_21;
    wire [79:0] in_chan_dep_data_vec_21;
    wire [1:0] token_in_vec_21;
    wire [1:0] out_chan_dep_vld_vec_21;
    wire [39:0] out_chan_dep_data_21;
    wire [1:0] token_out_vec_21;
    wire dl_detect_out_21;
    wire dep_chan_vld_20_21;
    wire [39:0] dep_chan_data_20_21;
    wire token_20_21;
    wire dep_chan_vld_22_21;
    wire [39:0] dep_chan_data_22_21;
    wire token_22_21;
    wire [11:0] proc_dep_vld_vec_22;
    reg [11:0] proc_dep_vld_vec_22_reg;
    wire [11:0] in_chan_dep_vld_vec_22;
    wire [479:0] in_chan_dep_data_vec_22;
    wire [11:0] token_in_vec_22;
    wire [11:0] out_chan_dep_vld_vec_22;
    wire [39:0] out_chan_dep_data_22;
    wire [11:0] token_out_vec_22;
    wire dl_detect_out_22;
    wire dep_chan_vld_0_22;
    wire [39:0] dep_chan_data_0_22;
    wire token_0_22;
    wire dep_chan_vld_1_22;
    wire [39:0] dep_chan_data_1_22;
    wire token_1_22;
    wire dep_chan_vld_11_22;
    wire [39:0] dep_chan_data_11_22;
    wire token_11_22;
    wire dep_chan_vld_15_22;
    wire [39:0] dep_chan_data_15_22;
    wire token_15_22;
    wire dep_chan_vld_18_22;
    wire [39:0] dep_chan_data_18_22;
    wire token_18_22;
    wire dep_chan_vld_21_22;
    wire [39:0] dep_chan_data_21_22;
    wire token_21_22;
    wire dep_chan_vld_23_22;
    wire [39:0] dep_chan_data_23_22;
    wire token_23_22;
    wire dep_chan_vld_25_22;
    wire [39:0] dep_chan_data_25_22;
    wire token_25_22;
    wire dep_chan_vld_29_22;
    wire [39:0] dep_chan_data_29_22;
    wire token_29_22;
    wire dep_chan_vld_32_22;
    wire [39:0] dep_chan_data_32_22;
    wire token_32_22;
    wire dep_chan_vld_34_22;
    wire [39:0] dep_chan_data_34_22;
    wire token_34_22;
    wire dep_chan_vld_36_22;
    wire [39:0] dep_chan_data_36_22;
    wire token_36_22;
    wire [1:0] proc_dep_vld_vec_23;
    reg [1:0] proc_dep_vld_vec_23_reg;
    wire [1:0] in_chan_dep_vld_vec_23;
    wire [79:0] in_chan_dep_data_vec_23;
    wire [1:0] token_in_vec_23;
    wire [1:0] out_chan_dep_vld_vec_23;
    wire [39:0] out_chan_dep_data_23;
    wire [1:0] token_out_vec_23;
    wire dl_detect_out_23;
    wire dep_chan_vld_22_23;
    wire [39:0] dep_chan_data_22_23;
    wire token_22_23;
    wire dep_chan_vld_24_23;
    wire [39:0] dep_chan_data_24_23;
    wire token_24_23;
    wire [1:0] proc_dep_vld_vec_24;
    reg [1:0] proc_dep_vld_vec_24_reg;
    wire [1:0] in_chan_dep_vld_vec_24;
    wire [79:0] in_chan_dep_data_vec_24;
    wire [1:0] token_in_vec_24;
    wire [1:0] out_chan_dep_vld_vec_24;
    wire [39:0] out_chan_dep_data_24;
    wire [1:0] token_out_vec_24;
    wire dl_detect_out_24;
    wire dep_chan_vld_23_24;
    wire [39:0] dep_chan_data_23_24;
    wire token_23_24;
    wire dep_chan_vld_25_24;
    wire [39:0] dep_chan_data_25_24;
    wire token_25_24;
    wire [11:0] proc_dep_vld_vec_25;
    reg [11:0] proc_dep_vld_vec_25_reg;
    wire [11:0] in_chan_dep_vld_vec_25;
    wire [479:0] in_chan_dep_data_vec_25;
    wire [11:0] token_in_vec_25;
    wire [11:0] out_chan_dep_vld_vec_25;
    wire [39:0] out_chan_dep_data_25;
    wire [11:0] token_out_vec_25;
    wire dl_detect_out_25;
    wire dep_chan_vld_0_25;
    wire [39:0] dep_chan_data_0_25;
    wire token_0_25;
    wire dep_chan_vld_1_25;
    wire [39:0] dep_chan_data_1_25;
    wire token_1_25;
    wire dep_chan_vld_11_25;
    wire [39:0] dep_chan_data_11_25;
    wire token_11_25;
    wire dep_chan_vld_15_25;
    wire [39:0] dep_chan_data_15_25;
    wire token_15_25;
    wire dep_chan_vld_18_25;
    wire [39:0] dep_chan_data_18_25;
    wire token_18_25;
    wire dep_chan_vld_22_25;
    wire [39:0] dep_chan_data_22_25;
    wire token_22_25;
    wire dep_chan_vld_24_25;
    wire [39:0] dep_chan_data_24_25;
    wire token_24_25;
    wire dep_chan_vld_26_25;
    wire [39:0] dep_chan_data_26_25;
    wire token_26_25;
    wire dep_chan_vld_29_25;
    wire [39:0] dep_chan_data_29_25;
    wire token_29_25;
    wire dep_chan_vld_32_25;
    wire [39:0] dep_chan_data_32_25;
    wire token_32_25;
    wire dep_chan_vld_34_25;
    wire [39:0] dep_chan_data_34_25;
    wire token_34_25;
    wire dep_chan_vld_36_25;
    wire [39:0] dep_chan_data_36_25;
    wire token_36_25;
    wire [1:0] proc_dep_vld_vec_26;
    reg [1:0] proc_dep_vld_vec_26_reg;
    wire [1:0] in_chan_dep_vld_vec_26;
    wire [79:0] in_chan_dep_data_vec_26;
    wire [1:0] token_in_vec_26;
    wire [1:0] out_chan_dep_vld_vec_26;
    wire [39:0] out_chan_dep_data_26;
    wire [1:0] token_out_vec_26;
    wire dl_detect_out_26;
    wire dep_chan_vld_25_26;
    wire [39:0] dep_chan_data_25_26;
    wire token_25_26;
    wire dep_chan_vld_27_26;
    wire [39:0] dep_chan_data_27_26;
    wire token_27_26;
    wire [1:0] proc_dep_vld_vec_27;
    reg [1:0] proc_dep_vld_vec_27_reg;
    wire [1:0] in_chan_dep_vld_vec_27;
    wire [79:0] in_chan_dep_data_vec_27;
    wire [1:0] token_in_vec_27;
    wire [1:0] out_chan_dep_vld_vec_27;
    wire [39:0] out_chan_dep_data_27;
    wire [1:0] token_out_vec_27;
    wire dl_detect_out_27;
    wire dep_chan_vld_26_27;
    wire [39:0] dep_chan_data_26_27;
    wire token_26_27;
    wire dep_chan_vld_28_27;
    wire [39:0] dep_chan_data_28_27;
    wire token_28_27;
    wire [1:0] proc_dep_vld_vec_28;
    reg [1:0] proc_dep_vld_vec_28_reg;
    wire [1:0] in_chan_dep_vld_vec_28;
    wire [79:0] in_chan_dep_data_vec_28;
    wire [1:0] token_in_vec_28;
    wire [1:0] out_chan_dep_vld_vec_28;
    wire [39:0] out_chan_dep_data_28;
    wire [1:0] token_out_vec_28;
    wire dl_detect_out_28;
    wire dep_chan_vld_27_28;
    wire [39:0] dep_chan_data_27_28;
    wire token_27_28;
    wire dep_chan_vld_29_28;
    wire [39:0] dep_chan_data_29_28;
    wire token_29_28;
    wire [11:0] proc_dep_vld_vec_29;
    reg [11:0] proc_dep_vld_vec_29_reg;
    wire [11:0] in_chan_dep_vld_vec_29;
    wire [479:0] in_chan_dep_data_vec_29;
    wire [11:0] token_in_vec_29;
    wire [11:0] out_chan_dep_vld_vec_29;
    wire [39:0] out_chan_dep_data_29;
    wire [11:0] token_out_vec_29;
    wire dl_detect_out_29;
    wire dep_chan_vld_0_29;
    wire [39:0] dep_chan_data_0_29;
    wire token_0_29;
    wire dep_chan_vld_1_29;
    wire [39:0] dep_chan_data_1_29;
    wire token_1_29;
    wire dep_chan_vld_11_29;
    wire [39:0] dep_chan_data_11_29;
    wire token_11_29;
    wire dep_chan_vld_15_29;
    wire [39:0] dep_chan_data_15_29;
    wire token_15_29;
    wire dep_chan_vld_18_29;
    wire [39:0] dep_chan_data_18_29;
    wire token_18_29;
    wire dep_chan_vld_22_29;
    wire [39:0] dep_chan_data_22_29;
    wire token_22_29;
    wire dep_chan_vld_25_29;
    wire [39:0] dep_chan_data_25_29;
    wire token_25_29;
    wire dep_chan_vld_28_29;
    wire [39:0] dep_chan_data_28_29;
    wire token_28_29;
    wire dep_chan_vld_30_29;
    wire [39:0] dep_chan_data_30_29;
    wire token_30_29;
    wire dep_chan_vld_32_29;
    wire [39:0] dep_chan_data_32_29;
    wire token_32_29;
    wire dep_chan_vld_34_29;
    wire [39:0] dep_chan_data_34_29;
    wire token_34_29;
    wire dep_chan_vld_36_29;
    wire [39:0] dep_chan_data_36_29;
    wire token_36_29;
    wire [1:0] proc_dep_vld_vec_30;
    reg [1:0] proc_dep_vld_vec_30_reg;
    wire [1:0] in_chan_dep_vld_vec_30;
    wire [79:0] in_chan_dep_data_vec_30;
    wire [1:0] token_in_vec_30;
    wire [1:0] out_chan_dep_vld_vec_30;
    wire [39:0] out_chan_dep_data_30;
    wire [1:0] token_out_vec_30;
    wire dl_detect_out_30;
    wire dep_chan_vld_29_30;
    wire [39:0] dep_chan_data_29_30;
    wire token_29_30;
    wire dep_chan_vld_31_30;
    wire [39:0] dep_chan_data_31_30;
    wire token_31_30;
    wire [1:0] proc_dep_vld_vec_31;
    reg [1:0] proc_dep_vld_vec_31_reg;
    wire [1:0] in_chan_dep_vld_vec_31;
    wire [79:0] in_chan_dep_data_vec_31;
    wire [1:0] token_in_vec_31;
    wire [1:0] out_chan_dep_vld_vec_31;
    wire [39:0] out_chan_dep_data_31;
    wire [1:0] token_out_vec_31;
    wire dl_detect_out_31;
    wire dep_chan_vld_30_31;
    wire [39:0] dep_chan_data_30_31;
    wire token_30_31;
    wire dep_chan_vld_32_31;
    wire [39:0] dep_chan_data_32_31;
    wire token_32_31;
    wire [11:0] proc_dep_vld_vec_32;
    reg [11:0] proc_dep_vld_vec_32_reg;
    wire [11:0] in_chan_dep_vld_vec_32;
    wire [479:0] in_chan_dep_data_vec_32;
    wire [11:0] token_in_vec_32;
    wire [11:0] out_chan_dep_vld_vec_32;
    wire [39:0] out_chan_dep_data_32;
    wire [11:0] token_out_vec_32;
    wire dl_detect_out_32;
    wire dep_chan_vld_0_32;
    wire [39:0] dep_chan_data_0_32;
    wire token_0_32;
    wire dep_chan_vld_1_32;
    wire [39:0] dep_chan_data_1_32;
    wire token_1_32;
    wire dep_chan_vld_11_32;
    wire [39:0] dep_chan_data_11_32;
    wire token_11_32;
    wire dep_chan_vld_15_32;
    wire [39:0] dep_chan_data_15_32;
    wire token_15_32;
    wire dep_chan_vld_18_32;
    wire [39:0] dep_chan_data_18_32;
    wire token_18_32;
    wire dep_chan_vld_22_32;
    wire [39:0] dep_chan_data_22_32;
    wire token_22_32;
    wire dep_chan_vld_25_32;
    wire [39:0] dep_chan_data_25_32;
    wire token_25_32;
    wire dep_chan_vld_29_32;
    wire [39:0] dep_chan_data_29_32;
    wire token_29_32;
    wire dep_chan_vld_31_32;
    wire [39:0] dep_chan_data_31_32;
    wire token_31_32;
    wire dep_chan_vld_33_32;
    wire [39:0] dep_chan_data_33_32;
    wire token_33_32;
    wire dep_chan_vld_34_32;
    wire [39:0] dep_chan_data_34_32;
    wire token_34_32;
    wire dep_chan_vld_36_32;
    wire [39:0] dep_chan_data_36_32;
    wire token_36_32;
    wire [1:0] proc_dep_vld_vec_33;
    reg [1:0] proc_dep_vld_vec_33_reg;
    wire [1:0] in_chan_dep_vld_vec_33;
    wire [79:0] in_chan_dep_data_vec_33;
    wire [1:0] token_in_vec_33;
    wire [1:0] out_chan_dep_vld_vec_33;
    wire [39:0] out_chan_dep_data_33;
    wire [1:0] token_out_vec_33;
    wire dl_detect_out_33;
    wire dep_chan_vld_32_33;
    wire [39:0] dep_chan_data_32_33;
    wire token_32_33;
    wire dep_chan_vld_34_33;
    wire [39:0] dep_chan_data_34_33;
    wire token_34_33;
    wire [11:0] proc_dep_vld_vec_34;
    reg [11:0] proc_dep_vld_vec_34_reg;
    wire [11:0] in_chan_dep_vld_vec_34;
    wire [479:0] in_chan_dep_data_vec_34;
    wire [11:0] token_in_vec_34;
    wire [11:0] out_chan_dep_vld_vec_34;
    wire [39:0] out_chan_dep_data_34;
    wire [11:0] token_out_vec_34;
    wire dl_detect_out_34;
    wire dep_chan_vld_0_34;
    wire [39:0] dep_chan_data_0_34;
    wire token_0_34;
    wire dep_chan_vld_1_34;
    wire [39:0] dep_chan_data_1_34;
    wire token_1_34;
    wire dep_chan_vld_11_34;
    wire [39:0] dep_chan_data_11_34;
    wire token_11_34;
    wire dep_chan_vld_15_34;
    wire [39:0] dep_chan_data_15_34;
    wire token_15_34;
    wire dep_chan_vld_18_34;
    wire [39:0] dep_chan_data_18_34;
    wire token_18_34;
    wire dep_chan_vld_22_34;
    wire [39:0] dep_chan_data_22_34;
    wire token_22_34;
    wire dep_chan_vld_25_34;
    wire [39:0] dep_chan_data_25_34;
    wire token_25_34;
    wire dep_chan_vld_29_34;
    wire [39:0] dep_chan_data_29_34;
    wire token_29_34;
    wire dep_chan_vld_32_34;
    wire [39:0] dep_chan_data_32_34;
    wire token_32_34;
    wire dep_chan_vld_33_34;
    wire [39:0] dep_chan_data_33_34;
    wire token_33_34;
    wire dep_chan_vld_35_34;
    wire [39:0] dep_chan_data_35_34;
    wire token_35_34;
    wire dep_chan_vld_36_34;
    wire [39:0] dep_chan_data_36_34;
    wire token_36_34;
    wire [1:0] proc_dep_vld_vec_35;
    reg [1:0] proc_dep_vld_vec_35_reg;
    wire [1:0] in_chan_dep_vld_vec_35;
    wire [79:0] in_chan_dep_data_vec_35;
    wire [1:0] token_in_vec_35;
    wire [1:0] out_chan_dep_vld_vec_35;
    wire [39:0] out_chan_dep_data_35;
    wire [1:0] token_out_vec_35;
    wire dl_detect_out_35;
    wire dep_chan_vld_34_35;
    wire [39:0] dep_chan_data_34_35;
    wire token_34_35;
    wire dep_chan_vld_36_35;
    wire [39:0] dep_chan_data_36_35;
    wire token_36_35;
    wire [11:0] proc_dep_vld_vec_36;
    reg [11:0] proc_dep_vld_vec_36_reg;
    wire [11:0] in_chan_dep_vld_vec_36;
    wire [479:0] in_chan_dep_data_vec_36;
    wire [11:0] token_in_vec_36;
    wire [11:0] out_chan_dep_vld_vec_36;
    wire [39:0] out_chan_dep_data_36;
    wire [11:0] token_out_vec_36;
    wire dl_detect_out_36;
    wire dep_chan_vld_0_36;
    wire [39:0] dep_chan_data_0_36;
    wire token_0_36;
    wire dep_chan_vld_1_36;
    wire [39:0] dep_chan_data_1_36;
    wire token_1_36;
    wire dep_chan_vld_11_36;
    wire [39:0] dep_chan_data_11_36;
    wire token_11_36;
    wire dep_chan_vld_15_36;
    wire [39:0] dep_chan_data_15_36;
    wire token_15_36;
    wire dep_chan_vld_18_36;
    wire [39:0] dep_chan_data_18_36;
    wire token_18_36;
    wire dep_chan_vld_22_36;
    wire [39:0] dep_chan_data_22_36;
    wire token_22_36;
    wire dep_chan_vld_25_36;
    wire [39:0] dep_chan_data_25_36;
    wire token_25_36;
    wire dep_chan_vld_29_36;
    wire [39:0] dep_chan_data_29_36;
    wire token_29_36;
    wire dep_chan_vld_32_36;
    wire [39:0] dep_chan_data_32_36;
    wire token_32_36;
    wire dep_chan_vld_34_36;
    wire [39:0] dep_chan_data_34_36;
    wire token_34_36;
    wire dep_chan_vld_35_36;
    wire [39:0] dep_chan_data_35_36;
    wire token_35_36;
    wire dep_chan_vld_37_36;
    wire [39:0] dep_chan_data_37_36;
    wire token_37_36;
    wire [1:0] proc_dep_vld_vec_37;
    reg [1:0] proc_dep_vld_vec_37_reg;
    wire [1:0] in_chan_dep_vld_vec_37;
    wire [79:0] in_chan_dep_data_vec_37;
    wire [1:0] token_in_vec_37;
    wire [1:0] out_chan_dep_vld_vec_37;
    wire [39:0] out_chan_dep_data_37;
    wire [1:0] token_out_vec_37;
    wire dl_detect_out_37;
    wire dep_chan_vld_36_37;
    wire [39:0] dep_chan_data_36_37;
    wire token_36_37;
    wire dep_chan_vld_38_37;
    wire [39:0] dep_chan_data_38_37;
    wire token_38_37;
    wire [1:0] proc_dep_vld_vec_38;
    reg [1:0] proc_dep_vld_vec_38_reg;
    wire [1:0] in_chan_dep_vld_vec_38;
    wire [79:0] in_chan_dep_data_vec_38;
    wire [1:0] token_in_vec_38;
    wire [1:0] out_chan_dep_vld_vec_38;
    wire [39:0] out_chan_dep_data_38;
    wire [1:0] token_out_vec_38;
    wire dl_detect_out_38;
    wire dep_chan_vld_37_38;
    wire [39:0] dep_chan_data_37_38;
    wire token_37_38;
    wire dep_chan_vld_39_38;
    wire [39:0] dep_chan_data_39_38;
    wire token_39_38;
    wire [0:0] proc_dep_vld_vec_39;
    reg [0:0] proc_dep_vld_vec_39_reg;
    wire [0:0] in_chan_dep_vld_vec_39;
    wire [39:0] in_chan_dep_data_vec_39;
    wire [0:0] token_in_vec_39;
    wire [0:0] out_chan_dep_vld_vec_39;
    wire [39:0] out_chan_dep_data_39;
    wire [0:0] token_out_vec_39;
    wire dl_detect_out_39;
    wire dep_chan_vld_38_39;
    wire [39:0] dep_chan_data_38_39;
    wire token_38_39;
    wire [39:0] dl_in_vec;
    wire dl_detect_out;
    wire [39:0] origin;
    wire token_clear;

    // delay ap_idle for one cycle
    reg [0:0] AESL_inst_myproject$Block_ap_fixed_base_exit3121_proc_U0$ap_idle;
    always @ (negedge reset or posedge clock) begin
        if (~reset) begin
            AESL_inst_myproject$Block_ap_fixed_base_exit3121_proc_U0$ap_idle <= 'b0;
        end
        else begin
            AESL_inst_myproject$Block_ap_fixed_base_exit3121_proc_U0$ap_idle <= AESL_inst_myproject.Block_ap_fixed_base_exit3121_proc_U0.ap_idle;
        end
    end
    // Process: AESL_inst_myproject.Block_ap_fixed_base_exit3121_proc_U0
    AESL_deadlock_detect_unit #(40, 0, 10, 10) AESL_deadlock_detect_unit_0 (
        .reset(reset),
        .clock(clock),
        .proc_dep_vld_vec(proc_dep_vld_vec_0),
        .in_chan_dep_vld_vec(in_chan_dep_vld_vec_0),
        .in_chan_dep_data_vec(in_chan_dep_data_vec_0),
        .token_in_vec(token_in_vec_0),
        .dl_detect_in(dl_detect_out),
        .origin(origin[0]),
        .token_clear(token_clear),
        .out_chan_dep_vld_vec(out_chan_dep_vld_vec_0),
        .out_chan_dep_data(out_chan_dep_data_0),
        .token_out_vec(token_out_vec_0),
        .dl_detect_out(dl_in_vec[0]));

    assign proc_dep_vld_vec_0[0] = dl_detect_out ? proc_dep_vld_vec_0_reg[0] : (((AESL_inst_myproject.Block_ap_fixed_base_exit3121_proc_U0_ap_ready_count[0]) & AESL_inst_myproject.Block_ap_fixed_base_exit3121_proc_U0.ap_idle & ~(AESL_inst_myproject.resize_nearest_me_ap_fixed_32_16_5_3_0_config2_U0_ap_ready_count[0])));
    assign proc_dep_vld_vec_0[1] = dl_detect_out ? proc_dep_vld_vec_0_reg[1] : (((AESL_inst_myproject.Block_ap_fixed_base_exit3121_proc_U0_ap_ready_count[0]) & AESL_inst_myproject.Block_ap_fixed_base_exit3121_proc_U0.ap_idle & ~(AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config13_U0_ap_ready_count[0])));
    assign proc_dep_vld_vec_0[2] = dl_detect_out ? proc_dep_vld_vec_0_reg[2] : (((AESL_inst_myproject.Block_ap_fixed_base_exit3121_proc_U0_ap_ready_count[0]) & AESL_inst_myproject.Block_ap_fixed_base_exit3121_proc_U0.ap_idle & ~(AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config18_U0_ap_ready_count[0])));
    assign proc_dep_vld_vec_0[3] = dl_detect_out ? proc_dep_vld_vec_0_reg[3] : (((AESL_inst_myproject.Block_ap_fixed_base_exit3121_proc_U0_ap_ready_count[0]) & AESL_inst_myproject.Block_ap_fixed_base_exit3121_proc_U0.ap_idle & ~(AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config22_U0_ap_ready_count[0])));
    assign proc_dep_vld_vec_0[4] = dl_detect_out ? proc_dep_vld_vec_0_reg[4] : (((AESL_inst_myproject.Block_ap_fixed_base_exit3121_proc_U0_ap_ready_count[0]) & AESL_inst_myproject.Block_ap_fixed_base_exit3121_proc_U0.ap_idle & ~(AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config27_U0_ap_ready_count[0])));
    assign proc_dep_vld_vec_0[5] = dl_detect_out ? proc_dep_vld_vec_0_reg[5] : (((AESL_inst_myproject.Block_ap_fixed_base_exit3121_proc_U0_ap_ready_count[0]) & AESL_inst_myproject.Block_ap_fixed_base_exit3121_proc_U0.ap_idle & ~(AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config31_U0_ap_ready_count[0])));
    assign proc_dep_vld_vec_0[6] = dl_detect_out ? proc_dep_vld_vec_0_reg[6] : (((AESL_inst_myproject.Block_ap_fixed_base_exit3121_proc_U0_ap_ready_count[0]) & AESL_inst_myproject.Block_ap_fixed_base_exit3121_proc_U0.ap_idle & ~(AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config36_U0_ap_ready_count[0])));
    assign proc_dep_vld_vec_0[7] = dl_detect_out ? proc_dep_vld_vec_0_reg[7] : (((AESL_inst_myproject.Block_ap_fixed_base_exit3121_proc_U0_ap_ready_count[0]) & AESL_inst_myproject.Block_ap_fixed_base_exit3121_proc_U0.ap_idle & ~(AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config40_U0_ap_ready_count[0])));
    assign proc_dep_vld_vec_0[8] = dl_detect_out ? proc_dep_vld_vec_0_reg[8] : (((AESL_inst_myproject.Block_ap_fixed_base_exit3121_proc_U0_ap_ready_count[0]) & AESL_inst_myproject.Block_ap_fixed_base_exit3121_proc_U0.ap_idle & ~(AESL_inst_myproject.dense_large_stream_me_ap_fixed_ap_fixed_config45_U0_ap_ready_count[0])));
    assign proc_dep_vld_vec_0[9] = dl_detect_out ? proc_dep_vld_vec_0_reg[9] : (((AESL_inst_myproject.Block_ap_fixed_base_exit3121_proc_U0_ap_ready_count[0]) & AESL_inst_myproject.Block_ap_fixed_base_exit3121_proc_U0.ap_idle & ~(AESL_inst_myproject.dense_large_stream_me_ap_fixed_ap_fixed_config49_U0_ap_ready_count[0])));
    always @ (negedge reset or posedge clock) begin
        if (~reset) begin
            proc_dep_vld_vec_0_reg <= 'b0;
        end
        else begin
            proc_dep_vld_vec_0_reg <= proc_dep_vld_vec_0;
        end
    end
    assign in_chan_dep_vld_vec_0[0] = dep_chan_vld_1_0;
    assign in_chan_dep_data_vec_0[39 : 0] = dep_chan_data_1_0;
    assign token_in_vec_0[0] = token_1_0;
    assign in_chan_dep_vld_vec_0[1] = dep_chan_vld_11_0;
    assign in_chan_dep_data_vec_0[79 : 40] = dep_chan_data_11_0;
    assign token_in_vec_0[1] = token_11_0;
    assign in_chan_dep_vld_vec_0[2] = dep_chan_vld_15_0;
    assign in_chan_dep_data_vec_0[119 : 80] = dep_chan_data_15_0;
    assign token_in_vec_0[2] = token_15_0;
    assign in_chan_dep_vld_vec_0[3] = dep_chan_vld_18_0;
    assign in_chan_dep_data_vec_0[159 : 120] = dep_chan_data_18_0;
    assign token_in_vec_0[3] = token_18_0;
    assign in_chan_dep_vld_vec_0[4] = dep_chan_vld_22_0;
    assign in_chan_dep_data_vec_0[199 : 160] = dep_chan_data_22_0;
    assign token_in_vec_0[4] = token_22_0;
    assign in_chan_dep_vld_vec_0[5] = dep_chan_vld_25_0;
    assign in_chan_dep_data_vec_0[239 : 200] = dep_chan_data_25_0;
    assign token_in_vec_0[5] = token_25_0;
    assign in_chan_dep_vld_vec_0[6] = dep_chan_vld_29_0;
    assign in_chan_dep_data_vec_0[279 : 240] = dep_chan_data_29_0;
    assign token_in_vec_0[6] = token_29_0;
    assign in_chan_dep_vld_vec_0[7] = dep_chan_vld_32_0;
    assign in_chan_dep_data_vec_0[319 : 280] = dep_chan_data_32_0;
    assign token_in_vec_0[7] = token_32_0;
    assign in_chan_dep_vld_vec_0[8] = dep_chan_vld_34_0;
    assign in_chan_dep_data_vec_0[359 : 320] = dep_chan_data_34_0;
    assign token_in_vec_0[8] = token_34_0;
    assign in_chan_dep_vld_vec_0[9] = dep_chan_vld_36_0;
    assign in_chan_dep_data_vec_0[399 : 360] = dep_chan_data_36_0;
    assign token_in_vec_0[9] = token_36_0;
    assign dep_chan_vld_0_1 = out_chan_dep_vld_vec_0[0];
    assign dep_chan_data_0_1 = out_chan_dep_data_0;
    assign token_0_1 = token_out_vec_0[0];
    assign dep_chan_vld_0_11 = out_chan_dep_vld_vec_0[1];
    assign dep_chan_data_0_11 = out_chan_dep_data_0;
    assign token_0_11 = token_out_vec_0[1];
    assign dep_chan_vld_0_15 = out_chan_dep_vld_vec_0[2];
    assign dep_chan_data_0_15 = out_chan_dep_data_0;
    assign token_0_15 = token_out_vec_0[2];
    assign dep_chan_vld_0_18 = out_chan_dep_vld_vec_0[3];
    assign dep_chan_data_0_18 = out_chan_dep_data_0;
    assign token_0_18 = token_out_vec_0[3];
    assign dep_chan_vld_0_22 = out_chan_dep_vld_vec_0[4];
    assign dep_chan_data_0_22 = out_chan_dep_data_0;
    assign token_0_22 = token_out_vec_0[4];
    assign dep_chan_vld_0_25 = out_chan_dep_vld_vec_0[5];
    assign dep_chan_data_0_25 = out_chan_dep_data_0;
    assign token_0_25 = token_out_vec_0[5];
    assign dep_chan_vld_0_29 = out_chan_dep_vld_vec_0[6];
    assign dep_chan_data_0_29 = out_chan_dep_data_0;
    assign token_0_29 = token_out_vec_0[6];
    assign dep_chan_vld_0_32 = out_chan_dep_vld_vec_0[7];
    assign dep_chan_data_0_32 = out_chan_dep_data_0;
    assign token_0_32 = token_out_vec_0[7];
    assign dep_chan_vld_0_34 = out_chan_dep_vld_vec_0[8];
    assign dep_chan_data_0_34 = out_chan_dep_data_0;
    assign token_0_34 = token_out_vec_0[8];
    assign dep_chan_vld_0_36 = out_chan_dep_vld_vec_0[9];
    assign dep_chan_data_0_36 = out_chan_dep_data_0;
    assign token_0_36 = token_out_vec_0[9];

    // delay ap_idle for one cycle
    reg [0:0] AESL_inst_myproject$resize_nearest_me_ap_fixed_32_16_5_3_0_config2_U0$ap_idle;
    always @ (negedge reset or posedge clock) begin
        if (~reset) begin
            AESL_inst_myproject$resize_nearest_me_ap_fixed_32_16_5_3_0_config2_U0$ap_idle <= 'b0;
        end
        else begin
            AESL_inst_myproject$resize_nearest_me_ap_fixed_32_16_5_3_0_config2_U0$ap_idle <= AESL_inst_myproject.resize_nearest_me_ap_fixed_32_16_5_3_0_config2_U0.ap_idle;
        end
    end
    // Process: AESL_inst_myproject.resize_nearest_me_ap_fixed_32_16_5_3_0_config2_U0
    AESL_deadlock_detect_unit #(40, 1, 11, 11) AESL_deadlock_detect_unit_1 (
        .reset(reset),
        .clock(clock),
        .proc_dep_vld_vec(proc_dep_vld_vec_1),
        .in_chan_dep_vld_vec(in_chan_dep_vld_vec_1),
        .in_chan_dep_data_vec(in_chan_dep_data_vec_1),
        .token_in_vec(token_in_vec_1),
        .dl_detect_in(dl_detect_out),
        .origin(origin[1]),
        .token_clear(token_clear),
        .out_chan_dep_vld_vec(out_chan_dep_vld_vec_1),
        .out_chan_dep_data(out_chan_dep_data_1),
        .token_out_vec(token_out_vec_1),
        .dl_detect_out(dl_in_vec[1]));

    assign proc_dep_vld_vec_1[0] = dl_detect_out ? proc_dep_vld_vec_1_reg[0] : (~AESL_inst_myproject.resize_nearest_me_ap_fixed_32_16_5_3_0_config2_U0.resized_V_V_blk_n | (~AESL_inst_myproject.start_for_normalize_me_ap_fixed_ap_fixed_32_16_5_3_0_confBRe_U.if_full_n & AESL_inst_myproject.normalize_me_ap_fixed_ap_fixed_32_16_5_3_0_config3_U0.ap_done));
    assign proc_dep_vld_vec_1[1] = dl_detect_out ? proc_dep_vld_vec_1_reg[1] : (((AESL_inst_myproject.resize_nearest_me_ap_fixed_32_16_5_3_0_config2_U0_ap_ready_count[0]) & AESL_inst_myproject.resize_nearest_me_ap_fixed_32_16_5_3_0_config2_U0.ap_idle & ~(AESL_inst_myproject.Block_ap_fixed_base_exit3121_proc_U0_ap_ready_count[0])));
    assign proc_dep_vld_vec_1[2] = dl_detect_out ? proc_dep_vld_vec_1_reg[2] : (((AESL_inst_myproject.resize_nearest_me_ap_fixed_32_16_5_3_0_config2_U0_ap_ready_count[0]) & AESL_inst_myproject.resize_nearest_me_ap_fixed_32_16_5_3_0_config2_U0.ap_idle & ~(AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config13_U0_ap_ready_count[0])));
    assign proc_dep_vld_vec_1[3] = dl_detect_out ? proc_dep_vld_vec_1_reg[3] : (((AESL_inst_myproject.resize_nearest_me_ap_fixed_32_16_5_3_0_config2_U0_ap_ready_count[0]) & AESL_inst_myproject.resize_nearest_me_ap_fixed_32_16_5_3_0_config2_U0.ap_idle & ~(AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config18_U0_ap_ready_count[0])));
    assign proc_dep_vld_vec_1[4] = dl_detect_out ? proc_dep_vld_vec_1_reg[4] : (((AESL_inst_myproject.resize_nearest_me_ap_fixed_32_16_5_3_0_config2_U0_ap_ready_count[0]) & AESL_inst_myproject.resize_nearest_me_ap_fixed_32_16_5_3_0_config2_U0.ap_idle & ~(AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config22_U0_ap_ready_count[0])));
    assign proc_dep_vld_vec_1[5] = dl_detect_out ? proc_dep_vld_vec_1_reg[5] : (((AESL_inst_myproject.resize_nearest_me_ap_fixed_32_16_5_3_0_config2_U0_ap_ready_count[0]) & AESL_inst_myproject.resize_nearest_me_ap_fixed_32_16_5_3_0_config2_U0.ap_idle & ~(AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config27_U0_ap_ready_count[0])));
    assign proc_dep_vld_vec_1[6] = dl_detect_out ? proc_dep_vld_vec_1_reg[6] : (((AESL_inst_myproject.resize_nearest_me_ap_fixed_32_16_5_3_0_config2_U0_ap_ready_count[0]) & AESL_inst_myproject.resize_nearest_me_ap_fixed_32_16_5_3_0_config2_U0.ap_idle & ~(AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config31_U0_ap_ready_count[0])));
    assign proc_dep_vld_vec_1[7] = dl_detect_out ? proc_dep_vld_vec_1_reg[7] : (((AESL_inst_myproject.resize_nearest_me_ap_fixed_32_16_5_3_0_config2_U0_ap_ready_count[0]) & AESL_inst_myproject.resize_nearest_me_ap_fixed_32_16_5_3_0_config2_U0.ap_idle & ~(AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config36_U0_ap_ready_count[0])));
    assign proc_dep_vld_vec_1[8] = dl_detect_out ? proc_dep_vld_vec_1_reg[8] : (((AESL_inst_myproject.resize_nearest_me_ap_fixed_32_16_5_3_0_config2_U0_ap_ready_count[0]) & AESL_inst_myproject.resize_nearest_me_ap_fixed_32_16_5_3_0_config2_U0.ap_idle & ~(AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config40_U0_ap_ready_count[0])));
    assign proc_dep_vld_vec_1[9] = dl_detect_out ? proc_dep_vld_vec_1_reg[9] : (((AESL_inst_myproject.resize_nearest_me_ap_fixed_32_16_5_3_0_config2_U0_ap_ready_count[0]) & AESL_inst_myproject.resize_nearest_me_ap_fixed_32_16_5_3_0_config2_U0.ap_idle & ~(AESL_inst_myproject.dense_large_stream_me_ap_fixed_ap_fixed_config45_U0_ap_ready_count[0])));
    assign proc_dep_vld_vec_1[10] = dl_detect_out ? proc_dep_vld_vec_1_reg[10] : (((AESL_inst_myproject.resize_nearest_me_ap_fixed_32_16_5_3_0_config2_U0_ap_ready_count[0]) & AESL_inst_myproject.resize_nearest_me_ap_fixed_32_16_5_3_0_config2_U0.ap_idle & ~(AESL_inst_myproject.dense_large_stream_me_ap_fixed_ap_fixed_config49_U0_ap_ready_count[0])));
    always @ (negedge reset or posedge clock) begin
        if (~reset) begin
            proc_dep_vld_vec_1_reg <= 'b0;
        end
        else begin
            proc_dep_vld_vec_1_reg <= proc_dep_vld_vec_1;
        end
    end
    assign in_chan_dep_vld_vec_1[0] = dep_chan_vld_0_1;
    assign in_chan_dep_data_vec_1[39 : 0] = dep_chan_data_0_1;
    assign token_in_vec_1[0] = token_0_1;
    assign in_chan_dep_vld_vec_1[1] = dep_chan_vld_2_1;
    assign in_chan_dep_data_vec_1[79 : 40] = dep_chan_data_2_1;
    assign token_in_vec_1[1] = token_2_1;
    assign in_chan_dep_vld_vec_1[2] = dep_chan_vld_11_1;
    assign in_chan_dep_data_vec_1[119 : 80] = dep_chan_data_11_1;
    assign token_in_vec_1[2] = token_11_1;
    assign in_chan_dep_vld_vec_1[3] = dep_chan_vld_15_1;
    assign in_chan_dep_data_vec_1[159 : 120] = dep_chan_data_15_1;
    assign token_in_vec_1[3] = token_15_1;
    assign in_chan_dep_vld_vec_1[4] = dep_chan_vld_18_1;
    assign in_chan_dep_data_vec_1[199 : 160] = dep_chan_data_18_1;
    assign token_in_vec_1[4] = token_18_1;
    assign in_chan_dep_vld_vec_1[5] = dep_chan_vld_22_1;
    assign in_chan_dep_data_vec_1[239 : 200] = dep_chan_data_22_1;
    assign token_in_vec_1[5] = token_22_1;
    assign in_chan_dep_vld_vec_1[6] = dep_chan_vld_25_1;
    assign in_chan_dep_data_vec_1[279 : 240] = dep_chan_data_25_1;
    assign token_in_vec_1[6] = token_25_1;
    assign in_chan_dep_vld_vec_1[7] = dep_chan_vld_29_1;
    assign in_chan_dep_data_vec_1[319 : 280] = dep_chan_data_29_1;
    assign token_in_vec_1[7] = token_29_1;
    assign in_chan_dep_vld_vec_1[8] = dep_chan_vld_32_1;
    assign in_chan_dep_data_vec_1[359 : 320] = dep_chan_data_32_1;
    assign token_in_vec_1[8] = token_32_1;
    assign in_chan_dep_vld_vec_1[9] = dep_chan_vld_34_1;
    assign in_chan_dep_data_vec_1[399 : 360] = dep_chan_data_34_1;
    assign token_in_vec_1[9] = token_34_1;
    assign in_chan_dep_vld_vec_1[10] = dep_chan_vld_36_1;
    assign in_chan_dep_data_vec_1[439 : 400] = dep_chan_data_36_1;
    assign token_in_vec_1[10] = token_36_1;
    assign dep_chan_vld_1_2 = out_chan_dep_vld_vec_1[0];
    assign dep_chan_data_1_2 = out_chan_dep_data_1;
    assign token_1_2 = token_out_vec_1[0];
    assign dep_chan_vld_1_0 = out_chan_dep_vld_vec_1[1];
    assign dep_chan_data_1_0 = out_chan_dep_data_1;
    assign token_1_0 = token_out_vec_1[1];
    assign dep_chan_vld_1_11 = out_chan_dep_vld_vec_1[2];
    assign dep_chan_data_1_11 = out_chan_dep_data_1;
    assign token_1_11 = token_out_vec_1[2];
    assign dep_chan_vld_1_15 = out_chan_dep_vld_vec_1[3];
    assign dep_chan_data_1_15 = out_chan_dep_data_1;
    assign token_1_15 = token_out_vec_1[3];
    assign dep_chan_vld_1_18 = out_chan_dep_vld_vec_1[4];
    assign dep_chan_data_1_18 = out_chan_dep_data_1;
    assign token_1_18 = token_out_vec_1[4];
    assign dep_chan_vld_1_22 = out_chan_dep_vld_vec_1[5];
    assign dep_chan_data_1_22 = out_chan_dep_data_1;
    assign token_1_22 = token_out_vec_1[5];
    assign dep_chan_vld_1_25 = out_chan_dep_vld_vec_1[6];
    assign dep_chan_data_1_25 = out_chan_dep_data_1;
    assign token_1_25 = token_out_vec_1[6];
    assign dep_chan_vld_1_29 = out_chan_dep_vld_vec_1[7];
    assign dep_chan_data_1_29 = out_chan_dep_data_1;
    assign token_1_29 = token_out_vec_1[7];
    assign dep_chan_vld_1_32 = out_chan_dep_vld_vec_1[8];
    assign dep_chan_data_1_32 = out_chan_dep_data_1;
    assign token_1_32 = token_out_vec_1[8];
    assign dep_chan_vld_1_34 = out_chan_dep_vld_vec_1[9];
    assign dep_chan_data_1_34 = out_chan_dep_data_1;
    assign token_1_34 = token_out_vec_1[9];
    assign dep_chan_vld_1_36 = out_chan_dep_vld_vec_1[10];
    assign dep_chan_data_1_36 = out_chan_dep_data_1;
    assign token_1_36 = token_out_vec_1[10];

    // delay ap_idle for one cycle
    reg [0:0] AESL_inst_myproject$normalize_me_ap_fixed_ap_fixed_32_16_5_3_0_config3_U0$ap_idle;
    always @ (negedge reset or posedge clock) begin
        if (~reset) begin
            AESL_inst_myproject$normalize_me_ap_fixed_ap_fixed_32_16_5_3_0_config3_U0$ap_idle <= 'b0;
        end
        else begin
            AESL_inst_myproject$normalize_me_ap_fixed_ap_fixed_32_16_5_3_0_config3_U0$ap_idle <= AESL_inst_myproject.normalize_me_ap_fixed_ap_fixed_32_16_5_3_0_config3_U0.ap_idle;
        end
    end
    // Process: AESL_inst_myproject.normalize_me_ap_fixed_ap_fixed_32_16_5_3_0_config3_U0
    AESL_deadlock_detect_unit #(40, 2, 2, 2) AESL_deadlock_detect_unit_2 (
        .reset(reset),
        .clock(clock),
        .proc_dep_vld_vec(proc_dep_vld_vec_2),
        .in_chan_dep_vld_vec(in_chan_dep_vld_vec_2),
        .in_chan_dep_data_vec(in_chan_dep_data_vec_2),
        .token_in_vec(token_in_vec_2),
        .dl_detect_in(dl_detect_out),
        .origin(origin[2]),
        .token_clear(token_clear),
        .out_chan_dep_vld_vec(out_chan_dep_vld_vec_2),
        .out_chan_dep_data(out_chan_dep_data_2),
        .token_out_vec(token_out_vec_2),
        .dl_detect_out(dl_in_vec[2]));

    assign proc_dep_vld_vec_2[0] = dl_detect_out ? proc_dep_vld_vec_2_reg[0] : (~AESL_inst_myproject.normalize_me_ap_fixed_ap_fixed_32_16_5_3_0_config3_U0.data_V_V_blk_n | (~AESL_inst_myproject.start_for_normalize_me_ap_fixed_ap_fixed_32_16_5_3_0_confBRe_U.if_empty_n & (AESL_inst_myproject.normalize_me_ap_fixed_ap_fixed_32_16_5_3_0_config3_U0.ap_ready | AESL_inst_myproject$normalize_me_ap_fixed_ap_fixed_32_16_5_3_0_config3_U0$ap_idle)));
    assign proc_dep_vld_vec_2[1] = dl_detect_out ? proc_dep_vld_vec_2_reg[1] : (~AESL_inst_myproject.normalize_me_ap_fixed_ap_fixed_32_16_5_3_0_config3_U0.res_V_V_blk_n | (~AESL_inst_myproject.start_for_zeropad2d_cl_me_ap_fixed_ap_fixed_config56_U0_U.if_full_n & AESL_inst_myproject.zeropad2d_cl_me_ap_fixed_ap_fixed_config56_U0.ap_done));
    always @ (negedge reset or posedge clock) begin
        if (~reset) begin
            proc_dep_vld_vec_2_reg <= 'b0;
        end
        else begin
            proc_dep_vld_vec_2_reg <= proc_dep_vld_vec_2;
        end
    end
    assign in_chan_dep_vld_vec_2[0] = dep_chan_vld_1_2;
    assign in_chan_dep_data_vec_2[39 : 0] = dep_chan_data_1_2;
    assign token_in_vec_2[0] = token_1_2;
    assign in_chan_dep_vld_vec_2[1] = dep_chan_vld_3_2;
    assign in_chan_dep_data_vec_2[79 : 40] = dep_chan_data_3_2;
    assign token_in_vec_2[1] = token_3_2;
    assign dep_chan_vld_2_1 = out_chan_dep_vld_vec_2[0];
    assign dep_chan_data_2_1 = out_chan_dep_data_2;
    assign token_2_1 = token_out_vec_2[0];
    assign dep_chan_vld_2_3 = out_chan_dep_vld_vec_2[1];
    assign dep_chan_data_2_3 = out_chan_dep_data_2;
    assign token_2_3 = token_out_vec_2[1];

    // delay ap_idle for one cycle
    reg [0:0] AESL_inst_myproject$zeropad2d_cl_me_ap_fixed_ap_fixed_config56_U0$ap_idle;
    always @ (negedge reset or posedge clock) begin
        if (~reset) begin
            AESL_inst_myproject$zeropad2d_cl_me_ap_fixed_ap_fixed_config56_U0$ap_idle <= 'b0;
        end
        else begin
            AESL_inst_myproject$zeropad2d_cl_me_ap_fixed_ap_fixed_config56_U0$ap_idle <= AESL_inst_myproject.zeropad2d_cl_me_ap_fixed_ap_fixed_config56_U0.ap_idle;
        end
    end
    // Process: AESL_inst_myproject.zeropad2d_cl_me_ap_fixed_ap_fixed_config56_U0
    AESL_deadlock_detect_unit #(40, 3, 2, 2) AESL_deadlock_detect_unit_3 (
        .reset(reset),
        .clock(clock),
        .proc_dep_vld_vec(proc_dep_vld_vec_3),
        .in_chan_dep_vld_vec(in_chan_dep_vld_vec_3),
        .in_chan_dep_data_vec(in_chan_dep_data_vec_3),
        .token_in_vec(token_in_vec_3),
        .dl_detect_in(dl_detect_out),
        .origin(origin[3]),
        .token_clear(token_clear),
        .out_chan_dep_vld_vec(out_chan_dep_vld_vec_3),
        .out_chan_dep_data(out_chan_dep_data_3),
        .token_out_vec(token_out_vec_3),
        .dl_detect_out(dl_in_vec[3]));

    assign proc_dep_vld_vec_3[0] = dl_detect_out ? proc_dep_vld_vec_3_reg[0] : (~AESL_inst_myproject.zeropad2d_cl_me_ap_fixed_ap_fixed_config56_U0.data_V_V_blk_n | (~AESL_inst_myproject.start_for_zeropad2d_cl_me_ap_fixed_ap_fixed_config56_U0_U.if_empty_n & (AESL_inst_myproject.zeropad2d_cl_me_ap_fixed_ap_fixed_config56_U0.ap_ready | AESL_inst_myproject$zeropad2d_cl_me_ap_fixed_ap_fixed_config56_U0$ap_idle)));
    assign proc_dep_vld_vec_3[1] = dl_detect_out ? proc_dep_vld_vec_3_reg[1] : (~AESL_inst_myproject.zeropad2d_cl_me_ap_fixed_ap_fixed_config56_U0.res_V_V_blk_n | (~AESL_inst_myproject.start_for_conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_conBSe_U.if_full_n & AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config4_U0.ap_done));
    always @ (negedge reset or posedge clock) begin
        if (~reset) begin
            proc_dep_vld_vec_3_reg <= 'b0;
        end
        else begin
            proc_dep_vld_vec_3_reg <= proc_dep_vld_vec_3;
        end
    end
    assign in_chan_dep_vld_vec_3[0] = dep_chan_vld_2_3;
    assign in_chan_dep_data_vec_3[39 : 0] = dep_chan_data_2_3;
    assign token_in_vec_3[0] = token_2_3;
    assign in_chan_dep_vld_vec_3[1] = dep_chan_vld_4_3;
    assign in_chan_dep_data_vec_3[79 : 40] = dep_chan_data_4_3;
    assign token_in_vec_3[1] = token_4_3;
    assign dep_chan_vld_3_2 = out_chan_dep_vld_vec_3[0];
    assign dep_chan_data_3_2 = out_chan_dep_data_3;
    assign token_3_2 = token_out_vec_3[0];
    assign dep_chan_vld_3_4 = out_chan_dep_vld_vec_3[1];
    assign dep_chan_data_3_4 = out_chan_dep_data_3;
    assign token_3_4 = token_out_vec_3[1];

    // delay ap_idle for one cycle
    reg [0:0] AESL_inst_myproject$conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config4_U0$ap_idle;
    always @ (negedge reset or posedge clock) begin
        if (~reset) begin
            AESL_inst_myproject$conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config4_U0$ap_idle <= 'b0;
        end
        else begin
            AESL_inst_myproject$conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config4_U0$ap_idle <= AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config4_U0.ap_idle;
        end
    end
    // Process: AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config4_U0
    AESL_deadlock_detect_unit #(40, 4, 2, 2) AESL_deadlock_detect_unit_4 (
        .reset(reset),
        .clock(clock),
        .proc_dep_vld_vec(proc_dep_vld_vec_4),
        .in_chan_dep_vld_vec(in_chan_dep_vld_vec_4),
        .in_chan_dep_data_vec(in_chan_dep_data_vec_4),
        .token_in_vec(token_in_vec_4),
        .dl_detect_in(dl_detect_out),
        .origin(origin[4]),
        .token_clear(token_clear),
        .out_chan_dep_vld_vec(out_chan_dep_vld_vec_4),
        .out_chan_dep_data(out_chan_dep_data_4),
        .token_out_vec(token_out_vec_4),
        .dl_detect_out(dl_in_vec[4]));

    assign proc_dep_vld_vec_4[0] = dl_detect_out ? proc_dep_vld_vec_4_reg[0] : (~AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config4_U0.data_V_V_blk_n | (~AESL_inst_myproject.start_for_conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_conBSe_U.if_empty_n & (AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config4_U0.ap_ready | AESL_inst_myproject$conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config4_U0$ap_idle)));
    assign proc_dep_vld_vec_4[1] = dl_detect_out ? proc_dep_vld_vec_4_reg[1] : (~AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config4_U0.res_V_V_blk_n | (~AESL_inst_myproject.start_for_leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_confiBTe_U.if_full_n & AESL_inst_myproject.leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_config7_573_U0.ap_done));
    always @ (negedge reset or posedge clock) begin
        if (~reset) begin
            proc_dep_vld_vec_4_reg <= 'b0;
        end
        else begin
            proc_dep_vld_vec_4_reg <= proc_dep_vld_vec_4;
        end
    end
    assign in_chan_dep_vld_vec_4[0] = dep_chan_vld_3_4;
    assign in_chan_dep_data_vec_4[39 : 0] = dep_chan_data_3_4;
    assign token_in_vec_4[0] = token_3_4;
    assign in_chan_dep_vld_vec_4[1] = dep_chan_vld_5_4;
    assign in_chan_dep_data_vec_4[79 : 40] = dep_chan_data_5_4;
    assign token_in_vec_4[1] = token_5_4;
    assign dep_chan_vld_4_3 = out_chan_dep_vld_vec_4[0];
    assign dep_chan_data_4_3 = out_chan_dep_data_4;
    assign token_4_3 = token_out_vec_4[0];
    assign dep_chan_vld_4_5 = out_chan_dep_vld_vec_4[1];
    assign dep_chan_data_4_5 = out_chan_dep_data_4;
    assign token_4_5 = token_out_vec_4[1];

    // delay ap_idle for one cycle
    reg [0:0] AESL_inst_myproject$leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_config7_573_U0$ap_idle;
    always @ (negedge reset or posedge clock) begin
        if (~reset) begin
            AESL_inst_myproject$leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_config7_573_U0$ap_idle <= 'b0;
        end
        else begin
            AESL_inst_myproject$leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_config7_573_U0$ap_idle <= AESL_inst_myproject.leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_config7_573_U0.ap_idle;
        end
    end
    // Process: AESL_inst_myproject.leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_config7_573_U0
    AESL_deadlock_detect_unit #(40, 5, 2, 2) AESL_deadlock_detect_unit_5 (
        .reset(reset),
        .clock(clock),
        .proc_dep_vld_vec(proc_dep_vld_vec_5),
        .in_chan_dep_vld_vec(in_chan_dep_vld_vec_5),
        .in_chan_dep_data_vec(in_chan_dep_data_vec_5),
        .token_in_vec(token_in_vec_5),
        .dl_detect_in(dl_detect_out),
        .origin(origin[5]),
        .token_clear(token_clear),
        .out_chan_dep_vld_vec(out_chan_dep_vld_vec_5),
        .out_chan_dep_data(out_chan_dep_data_5),
        .token_out_vec(token_out_vec_5),
        .dl_detect_out(dl_in_vec[5]));

    assign proc_dep_vld_vec_5[0] = dl_detect_out ? proc_dep_vld_vec_5_reg[0] : (~AESL_inst_myproject.leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_config7_573_U0.data_V_V_blk_n | (~AESL_inst_myproject.start_for_leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_confiBTe_U.if_empty_n & (AESL_inst_myproject.leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_config7_573_U0.ap_ready | AESL_inst_myproject$leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_config7_573_U0$ap_idle)));
    assign proc_dep_vld_vec_5[1] = dl_detect_out ? proc_dep_vld_vec_5_reg[1] : (~AESL_inst_myproject.leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_config7_573_U0.res_V_V_blk_n | (~AESL_inst_myproject.start_for_pooling2d_large_cl_nopad_pad_me_ap_fixed_ap_fixBUe_U.if_full_n & AESL_inst_myproject.pooling2d_large_cl_nopad_pad_me_ap_fixed_ap_fixed_config8_U0.ap_done));
    always @ (negedge reset or posedge clock) begin
        if (~reset) begin
            proc_dep_vld_vec_5_reg <= 'b0;
        end
        else begin
            proc_dep_vld_vec_5_reg <= proc_dep_vld_vec_5;
        end
    end
    assign in_chan_dep_vld_vec_5[0] = dep_chan_vld_4_5;
    assign in_chan_dep_data_vec_5[39 : 0] = dep_chan_data_4_5;
    assign token_in_vec_5[0] = token_4_5;
    assign in_chan_dep_vld_vec_5[1] = dep_chan_vld_6_5;
    assign in_chan_dep_data_vec_5[79 : 40] = dep_chan_data_6_5;
    assign token_in_vec_5[1] = token_6_5;
    assign dep_chan_vld_5_4 = out_chan_dep_vld_vec_5[0];
    assign dep_chan_data_5_4 = out_chan_dep_data_5;
    assign token_5_4 = token_out_vec_5[0];
    assign dep_chan_vld_5_6 = out_chan_dep_vld_vec_5[1];
    assign dep_chan_data_5_6 = out_chan_dep_data_5;
    assign token_5_6 = token_out_vec_5[1];

    // delay ap_idle for one cycle
    reg [0:0] AESL_inst_myproject$pooling2d_large_cl_nopad_pad_me_ap_fixed_ap_fixed_config8_U0$ap_idle;
    always @ (negedge reset or posedge clock) begin
        if (~reset) begin
            AESL_inst_myproject$pooling2d_large_cl_nopad_pad_me_ap_fixed_ap_fixed_config8_U0$ap_idle <= 'b0;
        end
        else begin
            AESL_inst_myproject$pooling2d_large_cl_nopad_pad_me_ap_fixed_ap_fixed_config8_U0$ap_idle <= AESL_inst_myproject.pooling2d_large_cl_nopad_pad_me_ap_fixed_ap_fixed_config8_U0.ap_idle;
        end
    end
    // Process: AESL_inst_myproject.pooling2d_large_cl_nopad_pad_me_ap_fixed_ap_fixed_config8_U0
    AESL_deadlock_detect_unit #(40, 6, 2, 2) AESL_deadlock_detect_unit_6 (
        .reset(reset),
        .clock(clock),
        .proc_dep_vld_vec(proc_dep_vld_vec_6),
        .in_chan_dep_vld_vec(in_chan_dep_vld_vec_6),
        .in_chan_dep_data_vec(in_chan_dep_data_vec_6),
        .token_in_vec(token_in_vec_6),
        .dl_detect_in(dl_detect_out),
        .origin(origin[6]),
        .token_clear(token_clear),
        .out_chan_dep_vld_vec(out_chan_dep_vld_vec_6),
        .out_chan_dep_data(out_chan_dep_data_6),
        .token_out_vec(token_out_vec_6),
        .dl_detect_out(dl_in_vec[6]));

    assign proc_dep_vld_vec_6[0] = dl_detect_out ? proc_dep_vld_vec_6_reg[0] : (~AESL_inst_myproject.pooling2d_large_cl_nopad_pad_me_ap_fixed_ap_fixed_config8_U0.data_V_V_blk_n | (~AESL_inst_myproject.start_for_pooling2d_large_cl_nopad_pad_me_ap_fixed_ap_fixBUe_U.if_empty_n & (AESL_inst_myproject.pooling2d_large_cl_nopad_pad_me_ap_fixed_ap_fixed_config8_U0.ap_ready | AESL_inst_myproject$pooling2d_large_cl_nopad_pad_me_ap_fixed_ap_fixed_config8_U0$ap_idle)));
    assign proc_dep_vld_vec_6[1] = dl_detect_out ? proc_dep_vld_vec_6_reg[1] : (~AESL_inst_myproject.pooling2d_large_cl_nopad_pad_me_ap_fixed_ap_fixed_config8_U0.res_V_V_blk_n | (~AESL_inst_myproject.start_for_zeropad2d_cl_me_ap_fixed_ap_fixed_config57_U0_U.if_full_n & AESL_inst_myproject.zeropad2d_cl_me_ap_fixed_ap_fixed_config57_U0.ap_done));
    always @ (negedge reset or posedge clock) begin
        if (~reset) begin
            proc_dep_vld_vec_6_reg <= 'b0;
        end
        else begin
            proc_dep_vld_vec_6_reg <= proc_dep_vld_vec_6;
        end
    end
    assign in_chan_dep_vld_vec_6[0] = dep_chan_vld_5_6;
    assign in_chan_dep_data_vec_6[39 : 0] = dep_chan_data_5_6;
    assign token_in_vec_6[0] = token_5_6;
    assign in_chan_dep_vld_vec_6[1] = dep_chan_vld_7_6;
    assign in_chan_dep_data_vec_6[79 : 40] = dep_chan_data_7_6;
    assign token_in_vec_6[1] = token_7_6;
    assign dep_chan_vld_6_5 = out_chan_dep_vld_vec_6[0];
    assign dep_chan_data_6_5 = out_chan_dep_data_6;
    assign token_6_5 = token_out_vec_6[0];
    assign dep_chan_vld_6_7 = out_chan_dep_vld_vec_6[1];
    assign dep_chan_data_6_7 = out_chan_dep_data_6;
    assign token_6_7 = token_out_vec_6[1];

    // delay ap_idle for one cycle
    reg [0:0] AESL_inst_myproject$zeropad2d_cl_me_ap_fixed_ap_fixed_config57_U0$ap_idle;
    always @ (negedge reset or posedge clock) begin
        if (~reset) begin
            AESL_inst_myproject$zeropad2d_cl_me_ap_fixed_ap_fixed_config57_U0$ap_idle <= 'b0;
        end
        else begin
            AESL_inst_myproject$zeropad2d_cl_me_ap_fixed_ap_fixed_config57_U0$ap_idle <= AESL_inst_myproject.zeropad2d_cl_me_ap_fixed_ap_fixed_config57_U0.ap_idle;
        end
    end
    // Process: AESL_inst_myproject.zeropad2d_cl_me_ap_fixed_ap_fixed_config57_U0
    AESL_deadlock_detect_unit #(40, 7, 2, 2) AESL_deadlock_detect_unit_7 (
        .reset(reset),
        .clock(clock),
        .proc_dep_vld_vec(proc_dep_vld_vec_7),
        .in_chan_dep_vld_vec(in_chan_dep_vld_vec_7),
        .in_chan_dep_data_vec(in_chan_dep_data_vec_7),
        .token_in_vec(token_in_vec_7),
        .dl_detect_in(dl_detect_out),
        .origin(origin[7]),
        .token_clear(token_clear),
        .out_chan_dep_vld_vec(out_chan_dep_vld_vec_7),
        .out_chan_dep_data(out_chan_dep_data_7),
        .token_out_vec(token_out_vec_7),
        .dl_detect_out(dl_in_vec[7]));

    assign proc_dep_vld_vec_7[0] = dl_detect_out ? proc_dep_vld_vec_7_reg[0] : (~AESL_inst_myproject.zeropad2d_cl_me_ap_fixed_ap_fixed_config57_U0.data_V_V_blk_n | (~AESL_inst_myproject.start_for_zeropad2d_cl_me_ap_fixed_ap_fixed_config57_U0_U.if_empty_n & (AESL_inst_myproject.zeropad2d_cl_me_ap_fixed_ap_fixed_config57_U0.ap_ready | AESL_inst_myproject$zeropad2d_cl_me_ap_fixed_ap_fixed_config57_U0$ap_idle)));
    assign proc_dep_vld_vec_7[1] = dl_detect_out ? proc_dep_vld_vec_7_reg[1] : (~AESL_inst_myproject.zeropad2d_cl_me_ap_fixed_ap_fixed_config57_U0.res_V_V_blk_n | (~AESL_inst_myproject.start_for_conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_conBVe_U.if_full_n & AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config9_U0.ap_done));
    always @ (negedge reset or posedge clock) begin
        if (~reset) begin
            proc_dep_vld_vec_7_reg <= 'b0;
        end
        else begin
            proc_dep_vld_vec_7_reg <= proc_dep_vld_vec_7;
        end
    end
    assign in_chan_dep_vld_vec_7[0] = dep_chan_vld_6_7;
    assign in_chan_dep_data_vec_7[39 : 0] = dep_chan_data_6_7;
    assign token_in_vec_7[0] = token_6_7;
    assign in_chan_dep_vld_vec_7[1] = dep_chan_vld_8_7;
    assign in_chan_dep_data_vec_7[79 : 40] = dep_chan_data_8_7;
    assign token_in_vec_7[1] = token_8_7;
    assign dep_chan_vld_7_6 = out_chan_dep_vld_vec_7[0];
    assign dep_chan_data_7_6 = out_chan_dep_data_7;
    assign token_7_6 = token_out_vec_7[0];
    assign dep_chan_vld_7_8 = out_chan_dep_vld_vec_7[1];
    assign dep_chan_data_7_8 = out_chan_dep_data_7;
    assign token_7_8 = token_out_vec_7[1];

    // delay ap_idle for one cycle
    reg [0:0] AESL_inst_myproject$conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config9_U0$ap_idle;
    always @ (negedge reset or posedge clock) begin
        if (~reset) begin
            AESL_inst_myproject$conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config9_U0$ap_idle <= 'b0;
        end
        else begin
            AESL_inst_myproject$conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config9_U0$ap_idle <= AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config9_U0.ap_idle;
        end
    end
    // Process: AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config9_U0
    AESL_deadlock_detect_unit #(40, 8, 2, 2) AESL_deadlock_detect_unit_8 (
        .reset(reset),
        .clock(clock),
        .proc_dep_vld_vec(proc_dep_vld_vec_8),
        .in_chan_dep_vld_vec(in_chan_dep_vld_vec_8),
        .in_chan_dep_data_vec(in_chan_dep_data_vec_8),
        .token_in_vec(token_in_vec_8),
        .dl_detect_in(dl_detect_out),
        .origin(origin[8]),
        .token_clear(token_clear),
        .out_chan_dep_vld_vec(out_chan_dep_vld_vec_8),
        .out_chan_dep_data(out_chan_dep_data_8),
        .token_out_vec(token_out_vec_8),
        .dl_detect_out(dl_in_vec[8]));

    assign proc_dep_vld_vec_8[0] = dl_detect_out ? proc_dep_vld_vec_8_reg[0] : (~AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config9_U0.data_V_V_blk_n | (~AESL_inst_myproject.start_for_conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_conBVe_U.if_empty_n & (AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config9_U0.ap_ready | AESL_inst_myproject$conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config9_U0$ap_idle)));
    assign proc_dep_vld_vec_8[1] = dl_detect_out ? proc_dep_vld_vec_8_reg[1] : (~AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config9_U0.res_V_V_blk_n | (~AESL_inst_myproject.start_for_leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_confiBWe_U.if_full_n & AESL_inst_myproject.leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_config12_574_U0.ap_done));
    always @ (negedge reset or posedge clock) begin
        if (~reset) begin
            proc_dep_vld_vec_8_reg <= 'b0;
        end
        else begin
            proc_dep_vld_vec_8_reg <= proc_dep_vld_vec_8;
        end
    end
    assign in_chan_dep_vld_vec_8[0] = dep_chan_vld_7_8;
    assign in_chan_dep_data_vec_8[39 : 0] = dep_chan_data_7_8;
    assign token_in_vec_8[0] = token_7_8;
    assign in_chan_dep_vld_vec_8[1] = dep_chan_vld_9_8;
    assign in_chan_dep_data_vec_8[79 : 40] = dep_chan_data_9_8;
    assign token_in_vec_8[1] = token_9_8;
    assign dep_chan_vld_8_7 = out_chan_dep_vld_vec_8[0];
    assign dep_chan_data_8_7 = out_chan_dep_data_8;
    assign token_8_7 = token_out_vec_8[0];
    assign dep_chan_vld_8_9 = out_chan_dep_vld_vec_8[1];
    assign dep_chan_data_8_9 = out_chan_dep_data_8;
    assign token_8_9 = token_out_vec_8[1];

    // delay ap_idle for one cycle
    reg [0:0] AESL_inst_myproject$leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_config12_574_U0$ap_idle;
    always @ (negedge reset or posedge clock) begin
        if (~reset) begin
            AESL_inst_myproject$leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_config12_574_U0$ap_idle <= 'b0;
        end
        else begin
            AESL_inst_myproject$leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_config12_574_U0$ap_idle <= AESL_inst_myproject.leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_config12_574_U0.ap_idle;
        end
    end
    // Process: AESL_inst_myproject.leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_config12_574_U0
    AESL_deadlock_detect_unit #(40, 9, 2, 2) AESL_deadlock_detect_unit_9 (
        .reset(reset),
        .clock(clock),
        .proc_dep_vld_vec(proc_dep_vld_vec_9),
        .in_chan_dep_vld_vec(in_chan_dep_vld_vec_9),
        .in_chan_dep_data_vec(in_chan_dep_data_vec_9),
        .token_in_vec(token_in_vec_9),
        .dl_detect_in(dl_detect_out),
        .origin(origin[9]),
        .token_clear(token_clear),
        .out_chan_dep_vld_vec(out_chan_dep_vld_vec_9),
        .out_chan_dep_data(out_chan_dep_data_9),
        .token_out_vec(token_out_vec_9),
        .dl_detect_out(dl_in_vec[9]));

    assign proc_dep_vld_vec_9[0] = dl_detect_out ? proc_dep_vld_vec_9_reg[0] : (~AESL_inst_myproject.leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_config12_574_U0.data_V_V_blk_n | (~AESL_inst_myproject.start_for_leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_confiBWe_U.if_empty_n & (AESL_inst_myproject.leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_config12_574_U0.ap_ready | AESL_inst_myproject$leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_config12_574_U0$ap_idle)));
    assign proc_dep_vld_vec_9[1] = dl_detect_out ? proc_dep_vld_vec_9_reg[1] : (~AESL_inst_myproject.leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_config12_574_U0.res_V_V_blk_n | (~AESL_inst_myproject.start_for_zeropad2d_cl_me_ap_fixed_ap_fixed_config58_U0_U.if_full_n & AESL_inst_myproject.zeropad2d_cl_me_ap_fixed_ap_fixed_config58_U0.ap_done));
    always @ (negedge reset or posedge clock) begin
        if (~reset) begin
            proc_dep_vld_vec_9_reg <= 'b0;
        end
        else begin
            proc_dep_vld_vec_9_reg <= proc_dep_vld_vec_9;
        end
    end
    assign in_chan_dep_vld_vec_9[0] = dep_chan_vld_8_9;
    assign in_chan_dep_data_vec_9[39 : 0] = dep_chan_data_8_9;
    assign token_in_vec_9[0] = token_8_9;
    assign in_chan_dep_vld_vec_9[1] = dep_chan_vld_10_9;
    assign in_chan_dep_data_vec_9[79 : 40] = dep_chan_data_10_9;
    assign token_in_vec_9[1] = token_10_9;
    assign dep_chan_vld_9_8 = out_chan_dep_vld_vec_9[0];
    assign dep_chan_data_9_8 = out_chan_dep_data_9;
    assign token_9_8 = token_out_vec_9[0];
    assign dep_chan_vld_9_10 = out_chan_dep_vld_vec_9[1];
    assign dep_chan_data_9_10 = out_chan_dep_data_9;
    assign token_9_10 = token_out_vec_9[1];

    // delay ap_idle for one cycle
    reg [0:0] AESL_inst_myproject$zeropad2d_cl_me_ap_fixed_ap_fixed_config58_U0$ap_idle;
    always @ (negedge reset or posedge clock) begin
        if (~reset) begin
            AESL_inst_myproject$zeropad2d_cl_me_ap_fixed_ap_fixed_config58_U0$ap_idle <= 'b0;
        end
        else begin
            AESL_inst_myproject$zeropad2d_cl_me_ap_fixed_ap_fixed_config58_U0$ap_idle <= AESL_inst_myproject.zeropad2d_cl_me_ap_fixed_ap_fixed_config58_U0.ap_idle;
        end
    end
    // Process: AESL_inst_myproject.zeropad2d_cl_me_ap_fixed_ap_fixed_config58_U0
    AESL_deadlock_detect_unit #(40, 10, 2, 2) AESL_deadlock_detect_unit_10 (
        .reset(reset),
        .clock(clock),
        .proc_dep_vld_vec(proc_dep_vld_vec_10),
        .in_chan_dep_vld_vec(in_chan_dep_vld_vec_10),
        .in_chan_dep_data_vec(in_chan_dep_data_vec_10),
        .token_in_vec(token_in_vec_10),
        .dl_detect_in(dl_detect_out),
        .origin(origin[10]),
        .token_clear(token_clear),
        .out_chan_dep_vld_vec(out_chan_dep_vld_vec_10),
        .out_chan_dep_data(out_chan_dep_data_10),
        .token_out_vec(token_out_vec_10),
        .dl_detect_out(dl_in_vec[10]));

    assign proc_dep_vld_vec_10[0] = dl_detect_out ? proc_dep_vld_vec_10_reg[0] : (~AESL_inst_myproject.zeropad2d_cl_me_ap_fixed_ap_fixed_config58_U0.data_V_V_blk_n | (~AESL_inst_myproject.start_for_zeropad2d_cl_me_ap_fixed_ap_fixed_config58_U0_U.if_empty_n & (AESL_inst_myproject.zeropad2d_cl_me_ap_fixed_ap_fixed_config58_U0.ap_ready | AESL_inst_myproject$zeropad2d_cl_me_ap_fixed_ap_fixed_config58_U0$ap_idle)));
    assign proc_dep_vld_vec_10[1] = dl_detect_out ? proc_dep_vld_vec_10_reg[1] : (~AESL_inst_myproject.zeropad2d_cl_me_ap_fixed_ap_fixed_config58_U0.res_V_V_blk_n);
    always @ (negedge reset or posedge clock) begin
        if (~reset) begin
            proc_dep_vld_vec_10_reg <= 'b0;
        end
        else begin
            proc_dep_vld_vec_10_reg <= proc_dep_vld_vec_10;
        end
    end
    assign in_chan_dep_vld_vec_10[0] = dep_chan_vld_9_10;
    assign in_chan_dep_data_vec_10[39 : 0] = dep_chan_data_9_10;
    assign token_in_vec_10[0] = token_9_10;
    assign in_chan_dep_vld_vec_10[1] = dep_chan_vld_11_10;
    assign in_chan_dep_data_vec_10[79 : 40] = dep_chan_data_11_10;
    assign token_in_vec_10[1] = token_11_10;
    assign dep_chan_vld_10_9 = out_chan_dep_vld_vec_10[0];
    assign dep_chan_data_10_9 = out_chan_dep_data_10;
    assign token_10_9 = token_out_vec_10[0];
    assign dep_chan_vld_10_11 = out_chan_dep_vld_vec_10[1];
    assign dep_chan_data_10_11 = out_chan_dep_data_10;
    assign token_10_11 = token_out_vec_10[1];

    // delay ap_idle for one cycle
    reg [0:0] AESL_inst_myproject$conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config13_U0$ap_idle;
    always @ (negedge reset or posedge clock) begin
        if (~reset) begin
            AESL_inst_myproject$conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config13_U0$ap_idle <= 'b0;
        end
        else begin
            AESL_inst_myproject$conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config13_U0$ap_idle <= AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config13_U0.ap_idle;
        end
    end
    // Process: AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config13_U0
    AESL_deadlock_detect_unit #(40, 11, 12, 12) AESL_deadlock_detect_unit_11 (
        .reset(reset),
        .clock(clock),
        .proc_dep_vld_vec(proc_dep_vld_vec_11),
        .in_chan_dep_vld_vec(in_chan_dep_vld_vec_11),
        .in_chan_dep_data_vec(in_chan_dep_data_vec_11),
        .token_in_vec(token_in_vec_11),
        .dl_detect_in(dl_detect_out),
        .origin(origin[11]),
        .token_clear(token_clear),
        .out_chan_dep_vld_vec(out_chan_dep_vld_vec_11),
        .out_chan_dep_data(out_chan_dep_data_11),
        .token_out_vec(token_out_vec_11),
        .dl_detect_out(dl_in_vec[11]));

    assign proc_dep_vld_vec_11[0] = dl_detect_out ? proc_dep_vld_vec_11_reg[0] : (~AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config13_U0.data_V_V_blk_n);
    assign proc_dep_vld_vec_11[1] = dl_detect_out ? proc_dep_vld_vec_11_reg[1] : (~AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config13_U0.res_V_V_blk_n | (~AESL_inst_myproject.start_for_leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_confiBXe_U.if_full_n & AESL_inst_myproject.leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_config16_575_U0.ap_done));
    assign proc_dep_vld_vec_11[2] = dl_detect_out ? proc_dep_vld_vec_11_reg[2] : (((AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config13_U0_ap_ready_count[0]) & AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config13_U0.ap_idle & ~(AESL_inst_myproject.Block_ap_fixed_base_exit3121_proc_U0_ap_ready_count[0])));
    assign proc_dep_vld_vec_11[3] = dl_detect_out ? proc_dep_vld_vec_11_reg[3] : (((AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config13_U0_ap_ready_count[0]) & AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config13_U0.ap_idle & ~(AESL_inst_myproject.resize_nearest_me_ap_fixed_32_16_5_3_0_config2_U0_ap_ready_count[0])));
    assign proc_dep_vld_vec_11[4] = dl_detect_out ? proc_dep_vld_vec_11_reg[4] : (((AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config13_U0_ap_ready_count[0]) & AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config13_U0.ap_idle & ~(AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config18_U0_ap_ready_count[0])));
    assign proc_dep_vld_vec_11[5] = dl_detect_out ? proc_dep_vld_vec_11_reg[5] : (((AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config13_U0_ap_ready_count[0]) & AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config13_U0.ap_idle & ~(AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config22_U0_ap_ready_count[0])));
    assign proc_dep_vld_vec_11[6] = dl_detect_out ? proc_dep_vld_vec_11_reg[6] : (((AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config13_U0_ap_ready_count[0]) & AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config13_U0.ap_idle & ~(AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config27_U0_ap_ready_count[0])));
    assign proc_dep_vld_vec_11[7] = dl_detect_out ? proc_dep_vld_vec_11_reg[7] : (((AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config13_U0_ap_ready_count[0]) & AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config13_U0.ap_idle & ~(AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config31_U0_ap_ready_count[0])));
    assign proc_dep_vld_vec_11[8] = dl_detect_out ? proc_dep_vld_vec_11_reg[8] : (((AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config13_U0_ap_ready_count[0]) & AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config13_U0.ap_idle & ~(AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config36_U0_ap_ready_count[0])));
    assign proc_dep_vld_vec_11[9] = dl_detect_out ? proc_dep_vld_vec_11_reg[9] : (((AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config13_U0_ap_ready_count[0]) & AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config13_U0.ap_idle & ~(AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config40_U0_ap_ready_count[0])));
    assign proc_dep_vld_vec_11[10] = dl_detect_out ? proc_dep_vld_vec_11_reg[10] : (((AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config13_U0_ap_ready_count[0]) & AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config13_U0.ap_idle & ~(AESL_inst_myproject.dense_large_stream_me_ap_fixed_ap_fixed_config45_U0_ap_ready_count[0])));
    assign proc_dep_vld_vec_11[11] = dl_detect_out ? proc_dep_vld_vec_11_reg[11] : (((AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config13_U0_ap_ready_count[0]) & AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config13_U0.ap_idle & ~(AESL_inst_myproject.dense_large_stream_me_ap_fixed_ap_fixed_config49_U0_ap_ready_count[0])));
    always @ (negedge reset or posedge clock) begin
        if (~reset) begin
            proc_dep_vld_vec_11_reg <= 'b0;
        end
        else begin
            proc_dep_vld_vec_11_reg <= proc_dep_vld_vec_11;
        end
    end
    assign in_chan_dep_vld_vec_11[0] = dep_chan_vld_0_11;
    assign in_chan_dep_data_vec_11[39 : 0] = dep_chan_data_0_11;
    assign token_in_vec_11[0] = token_0_11;
    assign in_chan_dep_vld_vec_11[1] = dep_chan_vld_1_11;
    assign in_chan_dep_data_vec_11[79 : 40] = dep_chan_data_1_11;
    assign token_in_vec_11[1] = token_1_11;
    assign in_chan_dep_vld_vec_11[2] = dep_chan_vld_10_11;
    assign in_chan_dep_data_vec_11[119 : 80] = dep_chan_data_10_11;
    assign token_in_vec_11[2] = token_10_11;
    assign in_chan_dep_vld_vec_11[3] = dep_chan_vld_12_11;
    assign in_chan_dep_data_vec_11[159 : 120] = dep_chan_data_12_11;
    assign token_in_vec_11[3] = token_12_11;
    assign in_chan_dep_vld_vec_11[4] = dep_chan_vld_15_11;
    assign in_chan_dep_data_vec_11[199 : 160] = dep_chan_data_15_11;
    assign token_in_vec_11[4] = token_15_11;
    assign in_chan_dep_vld_vec_11[5] = dep_chan_vld_18_11;
    assign in_chan_dep_data_vec_11[239 : 200] = dep_chan_data_18_11;
    assign token_in_vec_11[5] = token_18_11;
    assign in_chan_dep_vld_vec_11[6] = dep_chan_vld_22_11;
    assign in_chan_dep_data_vec_11[279 : 240] = dep_chan_data_22_11;
    assign token_in_vec_11[6] = token_22_11;
    assign in_chan_dep_vld_vec_11[7] = dep_chan_vld_25_11;
    assign in_chan_dep_data_vec_11[319 : 280] = dep_chan_data_25_11;
    assign token_in_vec_11[7] = token_25_11;
    assign in_chan_dep_vld_vec_11[8] = dep_chan_vld_29_11;
    assign in_chan_dep_data_vec_11[359 : 320] = dep_chan_data_29_11;
    assign token_in_vec_11[8] = token_29_11;
    assign in_chan_dep_vld_vec_11[9] = dep_chan_vld_32_11;
    assign in_chan_dep_data_vec_11[399 : 360] = dep_chan_data_32_11;
    assign token_in_vec_11[9] = token_32_11;
    assign in_chan_dep_vld_vec_11[10] = dep_chan_vld_34_11;
    assign in_chan_dep_data_vec_11[439 : 400] = dep_chan_data_34_11;
    assign token_in_vec_11[10] = token_34_11;
    assign in_chan_dep_vld_vec_11[11] = dep_chan_vld_36_11;
    assign in_chan_dep_data_vec_11[479 : 440] = dep_chan_data_36_11;
    assign token_in_vec_11[11] = token_36_11;
    assign dep_chan_vld_11_10 = out_chan_dep_vld_vec_11[0];
    assign dep_chan_data_11_10 = out_chan_dep_data_11;
    assign token_11_10 = token_out_vec_11[0];
    assign dep_chan_vld_11_12 = out_chan_dep_vld_vec_11[1];
    assign dep_chan_data_11_12 = out_chan_dep_data_11;
    assign token_11_12 = token_out_vec_11[1];
    assign dep_chan_vld_11_0 = out_chan_dep_vld_vec_11[2];
    assign dep_chan_data_11_0 = out_chan_dep_data_11;
    assign token_11_0 = token_out_vec_11[2];
    assign dep_chan_vld_11_1 = out_chan_dep_vld_vec_11[3];
    assign dep_chan_data_11_1 = out_chan_dep_data_11;
    assign token_11_1 = token_out_vec_11[3];
    assign dep_chan_vld_11_15 = out_chan_dep_vld_vec_11[4];
    assign dep_chan_data_11_15 = out_chan_dep_data_11;
    assign token_11_15 = token_out_vec_11[4];
    assign dep_chan_vld_11_18 = out_chan_dep_vld_vec_11[5];
    assign dep_chan_data_11_18 = out_chan_dep_data_11;
    assign token_11_18 = token_out_vec_11[5];
    assign dep_chan_vld_11_22 = out_chan_dep_vld_vec_11[6];
    assign dep_chan_data_11_22 = out_chan_dep_data_11;
    assign token_11_22 = token_out_vec_11[6];
    assign dep_chan_vld_11_25 = out_chan_dep_vld_vec_11[7];
    assign dep_chan_data_11_25 = out_chan_dep_data_11;
    assign token_11_25 = token_out_vec_11[7];
    assign dep_chan_vld_11_29 = out_chan_dep_vld_vec_11[8];
    assign dep_chan_data_11_29 = out_chan_dep_data_11;
    assign token_11_29 = token_out_vec_11[8];
    assign dep_chan_vld_11_32 = out_chan_dep_vld_vec_11[9];
    assign dep_chan_data_11_32 = out_chan_dep_data_11;
    assign token_11_32 = token_out_vec_11[9];
    assign dep_chan_vld_11_34 = out_chan_dep_vld_vec_11[10];
    assign dep_chan_data_11_34 = out_chan_dep_data_11;
    assign token_11_34 = token_out_vec_11[10];
    assign dep_chan_vld_11_36 = out_chan_dep_vld_vec_11[11];
    assign dep_chan_data_11_36 = out_chan_dep_data_11;
    assign token_11_36 = token_out_vec_11[11];

    // delay ap_idle for one cycle
    reg [0:0] AESL_inst_myproject$leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_config16_575_U0$ap_idle;
    always @ (negedge reset or posedge clock) begin
        if (~reset) begin
            AESL_inst_myproject$leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_config16_575_U0$ap_idle <= 'b0;
        end
        else begin
            AESL_inst_myproject$leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_config16_575_U0$ap_idle <= AESL_inst_myproject.leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_config16_575_U0.ap_idle;
        end
    end
    // Process: AESL_inst_myproject.leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_config16_575_U0
    AESL_deadlock_detect_unit #(40, 12, 2, 2) AESL_deadlock_detect_unit_12 (
        .reset(reset),
        .clock(clock),
        .proc_dep_vld_vec(proc_dep_vld_vec_12),
        .in_chan_dep_vld_vec(in_chan_dep_vld_vec_12),
        .in_chan_dep_data_vec(in_chan_dep_data_vec_12),
        .token_in_vec(token_in_vec_12),
        .dl_detect_in(dl_detect_out),
        .origin(origin[12]),
        .token_clear(token_clear),
        .out_chan_dep_vld_vec(out_chan_dep_vld_vec_12),
        .out_chan_dep_data(out_chan_dep_data_12),
        .token_out_vec(token_out_vec_12),
        .dl_detect_out(dl_in_vec[12]));

    assign proc_dep_vld_vec_12[0] = dl_detect_out ? proc_dep_vld_vec_12_reg[0] : (~AESL_inst_myproject.leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_config16_575_U0.data_V_V_blk_n | (~AESL_inst_myproject.start_for_leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_confiBXe_U.if_empty_n & (AESL_inst_myproject.leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_config16_575_U0.ap_ready | AESL_inst_myproject$leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_config16_575_U0$ap_idle)));
    assign proc_dep_vld_vec_12[1] = dl_detect_out ? proc_dep_vld_vec_12_reg[1] : (~AESL_inst_myproject.leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_config16_575_U0.res_V_V_blk_n | (~AESL_inst_myproject.start_for_pooling2d_large_cl_nopad_pad_me_2_U0_U.if_full_n & AESL_inst_myproject.pooling2d_large_cl_nopad_pad_me_2_U0.ap_done));
    always @ (negedge reset or posedge clock) begin
        if (~reset) begin
            proc_dep_vld_vec_12_reg <= 'b0;
        end
        else begin
            proc_dep_vld_vec_12_reg <= proc_dep_vld_vec_12;
        end
    end
    assign in_chan_dep_vld_vec_12[0] = dep_chan_vld_11_12;
    assign in_chan_dep_data_vec_12[39 : 0] = dep_chan_data_11_12;
    assign token_in_vec_12[0] = token_11_12;
    assign in_chan_dep_vld_vec_12[1] = dep_chan_vld_13_12;
    assign in_chan_dep_data_vec_12[79 : 40] = dep_chan_data_13_12;
    assign token_in_vec_12[1] = token_13_12;
    assign dep_chan_vld_12_11 = out_chan_dep_vld_vec_12[0];
    assign dep_chan_data_12_11 = out_chan_dep_data_12;
    assign token_12_11 = token_out_vec_12[0];
    assign dep_chan_vld_12_13 = out_chan_dep_vld_vec_12[1];
    assign dep_chan_data_12_13 = out_chan_dep_data_12;
    assign token_12_13 = token_out_vec_12[1];

    // delay ap_idle for one cycle
    reg [0:0] AESL_inst_myproject$pooling2d_large_cl_nopad_pad_me_2_U0$ap_idle;
    always @ (negedge reset or posedge clock) begin
        if (~reset) begin
            AESL_inst_myproject$pooling2d_large_cl_nopad_pad_me_2_U0$ap_idle <= 'b0;
        end
        else begin
            AESL_inst_myproject$pooling2d_large_cl_nopad_pad_me_2_U0$ap_idle <= AESL_inst_myproject.pooling2d_large_cl_nopad_pad_me_2_U0.ap_idle;
        end
    end
    // Process: AESL_inst_myproject.pooling2d_large_cl_nopad_pad_me_2_U0
    AESL_deadlock_detect_unit #(40, 13, 2, 2) AESL_deadlock_detect_unit_13 (
        .reset(reset),
        .clock(clock),
        .proc_dep_vld_vec(proc_dep_vld_vec_13),
        .in_chan_dep_vld_vec(in_chan_dep_vld_vec_13),
        .in_chan_dep_data_vec(in_chan_dep_data_vec_13),
        .token_in_vec(token_in_vec_13),
        .dl_detect_in(dl_detect_out),
        .origin(origin[13]),
        .token_clear(token_clear),
        .out_chan_dep_vld_vec(out_chan_dep_vld_vec_13),
        .out_chan_dep_data(out_chan_dep_data_13),
        .token_out_vec(token_out_vec_13),
        .dl_detect_out(dl_in_vec[13]));

    assign proc_dep_vld_vec_13[0] = dl_detect_out ? proc_dep_vld_vec_13_reg[0] : (~AESL_inst_myproject.pooling2d_large_cl_nopad_pad_me_2_U0.data_V_V_blk_n | (~AESL_inst_myproject.start_for_pooling2d_large_cl_nopad_pad_me_2_U0_U.if_empty_n & (AESL_inst_myproject.pooling2d_large_cl_nopad_pad_me_2_U0.ap_ready | AESL_inst_myproject$pooling2d_large_cl_nopad_pad_me_2_U0$ap_idle)));
    assign proc_dep_vld_vec_13[1] = dl_detect_out ? proc_dep_vld_vec_13_reg[1] : (~AESL_inst_myproject.pooling2d_large_cl_nopad_pad_me_2_U0.res_V_V_blk_n | (~AESL_inst_myproject.start_for_zeropad2d_cl_me_ap_fixed_ap_fixed_config59_U0_U.if_full_n & AESL_inst_myproject.zeropad2d_cl_me_ap_fixed_ap_fixed_config59_U0.ap_done));
    always @ (negedge reset or posedge clock) begin
        if (~reset) begin
            proc_dep_vld_vec_13_reg <= 'b0;
        end
        else begin
            proc_dep_vld_vec_13_reg <= proc_dep_vld_vec_13;
        end
    end
    assign in_chan_dep_vld_vec_13[0] = dep_chan_vld_12_13;
    assign in_chan_dep_data_vec_13[39 : 0] = dep_chan_data_12_13;
    assign token_in_vec_13[0] = token_12_13;
    assign in_chan_dep_vld_vec_13[1] = dep_chan_vld_14_13;
    assign in_chan_dep_data_vec_13[79 : 40] = dep_chan_data_14_13;
    assign token_in_vec_13[1] = token_14_13;
    assign dep_chan_vld_13_12 = out_chan_dep_vld_vec_13[0];
    assign dep_chan_data_13_12 = out_chan_dep_data_13;
    assign token_13_12 = token_out_vec_13[0];
    assign dep_chan_vld_13_14 = out_chan_dep_vld_vec_13[1];
    assign dep_chan_data_13_14 = out_chan_dep_data_13;
    assign token_13_14 = token_out_vec_13[1];

    // delay ap_idle for one cycle
    reg [0:0] AESL_inst_myproject$zeropad2d_cl_me_ap_fixed_ap_fixed_config59_U0$ap_idle;
    always @ (negedge reset or posedge clock) begin
        if (~reset) begin
            AESL_inst_myproject$zeropad2d_cl_me_ap_fixed_ap_fixed_config59_U0$ap_idle <= 'b0;
        end
        else begin
            AESL_inst_myproject$zeropad2d_cl_me_ap_fixed_ap_fixed_config59_U0$ap_idle <= AESL_inst_myproject.zeropad2d_cl_me_ap_fixed_ap_fixed_config59_U0.ap_idle;
        end
    end
    // Process: AESL_inst_myproject.zeropad2d_cl_me_ap_fixed_ap_fixed_config59_U0
    AESL_deadlock_detect_unit #(40, 14, 2, 2) AESL_deadlock_detect_unit_14 (
        .reset(reset),
        .clock(clock),
        .proc_dep_vld_vec(proc_dep_vld_vec_14),
        .in_chan_dep_vld_vec(in_chan_dep_vld_vec_14),
        .in_chan_dep_data_vec(in_chan_dep_data_vec_14),
        .token_in_vec(token_in_vec_14),
        .dl_detect_in(dl_detect_out),
        .origin(origin[14]),
        .token_clear(token_clear),
        .out_chan_dep_vld_vec(out_chan_dep_vld_vec_14),
        .out_chan_dep_data(out_chan_dep_data_14),
        .token_out_vec(token_out_vec_14),
        .dl_detect_out(dl_in_vec[14]));

    assign proc_dep_vld_vec_14[0] = dl_detect_out ? proc_dep_vld_vec_14_reg[0] : (~AESL_inst_myproject.zeropad2d_cl_me_ap_fixed_ap_fixed_config59_U0.data_V_V_blk_n | (~AESL_inst_myproject.start_for_zeropad2d_cl_me_ap_fixed_ap_fixed_config59_U0_U.if_empty_n & (AESL_inst_myproject.zeropad2d_cl_me_ap_fixed_ap_fixed_config59_U0.ap_ready | AESL_inst_myproject$zeropad2d_cl_me_ap_fixed_ap_fixed_config59_U0$ap_idle)));
    assign proc_dep_vld_vec_14[1] = dl_detect_out ? proc_dep_vld_vec_14_reg[1] : (~AESL_inst_myproject.zeropad2d_cl_me_ap_fixed_ap_fixed_config59_U0.res_V_V_blk_n);
    always @ (negedge reset or posedge clock) begin
        if (~reset) begin
            proc_dep_vld_vec_14_reg <= 'b0;
        end
        else begin
            proc_dep_vld_vec_14_reg <= proc_dep_vld_vec_14;
        end
    end
    assign in_chan_dep_vld_vec_14[0] = dep_chan_vld_13_14;
    assign in_chan_dep_data_vec_14[39 : 0] = dep_chan_data_13_14;
    assign token_in_vec_14[0] = token_13_14;
    assign in_chan_dep_vld_vec_14[1] = dep_chan_vld_15_14;
    assign in_chan_dep_data_vec_14[79 : 40] = dep_chan_data_15_14;
    assign token_in_vec_14[1] = token_15_14;
    assign dep_chan_vld_14_13 = out_chan_dep_vld_vec_14[0];
    assign dep_chan_data_14_13 = out_chan_dep_data_14;
    assign token_14_13 = token_out_vec_14[0];
    assign dep_chan_vld_14_15 = out_chan_dep_vld_vec_14[1];
    assign dep_chan_data_14_15 = out_chan_dep_data_14;
    assign token_14_15 = token_out_vec_14[1];

    // delay ap_idle for one cycle
    reg [0:0] AESL_inst_myproject$conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config18_U0$ap_idle;
    always @ (negedge reset or posedge clock) begin
        if (~reset) begin
            AESL_inst_myproject$conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config18_U0$ap_idle <= 'b0;
        end
        else begin
            AESL_inst_myproject$conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config18_U0$ap_idle <= AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config18_U0.ap_idle;
        end
    end
    // Process: AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config18_U0
    AESL_deadlock_detect_unit #(40, 15, 12, 12) AESL_deadlock_detect_unit_15 (
        .reset(reset),
        .clock(clock),
        .proc_dep_vld_vec(proc_dep_vld_vec_15),
        .in_chan_dep_vld_vec(in_chan_dep_vld_vec_15),
        .in_chan_dep_data_vec(in_chan_dep_data_vec_15),
        .token_in_vec(token_in_vec_15),
        .dl_detect_in(dl_detect_out),
        .origin(origin[15]),
        .token_clear(token_clear),
        .out_chan_dep_vld_vec(out_chan_dep_vld_vec_15),
        .out_chan_dep_data(out_chan_dep_data_15),
        .token_out_vec(token_out_vec_15),
        .dl_detect_out(dl_in_vec[15]));

    assign proc_dep_vld_vec_15[0] = dl_detect_out ? proc_dep_vld_vec_15_reg[0] : (~AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config18_U0.data_V_V_blk_n);
    assign proc_dep_vld_vec_15[1] = dl_detect_out ? proc_dep_vld_vec_15_reg[1] : (~AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config18_U0.res_V_V_blk_n | (~AESL_inst_myproject.start_for_leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_confiBYe_U.if_full_n & AESL_inst_myproject.leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_config21_576_U0.ap_done));
    assign proc_dep_vld_vec_15[2] = dl_detect_out ? proc_dep_vld_vec_15_reg[2] : (((AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config18_U0_ap_ready_count[0]) & AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config18_U0.ap_idle & ~(AESL_inst_myproject.Block_ap_fixed_base_exit3121_proc_U0_ap_ready_count[0])));
    assign proc_dep_vld_vec_15[3] = dl_detect_out ? proc_dep_vld_vec_15_reg[3] : (((AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config18_U0_ap_ready_count[0]) & AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config18_U0.ap_idle & ~(AESL_inst_myproject.resize_nearest_me_ap_fixed_32_16_5_3_0_config2_U0_ap_ready_count[0])));
    assign proc_dep_vld_vec_15[4] = dl_detect_out ? proc_dep_vld_vec_15_reg[4] : (((AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config18_U0_ap_ready_count[0]) & AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config18_U0.ap_idle & ~(AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config13_U0_ap_ready_count[0])));
    assign proc_dep_vld_vec_15[5] = dl_detect_out ? proc_dep_vld_vec_15_reg[5] : (((AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config18_U0_ap_ready_count[0]) & AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config18_U0.ap_idle & ~(AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config22_U0_ap_ready_count[0])));
    assign proc_dep_vld_vec_15[6] = dl_detect_out ? proc_dep_vld_vec_15_reg[6] : (((AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config18_U0_ap_ready_count[0]) & AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config18_U0.ap_idle & ~(AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config27_U0_ap_ready_count[0])));
    assign proc_dep_vld_vec_15[7] = dl_detect_out ? proc_dep_vld_vec_15_reg[7] : (((AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config18_U0_ap_ready_count[0]) & AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config18_U0.ap_idle & ~(AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config31_U0_ap_ready_count[0])));
    assign proc_dep_vld_vec_15[8] = dl_detect_out ? proc_dep_vld_vec_15_reg[8] : (((AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config18_U0_ap_ready_count[0]) & AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config18_U0.ap_idle & ~(AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config36_U0_ap_ready_count[0])));
    assign proc_dep_vld_vec_15[9] = dl_detect_out ? proc_dep_vld_vec_15_reg[9] : (((AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config18_U0_ap_ready_count[0]) & AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config18_U0.ap_idle & ~(AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config40_U0_ap_ready_count[0])));
    assign proc_dep_vld_vec_15[10] = dl_detect_out ? proc_dep_vld_vec_15_reg[10] : (((AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config18_U0_ap_ready_count[0]) & AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config18_U0.ap_idle & ~(AESL_inst_myproject.dense_large_stream_me_ap_fixed_ap_fixed_config45_U0_ap_ready_count[0])));
    assign proc_dep_vld_vec_15[11] = dl_detect_out ? proc_dep_vld_vec_15_reg[11] : (((AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config18_U0_ap_ready_count[0]) & AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config18_U0.ap_idle & ~(AESL_inst_myproject.dense_large_stream_me_ap_fixed_ap_fixed_config49_U0_ap_ready_count[0])));
    always @ (negedge reset or posedge clock) begin
        if (~reset) begin
            proc_dep_vld_vec_15_reg <= 'b0;
        end
        else begin
            proc_dep_vld_vec_15_reg <= proc_dep_vld_vec_15;
        end
    end
    assign in_chan_dep_vld_vec_15[0] = dep_chan_vld_0_15;
    assign in_chan_dep_data_vec_15[39 : 0] = dep_chan_data_0_15;
    assign token_in_vec_15[0] = token_0_15;
    assign in_chan_dep_vld_vec_15[1] = dep_chan_vld_1_15;
    assign in_chan_dep_data_vec_15[79 : 40] = dep_chan_data_1_15;
    assign token_in_vec_15[1] = token_1_15;
    assign in_chan_dep_vld_vec_15[2] = dep_chan_vld_11_15;
    assign in_chan_dep_data_vec_15[119 : 80] = dep_chan_data_11_15;
    assign token_in_vec_15[2] = token_11_15;
    assign in_chan_dep_vld_vec_15[3] = dep_chan_vld_14_15;
    assign in_chan_dep_data_vec_15[159 : 120] = dep_chan_data_14_15;
    assign token_in_vec_15[3] = token_14_15;
    assign in_chan_dep_vld_vec_15[4] = dep_chan_vld_16_15;
    assign in_chan_dep_data_vec_15[199 : 160] = dep_chan_data_16_15;
    assign token_in_vec_15[4] = token_16_15;
    assign in_chan_dep_vld_vec_15[5] = dep_chan_vld_18_15;
    assign in_chan_dep_data_vec_15[239 : 200] = dep_chan_data_18_15;
    assign token_in_vec_15[5] = token_18_15;
    assign in_chan_dep_vld_vec_15[6] = dep_chan_vld_22_15;
    assign in_chan_dep_data_vec_15[279 : 240] = dep_chan_data_22_15;
    assign token_in_vec_15[6] = token_22_15;
    assign in_chan_dep_vld_vec_15[7] = dep_chan_vld_25_15;
    assign in_chan_dep_data_vec_15[319 : 280] = dep_chan_data_25_15;
    assign token_in_vec_15[7] = token_25_15;
    assign in_chan_dep_vld_vec_15[8] = dep_chan_vld_29_15;
    assign in_chan_dep_data_vec_15[359 : 320] = dep_chan_data_29_15;
    assign token_in_vec_15[8] = token_29_15;
    assign in_chan_dep_vld_vec_15[9] = dep_chan_vld_32_15;
    assign in_chan_dep_data_vec_15[399 : 360] = dep_chan_data_32_15;
    assign token_in_vec_15[9] = token_32_15;
    assign in_chan_dep_vld_vec_15[10] = dep_chan_vld_34_15;
    assign in_chan_dep_data_vec_15[439 : 400] = dep_chan_data_34_15;
    assign token_in_vec_15[10] = token_34_15;
    assign in_chan_dep_vld_vec_15[11] = dep_chan_vld_36_15;
    assign in_chan_dep_data_vec_15[479 : 440] = dep_chan_data_36_15;
    assign token_in_vec_15[11] = token_36_15;
    assign dep_chan_vld_15_14 = out_chan_dep_vld_vec_15[0];
    assign dep_chan_data_15_14 = out_chan_dep_data_15;
    assign token_15_14 = token_out_vec_15[0];
    assign dep_chan_vld_15_16 = out_chan_dep_vld_vec_15[1];
    assign dep_chan_data_15_16 = out_chan_dep_data_15;
    assign token_15_16 = token_out_vec_15[1];
    assign dep_chan_vld_15_0 = out_chan_dep_vld_vec_15[2];
    assign dep_chan_data_15_0 = out_chan_dep_data_15;
    assign token_15_0 = token_out_vec_15[2];
    assign dep_chan_vld_15_1 = out_chan_dep_vld_vec_15[3];
    assign dep_chan_data_15_1 = out_chan_dep_data_15;
    assign token_15_1 = token_out_vec_15[3];
    assign dep_chan_vld_15_11 = out_chan_dep_vld_vec_15[4];
    assign dep_chan_data_15_11 = out_chan_dep_data_15;
    assign token_15_11 = token_out_vec_15[4];
    assign dep_chan_vld_15_18 = out_chan_dep_vld_vec_15[5];
    assign dep_chan_data_15_18 = out_chan_dep_data_15;
    assign token_15_18 = token_out_vec_15[5];
    assign dep_chan_vld_15_22 = out_chan_dep_vld_vec_15[6];
    assign dep_chan_data_15_22 = out_chan_dep_data_15;
    assign token_15_22 = token_out_vec_15[6];
    assign dep_chan_vld_15_25 = out_chan_dep_vld_vec_15[7];
    assign dep_chan_data_15_25 = out_chan_dep_data_15;
    assign token_15_25 = token_out_vec_15[7];
    assign dep_chan_vld_15_29 = out_chan_dep_vld_vec_15[8];
    assign dep_chan_data_15_29 = out_chan_dep_data_15;
    assign token_15_29 = token_out_vec_15[8];
    assign dep_chan_vld_15_32 = out_chan_dep_vld_vec_15[9];
    assign dep_chan_data_15_32 = out_chan_dep_data_15;
    assign token_15_32 = token_out_vec_15[9];
    assign dep_chan_vld_15_34 = out_chan_dep_vld_vec_15[10];
    assign dep_chan_data_15_34 = out_chan_dep_data_15;
    assign token_15_34 = token_out_vec_15[10];
    assign dep_chan_vld_15_36 = out_chan_dep_vld_vec_15[11];
    assign dep_chan_data_15_36 = out_chan_dep_data_15;
    assign token_15_36 = token_out_vec_15[11];

    // delay ap_idle for one cycle
    reg [0:0] AESL_inst_myproject$leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_config21_576_U0$ap_idle;
    always @ (negedge reset or posedge clock) begin
        if (~reset) begin
            AESL_inst_myproject$leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_config21_576_U0$ap_idle <= 'b0;
        end
        else begin
            AESL_inst_myproject$leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_config21_576_U0$ap_idle <= AESL_inst_myproject.leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_config21_576_U0.ap_idle;
        end
    end
    // Process: AESL_inst_myproject.leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_config21_576_U0
    AESL_deadlock_detect_unit #(40, 16, 2, 2) AESL_deadlock_detect_unit_16 (
        .reset(reset),
        .clock(clock),
        .proc_dep_vld_vec(proc_dep_vld_vec_16),
        .in_chan_dep_vld_vec(in_chan_dep_vld_vec_16),
        .in_chan_dep_data_vec(in_chan_dep_data_vec_16),
        .token_in_vec(token_in_vec_16),
        .dl_detect_in(dl_detect_out),
        .origin(origin[16]),
        .token_clear(token_clear),
        .out_chan_dep_vld_vec(out_chan_dep_vld_vec_16),
        .out_chan_dep_data(out_chan_dep_data_16),
        .token_out_vec(token_out_vec_16),
        .dl_detect_out(dl_in_vec[16]));

    assign proc_dep_vld_vec_16[0] = dl_detect_out ? proc_dep_vld_vec_16_reg[0] : (~AESL_inst_myproject.leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_config21_576_U0.data_V_V_blk_n | (~AESL_inst_myproject.start_for_leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_confiBYe_U.if_empty_n & (AESL_inst_myproject.leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_config21_576_U0.ap_ready | AESL_inst_myproject$leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_config21_576_U0$ap_idle)));
    assign proc_dep_vld_vec_16[1] = dl_detect_out ? proc_dep_vld_vec_16_reg[1] : (~AESL_inst_myproject.leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_config21_576_U0.res_V_V_blk_n | (~AESL_inst_myproject.start_for_zeropad2d_cl_me_ap_fixed_ap_fixed_config60_U0_U.if_full_n & AESL_inst_myproject.zeropad2d_cl_me_ap_fixed_ap_fixed_config60_U0.ap_done));
    always @ (negedge reset or posedge clock) begin
        if (~reset) begin
            proc_dep_vld_vec_16_reg <= 'b0;
        end
        else begin
            proc_dep_vld_vec_16_reg <= proc_dep_vld_vec_16;
        end
    end
    assign in_chan_dep_vld_vec_16[0] = dep_chan_vld_15_16;
    assign in_chan_dep_data_vec_16[39 : 0] = dep_chan_data_15_16;
    assign token_in_vec_16[0] = token_15_16;
    assign in_chan_dep_vld_vec_16[1] = dep_chan_vld_17_16;
    assign in_chan_dep_data_vec_16[79 : 40] = dep_chan_data_17_16;
    assign token_in_vec_16[1] = token_17_16;
    assign dep_chan_vld_16_15 = out_chan_dep_vld_vec_16[0];
    assign dep_chan_data_16_15 = out_chan_dep_data_16;
    assign token_16_15 = token_out_vec_16[0];
    assign dep_chan_vld_16_17 = out_chan_dep_vld_vec_16[1];
    assign dep_chan_data_16_17 = out_chan_dep_data_16;
    assign token_16_17 = token_out_vec_16[1];

    // delay ap_idle for one cycle
    reg [0:0] AESL_inst_myproject$zeropad2d_cl_me_ap_fixed_ap_fixed_config60_U0$ap_idle;
    always @ (negedge reset or posedge clock) begin
        if (~reset) begin
            AESL_inst_myproject$zeropad2d_cl_me_ap_fixed_ap_fixed_config60_U0$ap_idle <= 'b0;
        end
        else begin
            AESL_inst_myproject$zeropad2d_cl_me_ap_fixed_ap_fixed_config60_U0$ap_idle <= AESL_inst_myproject.zeropad2d_cl_me_ap_fixed_ap_fixed_config60_U0.ap_idle;
        end
    end
    // Process: AESL_inst_myproject.zeropad2d_cl_me_ap_fixed_ap_fixed_config60_U0
    AESL_deadlock_detect_unit #(40, 17, 2, 2) AESL_deadlock_detect_unit_17 (
        .reset(reset),
        .clock(clock),
        .proc_dep_vld_vec(proc_dep_vld_vec_17),
        .in_chan_dep_vld_vec(in_chan_dep_vld_vec_17),
        .in_chan_dep_data_vec(in_chan_dep_data_vec_17),
        .token_in_vec(token_in_vec_17),
        .dl_detect_in(dl_detect_out),
        .origin(origin[17]),
        .token_clear(token_clear),
        .out_chan_dep_vld_vec(out_chan_dep_vld_vec_17),
        .out_chan_dep_data(out_chan_dep_data_17),
        .token_out_vec(token_out_vec_17),
        .dl_detect_out(dl_in_vec[17]));

    assign proc_dep_vld_vec_17[0] = dl_detect_out ? proc_dep_vld_vec_17_reg[0] : (~AESL_inst_myproject.zeropad2d_cl_me_ap_fixed_ap_fixed_config60_U0.data_V_V_blk_n | (~AESL_inst_myproject.start_for_zeropad2d_cl_me_ap_fixed_ap_fixed_config60_U0_U.if_empty_n & (AESL_inst_myproject.zeropad2d_cl_me_ap_fixed_ap_fixed_config60_U0.ap_ready | AESL_inst_myproject$zeropad2d_cl_me_ap_fixed_ap_fixed_config60_U0$ap_idle)));
    assign proc_dep_vld_vec_17[1] = dl_detect_out ? proc_dep_vld_vec_17_reg[1] : (~AESL_inst_myproject.zeropad2d_cl_me_ap_fixed_ap_fixed_config60_U0.res_V_V_blk_n);
    always @ (negedge reset or posedge clock) begin
        if (~reset) begin
            proc_dep_vld_vec_17_reg <= 'b0;
        end
        else begin
            proc_dep_vld_vec_17_reg <= proc_dep_vld_vec_17;
        end
    end
    assign in_chan_dep_vld_vec_17[0] = dep_chan_vld_16_17;
    assign in_chan_dep_data_vec_17[39 : 0] = dep_chan_data_16_17;
    assign token_in_vec_17[0] = token_16_17;
    assign in_chan_dep_vld_vec_17[1] = dep_chan_vld_18_17;
    assign in_chan_dep_data_vec_17[79 : 40] = dep_chan_data_18_17;
    assign token_in_vec_17[1] = token_18_17;
    assign dep_chan_vld_17_16 = out_chan_dep_vld_vec_17[0];
    assign dep_chan_data_17_16 = out_chan_dep_data_17;
    assign token_17_16 = token_out_vec_17[0];
    assign dep_chan_vld_17_18 = out_chan_dep_vld_vec_17[1];
    assign dep_chan_data_17_18 = out_chan_dep_data_17;
    assign token_17_18 = token_out_vec_17[1];

    // delay ap_idle for one cycle
    reg [0:0] AESL_inst_myproject$conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config22_U0$ap_idle;
    always @ (negedge reset or posedge clock) begin
        if (~reset) begin
            AESL_inst_myproject$conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config22_U0$ap_idle <= 'b0;
        end
        else begin
            AESL_inst_myproject$conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config22_U0$ap_idle <= AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config22_U0.ap_idle;
        end
    end
    // Process: AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config22_U0
    AESL_deadlock_detect_unit #(40, 18, 12, 12) AESL_deadlock_detect_unit_18 (
        .reset(reset),
        .clock(clock),
        .proc_dep_vld_vec(proc_dep_vld_vec_18),
        .in_chan_dep_vld_vec(in_chan_dep_vld_vec_18),
        .in_chan_dep_data_vec(in_chan_dep_data_vec_18),
        .token_in_vec(token_in_vec_18),
        .dl_detect_in(dl_detect_out),
        .origin(origin[18]),
        .token_clear(token_clear),
        .out_chan_dep_vld_vec(out_chan_dep_vld_vec_18),
        .out_chan_dep_data(out_chan_dep_data_18),
        .token_out_vec(token_out_vec_18),
        .dl_detect_out(dl_in_vec[18]));

    assign proc_dep_vld_vec_18[0] = dl_detect_out ? proc_dep_vld_vec_18_reg[0] : (~AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config22_U0.data_V_V_blk_n);
    assign proc_dep_vld_vec_18[1] = dl_detect_out ? proc_dep_vld_vec_18_reg[1] : (~AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config22_U0.res_V_V_blk_n | (~AESL_inst_myproject.start_for_leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_confiBZe_U.if_full_n & AESL_inst_myproject.leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_config25_577_U0.ap_done));
    assign proc_dep_vld_vec_18[2] = dl_detect_out ? proc_dep_vld_vec_18_reg[2] : (((AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config22_U0_ap_ready_count[0]) & AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config22_U0.ap_idle & ~(AESL_inst_myproject.Block_ap_fixed_base_exit3121_proc_U0_ap_ready_count[0])));
    assign proc_dep_vld_vec_18[3] = dl_detect_out ? proc_dep_vld_vec_18_reg[3] : (((AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config22_U0_ap_ready_count[0]) & AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config22_U0.ap_idle & ~(AESL_inst_myproject.resize_nearest_me_ap_fixed_32_16_5_3_0_config2_U0_ap_ready_count[0])));
    assign proc_dep_vld_vec_18[4] = dl_detect_out ? proc_dep_vld_vec_18_reg[4] : (((AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config22_U0_ap_ready_count[0]) & AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config22_U0.ap_idle & ~(AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config13_U0_ap_ready_count[0])));
    assign proc_dep_vld_vec_18[5] = dl_detect_out ? proc_dep_vld_vec_18_reg[5] : (((AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config22_U0_ap_ready_count[0]) & AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config22_U0.ap_idle & ~(AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config18_U0_ap_ready_count[0])));
    assign proc_dep_vld_vec_18[6] = dl_detect_out ? proc_dep_vld_vec_18_reg[6] : (((AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config22_U0_ap_ready_count[0]) & AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config22_U0.ap_idle & ~(AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config27_U0_ap_ready_count[0])));
    assign proc_dep_vld_vec_18[7] = dl_detect_out ? proc_dep_vld_vec_18_reg[7] : (((AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config22_U0_ap_ready_count[0]) & AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config22_U0.ap_idle & ~(AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config31_U0_ap_ready_count[0])));
    assign proc_dep_vld_vec_18[8] = dl_detect_out ? proc_dep_vld_vec_18_reg[8] : (((AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config22_U0_ap_ready_count[0]) & AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config22_U0.ap_idle & ~(AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config36_U0_ap_ready_count[0])));
    assign proc_dep_vld_vec_18[9] = dl_detect_out ? proc_dep_vld_vec_18_reg[9] : (((AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config22_U0_ap_ready_count[0]) & AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config22_U0.ap_idle & ~(AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config40_U0_ap_ready_count[0])));
    assign proc_dep_vld_vec_18[10] = dl_detect_out ? proc_dep_vld_vec_18_reg[10] : (((AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config22_U0_ap_ready_count[0]) & AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config22_U0.ap_idle & ~(AESL_inst_myproject.dense_large_stream_me_ap_fixed_ap_fixed_config45_U0_ap_ready_count[0])));
    assign proc_dep_vld_vec_18[11] = dl_detect_out ? proc_dep_vld_vec_18_reg[11] : (((AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config22_U0_ap_ready_count[0]) & AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config22_U0.ap_idle & ~(AESL_inst_myproject.dense_large_stream_me_ap_fixed_ap_fixed_config49_U0_ap_ready_count[0])));
    always @ (negedge reset or posedge clock) begin
        if (~reset) begin
            proc_dep_vld_vec_18_reg <= 'b0;
        end
        else begin
            proc_dep_vld_vec_18_reg <= proc_dep_vld_vec_18;
        end
    end
    assign in_chan_dep_vld_vec_18[0] = dep_chan_vld_0_18;
    assign in_chan_dep_data_vec_18[39 : 0] = dep_chan_data_0_18;
    assign token_in_vec_18[0] = token_0_18;
    assign in_chan_dep_vld_vec_18[1] = dep_chan_vld_1_18;
    assign in_chan_dep_data_vec_18[79 : 40] = dep_chan_data_1_18;
    assign token_in_vec_18[1] = token_1_18;
    assign in_chan_dep_vld_vec_18[2] = dep_chan_vld_11_18;
    assign in_chan_dep_data_vec_18[119 : 80] = dep_chan_data_11_18;
    assign token_in_vec_18[2] = token_11_18;
    assign in_chan_dep_vld_vec_18[3] = dep_chan_vld_15_18;
    assign in_chan_dep_data_vec_18[159 : 120] = dep_chan_data_15_18;
    assign token_in_vec_18[3] = token_15_18;
    assign in_chan_dep_vld_vec_18[4] = dep_chan_vld_17_18;
    assign in_chan_dep_data_vec_18[199 : 160] = dep_chan_data_17_18;
    assign token_in_vec_18[4] = token_17_18;
    assign in_chan_dep_vld_vec_18[5] = dep_chan_vld_19_18;
    assign in_chan_dep_data_vec_18[239 : 200] = dep_chan_data_19_18;
    assign token_in_vec_18[5] = token_19_18;
    assign in_chan_dep_vld_vec_18[6] = dep_chan_vld_22_18;
    assign in_chan_dep_data_vec_18[279 : 240] = dep_chan_data_22_18;
    assign token_in_vec_18[6] = token_22_18;
    assign in_chan_dep_vld_vec_18[7] = dep_chan_vld_25_18;
    assign in_chan_dep_data_vec_18[319 : 280] = dep_chan_data_25_18;
    assign token_in_vec_18[7] = token_25_18;
    assign in_chan_dep_vld_vec_18[8] = dep_chan_vld_29_18;
    assign in_chan_dep_data_vec_18[359 : 320] = dep_chan_data_29_18;
    assign token_in_vec_18[8] = token_29_18;
    assign in_chan_dep_vld_vec_18[9] = dep_chan_vld_32_18;
    assign in_chan_dep_data_vec_18[399 : 360] = dep_chan_data_32_18;
    assign token_in_vec_18[9] = token_32_18;
    assign in_chan_dep_vld_vec_18[10] = dep_chan_vld_34_18;
    assign in_chan_dep_data_vec_18[439 : 400] = dep_chan_data_34_18;
    assign token_in_vec_18[10] = token_34_18;
    assign in_chan_dep_vld_vec_18[11] = dep_chan_vld_36_18;
    assign in_chan_dep_data_vec_18[479 : 440] = dep_chan_data_36_18;
    assign token_in_vec_18[11] = token_36_18;
    assign dep_chan_vld_18_17 = out_chan_dep_vld_vec_18[0];
    assign dep_chan_data_18_17 = out_chan_dep_data_18;
    assign token_18_17 = token_out_vec_18[0];
    assign dep_chan_vld_18_19 = out_chan_dep_vld_vec_18[1];
    assign dep_chan_data_18_19 = out_chan_dep_data_18;
    assign token_18_19 = token_out_vec_18[1];
    assign dep_chan_vld_18_0 = out_chan_dep_vld_vec_18[2];
    assign dep_chan_data_18_0 = out_chan_dep_data_18;
    assign token_18_0 = token_out_vec_18[2];
    assign dep_chan_vld_18_1 = out_chan_dep_vld_vec_18[3];
    assign dep_chan_data_18_1 = out_chan_dep_data_18;
    assign token_18_1 = token_out_vec_18[3];
    assign dep_chan_vld_18_11 = out_chan_dep_vld_vec_18[4];
    assign dep_chan_data_18_11 = out_chan_dep_data_18;
    assign token_18_11 = token_out_vec_18[4];
    assign dep_chan_vld_18_15 = out_chan_dep_vld_vec_18[5];
    assign dep_chan_data_18_15 = out_chan_dep_data_18;
    assign token_18_15 = token_out_vec_18[5];
    assign dep_chan_vld_18_22 = out_chan_dep_vld_vec_18[6];
    assign dep_chan_data_18_22 = out_chan_dep_data_18;
    assign token_18_22 = token_out_vec_18[6];
    assign dep_chan_vld_18_25 = out_chan_dep_vld_vec_18[7];
    assign dep_chan_data_18_25 = out_chan_dep_data_18;
    assign token_18_25 = token_out_vec_18[7];
    assign dep_chan_vld_18_29 = out_chan_dep_vld_vec_18[8];
    assign dep_chan_data_18_29 = out_chan_dep_data_18;
    assign token_18_29 = token_out_vec_18[8];
    assign dep_chan_vld_18_32 = out_chan_dep_vld_vec_18[9];
    assign dep_chan_data_18_32 = out_chan_dep_data_18;
    assign token_18_32 = token_out_vec_18[9];
    assign dep_chan_vld_18_34 = out_chan_dep_vld_vec_18[10];
    assign dep_chan_data_18_34 = out_chan_dep_data_18;
    assign token_18_34 = token_out_vec_18[10];
    assign dep_chan_vld_18_36 = out_chan_dep_vld_vec_18[11];
    assign dep_chan_data_18_36 = out_chan_dep_data_18;
    assign token_18_36 = token_out_vec_18[11];

    // delay ap_idle for one cycle
    reg [0:0] AESL_inst_myproject$leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_config25_577_U0$ap_idle;
    always @ (negedge reset or posedge clock) begin
        if (~reset) begin
            AESL_inst_myproject$leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_config25_577_U0$ap_idle <= 'b0;
        end
        else begin
            AESL_inst_myproject$leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_config25_577_U0$ap_idle <= AESL_inst_myproject.leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_config25_577_U0.ap_idle;
        end
    end
    // Process: AESL_inst_myproject.leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_config25_577_U0
    AESL_deadlock_detect_unit #(40, 19, 2, 2) AESL_deadlock_detect_unit_19 (
        .reset(reset),
        .clock(clock),
        .proc_dep_vld_vec(proc_dep_vld_vec_19),
        .in_chan_dep_vld_vec(in_chan_dep_vld_vec_19),
        .in_chan_dep_data_vec(in_chan_dep_data_vec_19),
        .token_in_vec(token_in_vec_19),
        .dl_detect_in(dl_detect_out),
        .origin(origin[19]),
        .token_clear(token_clear),
        .out_chan_dep_vld_vec(out_chan_dep_vld_vec_19),
        .out_chan_dep_data(out_chan_dep_data_19),
        .token_out_vec(token_out_vec_19),
        .dl_detect_out(dl_in_vec[19]));

    assign proc_dep_vld_vec_19[0] = dl_detect_out ? proc_dep_vld_vec_19_reg[0] : (~AESL_inst_myproject.leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_config25_577_U0.data_V_V_blk_n | (~AESL_inst_myproject.start_for_leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_confiBZe_U.if_empty_n & (AESL_inst_myproject.leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_config25_577_U0.ap_ready | AESL_inst_myproject$leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_config25_577_U0$ap_idle)));
    assign proc_dep_vld_vec_19[1] = dl_detect_out ? proc_dep_vld_vec_19_reg[1] : (~AESL_inst_myproject.leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_config25_577_U0.res_V_V_blk_n | (~AESL_inst_myproject.start_for_pooling2d_large_cl_nopad_pad_me_1_U0_U.if_full_n & AESL_inst_myproject.pooling2d_large_cl_nopad_pad_me_1_U0.ap_done));
    always @ (negedge reset or posedge clock) begin
        if (~reset) begin
            proc_dep_vld_vec_19_reg <= 'b0;
        end
        else begin
            proc_dep_vld_vec_19_reg <= proc_dep_vld_vec_19;
        end
    end
    assign in_chan_dep_vld_vec_19[0] = dep_chan_vld_18_19;
    assign in_chan_dep_data_vec_19[39 : 0] = dep_chan_data_18_19;
    assign token_in_vec_19[0] = token_18_19;
    assign in_chan_dep_vld_vec_19[1] = dep_chan_vld_20_19;
    assign in_chan_dep_data_vec_19[79 : 40] = dep_chan_data_20_19;
    assign token_in_vec_19[1] = token_20_19;
    assign dep_chan_vld_19_18 = out_chan_dep_vld_vec_19[0];
    assign dep_chan_data_19_18 = out_chan_dep_data_19;
    assign token_19_18 = token_out_vec_19[0];
    assign dep_chan_vld_19_20 = out_chan_dep_vld_vec_19[1];
    assign dep_chan_data_19_20 = out_chan_dep_data_19;
    assign token_19_20 = token_out_vec_19[1];

    // delay ap_idle for one cycle
    reg [0:0] AESL_inst_myproject$pooling2d_large_cl_nopad_pad_me_1_U0$ap_idle;
    always @ (negedge reset or posedge clock) begin
        if (~reset) begin
            AESL_inst_myproject$pooling2d_large_cl_nopad_pad_me_1_U0$ap_idle <= 'b0;
        end
        else begin
            AESL_inst_myproject$pooling2d_large_cl_nopad_pad_me_1_U0$ap_idle <= AESL_inst_myproject.pooling2d_large_cl_nopad_pad_me_1_U0.ap_idle;
        end
    end
    // Process: AESL_inst_myproject.pooling2d_large_cl_nopad_pad_me_1_U0
    AESL_deadlock_detect_unit #(40, 20, 2, 2) AESL_deadlock_detect_unit_20 (
        .reset(reset),
        .clock(clock),
        .proc_dep_vld_vec(proc_dep_vld_vec_20),
        .in_chan_dep_vld_vec(in_chan_dep_vld_vec_20),
        .in_chan_dep_data_vec(in_chan_dep_data_vec_20),
        .token_in_vec(token_in_vec_20),
        .dl_detect_in(dl_detect_out),
        .origin(origin[20]),
        .token_clear(token_clear),
        .out_chan_dep_vld_vec(out_chan_dep_vld_vec_20),
        .out_chan_dep_data(out_chan_dep_data_20),
        .token_out_vec(token_out_vec_20),
        .dl_detect_out(dl_in_vec[20]));

    assign proc_dep_vld_vec_20[0] = dl_detect_out ? proc_dep_vld_vec_20_reg[0] : (~AESL_inst_myproject.pooling2d_large_cl_nopad_pad_me_1_U0.data_V_V_blk_n | (~AESL_inst_myproject.start_for_pooling2d_large_cl_nopad_pad_me_1_U0_U.if_empty_n & (AESL_inst_myproject.pooling2d_large_cl_nopad_pad_me_1_U0.ap_ready | AESL_inst_myproject$pooling2d_large_cl_nopad_pad_me_1_U0$ap_idle)));
    assign proc_dep_vld_vec_20[1] = dl_detect_out ? proc_dep_vld_vec_20_reg[1] : (~AESL_inst_myproject.pooling2d_large_cl_nopad_pad_me_1_U0.res_V_V_blk_n | (~AESL_inst_myproject.start_for_zeropad2d_cl_me_ap_fixed_ap_fixed_config61_U0_U.if_full_n & AESL_inst_myproject.zeropad2d_cl_me_ap_fixed_ap_fixed_config61_U0.ap_done));
    always @ (negedge reset or posedge clock) begin
        if (~reset) begin
            proc_dep_vld_vec_20_reg <= 'b0;
        end
        else begin
            proc_dep_vld_vec_20_reg <= proc_dep_vld_vec_20;
        end
    end
    assign in_chan_dep_vld_vec_20[0] = dep_chan_vld_19_20;
    assign in_chan_dep_data_vec_20[39 : 0] = dep_chan_data_19_20;
    assign token_in_vec_20[0] = token_19_20;
    assign in_chan_dep_vld_vec_20[1] = dep_chan_vld_21_20;
    assign in_chan_dep_data_vec_20[79 : 40] = dep_chan_data_21_20;
    assign token_in_vec_20[1] = token_21_20;
    assign dep_chan_vld_20_19 = out_chan_dep_vld_vec_20[0];
    assign dep_chan_data_20_19 = out_chan_dep_data_20;
    assign token_20_19 = token_out_vec_20[0];
    assign dep_chan_vld_20_21 = out_chan_dep_vld_vec_20[1];
    assign dep_chan_data_20_21 = out_chan_dep_data_20;
    assign token_20_21 = token_out_vec_20[1];

    // delay ap_idle for one cycle
    reg [0:0] AESL_inst_myproject$zeropad2d_cl_me_ap_fixed_ap_fixed_config61_U0$ap_idle;
    always @ (negedge reset or posedge clock) begin
        if (~reset) begin
            AESL_inst_myproject$zeropad2d_cl_me_ap_fixed_ap_fixed_config61_U0$ap_idle <= 'b0;
        end
        else begin
            AESL_inst_myproject$zeropad2d_cl_me_ap_fixed_ap_fixed_config61_U0$ap_idle <= AESL_inst_myproject.zeropad2d_cl_me_ap_fixed_ap_fixed_config61_U0.ap_idle;
        end
    end
    // Process: AESL_inst_myproject.zeropad2d_cl_me_ap_fixed_ap_fixed_config61_U0
    AESL_deadlock_detect_unit #(40, 21, 2, 2) AESL_deadlock_detect_unit_21 (
        .reset(reset),
        .clock(clock),
        .proc_dep_vld_vec(proc_dep_vld_vec_21),
        .in_chan_dep_vld_vec(in_chan_dep_vld_vec_21),
        .in_chan_dep_data_vec(in_chan_dep_data_vec_21),
        .token_in_vec(token_in_vec_21),
        .dl_detect_in(dl_detect_out),
        .origin(origin[21]),
        .token_clear(token_clear),
        .out_chan_dep_vld_vec(out_chan_dep_vld_vec_21),
        .out_chan_dep_data(out_chan_dep_data_21),
        .token_out_vec(token_out_vec_21),
        .dl_detect_out(dl_in_vec[21]));

    assign proc_dep_vld_vec_21[0] = dl_detect_out ? proc_dep_vld_vec_21_reg[0] : (~AESL_inst_myproject.zeropad2d_cl_me_ap_fixed_ap_fixed_config61_U0.data_V_V_blk_n | (~AESL_inst_myproject.start_for_zeropad2d_cl_me_ap_fixed_ap_fixed_config61_U0_U.if_empty_n & (AESL_inst_myproject.zeropad2d_cl_me_ap_fixed_ap_fixed_config61_U0.ap_ready | AESL_inst_myproject$zeropad2d_cl_me_ap_fixed_ap_fixed_config61_U0$ap_idle)));
    assign proc_dep_vld_vec_21[1] = dl_detect_out ? proc_dep_vld_vec_21_reg[1] : (~AESL_inst_myproject.zeropad2d_cl_me_ap_fixed_ap_fixed_config61_U0.res_V_V_blk_n);
    always @ (negedge reset or posedge clock) begin
        if (~reset) begin
            proc_dep_vld_vec_21_reg <= 'b0;
        end
        else begin
            proc_dep_vld_vec_21_reg <= proc_dep_vld_vec_21;
        end
    end
    assign in_chan_dep_vld_vec_21[0] = dep_chan_vld_20_21;
    assign in_chan_dep_data_vec_21[39 : 0] = dep_chan_data_20_21;
    assign token_in_vec_21[0] = token_20_21;
    assign in_chan_dep_vld_vec_21[1] = dep_chan_vld_22_21;
    assign in_chan_dep_data_vec_21[79 : 40] = dep_chan_data_22_21;
    assign token_in_vec_21[1] = token_22_21;
    assign dep_chan_vld_21_20 = out_chan_dep_vld_vec_21[0];
    assign dep_chan_data_21_20 = out_chan_dep_data_21;
    assign token_21_20 = token_out_vec_21[0];
    assign dep_chan_vld_21_22 = out_chan_dep_vld_vec_21[1];
    assign dep_chan_data_21_22 = out_chan_dep_data_21;
    assign token_21_22 = token_out_vec_21[1];

    // delay ap_idle for one cycle
    reg [0:0] AESL_inst_myproject$conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config27_U0$ap_idle;
    always @ (negedge reset or posedge clock) begin
        if (~reset) begin
            AESL_inst_myproject$conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config27_U0$ap_idle <= 'b0;
        end
        else begin
            AESL_inst_myproject$conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config27_U0$ap_idle <= AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config27_U0.ap_idle;
        end
    end
    // Process: AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config27_U0
    AESL_deadlock_detect_unit #(40, 22, 12, 12) AESL_deadlock_detect_unit_22 (
        .reset(reset),
        .clock(clock),
        .proc_dep_vld_vec(proc_dep_vld_vec_22),
        .in_chan_dep_vld_vec(in_chan_dep_vld_vec_22),
        .in_chan_dep_data_vec(in_chan_dep_data_vec_22),
        .token_in_vec(token_in_vec_22),
        .dl_detect_in(dl_detect_out),
        .origin(origin[22]),
        .token_clear(token_clear),
        .out_chan_dep_vld_vec(out_chan_dep_vld_vec_22),
        .out_chan_dep_data(out_chan_dep_data_22),
        .token_out_vec(token_out_vec_22),
        .dl_detect_out(dl_in_vec[22]));

    assign proc_dep_vld_vec_22[0] = dl_detect_out ? proc_dep_vld_vec_22_reg[0] : (~AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config27_U0.data_V_V_blk_n);
    assign proc_dep_vld_vec_22[1] = dl_detect_out ? proc_dep_vld_vec_22_reg[1] : (~AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config27_U0.res_V_V_blk_n | (~AESL_inst_myproject.start_for_leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_confiB0e_U.if_full_n & AESL_inst_myproject.leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_config30_578_U0.ap_done));
    assign proc_dep_vld_vec_22[2] = dl_detect_out ? proc_dep_vld_vec_22_reg[2] : (((AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config27_U0_ap_ready_count[0]) & AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config27_U0.ap_idle & ~(AESL_inst_myproject.Block_ap_fixed_base_exit3121_proc_U0_ap_ready_count[0])));
    assign proc_dep_vld_vec_22[3] = dl_detect_out ? proc_dep_vld_vec_22_reg[3] : (((AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config27_U0_ap_ready_count[0]) & AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config27_U0.ap_idle & ~(AESL_inst_myproject.resize_nearest_me_ap_fixed_32_16_5_3_0_config2_U0_ap_ready_count[0])));
    assign proc_dep_vld_vec_22[4] = dl_detect_out ? proc_dep_vld_vec_22_reg[4] : (((AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config27_U0_ap_ready_count[0]) & AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config27_U0.ap_idle & ~(AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config13_U0_ap_ready_count[0])));
    assign proc_dep_vld_vec_22[5] = dl_detect_out ? proc_dep_vld_vec_22_reg[5] : (((AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config27_U0_ap_ready_count[0]) & AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config27_U0.ap_idle & ~(AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config18_U0_ap_ready_count[0])));
    assign proc_dep_vld_vec_22[6] = dl_detect_out ? proc_dep_vld_vec_22_reg[6] : (((AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config27_U0_ap_ready_count[0]) & AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config27_U0.ap_idle & ~(AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config22_U0_ap_ready_count[0])));
    assign proc_dep_vld_vec_22[7] = dl_detect_out ? proc_dep_vld_vec_22_reg[7] : (((AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config27_U0_ap_ready_count[0]) & AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config27_U0.ap_idle & ~(AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config31_U0_ap_ready_count[0])));
    assign proc_dep_vld_vec_22[8] = dl_detect_out ? proc_dep_vld_vec_22_reg[8] : (((AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config27_U0_ap_ready_count[0]) & AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config27_U0.ap_idle & ~(AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config36_U0_ap_ready_count[0])));
    assign proc_dep_vld_vec_22[9] = dl_detect_out ? proc_dep_vld_vec_22_reg[9] : (((AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config27_U0_ap_ready_count[0]) & AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config27_U0.ap_idle & ~(AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config40_U0_ap_ready_count[0])));
    assign proc_dep_vld_vec_22[10] = dl_detect_out ? proc_dep_vld_vec_22_reg[10] : (((AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config27_U0_ap_ready_count[0]) & AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config27_U0.ap_idle & ~(AESL_inst_myproject.dense_large_stream_me_ap_fixed_ap_fixed_config45_U0_ap_ready_count[0])));
    assign proc_dep_vld_vec_22[11] = dl_detect_out ? proc_dep_vld_vec_22_reg[11] : (((AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config27_U0_ap_ready_count[0]) & AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config27_U0.ap_idle & ~(AESL_inst_myproject.dense_large_stream_me_ap_fixed_ap_fixed_config49_U0_ap_ready_count[0])));
    always @ (negedge reset or posedge clock) begin
        if (~reset) begin
            proc_dep_vld_vec_22_reg <= 'b0;
        end
        else begin
            proc_dep_vld_vec_22_reg <= proc_dep_vld_vec_22;
        end
    end
    assign in_chan_dep_vld_vec_22[0] = dep_chan_vld_0_22;
    assign in_chan_dep_data_vec_22[39 : 0] = dep_chan_data_0_22;
    assign token_in_vec_22[0] = token_0_22;
    assign in_chan_dep_vld_vec_22[1] = dep_chan_vld_1_22;
    assign in_chan_dep_data_vec_22[79 : 40] = dep_chan_data_1_22;
    assign token_in_vec_22[1] = token_1_22;
    assign in_chan_dep_vld_vec_22[2] = dep_chan_vld_11_22;
    assign in_chan_dep_data_vec_22[119 : 80] = dep_chan_data_11_22;
    assign token_in_vec_22[2] = token_11_22;
    assign in_chan_dep_vld_vec_22[3] = dep_chan_vld_15_22;
    assign in_chan_dep_data_vec_22[159 : 120] = dep_chan_data_15_22;
    assign token_in_vec_22[3] = token_15_22;
    assign in_chan_dep_vld_vec_22[4] = dep_chan_vld_18_22;
    assign in_chan_dep_data_vec_22[199 : 160] = dep_chan_data_18_22;
    assign token_in_vec_22[4] = token_18_22;
    assign in_chan_dep_vld_vec_22[5] = dep_chan_vld_21_22;
    assign in_chan_dep_data_vec_22[239 : 200] = dep_chan_data_21_22;
    assign token_in_vec_22[5] = token_21_22;
    assign in_chan_dep_vld_vec_22[6] = dep_chan_vld_23_22;
    assign in_chan_dep_data_vec_22[279 : 240] = dep_chan_data_23_22;
    assign token_in_vec_22[6] = token_23_22;
    assign in_chan_dep_vld_vec_22[7] = dep_chan_vld_25_22;
    assign in_chan_dep_data_vec_22[319 : 280] = dep_chan_data_25_22;
    assign token_in_vec_22[7] = token_25_22;
    assign in_chan_dep_vld_vec_22[8] = dep_chan_vld_29_22;
    assign in_chan_dep_data_vec_22[359 : 320] = dep_chan_data_29_22;
    assign token_in_vec_22[8] = token_29_22;
    assign in_chan_dep_vld_vec_22[9] = dep_chan_vld_32_22;
    assign in_chan_dep_data_vec_22[399 : 360] = dep_chan_data_32_22;
    assign token_in_vec_22[9] = token_32_22;
    assign in_chan_dep_vld_vec_22[10] = dep_chan_vld_34_22;
    assign in_chan_dep_data_vec_22[439 : 400] = dep_chan_data_34_22;
    assign token_in_vec_22[10] = token_34_22;
    assign in_chan_dep_vld_vec_22[11] = dep_chan_vld_36_22;
    assign in_chan_dep_data_vec_22[479 : 440] = dep_chan_data_36_22;
    assign token_in_vec_22[11] = token_36_22;
    assign dep_chan_vld_22_21 = out_chan_dep_vld_vec_22[0];
    assign dep_chan_data_22_21 = out_chan_dep_data_22;
    assign token_22_21 = token_out_vec_22[0];
    assign dep_chan_vld_22_23 = out_chan_dep_vld_vec_22[1];
    assign dep_chan_data_22_23 = out_chan_dep_data_22;
    assign token_22_23 = token_out_vec_22[1];
    assign dep_chan_vld_22_0 = out_chan_dep_vld_vec_22[2];
    assign dep_chan_data_22_0 = out_chan_dep_data_22;
    assign token_22_0 = token_out_vec_22[2];
    assign dep_chan_vld_22_1 = out_chan_dep_vld_vec_22[3];
    assign dep_chan_data_22_1 = out_chan_dep_data_22;
    assign token_22_1 = token_out_vec_22[3];
    assign dep_chan_vld_22_11 = out_chan_dep_vld_vec_22[4];
    assign dep_chan_data_22_11 = out_chan_dep_data_22;
    assign token_22_11 = token_out_vec_22[4];
    assign dep_chan_vld_22_15 = out_chan_dep_vld_vec_22[5];
    assign dep_chan_data_22_15 = out_chan_dep_data_22;
    assign token_22_15 = token_out_vec_22[5];
    assign dep_chan_vld_22_18 = out_chan_dep_vld_vec_22[6];
    assign dep_chan_data_22_18 = out_chan_dep_data_22;
    assign token_22_18 = token_out_vec_22[6];
    assign dep_chan_vld_22_25 = out_chan_dep_vld_vec_22[7];
    assign dep_chan_data_22_25 = out_chan_dep_data_22;
    assign token_22_25 = token_out_vec_22[7];
    assign dep_chan_vld_22_29 = out_chan_dep_vld_vec_22[8];
    assign dep_chan_data_22_29 = out_chan_dep_data_22;
    assign token_22_29 = token_out_vec_22[8];
    assign dep_chan_vld_22_32 = out_chan_dep_vld_vec_22[9];
    assign dep_chan_data_22_32 = out_chan_dep_data_22;
    assign token_22_32 = token_out_vec_22[9];
    assign dep_chan_vld_22_34 = out_chan_dep_vld_vec_22[10];
    assign dep_chan_data_22_34 = out_chan_dep_data_22;
    assign token_22_34 = token_out_vec_22[10];
    assign dep_chan_vld_22_36 = out_chan_dep_vld_vec_22[11];
    assign dep_chan_data_22_36 = out_chan_dep_data_22;
    assign token_22_36 = token_out_vec_22[11];

    // delay ap_idle for one cycle
    reg [0:0] AESL_inst_myproject$leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_config30_578_U0$ap_idle;
    always @ (negedge reset or posedge clock) begin
        if (~reset) begin
            AESL_inst_myproject$leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_config30_578_U0$ap_idle <= 'b0;
        end
        else begin
            AESL_inst_myproject$leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_config30_578_U0$ap_idle <= AESL_inst_myproject.leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_config30_578_U0.ap_idle;
        end
    end
    // Process: AESL_inst_myproject.leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_config30_578_U0
    AESL_deadlock_detect_unit #(40, 23, 2, 2) AESL_deadlock_detect_unit_23 (
        .reset(reset),
        .clock(clock),
        .proc_dep_vld_vec(proc_dep_vld_vec_23),
        .in_chan_dep_vld_vec(in_chan_dep_vld_vec_23),
        .in_chan_dep_data_vec(in_chan_dep_data_vec_23),
        .token_in_vec(token_in_vec_23),
        .dl_detect_in(dl_detect_out),
        .origin(origin[23]),
        .token_clear(token_clear),
        .out_chan_dep_vld_vec(out_chan_dep_vld_vec_23),
        .out_chan_dep_data(out_chan_dep_data_23),
        .token_out_vec(token_out_vec_23),
        .dl_detect_out(dl_in_vec[23]));

    assign proc_dep_vld_vec_23[0] = dl_detect_out ? proc_dep_vld_vec_23_reg[0] : (~AESL_inst_myproject.leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_config30_578_U0.data_V_V_blk_n | (~AESL_inst_myproject.start_for_leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_confiB0e_U.if_empty_n & (AESL_inst_myproject.leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_config30_578_U0.ap_ready | AESL_inst_myproject$leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_config30_578_U0$ap_idle)));
    assign proc_dep_vld_vec_23[1] = dl_detect_out ? proc_dep_vld_vec_23_reg[1] : (~AESL_inst_myproject.leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_config30_578_U0.res_V_V_blk_n | (~AESL_inst_myproject.start_for_zeropad2d_cl_me_ap_fixed_ap_fixed_config62_U0_U.if_full_n & AESL_inst_myproject.zeropad2d_cl_me_ap_fixed_ap_fixed_config62_U0.ap_done));
    always @ (negedge reset or posedge clock) begin
        if (~reset) begin
            proc_dep_vld_vec_23_reg <= 'b0;
        end
        else begin
            proc_dep_vld_vec_23_reg <= proc_dep_vld_vec_23;
        end
    end
    assign in_chan_dep_vld_vec_23[0] = dep_chan_vld_22_23;
    assign in_chan_dep_data_vec_23[39 : 0] = dep_chan_data_22_23;
    assign token_in_vec_23[0] = token_22_23;
    assign in_chan_dep_vld_vec_23[1] = dep_chan_vld_24_23;
    assign in_chan_dep_data_vec_23[79 : 40] = dep_chan_data_24_23;
    assign token_in_vec_23[1] = token_24_23;
    assign dep_chan_vld_23_22 = out_chan_dep_vld_vec_23[0];
    assign dep_chan_data_23_22 = out_chan_dep_data_23;
    assign token_23_22 = token_out_vec_23[0];
    assign dep_chan_vld_23_24 = out_chan_dep_vld_vec_23[1];
    assign dep_chan_data_23_24 = out_chan_dep_data_23;
    assign token_23_24 = token_out_vec_23[1];

    // delay ap_idle for one cycle
    reg [0:0] AESL_inst_myproject$zeropad2d_cl_me_ap_fixed_ap_fixed_config62_U0$ap_idle;
    always @ (negedge reset or posedge clock) begin
        if (~reset) begin
            AESL_inst_myproject$zeropad2d_cl_me_ap_fixed_ap_fixed_config62_U0$ap_idle <= 'b0;
        end
        else begin
            AESL_inst_myproject$zeropad2d_cl_me_ap_fixed_ap_fixed_config62_U0$ap_idle <= AESL_inst_myproject.zeropad2d_cl_me_ap_fixed_ap_fixed_config62_U0.ap_idle;
        end
    end
    // Process: AESL_inst_myproject.zeropad2d_cl_me_ap_fixed_ap_fixed_config62_U0
    AESL_deadlock_detect_unit #(40, 24, 2, 2) AESL_deadlock_detect_unit_24 (
        .reset(reset),
        .clock(clock),
        .proc_dep_vld_vec(proc_dep_vld_vec_24),
        .in_chan_dep_vld_vec(in_chan_dep_vld_vec_24),
        .in_chan_dep_data_vec(in_chan_dep_data_vec_24),
        .token_in_vec(token_in_vec_24),
        .dl_detect_in(dl_detect_out),
        .origin(origin[24]),
        .token_clear(token_clear),
        .out_chan_dep_vld_vec(out_chan_dep_vld_vec_24),
        .out_chan_dep_data(out_chan_dep_data_24),
        .token_out_vec(token_out_vec_24),
        .dl_detect_out(dl_in_vec[24]));

    assign proc_dep_vld_vec_24[0] = dl_detect_out ? proc_dep_vld_vec_24_reg[0] : (~AESL_inst_myproject.zeropad2d_cl_me_ap_fixed_ap_fixed_config62_U0.data_V_V_blk_n | (~AESL_inst_myproject.start_for_zeropad2d_cl_me_ap_fixed_ap_fixed_config62_U0_U.if_empty_n & (AESL_inst_myproject.zeropad2d_cl_me_ap_fixed_ap_fixed_config62_U0.ap_ready | AESL_inst_myproject$zeropad2d_cl_me_ap_fixed_ap_fixed_config62_U0$ap_idle)));
    assign proc_dep_vld_vec_24[1] = dl_detect_out ? proc_dep_vld_vec_24_reg[1] : (~AESL_inst_myproject.zeropad2d_cl_me_ap_fixed_ap_fixed_config62_U0.res_V_V_blk_n);
    always @ (negedge reset or posedge clock) begin
        if (~reset) begin
            proc_dep_vld_vec_24_reg <= 'b0;
        end
        else begin
            proc_dep_vld_vec_24_reg <= proc_dep_vld_vec_24;
        end
    end
    assign in_chan_dep_vld_vec_24[0] = dep_chan_vld_23_24;
    assign in_chan_dep_data_vec_24[39 : 0] = dep_chan_data_23_24;
    assign token_in_vec_24[0] = token_23_24;
    assign in_chan_dep_vld_vec_24[1] = dep_chan_vld_25_24;
    assign in_chan_dep_data_vec_24[79 : 40] = dep_chan_data_25_24;
    assign token_in_vec_24[1] = token_25_24;
    assign dep_chan_vld_24_23 = out_chan_dep_vld_vec_24[0];
    assign dep_chan_data_24_23 = out_chan_dep_data_24;
    assign token_24_23 = token_out_vec_24[0];
    assign dep_chan_vld_24_25 = out_chan_dep_vld_vec_24[1];
    assign dep_chan_data_24_25 = out_chan_dep_data_24;
    assign token_24_25 = token_out_vec_24[1];

    // delay ap_idle for one cycle
    reg [0:0] AESL_inst_myproject$conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config31_U0$ap_idle;
    always @ (negedge reset or posedge clock) begin
        if (~reset) begin
            AESL_inst_myproject$conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config31_U0$ap_idle <= 'b0;
        end
        else begin
            AESL_inst_myproject$conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config31_U0$ap_idle <= AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config31_U0.ap_idle;
        end
    end
    // Process: AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config31_U0
    AESL_deadlock_detect_unit #(40, 25, 12, 12) AESL_deadlock_detect_unit_25 (
        .reset(reset),
        .clock(clock),
        .proc_dep_vld_vec(proc_dep_vld_vec_25),
        .in_chan_dep_vld_vec(in_chan_dep_vld_vec_25),
        .in_chan_dep_data_vec(in_chan_dep_data_vec_25),
        .token_in_vec(token_in_vec_25),
        .dl_detect_in(dl_detect_out),
        .origin(origin[25]),
        .token_clear(token_clear),
        .out_chan_dep_vld_vec(out_chan_dep_vld_vec_25),
        .out_chan_dep_data(out_chan_dep_data_25),
        .token_out_vec(token_out_vec_25),
        .dl_detect_out(dl_in_vec[25]));

    assign proc_dep_vld_vec_25[0] = dl_detect_out ? proc_dep_vld_vec_25_reg[0] : (~AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config31_U0.data_V_V_blk_n);
    assign proc_dep_vld_vec_25[1] = dl_detect_out ? proc_dep_vld_vec_25_reg[1] : (~AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config31_U0.res_V_V_blk_n | (~AESL_inst_myproject.start_for_leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_confiB1e_U.if_full_n & AESL_inst_myproject.leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_config34_579_U0.ap_done));
    assign proc_dep_vld_vec_25[2] = dl_detect_out ? proc_dep_vld_vec_25_reg[2] : (((AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config31_U0_ap_ready_count[0]) & AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config31_U0.ap_idle & ~(AESL_inst_myproject.Block_ap_fixed_base_exit3121_proc_U0_ap_ready_count[0])));
    assign proc_dep_vld_vec_25[3] = dl_detect_out ? proc_dep_vld_vec_25_reg[3] : (((AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config31_U0_ap_ready_count[0]) & AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config31_U0.ap_idle & ~(AESL_inst_myproject.resize_nearest_me_ap_fixed_32_16_5_3_0_config2_U0_ap_ready_count[0])));
    assign proc_dep_vld_vec_25[4] = dl_detect_out ? proc_dep_vld_vec_25_reg[4] : (((AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config31_U0_ap_ready_count[0]) & AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config31_U0.ap_idle & ~(AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config13_U0_ap_ready_count[0])));
    assign proc_dep_vld_vec_25[5] = dl_detect_out ? proc_dep_vld_vec_25_reg[5] : (((AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config31_U0_ap_ready_count[0]) & AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config31_U0.ap_idle & ~(AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config18_U0_ap_ready_count[0])));
    assign proc_dep_vld_vec_25[6] = dl_detect_out ? proc_dep_vld_vec_25_reg[6] : (((AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config31_U0_ap_ready_count[0]) & AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config31_U0.ap_idle & ~(AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config22_U0_ap_ready_count[0])));
    assign proc_dep_vld_vec_25[7] = dl_detect_out ? proc_dep_vld_vec_25_reg[7] : (((AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config31_U0_ap_ready_count[0]) & AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config31_U0.ap_idle & ~(AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config27_U0_ap_ready_count[0])));
    assign proc_dep_vld_vec_25[8] = dl_detect_out ? proc_dep_vld_vec_25_reg[8] : (((AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config31_U0_ap_ready_count[0]) & AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config31_U0.ap_idle & ~(AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config36_U0_ap_ready_count[0])));
    assign proc_dep_vld_vec_25[9] = dl_detect_out ? proc_dep_vld_vec_25_reg[9] : (((AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config31_U0_ap_ready_count[0]) & AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config31_U0.ap_idle & ~(AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config40_U0_ap_ready_count[0])));
    assign proc_dep_vld_vec_25[10] = dl_detect_out ? proc_dep_vld_vec_25_reg[10] : (((AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config31_U0_ap_ready_count[0]) & AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config31_U0.ap_idle & ~(AESL_inst_myproject.dense_large_stream_me_ap_fixed_ap_fixed_config45_U0_ap_ready_count[0])));
    assign proc_dep_vld_vec_25[11] = dl_detect_out ? proc_dep_vld_vec_25_reg[11] : (((AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config31_U0_ap_ready_count[0]) & AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config31_U0.ap_idle & ~(AESL_inst_myproject.dense_large_stream_me_ap_fixed_ap_fixed_config49_U0_ap_ready_count[0])));
    always @ (negedge reset or posedge clock) begin
        if (~reset) begin
            proc_dep_vld_vec_25_reg <= 'b0;
        end
        else begin
            proc_dep_vld_vec_25_reg <= proc_dep_vld_vec_25;
        end
    end
    assign in_chan_dep_vld_vec_25[0] = dep_chan_vld_0_25;
    assign in_chan_dep_data_vec_25[39 : 0] = dep_chan_data_0_25;
    assign token_in_vec_25[0] = token_0_25;
    assign in_chan_dep_vld_vec_25[1] = dep_chan_vld_1_25;
    assign in_chan_dep_data_vec_25[79 : 40] = dep_chan_data_1_25;
    assign token_in_vec_25[1] = token_1_25;
    assign in_chan_dep_vld_vec_25[2] = dep_chan_vld_11_25;
    assign in_chan_dep_data_vec_25[119 : 80] = dep_chan_data_11_25;
    assign token_in_vec_25[2] = token_11_25;
    assign in_chan_dep_vld_vec_25[3] = dep_chan_vld_15_25;
    assign in_chan_dep_data_vec_25[159 : 120] = dep_chan_data_15_25;
    assign token_in_vec_25[3] = token_15_25;
    assign in_chan_dep_vld_vec_25[4] = dep_chan_vld_18_25;
    assign in_chan_dep_data_vec_25[199 : 160] = dep_chan_data_18_25;
    assign token_in_vec_25[4] = token_18_25;
    assign in_chan_dep_vld_vec_25[5] = dep_chan_vld_22_25;
    assign in_chan_dep_data_vec_25[239 : 200] = dep_chan_data_22_25;
    assign token_in_vec_25[5] = token_22_25;
    assign in_chan_dep_vld_vec_25[6] = dep_chan_vld_24_25;
    assign in_chan_dep_data_vec_25[279 : 240] = dep_chan_data_24_25;
    assign token_in_vec_25[6] = token_24_25;
    assign in_chan_dep_vld_vec_25[7] = dep_chan_vld_26_25;
    assign in_chan_dep_data_vec_25[319 : 280] = dep_chan_data_26_25;
    assign token_in_vec_25[7] = token_26_25;
    assign in_chan_dep_vld_vec_25[8] = dep_chan_vld_29_25;
    assign in_chan_dep_data_vec_25[359 : 320] = dep_chan_data_29_25;
    assign token_in_vec_25[8] = token_29_25;
    assign in_chan_dep_vld_vec_25[9] = dep_chan_vld_32_25;
    assign in_chan_dep_data_vec_25[399 : 360] = dep_chan_data_32_25;
    assign token_in_vec_25[9] = token_32_25;
    assign in_chan_dep_vld_vec_25[10] = dep_chan_vld_34_25;
    assign in_chan_dep_data_vec_25[439 : 400] = dep_chan_data_34_25;
    assign token_in_vec_25[10] = token_34_25;
    assign in_chan_dep_vld_vec_25[11] = dep_chan_vld_36_25;
    assign in_chan_dep_data_vec_25[479 : 440] = dep_chan_data_36_25;
    assign token_in_vec_25[11] = token_36_25;
    assign dep_chan_vld_25_24 = out_chan_dep_vld_vec_25[0];
    assign dep_chan_data_25_24 = out_chan_dep_data_25;
    assign token_25_24 = token_out_vec_25[0];
    assign dep_chan_vld_25_26 = out_chan_dep_vld_vec_25[1];
    assign dep_chan_data_25_26 = out_chan_dep_data_25;
    assign token_25_26 = token_out_vec_25[1];
    assign dep_chan_vld_25_0 = out_chan_dep_vld_vec_25[2];
    assign dep_chan_data_25_0 = out_chan_dep_data_25;
    assign token_25_0 = token_out_vec_25[2];
    assign dep_chan_vld_25_1 = out_chan_dep_vld_vec_25[3];
    assign dep_chan_data_25_1 = out_chan_dep_data_25;
    assign token_25_1 = token_out_vec_25[3];
    assign dep_chan_vld_25_11 = out_chan_dep_vld_vec_25[4];
    assign dep_chan_data_25_11 = out_chan_dep_data_25;
    assign token_25_11 = token_out_vec_25[4];
    assign dep_chan_vld_25_15 = out_chan_dep_vld_vec_25[5];
    assign dep_chan_data_25_15 = out_chan_dep_data_25;
    assign token_25_15 = token_out_vec_25[5];
    assign dep_chan_vld_25_18 = out_chan_dep_vld_vec_25[6];
    assign dep_chan_data_25_18 = out_chan_dep_data_25;
    assign token_25_18 = token_out_vec_25[6];
    assign dep_chan_vld_25_22 = out_chan_dep_vld_vec_25[7];
    assign dep_chan_data_25_22 = out_chan_dep_data_25;
    assign token_25_22 = token_out_vec_25[7];
    assign dep_chan_vld_25_29 = out_chan_dep_vld_vec_25[8];
    assign dep_chan_data_25_29 = out_chan_dep_data_25;
    assign token_25_29 = token_out_vec_25[8];
    assign dep_chan_vld_25_32 = out_chan_dep_vld_vec_25[9];
    assign dep_chan_data_25_32 = out_chan_dep_data_25;
    assign token_25_32 = token_out_vec_25[9];
    assign dep_chan_vld_25_34 = out_chan_dep_vld_vec_25[10];
    assign dep_chan_data_25_34 = out_chan_dep_data_25;
    assign token_25_34 = token_out_vec_25[10];
    assign dep_chan_vld_25_36 = out_chan_dep_vld_vec_25[11];
    assign dep_chan_data_25_36 = out_chan_dep_data_25;
    assign token_25_36 = token_out_vec_25[11];

    // delay ap_idle for one cycle
    reg [0:0] AESL_inst_myproject$leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_config34_579_U0$ap_idle;
    always @ (negedge reset or posedge clock) begin
        if (~reset) begin
            AESL_inst_myproject$leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_config34_579_U0$ap_idle <= 'b0;
        end
        else begin
            AESL_inst_myproject$leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_config34_579_U0$ap_idle <= AESL_inst_myproject.leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_config34_579_U0.ap_idle;
        end
    end
    // Process: AESL_inst_myproject.leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_config34_579_U0
    AESL_deadlock_detect_unit #(40, 26, 2, 2) AESL_deadlock_detect_unit_26 (
        .reset(reset),
        .clock(clock),
        .proc_dep_vld_vec(proc_dep_vld_vec_26),
        .in_chan_dep_vld_vec(in_chan_dep_vld_vec_26),
        .in_chan_dep_data_vec(in_chan_dep_data_vec_26),
        .token_in_vec(token_in_vec_26),
        .dl_detect_in(dl_detect_out),
        .origin(origin[26]),
        .token_clear(token_clear),
        .out_chan_dep_vld_vec(out_chan_dep_vld_vec_26),
        .out_chan_dep_data(out_chan_dep_data_26),
        .token_out_vec(token_out_vec_26),
        .dl_detect_out(dl_in_vec[26]));

    assign proc_dep_vld_vec_26[0] = dl_detect_out ? proc_dep_vld_vec_26_reg[0] : (~AESL_inst_myproject.leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_config34_579_U0.data_V_V_blk_n | (~AESL_inst_myproject.start_for_leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_confiB1e_U.if_empty_n & (AESL_inst_myproject.leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_config34_579_U0.ap_ready | AESL_inst_myproject$leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_config34_579_U0$ap_idle)));
    assign proc_dep_vld_vec_26[1] = dl_detect_out ? proc_dep_vld_vec_26_reg[1] : (~AESL_inst_myproject.leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_config34_579_U0.res_V_V_blk_n | (~AESL_inst_myproject.start_for_pooling2d_large_cl_nopad_pad_me_U0_U.if_full_n & AESL_inst_myproject.pooling2d_large_cl_nopad_pad_me_U0.ap_done));
    always @ (negedge reset or posedge clock) begin
        if (~reset) begin
            proc_dep_vld_vec_26_reg <= 'b0;
        end
        else begin
            proc_dep_vld_vec_26_reg <= proc_dep_vld_vec_26;
        end
    end
    assign in_chan_dep_vld_vec_26[0] = dep_chan_vld_25_26;
    assign in_chan_dep_data_vec_26[39 : 0] = dep_chan_data_25_26;
    assign token_in_vec_26[0] = token_25_26;
    assign in_chan_dep_vld_vec_26[1] = dep_chan_vld_27_26;
    assign in_chan_dep_data_vec_26[79 : 40] = dep_chan_data_27_26;
    assign token_in_vec_26[1] = token_27_26;
    assign dep_chan_vld_26_25 = out_chan_dep_vld_vec_26[0];
    assign dep_chan_data_26_25 = out_chan_dep_data_26;
    assign token_26_25 = token_out_vec_26[0];
    assign dep_chan_vld_26_27 = out_chan_dep_vld_vec_26[1];
    assign dep_chan_data_26_27 = out_chan_dep_data_26;
    assign token_26_27 = token_out_vec_26[1];

    // delay ap_idle for one cycle
    reg [0:0] AESL_inst_myproject$pooling2d_large_cl_nopad_pad_me_U0$ap_idle;
    always @ (negedge reset or posedge clock) begin
        if (~reset) begin
            AESL_inst_myproject$pooling2d_large_cl_nopad_pad_me_U0$ap_idle <= 'b0;
        end
        else begin
            AESL_inst_myproject$pooling2d_large_cl_nopad_pad_me_U0$ap_idle <= AESL_inst_myproject.pooling2d_large_cl_nopad_pad_me_U0.ap_idle;
        end
    end
    // Process: AESL_inst_myproject.pooling2d_large_cl_nopad_pad_me_U0
    AESL_deadlock_detect_unit #(40, 27, 2, 2) AESL_deadlock_detect_unit_27 (
        .reset(reset),
        .clock(clock),
        .proc_dep_vld_vec(proc_dep_vld_vec_27),
        .in_chan_dep_vld_vec(in_chan_dep_vld_vec_27),
        .in_chan_dep_data_vec(in_chan_dep_data_vec_27),
        .token_in_vec(token_in_vec_27),
        .dl_detect_in(dl_detect_out),
        .origin(origin[27]),
        .token_clear(token_clear),
        .out_chan_dep_vld_vec(out_chan_dep_vld_vec_27),
        .out_chan_dep_data(out_chan_dep_data_27),
        .token_out_vec(token_out_vec_27),
        .dl_detect_out(dl_in_vec[27]));

    assign proc_dep_vld_vec_27[0] = dl_detect_out ? proc_dep_vld_vec_27_reg[0] : (~AESL_inst_myproject.pooling2d_large_cl_nopad_pad_me_U0.data_V_V_blk_n | (~AESL_inst_myproject.start_for_pooling2d_large_cl_nopad_pad_me_U0_U.if_empty_n & (AESL_inst_myproject.pooling2d_large_cl_nopad_pad_me_U0.ap_ready | AESL_inst_myproject$pooling2d_large_cl_nopad_pad_me_U0$ap_idle)));
    assign proc_dep_vld_vec_27[1] = dl_detect_out ? proc_dep_vld_vec_27_reg[1] : (~AESL_inst_myproject.pooling2d_large_cl_nopad_pad_me_U0.res_V_V_blk_n | (~AESL_inst_myproject.start_for_zeropad2d_cl_me_ap_fixed_ap_fixed_config63_U0_U.if_full_n & AESL_inst_myproject.zeropad2d_cl_me_ap_fixed_ap_fixed_config63_U0.ap_done));
    always @ (negedge reset or posedge clock) begin
        if (~reset) begin
            proc_dep_vld_vec_27_reg <= 'b0;
        end
        else begin
            proc_dep_vld_vec_27_reg <= proc_dep_vld_vec_27;
        end
    end
    assign in_chan_dep_vld_vec_27[0] = dep_chan_vld_26_27;
    assign in_chan_dep_data_vec_27[39 : 0] = dep_chan_data_26_27;
    assign token_in_vec_27[0] = token_26_27;
    assign in_chan_dep_vld_vec_27[1] = dep_chan_vld_28_27;
    assign in_chan_dep_data_vec_27[79 : 40] = dep_chan_data_28_27;
    assign token_in_vec_27[1] = token_28_27;
    assign dep_chan_vld_27_26 = out_chan_dep_vld_vec_27[0];
    assign dep_chan_data_27_26 = out_chan_dep_data_27;
    assign token_27_26 = token_out_vec_27[0];
    assign dep_chan_vld_27_28 = out_chan_dep_vld_vec_27[1];
    assign dep_chan_data_27_28 = out_chan_dep_data_27;
    assign token_27_28 = token_out_vec_27[1];

    // delay ap_idle for one cycle
    reg [0:0] AESL_inst_myproject$zeropad2d_cl_me_ap_fixed_ap_fixed_config63_U0$ap_idle;
    always @ (negedge reset or posedge clock) begin
        if (~reset) begin
            AESL_inst_myproject$zeropad2d_cl_me_ap_fixed_ap_fixed_config63_U0$ap_idle <= 'b0;
        end
        else begin
            AESL_inst_myproject$zeropad2d_cl_me_ap_fixed_ap_fixed_config63_U0$ap_idle <= AESL_inst_myproject.zeropad2d_cl_me_ap_fixed_ap_fixed_config63_U0.ap_idle;
        end
    end
    // Process: AESL_inst_myproject.zeropad2d_cl_me_ap_fixed_ap_fixed_config63_U0
    AESL_deadlock_detect_unit #(40, 28, 2, 2) AESL_deadlock_detect_unit_28 (
        .reset(reset),
        .clock(clock),
        .proc_dep_vld_vec(proc_dep_vld_vec_28),
        .in_chan_dep_vld_vec(in_chan_dep_vld_vec_28),
        .in_chan_dep_data_vec(in_chan_dep_data_vec_28),
        .token_in_vec(token_in_vec_28),
        .dl_detect_in(dl_detect_out),
        .origin(origin[28]),
        .token_clear(token_clear),
        .out_chan_dep_vld_vec(out_chan_dep_vld_vec_28),
        .out_chan_dep_data(out_chan_dep_data_28),
        .token_out_vec(token_out_vec_28),
        .dl_detect_out(dl_in_vec[28]));

    assign proc_dep_vld_vec_28[0] = dl_detect_out ? proc_dep_vld_vec_28_reg[0] : (~AESL_inst_myproject.zeropad2d_cl_me_ap_fixed_ap_fixed_config63_U0.data_V_V_blk_n | (~AESL_inst_myproject.start_for_zeropad2d_cl_me_ap_fixed_ap_fixed_config63_U0_U.if_empty_n & (AESL_inst_myproject.zeropad2d_cl_me_ap_fixed_ap_fixed_config63_U0.ap_ready | AESL_inst_myproject$zeropad2d_cl_me_ap_fixed_ap_fixed_config63_U0$ap_idle)));
    assign proc_dep_vld_vec_28[1] = dl_detect_out ? proc_dep_vld_vec_28_reg[1] : (~AESL_inst_myproject.zeropad2d_cl_me_ap_fixed_ap_fixed_config63_U0.res_V_V_blk_n);
    always @ (negedge reset or posedge clock) begin
        if (~reset) begin
            proc_dep_vld_vec_28_reg <= 'b0;
        end
        else begin
            proc_dep_vld_vec_28_reg <= proc_dep_vld_vec_28;
        end
    end
    assign in_chan_dep_vld_vec_28[0] = dep_chan_vld_27_28;
    assign in_chan_dep_data_vec_28[39 : 0] = dep_chan_data_27_28;
    assign token_in_vec_28[0] = token_27_28;
    assign in_chan_dep_vld_vec_28[1] = dep_chan_vld_29_28;
    assign in_chan_dep_data_vec_28[79 : 40] = dep_chan_data_29_28;
    assign token_in_vec_28[1] = token_29_28;
    assign dep_chan_vld_28_27 = out_chan_dep_vld_vec_28[0];
    assign dep_chan_data_28_27 = out_chan_dep_data_28;
    assign token_28_27 = token_out_vec_28[0];
    assign dep_chan_vld_28_29 = out_chan_dep_vld_vec_28[1];
    assign dep_chan_data_28_29 = out_chan_dep_data_28;
    assign token_28_29 = token_out_vec_28[1];

    // delay ap_idle for one cycle
    reg [0:0] AESL_inst_myproject$conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config36_U0$ap_idle;
    always @ (negedge reset or posedge clock) begin
        if (~reset) begin
            AESL_inst_myproject$conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config36_U0$ap_idle <= 'b0;
        end
        else begin
            AESL_inst_myproject$conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config36_U0$ap_idle <= AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config36_U0.ap_idle;
        end
    end
    // Process: AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config36_U0
    AESL_deadlock_detect_unit #(40, 29, 12, 12) AESL_deadlock_detect_unit_29 (
        .reset(reset),
        .clock(clock),
        .proc_dep_vld_vec(proc_dep_vld_vec_29),
        .in_chan_dep_vld_vec(in_chan_dep_vld_vec_29),
        .in_chan_dep_data_vec(in_chan_dep_data_vec_29),
        .token_in_vec(token_in_vec_29),
        .dl_detect_in(dl_detect_out),
        .origin(origin[29]),
        .token_clear(token_clear),
        .out_chan_dep_vld_vec(out_chan_dep_vld_vec_29),
        .out_chan_dep_data(out_chan_dep_data_29),
        .token_out_vec(token_out_vec_29),
        .dl_detect_out(dl_in_vec[29]));

    assign proc_dep_vld_vec_29[0] = dl_detect_out ? proc_dep_vld_vec_29_reg[0] : (~AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config36_U0.data_V_V_blk_n);
    assign proc_dep_vld_vec_29[1] = dl_detect_out ? proc_dep_vld_vec_29_reg[1] : (~AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config36_U0.res_V_V_blk_n | (~AESL_inst_myproject.start_for_leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_confiB2e_U.if_full_n & AESL_inst_myproject.leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_config39_580_U0.ap_done));
    assign proc_dep_vld_vec_29[2] = dl_detect_out ? proc_dep_vld_vec_29_reg[2] : (((AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config36_U0_ap_ready_count[0]) & AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config36_U0.ap_idle & ~(AESL_inst_myproject.Block_ap_fixed_base_exit3121_proc_U0_ap_ready_count[0])));
    assign proc_dep_vld_vec_29[3] = dl_detect_out ? proc_dep_vld_vec_29_reg[3] : (((AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config36_U0_ap_ready_count[0]) & AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config36_U0.ap_idle & ~(AESL_inst_myproject.resize_nearest_me_ap_fixed_32_16_5_3_0_config2_U0_ap_ready_count[0])));
    assign proc_dep_vld_vec_29[4] = dl_detect_out ? proc_dep_vld_vec_29_reg[4] : (((AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config36_U0_ap_ready_count[0]) & AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config36_U0.ap_idle & ~(AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config13_U0_ap_ready_count[0])));
    assign proc_dep_vld_vec_29[5] = dl_detect_out ? proc_dep_vld_vec_29_reg[5] : (((AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config36_U0_ap_ready_count[0]) & AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config36_U0.ap_idle & ~(AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config18_U0_ap_ready_count[0])));
    assign proc_dep_vld_vec_29[6] = dl_detect_out ? proc_dep_vld_vec_29_reg[6] : (((AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config36_U0_ap_ready_count[0]) & AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config36_U0.ap_idle & ~(AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config22_U0_ap_ready_count[0])));
    assign proc_dep_vld_vec_29[7] = dl_detect_out ? proc_dep_vld_vec_29_reg[7] : (((AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config36_U0_ap_ready_count[0]) & AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config36_U0.ap_idle & ~(AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config27_U0_ap_ready_count[0])));
    assign proc_dep_vld_vec_29[8] = dl_detect_out ? proc_dep_vld_vec_29_reg[8] : (((AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config36_U0_ap_ready_count[0]) & AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config36_U0.ap_idle & ~(AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config31_U0_ap_ready_count[0])));
    assign proc_dep_vld_vec_29[9] = dl_detect_out ? proc_dep_vld_vec_29_reg[9] : (((AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config36_U0_ap_ready_count[0]) & AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config36_U0.ap_idle & ~(AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config40_U0_ap_ready_count[0])));
    assign proc_dep_vld_vec_29[10] = dl_detect_out ? proc_dep_vld_vec_29_reg[10] : (((AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config36_U0_ap_ready_count[0]) & AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config36_U0.ap_idle & ~(AESL_inst_myproject.dense_large_stream_me_ap_fixed_ap_fixed_config45_U0_ap_ready_count[0])));
    assign proc_dep_vld_vec_29[11] = dl_detect_out ? proc_dep_vld_vec_29_reg[11] : (((AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config36_U0_ap_ready_count[0]) & AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config36_U0.ap_idle & ~(AESL_inst_myproject.dense_large_stream_me_ap_fixed_ap_fixed_config49_U0_ap_ready_count[0])));
    always @ (negedge reset or posedge clock) begin
        if (~reset) begin
            proc_dep_vld_vec_29_reg <= 'b0;
        end
        else begin
            proc_dep_vld_vec_29_reg <= proc_dep_vld_vec_29;
        end
    end
    assign in_chan_dep_vld_vec_29[0] = dep_chan_vld_0_29;
    assign in_chan_dep_data_vec_29[39 : 0] = dep_chan_data_0_29;
    assign token_in_vec_29[0] = token_0_29;
    assign in_chan_dep_vld_vec_29[1] = dep_chan_vld_1_29;
    assign in_chan_dep_data_vec_29[79 : 40] = dep_chan_data_1_29;
    assign token_in_vec_29[1] = token_1_29;
    assign in_chan_dep_vld_vec_29[2] = dep_chan_vld_11_29;
    assign in_chan_dep_data_vec_29[119 : 80] = dep_chan_data_11_29;
    assign token_in_vec_29[2] = token_11_29;
    assign in_chan_dep_vld_vec_29[3] = dep_chan_vld_15_29;
    assign in_chan_dep_data_vec_29[159 : 120] = dep_chan_data_15_29;
    assign token_in_vec_29[3] = token_15_29;
    assign in_chan_dep_vld_vec_29[4] = dep_chan_vld_18_29;
    assign in_chan_dep_data_vec_29[199 : 160] = dep_chan_data_18_29;
    assign token_in_vec_29[4] = token_18_29;
    assign in_chan_dep_vld_vec_29[5] = dep_chan_vld_22_29;
    assign in_chan_dep_data_vec_29[239 : 200] = dep_chan_data_22_29;
    assign token_in_vec_29[5] = token_22_29;
    assign in_chan_dep_vld_vec_29[6] = dep_chan_vld_25_29;
    assign in_chan_dep_data_vec_29[279 : 240] = dep_chan_data_25_29;
    assign token_in_vec_29[6] = token_25_29;
    assign in_chan_dep_vld_vec_29[7] = dep_chan_vld_28_29;
    assign in_chan_dep_data_vec_29[319 : 280] = dep_chan_data_28_29;
    assign token_in_vec_29[7] = token_28_29;
    assign in_chan_dep_vld_vec_29[8] = dep_chan_vld_30_29;
    assign in_chan_dep_data_vec_29[359 : 320] = dep_chan_data_30_29;
    assign token_in_vec_29[8] = token_30_29;
    assign in_chan_dep_vld_vec_29[9] = dep_chan_vld_32_29;
    assign in_chan_dep_data_vec_29[399 : 360] = dep_chan_data_32_29;
    assign token_in_vec_29[9] = token_32_29;
    assign in_chan_dep_vld_vec_29[10] = dep_chan_vld_34_29;
    assign in_chan_dep_data_vec_29[439 : 400] = dep_chan_data_34_29;
    assign token_in_vec_29[10] = token_34_29;
    assign in_chan_dep_vld_vec_29[11] = dep_chan_vld_36_29;
    assign in_chan_dep_data_vec_29[479 : 440] = dep_chan_data_36_29;
    assign token_in_vec_29[11] = token_36_29;
    assign dep_chan_vld_29_28 = out_chan_dep_vld_vec_29[0];
    assign dep_chan_data_29_28 = out_chan_dep_data_29;
    assign token_29_28 = token_out_vec_29[0];
    assign dep_chan_vld_29_30 = out_chan_dep_vld_vec_29[1];
    assign dep_chan_data_29_30 = out_chan_dep_data_29;
    assign token_29_30 = token_out_vec_29[1];
    assign dep_chan_vld_29_0 = out_chan_dep_vld_vec_29[2];
    assign dep_chan_data_29_0 = out_chan_dep_data_29;
    assign token_29_0 = token_out_vec_29[2];
    assign dep_chan_vld_29_1 = out_chan_dep_vld_vec_29[3];
    assign dep_chan_data_29_1 = out_chan_dep_data_29;
    assign token_29_1 = token_out_vec_29[3];
    assign dep_chan_vld_29_11 = out_chan_dep_vld_vec_29[4];
    assign dep_chan_data_29_11 = out_chan_dep_data_29;
    assign token_29_11 = token_out_vec_29[4];
    assign dep_chan_vld_29_15 = out_chan_dep_vld_vec_29[5];
    assign dep_chan_data_29_15 = out_chan_dep_data_29;
    assign token_29_15 = token_out_vec_29[5];
    assign dep_chan_vld_29_18 = out_chan_dep_vld_vec_29[6];
    assign dep_chan_data_29_18 = out_chan_dep_data_29;
    assign token_29_18 = token_out_vec_29[6];
    assign dep_chan_vld_29_22 = out_chan_dep_vld_vec_29[7];
    assign dep_chan_data_29_22 = out_chan_dep_data_29;
    assign token_29_22 = token_out_vec_29[7];
    assign dep_chan_vld_29_25 = out_chan_dep_vld_vec_29[8];
    assign dep_chan_data_29_25 = out_chan_dep_data_29;
    assign token_29_25 = token_out_vec_29[8];
    assign dep_chan_vld_29_32 = out_chan_dep_vld_vec_29[9];
    assign dep_chan_data_29_32 = out_chan_dep_data_29;
    assign token_29_32 = token_out_vec_29[9];
    assign dep_chan_vld_29_34 = out_chan_dep_vld_vec_29[10];
    assign dep_chan_data_29_34 = out_chan_dep_data_29;
    assign token_29_34 = token_out_vec_29[10];
    assign dep_chan_vld_29_36 = out_chan_dep_vld_vec_29[11];
    assign dep_chan_data_29_36 = out_chan_dep_data_29;
    assign token_29_36 = token_out_vec_29[11];

    // delay ap_idle for one cycle
    reg [0:0] AESL_inst_myproject$leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_config39_580_U0$ap_idle;
    always @ (negedge reset or posedge clock) begin
        if (~reset) begin
            AESL_inst_myproject$leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_config39_580_U0$ap_idle <= 'b0;
        end
        else begin
            AESL_inst_myproject$leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_config39_580_U0$ap_idle <= AESL_inst_myproject.leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_config39_580_U0.ap_idle;
        end
    end
    // Process: AESL_inst_myproject.leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_config39_580_U0
    AESL_deadlock_detect_unit #(40, 30, 2, 2) AESL_deadlock_detect_unit_30 (
        .reset(reset),
        .clock(clock),
        .proc_dep_vld_vec(proc_dep_vld_vec_30),
        .in_chan_dep_vld_vec(in_chan_dep_vld_vec_30),
        .in_chan_dep_data_vec(in_chan_dep_data_vec_30),
        .token_in_vec(token_in_vec_30),
        .dl_detect_in(dl_detect_out),
        .origin(origin[30]),
        .token_clear(token_clear),
        .out_chan_dep_vld_vec(out_chan_dep_vld_vec_30),
        .out_chan_dep_data(out_chan_dep_data_30),
        .token_out_vec(token_out_vec_30),
        .dl_detect_out(dl_in_vec[30]));

    assign proc_dep_vld_vec_30[0] = dl_detect_out ? proc_dep_vld_vec_30_reg[0] : (~AESL_inst_myproject.leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_config39_580_U0.data_V_V_blk_n | (~AESL_inst_myproject.start_for_leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_confiB2e_U.if_empty_n & (AESL_inst_myproject.leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_config39_580_U0.ap_ready | AESL_inst_myproject$leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_config39_580_U0$ap_idle)));
    assign proc_dep_vld_vec_30[1] = dl_detect_out ? proc_dep_vld_vec_30_reg[1] : (~AESL_inst_myproject.leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_config39_580_U0.res_V_V_blk_n | (~AESL_inst_myproject.start_for_zeropad2d_cl_me_ap_fixed_ap_fixed_config64_U0_U.if_full_n & AESL_inst_myproject.zeropad2d_cl_me_ap_fixed_ap_fixed_config64_U0.ap_done));
    always @ (negedge reset or posedge clock) begin
        if (~reset) begin
            proc_dep_vld_vec_30_reg <= 'b0;
        end
        else begin
            proc_dep_vld_vec_30_reg <= proc_dep_vld_vec_30;
        end
    end
    assign in_chan_dep_vld_vec_30[0] = dep_chan_vld_29_30;
    assign in_chan_dep_data_vec_30[39 : 0] = dep_chan_data_29_30;
    assign token_in_vec_30[0] = token_29_30;
    assign in_chan_dep_vld_vec_30[1] = dep_chan_vld_31_30;
    assign in_chan_dep_data_vec_30[79 : 40] = dep_chan_data_31_30;
    assign token_in_vec_30[1] = token_31_30;
    assign dep_chan_vld_30_29 = out_chan_dep_vld_vec_30[0];
    assign dep_chan_data_30_29 = out_chan_dep_data_30;
    assign token_30_29 = token_out_vec_30[0];
    assign dep_chan_vld_30_31 = out_chan_dep_vld_vec_30[1];
    assign dep_chan_data_30_31 = out_chan_dep_data_30;
    assign token_30_31 = token_out_vec_30[1];

    // delay ap_idle for one cycle
    reg [0:0] AESL_inst_myproject$zeropad2d_cl_me_ap_fixed_ap_fixed_config64_U0$ap_idle;
    always @ (negedge reset or posedge clock) begin
        if (~reset) begin
            AESL_inst_myproject$zeropad2d_cl_me_ap_fixed_ap_fixed_config64_U0$ap_idle <= 'b0;
        end
        else begin
            AESL_inst_myproject$zeropad2d_cl_me_ap_fixed_ap_fixed_config64_U0$ap_idle <= AESL_inst_myproject.zeropad2d_cl_me_ap_fixed_ap_fixed_config64_U0.ap_idle;
        end
    end
    // Process: AESL_inst_myproject.zeropad2d_cl_me_ap_fixed_ap_fixed_config64_U0
    AESL_deadlock_detect_unit #(40, 31, 2, 2) AESL_deadlock_detect_unit_31 (
        .reset(reset),
        .clock(clock),
        .proc_dep_vld_vec(proc_dep_vld_vec_31),
        .in_chan_dep_vld_vec(in_chan_dep_vld_vec_31),
        .in_chan_dep_data_vec(in_chan_dep_data_vec_31),
        .token_in_vec(token_in_vec_31),
        .dl_detect_in(dl_detect_out),
        .origin(origin[31]),
        .token_clear(token_clear),
        .out_chan_dep_vld_vec(out_chan_dep_vld_vec_31),
        .out_chan_dep_data(out_chan_dep_data_31),
        .token_out_vec(token_out_vec_31),
        .dl_detect_out(dl_in_vec[31]));

    assign proc_dep_vld_vec_31[0] = dl_detect_out ? proc_dep_vld_vec_31_reg[0] : (~AESL_inst_myproject.zeropad2d_cl_me_ap_fixed_ap_fixed_config64_U0.data_V_V_blk_n | (~AESL_inst_myproject.start_for_zeropad2d_cl_me_ap_fixed_ap_fixed_config64_U0_U.if_empty_n & (AESL_inst_myproject.zeropad2d_cl_me_ap_fixed_ap_fixed_config64_U0.ap_ready | AESL_inst_myproject$zeropad2d_cl_me_ap_fixed_ap_fixed_config64_U0$ap_idle)));
    assign proc_dep_vld_vec_31[1] = dl_detect_out ? proc_dep_vld_vec_31_reg[1] : (~AESL_inst_myproject.zeropad2d_cl_me_ap_fixed_ap_fixed_config64_U0.res_V_V_blk_n);
    always @ (negedge reset or posedge clock) begin
        if (~reset) begin
            proc_dep_vld_vec_31_reg <= 'b0;
        end
        else begin
            proc_dep_vld_vec_31_reg <= proc_dep_vld_vec_31;
        end
    end
    assign in_chan_dep_vld_vec_31[0] = dep_chan_vld_30_31;
    assign in_chan_dep_data_vec_31[39 : 0] = dep_chan_data_30_31;
    assign token_in_vec_31[0] = token_30_31;
    assign in_chan_dep_vld_vec_31[1] = dep_chan_vld_32_31;
    assign in_chan_dep_data_vec_31[79 : 40] = dep_chan_data_32_31;
    assign token_in_vec_31[1] = token_32_31;
    assign dep_chan_vld_31_30 = out_chan_dep_vld_vec_31[0];
    assign dep_chan_data_31_30 = out_chan_dep_data_31;
    assign token_31_30 = token_out_vec_31[0];
    assign dep_chan_vld_31_32 = out_chan_dep_vld_vec_31[1];
    assign dep_chan_data_31_32 = out_chan_dep_data_31;
    assign token_31_32 = token_out_vec_31[1];

    // delay ap_idle for one cycle
    reg [0:0] AESL_inst_myproject$conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config40_U0$ap_idle;
    always @ (negedge reset or posedge clock) begin
        if (~reset) begin
            AESL_inst_myproject$conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config40_U0$ap_idle <= 'b0;
        end
        else begin
            AESL_inst_myproject$conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config40_U0$ap_idle <= AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config40_U0.ap_idle;
        end
    end
    // Process: AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config40_U0
    AESL_deadlock_detect_unit #(40, 32, 12, 12) AESL_deadlock_detect_unit_32 (
        .reset(reset),
        .clock(clock),
        .proc_dep_vld_vec(proc_dep_vld_vec_32),
        .in_chan_dep_vld_vec(in_chan_dep_vld_vec_32),
        .in_chan_dep_data_vec(in_chan_dep_data_vec_32),
        .token_in_vec(token_in_vec_32),
        .dl_detect_in(dl_detect_out),
        .origin(origin[32]),
        .token_clear(token_clear),
        .out_chan_dep_vld_vec(out_chan_dep_vld_vec_32),
        .out_chan_dep_data(out_chan_dep_data_32),
        .token_out_vec(token_out_vec_32),
        .dl_detect_out(dl_in_vec[32]));

    assign proc_dep_vld_vec_32[0] = dl_detect_out ? proc_dep_vld_vec_32_reg[0] : (~AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config40_U0.data_V_V_blk_n);
    assign proc_dep_vld_vec_32[1] = dl_detect_out ? proc_dep_vld_vec_32_reg[1] : (~AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config40_U0.res_V_V_blk_n | (~AESL_inst_myproject.start_for_leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_confiB3e_U.if_full_n & AESL_inst_myproject.leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_config43_581_U0.ap_done));
    assign proc_dep_vld_vec_32[2] = dl_detect_out ? proc_dep_vld_vec_32_reg[2] : (((AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config40_U0_ap_ready_count[0]) & AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config40_U0.ap_idle & ~(AESL_inst_myproject.Block_ap_fixed_base_exit3121_proc_U0_ap_ready_count[0])));
    assign proc_dep_vld_vec_32[3] = dl_detect_out ? proc_dep_vld_vec_32_reg[3] : (((AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config40_U0_ap_ready_count[0]) & AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config40_U0.ap_idle & ~(AESL_inst_myproject.resize_nearest_me_ap_fixed_32_16_5_3_0_config2_U0_ap_ready_count[0])));
    assign proc_dep_vld_vec_32[4] = dl_detect_out ? proc_dep_vld_vec_32_reg[4] : (((AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config40_U0_ap_ready_count[0]) & AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config40_U0.ap_idle & ~(AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config13_U0_ap_ready_count[0])));
    assign proc_dep_vld_vec_32[5] = dl_detect_out ? proc_dep_vld_vec_32_reg[5] : (((AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config40_U0_ap_ready_count[0]) & AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config40_U0.ap_idle & ~(AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config18_U0_ap_ready_count[0])));
    assign proc_dep_vld_vec_32[6] = dl_detect_out ? proc_dep_vld_vec_32_reg[6] : (((AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config40_U0_ap_ready_count[0]) & AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config40_U0.ap_idle & ~(AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config22_U0_ap_ready_count[0])));
    assign proc_dep_vld_vec_32[7] = dl_detect_out ? proc_dep_vld_vec_32_reg[7] : (((AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config40_U0_ap_ready_count[0]) & AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config40_U0.ap_idle & ~(AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config27_U0_ap_ready_count[0])));
    assign proc_dep_vld_vec_32[8] = dl_detect_out ? proc_dep_vld_vec_32_reg[8] : (((AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config40_U0_ap_ready_count[0]) & AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config40_U0.ap_idle & ~(AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config31_U0_ap_ready_count[0])));
    assign proc_dep_vld_vec_32[9] = dl_detect_out ? proc_dep_vld_vec_32_reg[9] : (((AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config40_U0_ap_ready_count[0]) & AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config40_U0.ap_idle & ~(AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config36_U0_ap_ready_count[0])));
    assign proc_dep_vld_vec_32[10] = dl_detect_out ? proc_dep_vld_vec_32_reg[10] : (((AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config40_U0_ap_ready_count[0]) & AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config40_U0.ap_idle & ~(AESL_inst_myproject.dense_large_stream_me_ap_fixed_ap_fixed_config45_U0_ap_ready_count[0])));
    assign proc_dep_vld_vec_32[11] = dl_detect_out ? proc_dep_vld_vec_32_reg[11] : (((AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config40_U0_ap_ready_count[0]) & AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config40_U0.ap_idle & ~(AESL_inst_myproject.dense_large_stream_me_ap_fixed_ap_fixed_config49_U0_ap_ready_count[0])));
    always @ (negedge reset or posedge clock) begin
        if (~reset) begin
            proc_dep_vld_vec_32_reg <= 'b0;
        end
        else begin
            proc_dep_vld_vec_32_reg <= proc_dep_vld_vec_32;
        end
    end
    assign in_chan_dep_vld_vec_32[0] = dep_chan_vld_0_32;
    assign in_chan_dep_data_vec_32[39 : 0] = dep_chan_data_0_32;
    assign token_in_vec_32[0] = token_0_32;
    assign in_chan_dep_vld_vec_32[1] = dep_chan_vld_1_32;
    assign in_chan_dep_data_vec_32[79 : 40] = dep_chan_data_1_32;
    assign token_in_vec_32[1] = token_1_32;
    assign in_chan_dep_vld_vec_32[2] = dep_chan_vld_11_32;
    assign in_chan_dep_data_vec_32[119 : 80] = dep_chan_data_11_32;
    assign token_in_vec_32[2] = token_11_32;
    assign in_chan_dep_vld_vec_32[3] = dep_chan_vld_15_32;
    assign in_chan_dep_data_vec_32[159 : 120] = dep_chan_data_15_32;
    assign token_in_vec_32[3] = token_15_32;
    assign in_chan_dep_vld_vec_32[4] = dep_chan_vld_18_32;
    assign in_chan_dep_data_vec_32[199 : 160] = dep_chan_data_18_32;
    assign token_in_vec_32[4] = token_18_32;
    assign in_chan_dep_vld_vec_32[5] = dep_chan_vld_22_32;
    assign in_chan_dep_data_vec_32[239 : 200] = dep_chan_data_22_32;
    assign token_in_vec_32[5] = token_22_32;
    assign in_chan_dep_vld_vec_32[6] = dep_chan_vld_25_32;
    assign in_chan_dep_data_vec_32[279 : 240] = dep_chan_data_25_32;
    assign token_in_vec_32[6] = token_25_32;
    assign in_chan_dep_vld_vec_32[7] = dep_chan_vld_29_32;
    assign in_chan_dep_data_vec_32[319 : 280] = dep_chan_data_29_32;
    assign token_in_vec_32[7] = token_29_32;
    assign in_chan_dep_vld_vec_32[8] = dep_chan_vld_31_32;
    assign in_chan_dep_data_vec_32[359 : 320] = dep_chan_data_31_32;
    assign token_in_vec_32[8] = token_31_32;
    assign in_chan_dep_vld_vec_32[9] = dep_chan_vld_33_32;
    assign in_chan_dep_data_vec_32[399 : 360] = dep_chan_data_33_32;
    assign token_in_vec_32[9] = token_33_32;
    assign in_chan_dep_vld_vec_32[10] = dep_chan_vld_34_32;
    assign in_chan_dep_data_vec_32[439 : 400] = dep_chan_data_34_32;
    assign token_in_vec_32[10] = token_34_32;
    assign in_chan_dep_vld_vec_32[11] = dep_chan_vld_36_32;
    assign in_chan_dep_data_vec_32[479 : 440] = dep_chan_data_36_32;
    assign token_in_vec_32[11] = token_36_32;
    assign dep_chan_vld_32_31 = out_chan_dep_vld_vec_32[0];
    assign dep_chan_data_32_31 = out_chan_dep_data_32;
    assign token_32_31 = token_out_vec_32[0];
    assign dep_chan_vld_32_33 = out_chan_dep_vld_vec_32[1];
    assign dep_chan_data_32_33 = out_chan_dep_data_32;
    assign token_32_33 = token_out_vec_32[1];
    assign dep_chan_vld_32_0 = out_chan_dep_vld_vec_32[2];
    assign dep_chan_data_32_0 = out_chan_dep_data_32;
    assign token_32_0 = token_out_vec_32[2];
    assign dep_chan_vld_32_1 = out_chan_dep_vld_vec_32[3];
    assign dep_chan_data_32_1 = out_chan_dep_data_32;
    assign token_32_1 = token_out_vec_32[3];
    assign dep_chan_vld_32_11 = out_chan_dep_vld_vec_32[4];
    assign dep_chan_data_32_11 = out_chan_dep_data_32;
    assign token_32_11 = token_out_vec_32[4];
    assign dep_chan_vld_32_15 = out_chan_dep_vld_vec_32[5];
    assign dep_chan_data_32_15 = out_chan_dep_data_32;
    assign token_32_15 = token_out_vec_32[5];
    assign dep_chan_vld_32_18 = out_chan_dep_vld_vec_32[6];
    assign dep_chan_data_32_18 = out_chan_dep_data_32;
    assign token_32_18 = token_out_vec_32[6];
    assign dep_chan_vld_32_22 = out_chan_dep_vld_vec_32[7];
    assign dep_chan_data_32_22 = out_chan_dep_data_32;
    assign token_32_22 = token_out_vec_32[7];
    assign dep_chan_vld_32_25 = out_chan_dep_vld_vec_32[8];
    assign dep_chan_data_32_25 = out_chan_dep_data_32;
    assign token_32_25 = token_out_vec_32[8];
    assign dep_chan_vld_32_29 = out_chan_dep_vld_vec_32[9];
    assign dep_chan_data_32_29 = out_chan_dep_data_32;
    assign token_32_29 = token_out_vec_32[9];
    assign dep_chan_vld_32_34 = out_chan_dep_vld_vec_32[10];
    assign dep_chan_data_32_34 = out_chan_dep_data_32;
    assign token_32_34 = token_out_vec_32[10];
    assign dep_chan_vld_32_36 = out_chan_dep_vld_vec_32[11];
    assign dep_chan_data_32_36 = out_chan_dep_data_32;
    assign token_32_36 = token_out_vec_32[11];

    // delay ap_idle for one cycle
    reg [0:0] AESL_inst_myproject$leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_config43_581_U0$ap_idle;
    always @ (negedge reset or posedge clock) begin
        if (~reset) begin
            AESL_inst_myproject$leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_config43_581_U0$ap_idle <= 'b0;
        end
        else begin
            AESL_inst_myproject$leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_config43_581_U0$ap_idle <= AESL_inst_myproject.leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_config43_581_U0.ap_idle;
        end
    end
    // Process: AESL_inst_myproject.leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_config43_581_U0
    AESL_deadlock_detect_unit #(40, 33, 2, 2) AESL_deadlock_detect_unit_33 (
        .reset(reset),
        .clock(clock),
        .proc_dep_vld_vec(proc_dep_vld_vec_33),
        .in_chan_dep_vld_vec(in_chan_dep_vld_vec_33),
        .in_chan_dep_data_vec(in_chan_dep_data_vec_33),
        .token_in_vec(token_in_vec_33),
        .dl_detect_in(dl_detect_out),
        .origin(origin[33]),
        .token_clear(token_clear),
        .out_chan_dep_vld_vec(out_chan_dep_vld_vec_33),
        .out_chan_dep_data(out_chan_dep_data_33),
        .token_out_vec(token_out_vec_33),
        .dl_detect_out(dl_in_vec[33]));

    assign proc_dep_vld_vec_33[0] = dl_detect_out ? proc_dep_vld_vec_33_reg[0] : (~AESL_inst_myproject.leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_config43_581_U0.data_V_V_blk_n | (~AESL_inst_myproject.start_for_leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_confiB3e_U.if_empty_n & (AESL_inst_myproject.leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_config43_581_U0.ap_ready | AESL_inst_myproject$leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_config43_581_U0$ap_idle)));
    assign proc_dep_vld_vec_33[1] = dl_detect_out ? proc_dep_vld_vec_33_reg[1] : (~AESL_inst_myproject.leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_config43_581_U0.res_V_V_blk_n);
    always @ (negedge reset or posedge clock) begin
        if (~reset) begin
            proc_dep_vld_vec_33_reg <= 'b0;
        end
        else begin
            proc_dep_vld_vec_33_reg <= proc_dep_vld_vec_33;
        end
    end
    assign in_chan_dep_vld_vec_33[0] = dep_chan_vld_32_33;
    assign in_chan_dep_data_vec_33[39 : 0] = dep_chan_data_32_33;
    assign token_in_vec_33[0] = token_32_33;
    assign in_chan_dep_vld_vec_33[1] = dep_chan_vld_34_33;
    assign in_chan_dep_data_vec_33[79 : 40] = dep_chan_data_34_33;
    assign token_in_vec_33[1] = token_34_33;
    assign dep_chan_vld_33_32 = out_chan_dep_vld_vec_33[0];
    assign dep_chan_data_33_32 = out_chan_dep_data_33;
    assign token_33_32 = token_out_vec_33[0];
    assign dep_chan_vld_33_34 = out_chan_dep_vld_vec_33[1];
    assign dep_chan_data_33_34 = out_chan_dep_data_33;
    assign token_33_34 = token_out_vec_33[1];

    // delay ap_idle for one cycle
    reg [0:0] AESL_inst_myproject$dense_large_stream_me_ap_fixed_ap_fixed_config45_U0$ap_idle;
    always @ (negedge reset or posedge clock) begin
        if (~reset) begin
            AESL_inst_myproject$dense_large_stream_me_ap_fixed_ap_fixed_config45_U0$ap_idle <= 'b0;
        end
        else begin
            AESL_inst_myproject$dense_large_stream_me_ap_fixed_ap_fixed_config45_U0$ap_idle <= AESL_inst_myproject.dense_large_stream_me_ap_fixed_ap_fixed_config45_U0.ap_idle;
        end
    end
    // Process: AESL_inst_myproject.dense_large_stream_me_ap_fixed_ap_fixed_config45_U0
    AESL_deadlock_detect_unit #(40, 34, 12, 12) AESL_deadlock_detect_unit_34 (
        .reset(reset),
        .clock(clock),
        .proc_dep_vld_vec(proc_dep_vld_vec_34),
        .in_chan_dep_vld_vec(in_chan_dep_vld_vec_34),
        .in_chan_dep_data_vec(in_chan_dep_data_vec_34),
        .token_in_vec(token_in_vec_34),
        .dl_detect_in(dl_detect_out),
        .origin(origin[34]),
        .token_clear(token_clear),
        .out_chan_dep_vld_vec(out_chan_dep_vld_vec_34),
        .out_chan_dep_data(out_chan_dep_data_34),
        .token_out_vec(token_out_vec_34),
        .dl_detect_out(dl_in_vec[34]));

    assign proc_dep_vld_vec_34[0] = dl_detect_out ? proc_dep_vld_vec_34_reg[0] : (~AESL_inst_myproject.dense_large_stream_me_ap_fixed_ap_fixed_config45_U0.data_V_V_blk_n);
    assign proc_dep_vld_vec_34[1] = dl_detect_out ? proc_dep_vld_vec_34_reg[1] : (~AESL_inst_myproject.dense_large_stream_me_ap_fixed_ap_fixed_config45_U0.res_V_V_blk_n | (~AESL_inst_myproject.start_for_leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_confiB4e_U.if_full_n & AESL_inst_myproject.leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_config48_582_U0.ap_done));
    assign proc_dep_vld_vec_34[2] = dl_detect_out ? proc_dep_vld_vec_34_reg[2] : (((AESL_inst_myproject.dense_large_stream_me_ap_fixed_ap_fixed_config45_U0_ap_ready_count[0]) & AESL_inst_myproject.dense_large_stream_me_ap_fixed_ap_fixed_config45_U0.ap_idle & ~(AESL_inst_myproject.Block_ap_fixed_base_exit3121_proc_U0_ap_ready_count[0])));
    assign proc_dep_vld_vec_34[3] = dl_detect_out ? proc_dep_vld_vec_34_reg[3] : (((AESL_inst_myproject.dense_large_stream_me_ap_fixed_ap_fixed_config45_U0_ap_ready_count[0]) & AESL_inst_myproject.dense_large_stream_me_ap_fixed_ap_fixed_config45_U0.ap_idle & ~(AESL_inst_myproject.resize_nearest_me_ap_fixed_32_16_5_3_0_config2_U0_ap_ready_count[0])));
    assign proc_dep_vld_vec_34[4] = dl_detect_out ? proc_dep_vld_vec_34_reg[4] : (((AESL_inst_myproject.dense_large_stream_me_ap_fixed_ap_fixed_config45_U0_ap_ready_count[0]) & AESL_inst_myproject.dense_large_stream_me_ap_fixed_ap_fixed_config45_U0.ap_idle & ~(AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config13_U0_ap_ready_count[0])));
    assign proc_dep_vld_vec_34[5] = dl_detect_out ? proc_dep_vld_vec_34_reg[5] : (((AESL_inst_myproject.dense_large_stream_me_ap_fixed_ap_fixed_config45_U0_ap_ready_count[0]) & AESL_inst_myproject.dense_large_stream_me_ap_fixed_ap_fixed_config45_U0.ap_idle & ~(AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config18_U0_ap_ready_count[0])));
    assign proc_dep_vld_vec_34[6] = dl_detect_out ? proc_dep_vld_vec_34_reg[6] : (((AESL_inst_myproject.dense_large_stream_me_ap_fixed_ap_fixed_config45_U0_ap_ready_count[0]) & AESL_inst_myproject.dense_large_stream_me_ap_fixed_ap_fixed_config45_U0.ap_idle & ~(AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config22_U0_ap_ready_count[0])));
    assign proc_dep_vld_vec_34[7] = dl_detect_out ? proc_dep_vld_vec_34_reg[7] : (((AESL_inst_myproject.dense_large_stream_me_ap_fixed_ap_fixed_config45_U0_ap_ready_count[0]) & AESL_inst_myproject.dense_large_stream_me_ap_fixed_ap_fixed_config45_U0.ap_idle & ~(AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config27_U0_ap_ready_count[0])));
    assign proc_dep_vld_vec_34[8] = dl_detect_out ? proc_dep_vld_vec_34_reg[8] : (((AESL_inst_myproject.dense_large_stream_me_ap_fixed_ap_fixed_config45_U0_ap_ready_count[0]) & AESL_inst_myproject.dense_large_stream_me_ap_fixed_ap_fixed_config45_U0.ap_idle & ~(AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config31_U0_ap_ready_count[0])));
    assign proc_dep_vld_vec_34[9] = dl_detect_out ? proc_dep_vld_vec_34_reg[9] : (((AESL_inst_myproject.dense_large_stream_me_ap_fixed_ap_fixed_config45_U0_ap_ready_count[0]) & AESL_inst_myproject.dense_large_stream_me_ap_fixed_ap_fixed_config45_U0.ap_idle & ~(AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config36_U0_ap_ready_count[0])));
    assign proc_dep_vld_vec_34[10] = dl_detect_out ? proc_dep_vld_vec_34_reg[10] : (((AESL_inst_myproject.dense_large_stream_me_ap_fixed_ap_fixed_config45_U0_ap_ready_count[0]) & AESL_inst_myproject.dense_large_stream_me_ap_fixed_ap_fixed_config45_U0.ap_idle & ~(AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config40_U0_ap_ready_count[0])));
    assign proc_dep_vld_vec_34[11] = dl_detect_out ? proc_dep_vld_vec_34_reg[11] : (((AESL_inst_myproject.dense_large_stream_me_ap_fixed_ap_fixed_config45_U0_ap_ready_count[0]) & AESL_inst_myproject.dense_large_stream_me_ap_fixed_ap_fixed_config45_U0.ap_idle & ~(AESL_inst_myproject.dense_large_stream_me_ap_fixed_ap_fixed_config49_U0_ap_ready_count[0])));
    always @ (negedge reset or posedge clock) begin
        if (~reset) begin
            proc_dep_vld_vec_34_reg <= 'b0;
        end
        else begin
            proc_dep_vld_vec_34_reg <= proc_dep_vld_vec_34;
        end
    end
    assign in_chan_dep_vld_vec_34[0] = dep_chan_vld_0_34;
    assign in_chan_dep_data_vec_34[39 : 0] = dep_chan_data_0_34;
    assign token_in_vec_34[0] = token_0_34;
    assign in_chan_dep_vld_vec_34[1] = dep_chan_vld_1_34;
    assign in_chan_dep_data_vec_34[79 : 40] = dep_chan_data_1_34;
    assign token_in_vec_34[1] = token_1_34;
    assign in_chan_dep_vld_vec_34[2] = dep_chan_vld_11_34;
    assign in_chan_dep_data_vec_34[119 : 80] = dep_chan_data_11_34;
    assign token_in_vec_34[2] = token_11_34;
    assign in_chan_dep_vld_vec_34[3] = dep_chan_vld_15_34;
    assign in_chan_dep_data_vec_34[159 : 120] = dep_chan_data_15_34;
    assign token_in_vec_34[3] = token_15_34;
    assign in_chan_dep_vld_vec_34[4] = dep_chan_vld_18_34;
    assign in_chan_dep_data_vec_34[199 : 160] = dep_chan_data_18_34;
    assign token_in_vec_34[4] = token_18_34;
    assign in_chan_dep_vld_vec_34[5] = dep_chan_vld_22_34;
    assign in_chan_dep_data_vec_34[239 : 200] = dep_chan_data_22_34;
    assign token_in_vec_34[5] = token_22_34;
    assign in_chan_dep_vld_vec_34[6] = dep_chan_vld_25_34;
    assign in_chan_dep_data_vec_34[279 : 240] = dep_chan_data_25_34;
    assign token_in_vec_34[6] = token_25_34;
    assign in_chan_dep_vld_vec_34[7] = dep_chan_vld_29_34;
    assign in_chan_dep_data_vec_34[319 : 280] = dep_chan_data_29_34;
    assign token_in_vec_34[7] = token_29_34;
    assign in_chan_dep_vld_vec_34[8] = dep_chan_vld_32_34;
    assign in_chan_dep_data_vec_34[359 : 320] = dep_chan_data_32_34;
    assign token_in_vec_34[8] = token_32_34;
    assign in_chan_dep_vld_vec_34[9] = dep_chan_vld_33_34;
    assign in_chan_dep_data_vec_34[399 : 360] = dep_chan_data_33_34;
    assign token_in_vec_34[9] = token_33_34;
    assign in_chan_dep_vld_vec_34[10] = dep_chan_vld_35_34;
    assign in_chan_dep_data_vec_34[439 : 400] = dep_chan_data_35_34;
    assign token_in_vec_34[10] = token_35_34;
    assign in_chan_dep_vld_vec_34[11] = dep_chan_vld_36_34;
    assign in_chan_dep_data_vec_34[479 : 440] = dep_chan_data_36_34;
    assign token_in_vec_34[11] = token_36_34;
    assign dep_chan_vld_34_33 = out_chan_dep_vld_vec_34[0];
    assign dep_chan_data_34_33 = out_chan_dep_data_34;
    assign token_34_33 = token_out_vec_34[0];
    assign dep_chan_vld_34_35 = out_chan_dep_vld_vec_34[1];
    assign dep_chan_data_34_35 = out_chan_dep_data_34;
    assign token_34_35 = token_out_vec_34[1];
    assign dep_chan_vld_34_0 = out_chan_dep_vld_vec_34[2];
    assign dep_chan_data_34_0 = out_chan_dep_data_34;
    assign token_34_0 = token_out_vec_34[2];
    assign dep_chan_vld_34_1 = out_chan_dep_vld_vec_34[3];
    assign dep_chan_data_34_1 = out_chan_dep_data_34;
    assign token_34_1 = token_out_vec_34[3];
    assign dep_chan_vld_34_11 = out_chan_dep_vld_vec_34[4];
    assign dep_chan_data_34_11 = out_chan_dep_data_34;
    assign token_34_11 = token_out_vec_34[4];
    assign dep_chan_vld_34_15 = out_chan_dep_vld_vec_34[5];
    assign dep_chan_data_34_15 = out_chan_dep_data_34;
    assign token_34_15 = token_out_vec_34[5];
    assign dep_chan_vld_34_18 = out_chan_dep_vld_vec_34[6];
    assign dep_chan_data_34_18 = out_chan_dep_data_34;
    assign token_34_18 = token_out_vec_34[6];
    assign dep_chan_vld_34_22 = out_chan_dep_vld_vec_34[7];
    assign dep_chan_data_34_22 = out_chan_dep_data_34;
    assign token_34_22 = token_out_vec_34[7];
    assign dep_chan_vld_34_25 = out_chan_dep_vld_vec_34[8];
    assign dep_chan_data_34_25 = out_chan_dep_data_34;
    assign token_34_25 = token_out_vec_34[8];
    assign dep_chan_vld_34_29 = out_chan_dep_vld_vec_34[9];
    assign dep_chan_data_34_29 = out_chan_dep_data_34;
    assign token_34_29 = token_out_vec_34[9];
    assign dep_chan_vld_34_32 = out_chan_dep_vld_vec_34[10];
    assign dep_chan_data_34_32 = out_chan_dep_data_34;
    assign token_34_32 = token_out_vec_34[10];
    assign dep_chan_vld_34_36 = out_chan_dep_vld_vec_34[11];
    assign dep_chan_data_34_36 = out_chan_dep_data_34;
    assign token_34_36 = token_out_vec_34[11];

    // delay ap_idle for one cycle
    reg [0:0] AESL_inst_myproject$leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_config48_582_U0$ap_idle;
    always @ (negedge reset or posedge clock) begin
        if (~reset) begin
            AESL_inst_myproject$leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_config48_582_U0$ap_idle <= 'b0;
        end
        else begin
            AESL_inst_myproject$leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_config48_582_U0$ap_idle <= AESL_inst_myproject.leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_config48_582_U0.ap_idle;
        end
    end
    // Process: AESL_inst_myproject.leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_config48_582_U0
    AESL_deadlock_detect_unit #(40, 35, 2, 2) AESL_deadlock_detect_unit_35 (
        .reset(reset),
        .clock(clock),
        .proc_dep_vld_vec(proc_dep_vld_vec_35),
        .in_chan_dep_vld_vec(in_chan_dep_vld_vec_35),
        .in_chan_dep_data_vec(in_chan_dep_data_vec_35),
        .token_in_vec(token_in_vec_35),
        .dl_detect_in(dl_detect_out),
        .origin(origin[35]),
        .token_clear(token_clear),
        .out_chan_dep_vld_vec(out_chan_dep_vld_vec_35),
        .out_chan_dep_data(out_chan_dep_data_35),
        .token_out_vec(token_out_vec_35),
        .dl_detect_out(dl_in_vec[35]));

    assign proc_dep_vld_vec_35[0] = dl_detect_out ? proc_dep_vld_vec_35_reg[0] : (~AESL_inst_myproject.leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_config48_582_U0.data_V_V_blk_n | (~AESL_inst_myproject.start_for_leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_confiB4e_U.if_empty_n & (AESL_inst_myproject.leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_config48_582_U0.ap_ready | AESL_inst_myproject$leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_config48_582_U0$ap_idle)));
    assign proc_dep_vld_vec_35[1] = dl_detect_out ? proc_dep_vld_vec_35_reg[1] : (~AESL_inst_myproject.leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_config48_582_U0.res_V_V_blk_n);
    always @ (negedge reset or posedge clock) begin
        if (~reset) begin
            proc_dep_vld_vec_35_reg <= 'b0;
        end
        else begin
            proc_dep_vld_vec_35_reg <= proc_dep_vld_vec_35;
        end
    end
    assign in_chan_dep_vld_vec_35[0] = dep_chan_vld_34_35;
    assign in_chan_dep_data_vec_35[39 : 0] = dep_chan_data_34_35;
    assign token_in_vec_35[0] = token_34_35;
    assign in_chan_dep_vld_vec_35[1] = dep_chan_vld_36_35;
    assign in_chan_dep_data_vec_35[79 : 40] = dep_chan_data_36_35;
    assign token_in_vec_35[1] = token_36_35;
    assign dep_chan_vld_35_34 = out_chan_dep_vld_vec_35[0];
    assign dep_chan_data_35_34 = out_chan_dep_data_35;
    assign token_35_34 = token_out_vec_35[0];
    assign dep_chan_vld_35_36 = out_chan_dep_vld_vec_35[1];
    assign dep_chan_data_35_36 = out_chan_dep_data_35;
    assign token_35_36 = token_out_vec_35[1];

    // delay ap_idle for one cycle
    reg [0:0] AESL_inst_myproject$dense_large_stream_me_ap_fixed_ap_fixed_config49_U0$ap_idle;
    always @ (negedge reset or posedge clock) begin
        if (~reset) begin
            AESL_inst_myproject$dense_large_stream_me_ap_fixed_ap_fixed_config49_U0$ap_idle <= 'b0;
        end
        else begin
            AESL_inst_myproject$dense_large_stream_me_ap_fixed_ap_fixed_config49_U0$ap_idle <= AESL_inst_myproject.dense_large_stream_me_ap_fixed_ap_fixed_config49_U0.ap_idle;
        end
    end
    // Process: AESL_inst_myproject.dense_large_stream_me_ap_fixed_ap_fixed_config49_U0
    AESL_deadlock_detect_unit #(40, 36, 12, 12) AESL_deadlock_detect_unit_36 (
        .reset(reset),
        .clock(clock),
        .proc_dep_vld_vec(proc_dep_vld_vec_36),
        .in_chan_dep_vld_vec(in_chan_dep_vld_vec_36),
        .in_chan_dep_data_vec(in_chan_dep_data_vec_36),
        .token_in_vec(token_in_vec_36),
        .dl_detect_in(dl_detect_out),
        .origin(origin[36]),
        .token_clear(token_clear),
        .out_chan_dep_vld_vec(out_chan_dep_vld_vec_36),
        .out_chan_dep_data(out_chan_dep_data_36),
        .token_out_vec(token_out_vec_36),
        .dl_detect_out(dl_in_vec[36]));

    assign proc_dep_vld_vec_36[0] = dl_detect_out ? proc_dep_vld_vec_36_reg[0] : (~AESL_inst_myproject.dense_large_stream_me_ap_fixed_ap_fixed_config49_U0.data_V_V_blk_n);
    assign proc_dep_vld_vec_36[1] = dl_detect_out ? proc_dep_vld_vec_36_reg[1] : (~AESL_inst_myproject.dense_large_stream_me_ap_fixed_ap_fixed_config49_U0.res_V_V_blk_n | (~AESL_inst_myproject.start_for_leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_confiB5e_U.if_full_n & AESL_inst_myproject.leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_config52_583_U0.ap_done));
    assign proc_dep_vld_vec_36[2] = dl_detect_out ? proc_dep_vld_vec_36_reg[2] : (((AESL_inst_myproject.dense_large_stream_me_ap_fixed_ap_fixed_config49_U0_ap_ready_count[0]) & AESL_inst_myproject.dense_large_stream_me_ap_fixed_ap_fixed_config49_U0.ap_idle & ~(AESL_inst_myproject.Block_ap_fixed_base_exit3121_proc_U0_ap_ready_count[0])));
    assign proc_dep_vld_vec_36[3] = dl_detect_out ? proc_dep_vld_vec_36_reg[3] : (((AESL_inst_myproject.dense_large_stream_me_ap_fixed_ap_fixed_config49_U0_ap_ready_count[0]) & AESL_inst_myproject.dense_large_stream_me_ap_fixed_ap_fixed_config49_U0.ap_idle & ~(AESL_inst_myproject.resize_nearest_me_ap_fixed_32_16_5_3_0_config2_U0_ap_ready_count[0])));
    assign proc_dep_vld_vec_36[4] = dl_detect_out ? proc_dep_vld_vec_36_reg[4] : (((AESL_inst_myproject.dense_large_stream_me_ap_fixed_ap_fixed_config49_U0_ap_ready_count[0]) & AESL_inst_myproject.dense_large_stream_me_ap_fixed_ap_fixed_config49_U0.ap_idle & ~(AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config13_U0_ap_ready_count[0])));
    assign proc_dep_vld_vec_36[5] = dl_detect_out ? proc_dep_vld_vec_36_reg[5] : (((AESL_inst_myproject.dense_large_stream_me_ap_fixed_ap_fixed_config49_U0_ap_ready_count[0]) & AESL_inst_myproject.dense_large_stream_me_ap_fixed_ap_fixed_config49_U0.ap_idle & ~(AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config18_U0_ap_ready_count[0])));
    assign proc_dep_vld_vec_36[6] = dl_detect_out ? proc_dep_vld_vec_36_reg[6] : (((AESL_inst_myproject.dense_large_stream_me_ap_fixed_ap_fixed_config49_U0_ap_ready_count[0]) & AESL_inst_myproject.dense_large_stream_me_ap_fixed_ap_fixed_config49_U0.ap_idle & ~(AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config22_U0_ap_ready_count[0])));
    assign proc_dep_vld_vec_36[7] = dl_detect_out ? proc_dep_vld_vec_36_reg[7] : (((AESL_inst_myproject.dense_large_stream_me_ap_fixed_ap_fixed_config49_U0_ap_ready_count[0]) & AESL_inst_myproject.dense_large_stream_me_ap_fixed_ap_fixed_config49_U0.ap_idle & ~(AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config27_U0_ap_ready_count[0])));
    assign proc_dep_vld_vec_36[8] = dl_detect_out ? proc_dep_vld_vec_36_reg[8] : (((AESL_inst_myproject.dense_large_stream_me_ap_fixed_ap_fixed_config49_U0_ap_ready_count[0]) & AESL_inst_myproject.dense_large_stream_me_ap_fixed_ap_fixed_config49_U0.ap_idle & ~(AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config31_U0_ap_ready_count[0])));
    assign proc_dep_vld_vec_36[9] = dl_detect_out ? proc_dep_vld_vec_36_reg[9] : (((AESL_inst_myproject.dense_large_stream_me_ap_fixed_ap_fixed_config49_U0_ap_ready_count[0]) & AESL_inst_myproject.dense_large_stream_me_ap_fixed_ap_fixed_config49_U0.ap_idle & ~(AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config36_U0_ap_ready_count[0])));
    assign proc_dep_vld_vec_36[10] = dl_detect_out ? proc_dep_vld_vec_36_reg[10] : (((AESL_inst_myproject.dense_large_stream_me_ap_fixed_ap_fixed_config49_U0_ap_ready_count[0]) & AESL_inst_myproject.dense_large_stream_me_ap_fixed_ap_fixed_config49_U0.ap_idle & ~(AESL_inst_myproject.conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config40_U0_ap_ready_count[0])));
    assign proc_dep_vld_vec_36[11] = dl_detect_out ? proc_dep_vld_vec_36_reg[11] : (((AESL_inst_myproject.dense_large_stream_me_ap_fixed_ap_fixed_config49_U0_ap_ready_count[0]) & AESL_inst_myproject.dense_large_stream_me_ap_fixed_ap_fixed_config49_U0.ap_idle & ~(AESL_inst_myproject.dense_large_stream_me_ap_fixed_ap_fixed_config45_U0_ap_ready_count[0])));
    always @ (negedge reset or posedge clock) begin
        if (~reset) begin
            proc_dep_vld_vec_36_reg <= 'b0;
        end
        else begin
            proc_dep_vld_vec_36_reg <= proc_dep_vld_vec_36;
        end
    end
    assign in_chan_dep_vld_vec_36[0] = dep_chan_vld_0_36;
    assign in_chan_dep_data_vec_36[39 : 0] = dep_chan_data_0_36;
    assign token_in_vec_36[0] = token_0_36;
    assign in_chan_dep_vld_vec_36[1] = dep_chan_vld_1_36;
    assign in_chan_dep_data_vec_36[79 : 40] = dep_chan_data_1_36;
    assign token_in_vec_36[1] = token_1_36;
    assign in_chan_dep_vld_vec_36[2] = dep_chan_vld_11_36;
    assign in_chan_dep_data_vec_36[119 : 80] = dep_chan_data_11_36;
    assign token_in_vec_36[2] = token_11_36;
    assign in_chan_dep_vld_vec_36[3] = dep_chan_vld_15_36;
    assign in_chan_dep_data_vec_36[159 : 120] = dep_chan_data_15_36;
    assign token_in_vec_36[3] = token_15_36;
    assign in_chan_dep_vld_vec_36[4] = dep_chan_vld_18_36;
    assign in_chan_dep_data_vec_36[199 : 160] = dep_chan_data_18_36;
    assign token_in_vec_36[4] = token_18_36;
    assign in_chan_dep_vld_vec_36[5] = dep_chan_vld_22_36;
    assign in_chan_dep_data_vec_36[239 : 200] = dep_chan_data_22_36;
    assign token_in_vec_36[5] = token_22_36;
    assign in_chan_dep_vld_vec_36[6] = dep_chan_vld_25_36;
    assign in_chan_dep_data_vec_36[279 : 240] = dep_chan_data_25_36;
    assign token_in_vec_36[6] = token_25_36;
    assign in_chan_dep_vld_vec_36[7] = dep_chan_vld_29_36;
    assign in_chan_dep_data_vec_36[319 : 280] = dep_chan_data_29_36;
    assign token_in_vec_36[7] = token_29_36;
    assign in_chan_dep_vld_vec_36[8] = dep_chan_vld_32_36;
    assign in_chan_dep_data_vec_36[359 : 320] = dep_chan_data_32_36;
    assign token_in_vec_36[8] = token_32_36;
    assign in_chan_dep_vld_vec_36[9] = dep_chan_vld_34_36;
    assign in_chan_dep_data_vec_36[399 : 360] = dep_chan_data_34_36;
    assign token_in_vec_36[9] = token_34_36;
    assign in_chan_dep_vld_vec_36[10] = dep_chan_vld_35_36;
    assign in_chan_dep_data_vec_36[439 : 400] = dep_chan_data_35_36;
    assign token_in_vec_36[10] = token_35_36;
    assign in_chan_dep_vld_vec_36[11] = dep_chan_vld_37_36;
    assign in_chan_dep_data_vec_36[479 : 440] = dep_chan_data_37_36;
    assign token_in_vec_36[11] = token_37_36;
    assign dep_chan_vld_36_35 = out_chan_dep_vld_vec_36[0];
    assign dep_chan_data_36_35 = out_chan_dep_data_36;
    assign token_36_35 = token_out_vec_36[0];
    assign dep_chan_vld_36_37 = out_chan_dep_vld_vec_36[1];
    assign dep_chan_data_36_37 = out_chan_dep_data_36;
    assign token_36_37 = token_out_vec_36[1];
    assign dep_chan_vld_36_0 = out_chan_dep_vld_vec_36[2];
    assign dep_chan_data_36_0 = out_chan_dep_data_36;
    assign token_36_0 = token_out_vec_36[2];
    assign dep_chan_vld_36_1 = out_chan_dep_vld_vec_36[3];
    assign dep_chan_data_36_1 = out_chan_dep_data_36;
    assign token_36_1 = token_out_vec_36[3];
    assign dep_chan_vld_36_11 = out_chan_dep_vld_vec_36[4];
    assign dep_chan_data_36_11 = out_chan_dep_data_36;
    assign token_36_11 = token_out_vec_36[4];
    assign dep_chan_vld_36_15 = out_chan_dep_vld_vec_36[5];
    assign dep_chan_data_36_15 = out_chan_dep_data_36;
    assign token_36_15 = token_out_vec_36[5];
    assign dep_chan_vld_36_18 = out_chan_dep_vld_vec_36[6];
    assign dep_chan_data_36_18 = out_chan_dep_data_36;
    assign token_36_18 = token_out_vec_36[6];
    assign dep_chan_vld_36_22 = out_chan_dep_vld_vec_36[7];
    assign dep_chan_data_36_22 = out_chan_dep_data_36;
    assign token_36_22 = token_out_vec_36[7];
    assign dep_chan_vld_36_25 = out_chan_dep_vld_vec_36[8];
    assign dep_chan_data_36_25 = out_chan_dep_data_36;
    assign token_36_25 = token_out_vec_36[8];
    assign dep_chan_vld_36_29 = out_chan_dep_vld_vec_36[9];
    assign dep_chan_data_36_29 = out_chan_dep_data_36;
    assign token_36_29 = token_out_vec_36[9];
    assign dep_chan_vld_36_32 = out_chan_dep_vld_vec_36[10];
    assign dep_chan_data_36_32 = out_chan_dep_data_36;
    assign token_36_32 = token_out_vec_36[10];
    assign dep_chan_vld_36_34 = out_chan_dep_vld_vec_36[11];
    assign dep_chan_data_36_34 = out_chan_dep_data_36;
    assign token_36_34 = token_out_vec_36[11];

    // delay ap_idle for one cycle
    reg [0:0] AESL_inst_myproject$leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_config52_583_U0$ap_idle;
    always @ (negedge reset or posedge clock) begin
        if (~reset) begin
            AESL_inst_myproject$leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_config52_583_U0$ap_idle <= 'b0;
        end
        else begin
            AESL_inst_myproject$leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_config52_583_U0$ap_idle <= AESL_inst_myproject.leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_config52_583_U0.ap_idle;
        end
    end
    // Process: AESL_inst_myproject.leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_config52_583_U0
    AESL_deadlock_detect_unit #(40, 37, 2, 2) AESL_deadlock_detect_unit_37 (
        .reset(reset),
        .clock(clock),
        .proc_dep_vld_vec(proc_dep_vld_vec_37),
        .in_chan_dep_vld_vec(in_chan_dep_vld_vec_37),
        .in_chan_dep_data_vec(in_chan_dep_data_vec_37),
        .token_in_vec(token_in_vec_37),
        .dl_detect_in(dl_detect_out),
        .origin(origin[37]),
        .token_clear(token_clear),
        .out_chan_dep_vld_vec(out_chan_dep_vld_vec_37),
        .out_chan_dep_data(out_chan_dep_data_37),
        .token_out_vec(token_out_vec_37),
        .dl_detect_out(dl_in_vec[37]));

    assign proc_dep_vld_vec_37[0] = dl_detect_out ? proc_dep_vld_vec_37_reg[0] : (~AESL_inst_myproject.leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_config52_583_U0.data_V_V_blk_n | (~AESL_inst_myproject.start_for_leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_confiB5e_U.if_empty_n & (AESL_inst_myproject.leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_config52_583_U0.ap_ready | AESL_inst_myproject$leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_config52_583_U0$ap_idle)));
    assign proc_dep_vld_vec_37[1] = dl_detect_out ? proc_dep_vld_vec_37_reg[1] : (~AESL_inst_myproject.leaky_relu_me_ap_fixed_ap_fixed_LeakyReLU_config52_583_U0.res_V_V_blk_n | (~AESL_inst_myproject.start_for_dense_large_stream_me_ap_fixed_ap_fixed_config5B6e_U.if_full_n & AESL_inst_myproject.dense_large_stream_me_ap_fixed_ap_fixed_config53_U0.ap_done));
    always @ (negedge reset or posedge clock) begin
        if (~reset) begin
            proc_dep_vld_vec_37_reg <= 'b0;
        end
        else begin
            proc_dep_vld_vec_37_reg <= proc_dep_vld_vec_37;
        end
    end
    assign in_chan_dep_vld_vec_37[0] = dep_chan_vld_36_37;
    assign in_chan_dep_data_vec_37[39 : 0] = dep_chan_data_36_37;
    assign token_in_vec_37[0] = token_36_37;
    assign in_chan_dep_vld_vec_37[1] = dep_chan_vld_38_37;
    assign in_chan_dep_data_vec_37[79 : 40] = dep_chan_data_38_37;
    assign token_in_vec_37[1] = token_38_37;
    assign dep_chan_vld_37_36 = out_chan_dep_vld_vec_37[0];
    assign dep_chan_data_37_36 = out_chan_dep_data_37;
    assign token_37_36 = token_out_vec_37[0];
    assign dep_chan_vld_37_38 = out_chan_dep_vld_vec_37[1];
    assign dep_chan_data_37_38 = out_chan_dep_data_37;
    assign token_37_38 = token_out_vec_37[1];

    // delay ap_idle for one cycle
    reg [0:0] AESL_inst_myproject$dense_large_stream_me_ap_fixed_ap_fixed_config53_U0$ap_idle;
    always @ (negedge reset or posedge clock) begin
        if (~reset) begin
            AESL_inst_myproject$dense_large_stream_me_ap_fixed_ap_fixed_config53_U0$ap_idle <= 'b0;
        end
        else begin
            AESL_inst_myproject$dense_large_stream_me_ap_fixed_ap_fixed_config53_U0$ap_idle <= AESL_inst_myproject.dense_large_stream_me_ap_fixed_ap_fixed_config53_U0.ap_idle;
        end
    end
    // Process: AESL_inst_myproject.dense_large_stream_me_ap_fixed_ap_fixed_config53_U0
    AESL_deadlock_detect_unit #(40, 38, 2, 2) AESL_deadlock_detect_unit_38 (
        .reset(reset),
        .clock(clock),
        .proc_dep_vld_vec(proc_dep_vld_vec_38),
        .in_chan_dep_vld_vec(in_chan_dep_vld_vec_38),
        .in_chan_dep_data_vec(in_chan_dep_data_vec_38),
        .token_in_vec(token_in_vec_38),
        .dl_detect_in(dl_detect_out),
        .origin(origin[38]),
        .token_clear(token_clear),
        .out_chan_dep_vld_vec(out_chan_dep_vld_vec_38),
        .out_chan_dep_data(out_chan_dep_data_38),
        .token_out_vec(token_out_vec_38),
        .dl_detect_out(dl_in_vec[38]));

    assign proc_dep_vld_vec_38[0] = dl_detect_out ? proc_dep_vld_vec_38_reg[0] : (~AESL_inst_myproject.dense_large_stream_me_ap_fixed_ap_fixed_config53_U0.data_V_V_blk_n | (~AESL_inst_myproject.start_for_dense_large_stream_me_ap_fixed_ap_fixed_config5B6e_U.if_empty_n & (AESL_inst_myproject.dense_large_stream_me_ap_fixed_ap_fixed_config53_U0.ap_ready | AESL_inst_myproject$dense_large_stream_me_ap_fixed_ap_fixed_config53_U0$ap_idle)));
    assign proc_dep_vld_vec_38[1] = dl_detect_out ? proc_dep_vld_vec_38_reg[1] : (~AESL_inst_myproject.dense_large_stream_me_ap_fixed_ap_fixed_config53_U0.res_V_V_blk_n | (~AESL_inst_myproject.start_for_relu_me_ap_fixed_ap_fixed_32_16_5_3_0_relu_confB7e_U.if_full_n & AESL_inst_myproject.relu_me_ap_fixed_ap_fixed_32_16_5_3_0_relu_config55_U0.ap_done));
    always @ (negedge reset or posedge clock) begin
        if (~reset) begin
            proc_dep_vld_vec_38_reg <= 'b0;
        end
        else begin
            proc_dep_vld_vec_38_reg <= proc_dep_vld_vec_38;
        end
    end
    assign in_chan_dep_vld_vec_38[0] = dep_chan_vld_37_38;
    assign in_chan_dep_data_vec_38[39 : 0] = dep_chan_data_37_38;
    assign token_in_vec_38[0] = token_37_38;
    assign in_chan_dep_vld_vec_38[1] = dep_chan_vld_39_38;
    assign in_chan_dep_data_vec_38[79 : 40] = dep_chan_data_39_38;
    assign token_in_vec_38[1] = token_39_38;
    assign dep_chan_vld_38_37 = out_chan_dep_vld_vec_38[0];
    assign dep_chan_data_38_37 = out_chan_dep_data_38;
    assign token_38_37 = token_out_vec_38[0];
    assign dep_chan_vld_38_39 = out_chan_dep_vld_vec_38[1];
    assign dep_chan_data_38_39 = out_chan_dep_data_38;
    assign token_38_39 = token_out_vec_38[1];

    // delay ap_idle for one cycle
    reg [0:0] AESL_inst_myproject$relu_me_ap_fixed_ap_fixed_32_16_5_3_0_relu_config55_U0$ap_idle;
    always @ (negedge reset or posedge clock) begin
        if (~reset) begin
            AESL_inst_myproject$relu_me_ap_fixed_ap_fixed_32_16_5_3_0_relu_config55_U0$ap_idle <= 'b0;
        end
        else begin
            AESL_inst_myproject$relu_me_ap_fixed_ap_fixed_32_16_5_3_0_relu_config55_U0$ap_idle <= AESL_inst_myproject.relu_me_ap_fixed_ap_fixed_32_16_5_3_0_relu_config55_U0.ap_idle;
        end
    end
    // Process: AESL_inst_myproject.relu_me_ap_fixed_ap_fixed_32_16_5_3_0_relu_config55_U0
    AESL_deadlock_detect_unit #(40, 39, 1, 1) AESL_deadlock_detect_unit_39 (
        .reset(reset),
        .clock(clock),
        .proc_dep_vld_vec(proc_dep_vld_vec_39),
        .in_chan_dep_vld_vec(in_chan_dep_vld_vec_39),
        .in_chan_dep_data_vec(in_chan_dep_data_vec_39),
        .token_in_vec(token_in_vec_39),
        .dl_detect_in(dl_detect_out),
        .origin(origin[39]),
        .token_clear(token_clear),
        .out_chan_dep_vld_vec(out_chan_dep_vld_vec_39),
        .out_chan_dep_data(out_chan_dep_data_39),
        .token_out_vec(token_out_vec_39),
        .dl_detect_out(dl_in_vec[39]));

    assign proc_dep_vld_vec_39[0] = dl_detect_out ? proc_dep_vld_vec_39_reg[0] : (~AESL_inst_myproject.relu_me_ap_fixed_ap_fixed_32_16_5_3_0_relu_config55_U0.data_V_V_blk_n | (~AESL_inst_myproject.start_for_relu_me_ap_fixed_ap_fixed_32_16_5_3_0_relu_confB7e_U.if_empty_n & (AESL_inst_myproject.relu_me_ap_fixed_ap_fixed_32_16_5_3_0_relu_config55_U0.ap_ready | AESL_inst_myproject$relu_me_ap_fixed_ap_fixed_32_16_5_3_0_relu_config55_U0$ap_idle)));
    always @ (negedge reset or posedge clock) begin
        if (~reset) begin
            proc_dep_vld_vec_39_reg <= 'b0;
        end
        else begin
            proc_dep_vld_vec_39_reg <= proc_dep_vld_vec_39;
        end
    end
    assign in_chan_dep_vld_vec_39[0] = dep_chan_vld_38_39;
    assign in_chan_dep_data_vec_39[39 : 0] = dep_chan_data_38_39;
    assign token_in_vec_39[0] = token_38_39;
    assign dep_chan_vld_39_38 = out_chan_dep_vld_vec_39[0];
    assign dep_chan_data_39_38 = out_chan_dep_data_39;
    assign token_39_38 = token_out_vec_39[0];


    AESL_deadlock_report_unit #(40) AESL_deadlock_report_unit_inst (
        .reset(reset),
        .clock(clock),
        .dl_in_vec(dl_in_vec),
        .dl_detect_out(dl_detect_out),
        .origin(origin),
        .token_clear(token_clear));

endmodule
