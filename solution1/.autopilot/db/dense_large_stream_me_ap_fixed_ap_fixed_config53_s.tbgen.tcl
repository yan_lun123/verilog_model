set moduleName dense_large_stream_me_ap_fixed_ap_fixed_config53_s
set isTopModule 0
set isTaskLevelControl 1
set isCombinational 0
set isDatapathOnly 0
set isFreeRunPipelineModule 0
set isPipelined 0
set pipeline_type none
set FunctionProtocol ap_ctrl_hs
set isOneStateSeq 0
set ProfileFlag 0
set StallSigGenFlag 0
set isEnableWaveformDebug 1
set C_modelName {dense_large_stream_me<ap_fixed,ap_fixed,config53>}
set C_modelType { void 0 }
set C_modelArgList {
	{ data_V_V int 32 regular {fifo 0 volatile }  }
	{ res_V_V int 32 regular {fifo 1 volatile }  }
}
set C_modelArgMapList {[ 
	{ "Name" : "data_V_V", "interface" : "fifo", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "res_V_V", "interface" : "fifo", "bitwidth" : 32, "direction" : "WRITEONLY"} ]}
# RTL Port declarations: 
set portNum 16
set portList { 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst sc_in sc_logic 1 reset -1 active_high_sync } 
	{ ap_start sc_in sc_logic 1 start -1 } 
	{ start_full_n sc_in sc_logic 1 signal -1 } 
	{ ap_done sc_out sc_logic 1 predone -1 } 
	{ ap_continue sc_in sc_logic 1 continue -1 } 
	{ ap_idle sc_out sc_logic 1 done -1 } 
	{ ap_ready sc_out sc_logic 1 ready -1 } 
	{ start_out sc_out sc_logic 1 signal -1 } 
	{ start_write sc_out sc_logic 1 signal -1 } 
	{ data_V_V_dout sc_in sc_lv 32 signal 0 } 
	{ data_V_V_empty_n sc_in sc_logic 1 signal 0 } 
	{ data_V_V_read sc_out sc_logic 1 signal 0 } 
	{ res_V_V_din sc_out sc_lv 32 signal 1 } 
	{ res_V_V_full_n sc_in sc_logic 1 signal 1 } 
	{ res_V_V_write sc_out sc_logic 1 signal 1 } 
}
set NewPortList {[ 
	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst", "role": "default" }} , 
 	{ "name": "ap_start", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "start", "bundle":{"name": "ap_start", "role": "default" }} , 
 	{ "name": "start_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "start_full_n", "role": "default" }} , 
 	{ "name": "ap_done", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "predone", "bundle":{"name": "ap_done", "role": "default" }} , 
 	{ "name": "ap_continue", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "continue", "bundle":{"name": "ap_continue", "role": "default" }} , 
 	{ "name": "ap_idle", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "done", "bundle":{"name": "ap_idle", "role": "default" }} , 
 	{ "name": "ap_ready", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "ready", "bundle":{"name": "ap_ready", "role": "default" }} , 
 	{ "name": "start_out", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "start_out", "role": "default" }} , 
 	{ "name": "start_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "start_write", "role": "default" }} , 
 	{ "name": "data_V_V_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "data_V_V", "role": "dout" }} , 
 	{ "name": "data_V_V_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "data_V_V", "role": "empty_n" }} , 
 	{ "name": "data_V_V_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "data_V_V", "role": "read" }} , 
 	{ "name": "res_V_V_din", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "res_V_V", "role": "din" }} , 
 	{ "name": "res_V_V_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "res_V_V", "role": "full_n" }} , 
 	{ "name": "res_V_V_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "res_V_V", "role": "write" }}  ]}

set RtlHierarchyInfo {[
	{"ID" : "0", "Level" : "0", "Path" : "`AUTOTB_DUT_INST", "Parent" : "", "Child" : ["1", "2", "3"],
		"CDFG" : "dense_large_stream_me_ap_fixed_ap_fixed_config53_s",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "None", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "0",
		"VariableLatency" : "1", "ExactLatency" : "-1", "EstimateLatencyMin" : "1280", "EstimateLatencyMax" : "1280",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "0",
		"Port" : [
			{"Name" : "data_V_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "data_V_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "res_V_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "res_V_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "tmpdata_V_2_0", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_1", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_2", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_3", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_4", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_5", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_6", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_7", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_8", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_9", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_10", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_11", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_12", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_13", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_14", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_15", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_16", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_17", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_18", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_19", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_20", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_21", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_22", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_23", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_24", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_25", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_26", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_27", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_28", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_29", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_30", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_31", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_32", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_33", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_34", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_35", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_36", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_37", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_38", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_39", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_40", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_41", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_42", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_43", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_44", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_45", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_46", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_47", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_48", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_49", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_50", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_51", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_52", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_53", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_54", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_55", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_56", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_57", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_58", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_59", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_60", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_61", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_62", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_63", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_64", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_65", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_66", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_67", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_68", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_69", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_70", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_71", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_72", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_73", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_74", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_75", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_76", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_77", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_78", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_79", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_80", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_81", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_82", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_83", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_84", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_85", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_86", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_87", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_88", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_89", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_90", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_91", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_92", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_93", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_94", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_95", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_96", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_97", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_98", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_99", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_100", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_101", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_102", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_103", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_104", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_105", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_106", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_107", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_108", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_109", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_110", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_111", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_112", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_113", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_114", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_115", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_116", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_117", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_118", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_119", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_120", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_121", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_122", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_123", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_124", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_125", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_126", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_127", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_128", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_129", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_130", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_131", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_132", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_133", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_134", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_135", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_136", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_137", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_138", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_139", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_140", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_141", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_142", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_143", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_144", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_145", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_146", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_147", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_148", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_149", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_150", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_151", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_152", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_153", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_154", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_155", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_156", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_157", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_158", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_159", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_160", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_161", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_162", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_163", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_164", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_165", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_166", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_167", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_168", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_169", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_170", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_171", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_172", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_173", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_174", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_175", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_176", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_177", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_178", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_179", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_180", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_181", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_182", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_183", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_184", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_185", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_186", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_187", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_188", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_189", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_190", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_191", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_192", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_193", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_194", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_195", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_196", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_197", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_198", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_199", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_200", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_201", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_202", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_203", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_204", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_205", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_206", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_207", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_208", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_209", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_210", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_211", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_212", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_213", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_214", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_215", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_216", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_217", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_218", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_219", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_220", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_221", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_222", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_223", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_224", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_225", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_226", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_227", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_228", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_229", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_230", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_231", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_232", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_233", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_234", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_235", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_236", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_237", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_238", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_239", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_240", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_241", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_242", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_243", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_244", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_245", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_246", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_247", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_248", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_249", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_250", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_251", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_252", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_253", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_254", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "tmpdata_V_2_255", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "w53_V", "Type" : "Memory", "Direction" : "I"}]},
	{"ID" : "1", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.w53_V_U", "Parent" : "0"},
	{"ID" : "2", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.op_V_assign_0_i_product_dense_ap_fixed_ap_fixed_ap_fixed_s_fu_624", "Parent" : "0",
		"CDFG" : "product_dense_ap_fixed_ap_fixed_ap_fixed_s",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "0", "ap_start" : "0", "ap_ready" : "1", "ap_done" : "0", "ap_continue" : "0", "ap_idle" : "0",
		"Pipeline" : "None", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "0", "EstimateLatencyMin" : "0", "EstimateLatencyMax" : "0",
		"Combinational" : "1",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "0",
		"HasNonBlockingOperation" : "0",
		"Port" : [
			{"Name" : "a_V", "Type" : "None", "Direction" : "I"},
			{"Name" : "w_V", "Type" : "None", "Direction" : "I"}]},
	{"ID" : "3", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.myproject_mux_2568_32_1_1_U2863", "Parent" : "0"}]}


set ArgLastReadFirstWriteLatency {
	dense_large_stream_me_ap_fixed_ap_fixed_config53_s {
		data_V_V {Type I LastRead 255 FirstWrite -1}
		res_V_V {Type O LastRead -1 FirstWrite 256}
		tmpdata_V_2_0 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_1 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_2 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_3 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_4 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_5 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_6 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_7 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_8 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_9 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_10 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_11 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_12 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_13 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_14 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_15 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_16 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_17 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_18 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_19 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_20 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_21 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_22 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_23 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_24 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_25 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_26 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_27 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_28 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_29 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_30 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_31 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_32 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_33 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_34 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_35 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_36 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_37 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_38 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_39 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_40 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_41 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_42 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_43 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_44 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_45 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_46 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_47 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_48 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_49 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_50 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_51 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_52 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_53 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_54 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_55 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_56 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_57 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_58 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_59 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_60 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_61 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_62 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_63 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_64 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_65 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_66 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_67 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_68 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_69 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_70 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_71 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_72 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_73 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_74 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_75 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_76 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_77 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_78 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_79 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_80 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_81 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_82 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_83 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_84 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_85 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_86 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_87 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_88 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_89 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_90 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_91 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_92 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_93 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_94 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_95 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_96 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_97 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_98 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_99 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_100 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_101 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_102 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_103 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_104 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_105 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_106 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_107 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_108 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_109 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_110 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_111 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_112 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_113 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_114 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_115 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_116 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_117 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_118 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_119 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_120 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_121 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_122 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_123 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_124 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_125 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_126 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_127 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_128 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_129 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_130 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_131 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_132 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_133 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_134 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_135 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_136 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_137 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_138 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_139 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_140 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_141 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_142 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_143 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_144 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_145 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_146 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_147 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_148 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_149 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_150 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_151 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_152 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_153 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_154 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_155 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_156 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_157 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_158 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_159 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_160 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_161 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_162 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_163 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_164 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_165 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_166 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_167 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_168 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_169 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_170 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_171 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_172 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_173 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_174 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_175 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_176 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_177 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_178 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_179 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_180 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_181 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_182 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_183 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_184 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_185 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_186 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_187 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_188 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_189 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_190 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_191 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_192 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_193 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_194 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_195 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_196 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_197 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_198 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_199 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_200 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_201 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_202 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_203 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_204 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_205 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_206 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_207 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_208 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_209 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_210 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_211 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_212 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_213 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_214 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_215 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_216 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_217 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_218 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_219 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_220 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_221 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_222 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_223 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_224 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_225 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_226 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_227 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_228 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_229 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_230 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_231 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_232 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_233 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_234 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_235 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_236 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_237 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_238 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_239 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_240 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_241 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_242 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_243 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_244 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_245 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_246 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_247 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_248 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_249 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_250 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_251 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_252 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_253 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_254 {Type IO LastRead -1 FirstWrite -1}
		tmpdata_V_2_255 {Type IO LastRead -1 FirstWrite -1}
		w53_V {Type I LastRead -1 FirstWrite -1}}
	product_dense_ap_fixed_ap_fixed_ap_fixed_s {
		a_V {Type I LastRead 0 FirstWrite -1}
		w_V {Type I LastRead 0 FirstWrite -1}}}

set hasDtUnsupportedChannel 0

set PerformanceInfo {[
	{"Name" : "Latency", "Min" : "1280", "Max" : "1280"}
	, {"Name" : "Interval", "Min" : "1280", "Max" : "1280"}
]}

set PipelineEnableSignalInfo {[
]}

set Spec2ImplPortList { 
	data_V_V { ap_fifo {  { data_V_V_dout fifo_data 0 32 }  { data_V_V_empty_n fifo_status 0 1 }  { data_V_V_read fifo_update 1 1 } } }
	res_V_V { ap_fifo {  { res_V_V_din fifo_data 1 32 }  { res_V_V_full_n fifo_status 0 1 }  { res_V_V_write fifo_update 1 1 } } }
}
