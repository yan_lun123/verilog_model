set moduleName conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config27_s
set isTopModule 0
set isTaskLevelControl 1
set isCombinational 0
set isDatapathOnly 0
set isFreeRunPipelineModule 0
set isPipelined 0
set pipeline_type none
set FunctionProtocol ap_ctrl_hs
set isOneStateSeq 0
set ProfileFlag 0
set StallSigGenFlag 0
set isEnableWaveformDebug 1
set C_modelName {conv_2d_cl_me<ap_fixed,ap_fixed<32,16,5,3,0>,config27>}
set C_modelType { void 0 }
set C_modelArgList {
	{ data_V_V int 32 regular {fifo 0 volatile }  }
	{ res_V_V int 32 regular {fifo 1 volatile }  }
	{ weights_V int 4096 regular {array 576 { 1 3 } 1 1 }  }
}
set C_modelArgMapList {[ 
	{ "Name" : "data_V_V", "interface" : "fifo", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "res_V_V", "interface" : "fifo", "bitwidth" : 32, "direction" : "WRITEONLY"} , 
 	{ "Name" : "weights_V", "interface" : "memory", "bitwidth" : 4096, "direction" : "READONLY"} ]}
# RTL Port declarations: 
set portNum 19
set portList { 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst sc_in sc_logic 1 reset -1 active_high_sync } 
	{ ap_start sc_in sc_logic 1 start -1 } 
	{ start_full_n sc_in sc_logic 1 signal -1 } 
	{ ap_done sc_out sc_logic 1 predone -1 } 
	{ ap_continue sc_in sc_logic 1 continue -1 } 
	{ ap_idle sc_out sc_logic 1 done -1 } 
	{ ap_ready sc_out sc_logic 1 ready -1 } 
	{ start_out sc_out sc_logic 1 signal -1 } 
	{ start_write sc_out sc_logic 1 signal -1 } 
	{ data_V_V_dout sc_in sc_lv 32 signal 0 } 
	{ data_V_V_empty_n sc_in sc_logic 1 signal 0 } 
	{ data_V_V_read sc_out sc_logic 1 signal 0 } 
	{ res_V_V_din sc_out sc_lv 32 signal 1 } 
	{ res_V_V_full_n sc_in sc_logic 1 signal 1 } 
	{ res_V_V_write sc_out sc_logic 1 signal 1 } 
	{ weights_V_address0 sc_out sc_lv 10 signal 2 } 
	{ weights_V_ce0 sc_out sc_logic 1 signal 2 } 
	{ weights_V_q0 sc_in sc_lv 4096 signal 2 } 
}
set NewPortList {[ 
	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst", "role": "default" }} , 
 	{ "name": "ap_start", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "start", "bundle":{"name": "ap_start", "role": "default" }} , 
 	{ "name": "start_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "start_full_n", "role": "default" }} , 
 	{ "name": "ap_done", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "predone", "bundle":{"name": "ap_done", "role": "default" }} , 
 	{ "name": "ap_continue", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "continue", "bundle":{"name": "ap_continue", "role": "default" }} , 
 	{ "name": "ap_idle", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "done", "bundle":{"name": "ap_idle", "role": "default" }} , 
 	{ "name": "ap_ready", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "ready", "bundle":{"name": "ap_ready", "role": "default" }} , 
 	{ "name": "start_out", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "start_out", "role": "default" }} , 
 	{ "name": "start_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "start_write", "role": "default" }} , 
 	{ "name": "data_V_V_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "data_V_V", "role": "dout" }} , 
 	{ "name": "data_V_V_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "data_V_V", "role": "empty_n" }} , 
 	{ "name": "data_V_V_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "data_V_V", "role": "read" }} , 
 	{ "name": "res_V_V_din", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "res_V_V", "role": "din" }} , 
 	{ "name": "res_V_V_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "res_V_V", "role": "full_n" }} , 
 	{ "name": "res_V_V_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "res_V_V", "role": "write" }} , 
 	{ "name": "weights_V_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":10, "type": "signal", "bundle":{"name": "weights_V", "role": "address0" }} , 
 	{ "name": "weights_V_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "weights_V", "role": "ce0" }} , 
 	{ "name": "weights_V_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":4096, "type": "signal", "bundle":{"name": "weights_V", "role": "q0" }}  ]}

set RtlHierarchyInfo {[
	{"ID" : "0", "Level" : "0", "Path" : "`AUTOTB_DUT_INST", "Parent" : "", "Child" : ["1", "2", "3", "132", "133", "134", "135"],
		"CDFG" : "conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config27_s",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1",
		"Pipeline" : "None", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "0",
		"VariableLatency" : "1", "ExactLatency" : "-1", "EstimateLatencyMin" : "32401", "EstimateLatencyMax" : "143534665",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "0",
		"WaitState" : [
			{"State" : "ap_ST_fsm_state67", "FSM" : "ap_CS_fsm", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140"}],
		"Port" : [
			{"Name" : "data_V_V", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "data_V_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "res_V_V", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0",
				"BlockSignal" : [
					{"Name" : "res_V_V_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "weights_V", "Type" : "Memory", "Direction" : "I"},
			{"Name" : "layer_in_row_Array_V_0_0", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_0_0"}]},
			{"Name" : "layer_in_row_Array_V_1587_0", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_1587_0"}]},
			{"Name" : "layer_in_row_Array_V_0_1", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_0_1"}]},
			{"Name" : "layer_in_row_Array_V_1587_1", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_1587_1"}]},
			{"Name" : "layer_in_row_Array_V_0_2", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_0_2"}]},
			{"Name" : "layer_in_row_Array_V_1587_2", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_1587_2"}]},
			{"Name" : "layer_in_row_Array_V_0_3", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_0_3"}]},
			{"Name" : "layer_in_row_Array_V_1587_3", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_1587_3"}]},
			{"Name" : "layer_in_row_Array_V_0_4", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_0_4"}]},
			{"Name" : "layer_in_row_Array_V_1587_4", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_1587_4"}]},
			{"Name" : "layer_in_row_Array_V_0_5", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_0_5"}]},
			{"Name" : "layer_in_row_Array_V_1587_5", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_1587_5"}]},
			{"Name" : "layer_in_row_Array_V_0_6", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_0_6"}]},
			{"Name" : "layer_in_row_Array_V_1587_6", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_1587_6"}]},
			{"Name" : "layer_in_row_Array_V_0_7", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_0_7"}]},
			{"Name" : "layer_in_row_Array_V_1587_7", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_1587_7"}]},
			{"Name" : "layer_in_row_Array_V_0_8", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_0_8"}]},
			{"Name" : "layer_in_row_Array_V_1587_8", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_1587_8"}]},
			{"Name" : "layer_in_row_Array_V_0_9", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_0_9"}]},
			{"Name" : "layer_in_row_Array_V_1587_9", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_1587_9"}]},
			{"Name" : "layer_in_row_Array_V_0_10", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_0_10"}]},
			{"Name" : "layer_in_row_Array_V_1587_10", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_1587_10"}]},
			{"Name" : "layer_in_row_Array_V_0_11", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_0_11"}]},
			{"Name" : "layer_in_row_Array_V_1587_11", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_1587_11"}]},
			{"Name" : "layer_in_row_Array_V_0_12", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_0_12"}]},
			{"Name" : "layer_in_row_Array_V_1587_12", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_1587_12"}]},
			{"Name" : "layer_in_row_Array_V_0_13", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_0_13"}]},
			{"Name" : "layer_in_row_Array_V_1587_13", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_1587_13"}]},
			{"Name" : "layer_in_row_Array_V_0_14", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_0_14"}]},
			{"Name" : "layer_in_row_Array_V_1587_14", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_1587_14"}]},
			{"Name" : "layer_in_row_Array_V_0_15", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_0_15"}]},
			{"Name" : "layer_in_row_Array_V_1587_15", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_1587_15"}]},
			{"Name" : "layer_in_row_Array_V_0_16", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_0_16"}]},
			{"Name" : "layer_in_row_Array_V_1587_16", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_1587_16"}]},
			{"Name" : "layer_in_row_Array_V_0_17", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_0_17"}]},
			{"Name" : "layer_in_row_Array_V_1587_17", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_1587_17"}]},
			{"Name" : "layer_in_row_Array_V_0_18", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_0_18"}]},
			{"Name" : "layer_in_row_Array_V_1587_18", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_1587_18"}]},
			{"Name" : "layer_in_row_Array_V_0_19", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_0_19"}]},
			{"Name" : "layer_in_row_Array_V_1587_19", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_1587_19"}]},
			{"Name" : "layer_in_row_Array_V_0_20", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_0_20"}]},
			{"Name" : "layer_in_row_Array_V_1587_20", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_1587_20"}]},
			{"Name" : "layer_in_row_Array_V_0_21", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_0_21"}]},
			{"Name" : "layer_in_row_Array_V_1587_21", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_1587_21"}]},
			{"Name" : "layer_in_row_Array_V_0_22", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_0_22"}]},
			{"Name" : "layer_in_row_Array_V_1587_22", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_1587_22"}]},
			{"Name" : "layer_in_row_Array_V_0_23", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_0_23"}]},
			{"Name" : "layer_in_row_Array_V_1587_23", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_1587_23"}]},
			{"Name" : "layer_in_row_Array_V_0_24", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_0_24"}]},
			{"Name" : "layer_in_row_Array_V_1587_24", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_1587_24"}]},
			{"Name" : "layer_in_row_Array_V_0_25", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_0_25"}]},
			{"Name" : "layer_in_row_Array_V_1587_25", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_1587_25"}]},
			{"Name" : "layer_in_row_Array_V_0_26", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_0_26"}]},
			{"Name" : "layer_in_row_Array_V_1587_26", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_1587_26"}]},
			{"Name" : "layer_in_row_Array_V_0_27", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_0_27"}]},
			{"Name" : "layer_in_row_Array_V_1587_27", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_1587_27"}]},
			{"Name" : "layer_in_row_Array_V_0_28", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_0_28"}]},
			{"Name" : "layer_in_row_Array_V_1587_28", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_1587_28"}]},
			{"Name" : "layer_in_row_Array_V_0_29", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_0_29"}]},
			{"Name" : "layer_in_row_Array_V_1587_29", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_1587_29"}]},
			{"Name" : "layer_in_row_Array_V_0_30", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_0_30"}]},
			{"Name" : "layer_in_row_Array_V_1587_30", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_1587_30"}]},
			{"Name" : "layer_in_row_Array_V_0_31", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_0_31"}]},
			{"Name" : "layer_in_row_Array_V_1587_31", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_1587_31"}]},
			{"Name" : "layer_in_row_Array_V_0_32", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_0_32"}]},
			{"Name" : "layer_in_row_Array_V_1587_32", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_1587_32"}]},
			{"Name" : "layer_in_row_Array_V_0_33", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_0_33"}]},
			{"Name" : "layer_in_row_Array_V_1587_33", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_1587_33"}]},
			{"Name" : "layer_in_row_Array_V_0_34", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_0_34"}]},
			{"Name" : "layer_in_row_Array_V_1587_34", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_1587_34"}]},
			{"Name" : "layer_in_row_Array_V_0_35", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_0_35"}]},
			{"Name" : "layer_in_row_Array_V_1587_35", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_1587_35"}]},
			{"Name" : "layer_in_row_Array_V_0_36", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_0_36"}]},
			{"Name" : "layer_in_row_Array_V_1587_36", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_1587_36"}]},
			{"Name" : "layer_in_row_Array_V_0_37", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_0_37"}]},
			{"Name" : "layer_in_row_Array_V_1587_37", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_1587_37"}]},
			{"Name" : "layer_in_row_Array_V_0_38", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_0_38"}]},
			{"Name" : "layer_in_row_Array_V_1587_38", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_1587_38"}]},
			{"Name" : "layer_in_row_Array_V_0_39", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_0_39"}]},
			{"Name" : "layer_in_row_Array_V_1587_39", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_1587_39"}]},
			{"Name" : "layer_in_row_Array_V_0_40", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_0_40"}]},
			{"Name" : "layer_in_row_Array_V_1587_40", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_1587_40"}]},
			{"Name" : "layer_in_row_Array_V_0_41", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_0_41"}]},
			{"Name" : "layer_in_row_Array_V_1587_41", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_1587_41"}]},
			{"Name" : "layer_in_row_Array_V_0_42", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_0_42"}]},
			{"Name" : "layer_in_row_Array_V_1587_42", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_1587_42"}]},
			{"Name" : "layer_in_row_Array_V_0_43", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_0_43"}]},
			{"Name" : "layer_in_row_Array_V_1587_43", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_1587_43"}]},
			{"Name" : "layer_in_row_Array_V_0_44", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_0_44"}]},
			{"Name" : "layer_in_row_Array_V_1587_44", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_1587_44"}]},
			{"Name" : "layer_in_row_Array_V_0_45", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_0_45"}]},
			{"Name" : "layer_in_row_Array_V_1587_45", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_1587_45"}]},
			{"Name" : "layer_in_row_Array_V_0_46", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_0_46"}]},
			{"Name" : "layer_in_row_Array_V_1587_46", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_1587_46"}]},
			{"Name" : "layer_in_row_Array_V_0_47", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_0_47"}]},
			{"Name" : "layer_in_row_Array_V_1587_47", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_1587_47"}]},
			{"Name" : "layer_in_row_Array_V_0_48", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_0_48"}]},
			{"Name" : "layer_in_row_Array_V_1587_48", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_1587_48"}]},
			{"Name" : "layer_in_row_Array_V_0_49", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_0_49"}]},
			{"Name" : "layer_in_row_Array_V_1587_49", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_1587_49"}]},
			{"Name" : "layer_in_row_Array_V_0_50", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_0_50"}]},
			{"Name" : "layer_in_row_Array_V_1587_50", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_1587_50"}]},
			{"Name" : "layer_in_row_Array_V_0_51", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_0_51"}]},
			{"Name" : "layer_in_row_Array_V_1587_51", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_1587_51"}]},
			{"Name" : "layer_in_row_Array_V_0_52", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_0_52"}]},
			{"Name" : "layer_in_row_Array_V_1587_52", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_1587_52"}]},
			{"Name" : "layer_in_row_Array_V_0_53", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_0_53"}]},
			{"Name" : "layer_in_row_Array_V_1587_53", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_1587_53"}]},
			{"Name" : "layer_in_row_Array_V_0_54", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_0_54"}]},
			{"Name" : "layer_in_row_Array_V_1587_54", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_1587_54"}]},
			{"Name" : "layer_in_row_Array_V_0_55", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_0_55"}]},
			{"Name" : "layer_in_row_Array_V_1587_55", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_1587_55"}]},
			{"Name" : "layer_in_row_Array_V_0_56", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_0_56"}]},
			{"Name" : "layer_in_row_Array_V_1587_56", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_1587_56"}]},
			{"Name" : "layer_in_row_Array_V_0_57", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_0_57"}]},
			{"Name" : "layer_in_row_Array_V_1587_57", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_1587_57"}]},
			{"Name" : "layer_in_row_Array_V_0_58", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_0_58"}]},
			{"Name" : "layer_in_row_Array_V_1587_58", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_1587_58"}]},
			{"Name" : "layer_in_row_Array_V_0_59", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_0_59"}]},
			{"Name" : "layer_in_row_Array_V_1587_59", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_1587_59"}]},
			{"Name" : "layer_in_row_Array_V_0_60", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_0_60"}]},
			{"Name" : "layer_in_row_Array_V_1587_60", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_1587_60"}]},
			{"Name" : "layer_in_row_Array_V_0_61", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_0_61"}]},
			{"Name" : "layer_in_row_Array_V_1587_61", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_1587_61"}]},
			{"Name" : "layer_in_row_Array_V_0_62", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_0_62"}]},
			{"Name" : "layer_in_row_Array_V_1587_62", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_1587_62"}]},
			{"Name" : "layer_in_row_Array_V_0_63", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_0_63"}]},
			{"Name" : "layer_in_row_Array_V_1587_63", "Type" : "Memory", "Direction" : "X",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "layer_in_row_Array_V_1587_63"}]},
			{"Name" : "layer_in_V", "Type" : "Memory", "Direction" : "IO",
				"SubConnect" : [
					{"ID" : "3", "SubInstance" : "grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Port" : "output_V"}]},
			{"Name" : "sX_6", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "sY_6", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "pY_6", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "pX_6", "Type" : "OVld", "Direction" : "IO"}]},
	{"ID" : "1", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.layer_in_V_U", "Parent" : "0"},
	{"ID" : "2", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.tmpmult_V_U", "Parent" : "0"},
	{"ID" : "3", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140", "Parent" : "0", "Child" : ["4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59", "60", "61", "62", "63", "64", "65", "66", "67", "68", "69", "70", "71", "72", "73", "74", "75", "76", "77", "78", "79", "80", "81", "82", "83", "84", "85", "86", "87", "88", "89", "90", "91", "92", "93", "94", "95", "96", "97", "98", "99", "100", "101", "102", "103", "104", "105", "106", "107", "108", "109", "110", "111", "112", "113", "114", "115", "116", "117", "118", "119", "120", "121", "122", "123", "124", "125", "126", "127", "128", "129", "130", "131"],
		"CDFG" : "cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "0", "ap_idle" : "1",
		"Pipeline" : "None", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "0",
		"VariableLatency" : "1", "ExactLatency" : "-1", "EstimateLatencyMin" : "383", "EstimateLatencyMax" : "383",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "0",
		"HasNonBlockingOperation" : "0",
		"Port" : [
			{"Name" : "data_V_read", "Type" : "None", "Direction" : "I"},
			{"Name" : "output_V", "Type" : "Memory", "Direction" : "IO"},
			{"Name" : "layer_in_row_Array_V_0_0", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_1587_0", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_0_1", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_1587_1", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_0_2", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_1587_2", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_0_3", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_1587_3", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_0_4", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_1587_4", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_0_5", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_1587_5", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_0_6", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_1587_6", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_0_7", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_1587_7", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_0_8", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_1587_8", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_0_9", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_1587_9", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_0_10", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_1587_10", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_0_11", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_1587_11", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_0_12", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_1587_12", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_0_13", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_1587_13", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_0_14", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_1587_14", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_0_15", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_1587_15", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_0_16", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_1587_16", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_0_17", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_1587_17", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_0_18", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_1587_18", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_0_19", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_1587_19", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_0_20", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_1587_20", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_0_21", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_1587_21", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_0_22", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_1587_22", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_0_23", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_1587_23", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_0_24", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_1587_24", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_0_25", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_1587_25", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_0_26", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_1587_26", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_0_27", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_1587_27", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_0_28", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_1587_28", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_0_29", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_1587_29", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_0_30", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_1587_30", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_0_31", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_1587_31", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_0_32", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_1587_32", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_0_33", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_1587_33", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_0_34", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_1587_34", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_0_35", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_1587_35", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_0_36", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_1587_36", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_0_37", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_1587_37", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_0_38", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_1587_38", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_0_39", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_1587_39", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_0_40", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_1587_40", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_0_41", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_1587_41", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_0_42", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_1587_42", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_0_43", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_1587_43", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_0_44", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_1587_44", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_0_45", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_1587_45", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_0_46", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_1587_46", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_0_47", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_1587_47", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_0_48", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_1587_48", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_0_49", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_1587_49", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_0_50", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_1587_50", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_0_51", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_1587_51", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_0_52", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_1587_52", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_0_53", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_1587_53", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_0_54", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_1587_54", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_0_55", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_1587_55", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_0_56", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_1587_56", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_0_57", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_1587_57", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_0_58", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_1587_58", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_0_59", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_1587_59", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_0_60", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_1587_60", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_0_61", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_1587_61", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_0_62", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_1587_62", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_0_63", "Type" : "Memory", "Direction" : "X"},
			{"Name" : "layer_in_row_Array_V_1587_63", "Type" : "Memory", "Direction" : "X"}]},
	{"ID" : "4", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_0_0_U", "Parent" : "3"},
	{"ID" : "5", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_1587_0_U", "Parent" : "3"},
	{"ID" : "6", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_0_1_U", "Parent" : "3"},
	{"ID" : "7", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_1587_1_U", "Parent" : "3"},
	{"ID" : "8", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_0_2_U", "Parent" : "3"},
	{"ID" : "9", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_1587_2_U", "Parent" : "3"},
	{"ID" : "10", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_0_3_U", "Parent" : "3"},
	{"ID" : "11", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_1587_3_U", "Parent" : "3"},
	{"ID" : "12", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_0_4_U", "Parent" : "3"},
	{"ID" : "13", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_1587_4_U", "Parent" : "3"},
	{"ID" : "14", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_0_5_U", "Parent" : "3"},
	{"ID" : "15", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_1587_5_U", "Parent" : "3"},
	{"ID" : "16", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_0_6_U", "Parent" : "3"},
	{"ID" : "17", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_1587_6_U", "Parent" : "3"},
	{"ID" : "18", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_0_7_U", "Parent" : "3"},
	{"ID" : "19", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_1587_7_U", "Parent" : "3"},
	{"ID" : "20", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_0_8_U", "Parent" : "3"},
	{"ID" : "21", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_1587_8_U", "Parent" : "3"},
	{"ID" : "22", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_0_9_U", "Parent" : "3"},
	{"ID" : "23", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_1587_9_U", "Parent" : "3"},
	{"ID" : "24", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_0_10_U", "Parent" : "3"},
	{"ID" : "25", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_1587_10_U", "Parent" : "3"},
	{"ID" : "26", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_0_11_U", "Parent" : "3"},
	{"ID" : "27", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_1587_11_U", "Parent" : "3"},
	{"ID" : "28", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_0_12_U", "Parent" : "3"},
	{"ID" : "29", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_1587_12_U", "Parent" : "3"},
	{"ID" : "30", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_0_13_U", "Parent" : "3"},
	{"ID" : "31", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_1587_13_U", "Parent" : "3"},
	{"ID" : "32", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_0_14_U", "Parent" : "3"},
	{"ID" : "33", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_1587_14_U", "Parent" : "3"},
	{"ID" : "34", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_0_15_U", "Parent" : "3"},
	{"ID" : "35", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_1587_15_U", "Parent" : "3"},
	{"ID" : "36", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_0_16_U", "Parent" : "3"},
	{"ID" : "37", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_1587_16_U", "Parent" : "3"},
	{"ID" : "38", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_0_17_U", "Parent" : "3"},
	{"ID" : "39", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_1587_17_U", "Parent" : "3"},
	{"ID" : "40", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_0_18_U", "Parent" : "3"},
	{"ID" : "41", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_1587_18_U", "Parent" : "3"},
	{"ID" : "42", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_0_19_U", "Parent" : "3"},
	{"ID" : "43", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_1587_19_U", "Parent" : "3"},
	{"ID" : "44", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_0_20_U", "Parent" : "3"},
	{"ID" : "45", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_1587_20_U", "Parent" : "3"},
	{"ID" : "46", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_0_21_U", "Parent" : "3"},
	{"ID" : "47", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_1587_21_U", "Parent" : "3"},
	{"ID" : "48", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_0_22_U", "Parent" : "3"},
	{"ID" : "49", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_1587_22_U", "Parent" : "3"},
	{"ID" : "50", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_0_23_U", "Parent" : "3"},
	{"ID" : "51", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_1587_23_U", "Parent" : "3"},
	{"ID" : "52", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_0_24_U", "Parent" : "3"},
	{"ID" : "53", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_1587_24_U", "Parent" : "3"},
	{"ID" : "54", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_0_25_U", "Parent" : "3"},
	{"ID" : "55", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_1587_25_U", "Parent" : "3"},
	{"ID" : "56", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_0_26_U", "Parent" : "3"},
	{"ID" : "57", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_1587_26_U", "Parent" : "3"},
	{"ID" : "58", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_0_27_U", "Parent" : "3"},
	{"ID" : "59", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_1587_27_U", "Parent" : "3"},
	{"ID" : "60", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_0_28_U", "Parent" : "3"},
	{"ID" : "61", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_1587_28_U", "Parent" : "3"},
	{"ID" : "62", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_0_29_U", "Parent" : "3"},
	{"ID" : "63", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_1587_29_U", "Parent" : "3"},
	{"ID" : "64", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_0_30_U", "Parent" : "3"},
	{"ID" : "65", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_1587_30_U", "Parent" : "3"},
	{"ID" : "66", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_0_31_U", "Parent" : "3"},
	{"ID" : "67", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_1587_31_U", "Parent" : "3"},
	{"ID" : "68", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_0_32_U", "Parent" : "3"},
	{"ID" : "69", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_1587_32_U", "Parent" : "3"},
	{"ID" : "70", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_0_33_U", "Parent" : "3"},
	{"ID" : "71", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_1587_33_U", "Parent" : "3"},
	{"ID" : "72", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_0_34_U", "Parent" : "3"},
	{"ID" : "73", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_1587_34_U", "Parent" : "3"},
	{"ID" : "74", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_0_35_U", "Parent" : "3"},
	{"ID" : "75", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_1587_35_U", "Parent" : "3"},
	{"ID" : "76", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_0_36_U", "Parent" : "3"},
	{"ID" : "77", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_1587_36_U", "Parent" : "3"},
	{"ID" : "78", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_0_37_U", "Parent" : "3"},
	{"ID" : "79", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_1587_37_U", "Parent" : "3"},
	{"ID" : "80", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_0_38_U", "Parent" : "3"},
	{"ID" : "81", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_1587_38_U", "Parent" : "3"},
	{"ID" : "82", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_0_39_U", "Parent" : "3"},
	{"ID" : "83", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_1587_39_U", "Parent" : "3"},
	{"ID" : "84", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_0_40_U", "Parent" : "3"},
	{"ID" : "85", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_1587_40_U", "Parent" : "3"},
	{"ID" : "86", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_0_41_U", "Parent" : "3"},
	{"ID" : "87", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_1587_41_U", "Parent" : "3"},
	{"ID" : "88", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_0_42_U", "Parent" : "3"},
	{"ID" : "89", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_1587_42_U", "Parent" : "3"},
	{"ID" : "90", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_0_43_U", "Parent" : "3"},
	{"ID" : "91", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_1587_43_U", "Parent" : "3"},
	{"ID" : "92", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_0_44_U", "Parent" : "3"},
	{"ID" : "93", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_1587_44_U", "Parent" : "3"},
	{"ID" : "94", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_0_45_U", "Parent" : "3"},
	{"ID" : "95", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_1587_45_U", "Parent" : "3"},
	{"ID" : "96", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_0_46_U", "Parent" : "3"},
	{"ID" : "97", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_1587_46_U", "Parent" : "3"},
	{"ID" : "98", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_0_47_U", "Parent" : "3"},
	{"ID" : "99", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_1587_47_U", "Parent" : "3"},
	{"ID" : "100", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_0_48_U", "Parent" : "3"},
	{"ID" : "101", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_1587_48_U", "Parent" : "3"},
	{"ID" : "102", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_0_49_U", "Parent" : "3"},
	{"ID" : "103", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_1587_49_U", "Parent" : "3"},
	{"ID" : "104", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_0_50_U", "Parent" : "3"},
	{"ID" : "105", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_1587_50_U", "Parent" : "3"},
	{"ID" : "106", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_0_51_U", "Parent" : "3"},
	{"ID" : "107", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_1587_51_U", "Parent" : "3"},
	{"ID" : "108", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_0_52_U", "Parent" : "3"},
	{"ID" : "109", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_1587_52_U", "Parent" : "3"},
	{"ID" : "110", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_0_53_U", "Parent" : "3"},
	{"ID" : "111", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_1587_53_U", "Parent" : "3"},
	{"ID" : "112", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_0_54_U", "Parent" : "3"},
	{"ID" : "113", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_1587_54_U", "Parent" : "3"},
	{"ID" : "114", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_0_55_U", "Parent" : "3"},
	{"ID" : "115", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_1587_55_U", "Parent" : "3"},
	{"ID" : "116", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_0_56_U", "Parent" : "3"},
	{"ID" : "117", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_1587_56_U", "Parent" : "3"},
	{"ID" : "118", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_0_57_U", "Parent" : "3"},
	{"ID" : "119", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_1587_57_U", "Parent" : "3"},
	{"ID" : "120", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_0_58_U", "Parent" : "3"},
	{"ID" : "121", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_1587_58_U", "Parent" : "3"},
	{"ID" : "122", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_0_59_U", "Parent" : "3"},
	{"ID" : "123", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_1587_59_U", "Parent" : "3"},
	{"ID" : "124", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_0_60_U", "Parent" : "3"},
	{"ID" : "125", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_1587_60_U", "Parent" : "3"},
	{"ID" : "126", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_0_61_U", "Parent" : "3"},
	{"ID" : "127", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_1587_61_U", "Parent" : "3"},
	{"ID" : "128", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_0_62_U", "Parent" : "3"},
	{"ID" : "129", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_1587_62_U", "Parent" : "3"},
	{"ID" : "130", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_0_63_U", "Parent" : "3"},
	{"ID" : "131", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s_fu_54140.layer_in_row_Array_V_1587_63_U", "Parent" : "3"},
	{"ID" : "132", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.op_V_assign_6_product_dense_ap_fixed_ap_fixed_ap_fixed_s_fu_54403", "Parent" : "0",
		"CDFG" : "product_dense_ap_fixed_ap_fixed_ap_fixed_s",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "0", "ap_start" : "0", "ap_ready" : "1", "ap_done" : "0", "ap_continue" : "0", "ap_idle" : "0",
		"Pipeline" : "None", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "0", "EstimateLatencyMin" : "0", "EstimateLatencyMax" : "0",
		"Combinational" : "1",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "0",
		"HasNonBlockingOperation" : "0",
		"Port" : [
			{"Name" : "a_V", "Type" : "None", "Direction" : "I"},
			{"Name" : "w_V", "Type" : "None", "Direction" : "I"}]},
	{"ID" : "133", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.myproject_urem_17ns_11ns_17_21_seq_1_U226", "Parent" : "0"},
	{"ID" : "134", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.myproject_mux_1287_32_1_1_U227", "Parent" : "0"},
	{"ID" : "135", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.myproject_mul_mul_19ns_17ns_36_1_1_U228", "Parent" : "0"}]}


set ArgLastReadFirstWriteLatency {
	conv_2d_cl_me_ap_fixed_ap_fixed_32_16_5_3_0_config27_s {
		data_V_V {Type I LastRead 64 FirstWrite -1}
		res_V_V {Type O LastRead -1 FirstWrite 67}
		weights_V {Type I LastRead 89 FirstWrite -1}
		layer_in_row_Array_V_0_0 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_0 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_1 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_1 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_2 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_2 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_3 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_3 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_4 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_4 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_5 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_5 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_6 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_6 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_7 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_7 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_8 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_8 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_9 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_9 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_10 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_10 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_11 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_11 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_12 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_12 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_13 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_13 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_14 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_14 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_15 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_15 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_16 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_16 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_17 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_17 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_18 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_18 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_19 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_19 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_20 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_20 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_21 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_21 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_22 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_22 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_23 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_23 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_24 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_24 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_25 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_25 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_26 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_26 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_27 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_27 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_28 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_28 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_29 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_29 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_30 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_30 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_31 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_31 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_32 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_32 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_33 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_33 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_34 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_34 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_35 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_35 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_36 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_36 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_37 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_37 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_38 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_38 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_39 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_39 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_40 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_40 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_41 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_41 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_42 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_42 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_43 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_43 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_44 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_44 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_45 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_45 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_46 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_46 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_47 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_47 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_48 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_48 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_49 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_49 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_50 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_50 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_51 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_51 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_52 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_52 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_53 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_53 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_54 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_54 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_55 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_55 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_56 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_56 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_57 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_57 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_58 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_58 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_59 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_59 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_60 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_60 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_61 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_61 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_62 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_62 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_63 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_63 {Type X LastRead -1 FirstWrite -1}
		layer_in_V {Type IO LastRead -1 FirstWrite -1}
		sX_6 {Type IO LastRead -1 FirstWrite -1}
		sY_6 {Type IO LastRead -1 FirstWrite -1}
		pY_6 {Type IO LastRead -1 FirstWrite -1}
		pX_6 {Type IO LastRead -1 FirstWrite -1}}
	cnnshift_arr_ap_fixed_ap_fixed_32_16_5_3_0_config27_s {
		data_V_read {Type I LastRead 96 FirstWrite -1}
		output_V {Type IO LastRead 192 FirstWrite 96}
		layer_in_row_Array_V_0_0 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_0 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_1 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_1 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_2 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_2 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_3 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_3 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_4 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_4 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_5 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_5 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_6 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_6 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_7 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_7 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_8 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_8 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_9 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_9 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_10 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_10 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_11 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_11 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_12 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_12 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_13 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_13 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_14 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_14 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_15 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_15 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_16 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_16 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_17 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_17 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_18 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_18 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_19 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_19 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_20 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_20 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_21 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_21 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_22 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_22 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_23 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_23 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_24 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_24 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_25 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_25 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_26 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_26 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_27 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_27 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_28 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_28 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_29 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_29 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_30 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_30 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_31 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_31 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_32 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_32 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_33 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_33 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_34 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_34 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_35 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_35 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_36 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_36 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_37 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_37 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_38 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_38 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_39 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_39 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_40 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_40 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_41 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_41 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_42 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_42 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_43 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_43 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_44 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_44 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_45 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_45 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_46 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_46 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_47 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_47 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_48 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_48 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_49 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_49 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_50 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_50 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_51 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_51 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_52 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_52 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_53 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_53 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_54 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_54 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_55 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_55 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_56 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_56 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_57 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_57 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_58 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_58 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_59 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_59 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_60 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_60 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_61 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_61 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_62 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_62 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_0_63 {Type X LastRead -1 FirstWrite -1}
		layer_in_row_Array_V_1587_63 {Type X LastRead -1 FirstWrite -1}}
	product_dense_ap_fixed_ap_fixed_ap_fixed_s {
		a_V {Type I LastRead 0 FirstWrite -1}
		w_V {Type I LastRead 0 FirstWrite -1}}}

set hasDtUnsupportedChannel 0

set PerformanceInfo {[
	{"Name" : "Latency", "Min" : "32401", "Max" : "143534665"}
	, {"Name" : "Interval", "Min" : "32401", "Max" : "143534665"}
]}

set PipelineEnableSignalInfo {[
]}

set Spec2ImplPortList { 
	data_V_V { ap_fifo {  { data_V_V_dout fifo_data 0 32 }  { data_V_V_empty_n fifo_status 0 1 }  { data_V_V_read fifo_update 1 1 } } }
	res_V_V { ap_fifo {  { res_V_V_din fifo_data 1 32 }  { res_V_V_full_n fifo_status 0 1 }  { res_V_V_write fifo_update 1 1 } } }
	weights_V { ap_memory {  { weights_V_address0 mem_address 1 10 }  { weights_V_ce0 mem_ce 1 1 }  { weights_V_q0 mem_dout 0 4096 } } }
}
